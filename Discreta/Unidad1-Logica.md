# Logica 

> Una preposicion es todo enunciado al que se le puede asignar un valor de verdad

* Proposiciones 
	* Simple 
	* Compuestas 
		* Simples 
		* Operadores (Conectivos Logicos)

> Tautologia a aquella proposicion compuesta cuyo valor de verdad es siempre verdadero , sin importar las proposiciones simples 
> <br> Contradiccion a aquella proposicion compuesta cuyo valor de verdad es siempre falso , sin importar las proposiciones simples <br> Contingencia es cuando depende del valor de sus preposiciones simples . 

Conectivo|Se lee| Nombre
---------|------|-------
~| no | Negacion
$`\land`$ | Y | Producto logico o conjuncion
$`\lor`$ | O(inclusivo) | Suma logica o disyuncion
$`\Rightarrow`$ | si ..entonces..|Condicional 
$`\iff`$ | Si y solo si | Bicondicional 
$`\veebar`$ | O (Exclusivo)|Disyuncion Excluyente 

## Negacion (not)

p|~p
-|--
V|F
F|V

## Conjuncion 
> La conjuncion es una y logica , tiene que ocurrir uno y lo otro. 

p|q | $`p \land q `$ 
-|--|---------------
V|V|V
F|V|F
V|F|F
F|F|F

## Disyuncion 
> La Disyuncion o "o" logica , tiene que ser uno o el otro o ambos.

p|q | $`p \lor q `$
-|--|---------------
V|V|V
F|V|V
V|F|V
F|F|F

## Condicional 
> Si p entonces q  , a p se lo llama hipotesis(antecedente) y a q se lo llama conclusion (consecuente) 

p|q | $`p \Rightarrow q `$
-|--|---------------
V|V|V
F|V|V
V|F|F
F|F|V

## Bicondicional 
> P si y solo si q , seria como $`(p \rightarrow q) \land (q \rightarrow p)`$      

p|q | $`p \iff  q `$
-|--|---------------
V|V|V
F|V|F
V|F|F
F|F|V

##  Disyuncion Excluyente 

> Seria como una Xor en compuertas y daria verdadero a la salida si sus entradas son distintas .

p|q | $`p \veebar q `$
-|--|---------------
V|V|F
F|V|V
V|F|V
F|F|F

## Razonamiento Deductivo  valido 

1. Ley Modus Ponens 
    * $`[P\land(P \Rightarrow Q)] \Rightarrow Q `$
2. Ley Modus Tolens 
    * $`[(P \Rightarrow Q)\land (\overline{\rm Q})] \Rightarrow \overline{\rm Q}`$
3. Ley De Silogismo Hipotetico 
    * $`[(P \Rightarrow Q) \land (Q \Rightarrow R)] \Rightarrow (P \Rightarrow R) `$
4. Silogismo Disyuntivo 
    * $`[(P \lor Q) \land \overline{\rm Q}] \Rightarrow P`$
    * Es como hacer distributiva y luego resolver

## Identidad 

> En estos casos la V (Verdadero) y la F (falso)

1. Depende de  p 
    * $` (p \land V) \equiv p`$
    * $` (p \lor F) \equiv  p`$
2. No depende de p 
    * $` (p \lor V) \equiv V`$
    * $` (p \land F) \equiv F`$
3. Otros
    * $` (p \lor \overline{\rm p}) \equiv V`$
    * $` (p \land \overline{\rm p}) \equiv F`$

## Ejemplo (Simplificacion)

> $` ( p \land \overline{\rm q}) \Rightarrow (r \land \overline{\rm r}) `$

1. Equivalencia  
    * $` \overline{\rm (p \land \overline{\rm q})} \lor (r \land \overline{\rm r}) `$
2. DeMorgan 
    * $` (\overline{\rm p } \lor \overline{\rm \overline{\rm q})} \lor (r \land \overline{\rm r}) `$
3. Involucion
    * $` (\overline{\rm p} \lor q) \lor (r \land \overline{\rm r}) `$
4. Contradiccion
    * $` (\overline{\rm p} \lor q) \lor F `$
5. Identidad 
    * $` (\overline{\rm p}  \lor q)`$
6. Equivalencia
    * $` p \Rightarrow q `$

## Ejemplo2 (Metodo Directo) 

* p:Bailo
* q:Hago gimnasia
* r:Estoy delgada
* s:Intervenir concurso

> 
