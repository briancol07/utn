# Algebra 

Algebra la padeci demasiado pero espero que mis resumenes te sirvan como me sirvieron a mi !! 
No sirve de nada si solo lees , hay que practicar y es la escencia de todo 


> Resumen Basado en dos profesoras ( Andrea Rey - Maria Farini) mas apuntes de virtual pero es muy parecido a todos los demas asi que manos a la obra 


##  Temario 

* Vectores
	* Puntos y Rectas 
	* Operaciones  
	* Vector Determinado por dos puntos 
	* Distancia entre dos puntos
	* Expresiones Canonicas
	* Versor asociado a un vector 
	* Producto Escalar  
	*	Perpendicularidad 
* Plano y Recta
* Matrices 
* Determinantes 
* Sistemas de Ecuaciones Lineales 
* Espacios Vectoriales 
* Transformaciones Lineales 
* Autovalores & Autovectores
* Conicas 
* Numeros complejos 
* Superficies 



## Vectores 

### Puntos y Rectas 

> Primero veremos que es un punto , en dos coordenadas es simple , y en tres tambien se representa de la misma manera moviendote tantos lugares como dice 
>
> Ej (3,4,5) Seria (x,y,z) entonces serian 3 en x , 4 en y, 5 en z.

* Eje x --> Eje Abscisas
* Eje y --> Eje Ordenadas 
* Eje z --> Eje Cotas 

Que siginifica $`R^3`$ significa que tiene 3 ejes encambio los $`R^2`$ solo tiene 2 que son x e y 

### Vector 
Un vector es una flecha un Segmento Rectilinio Orientado 
Tiene : 
* Magnitud : Largo de la flecha 
* Direccion : La inclinacion
* Sentido : Hacia Donde Apunta

###  Operaciones 

1. Igualdad:  $`\vec{v} = \vec{w} \iff v_{x} = w_{x} , v_{y} = w_{y} , v_{z} = w_{z}`$
2. Suma: $`\vec{v}+\vec{w} = (v_{x}+w_{x},v_{y}+w_{y},v_{z}+w_{z})`$ 
3. Vector Nulo : $`\vec{0} = (0,0,0)`$
4. Opuesto : $`\vec{v} = \vec{-v}`$ 
5. Resta : $`\vec{v} - \vec{w} = \vec{v} + \vec{-w}`$
6. Norma :$` \parallel \vec{v} \parallel = \sqrt{v_{x}^2 + v_{y}^2 + v_{z}^2}`$
7. $`\parallel \vec{v} + \vec{w} \parallel  \leq \parallel \vec{v} \parallel+ \parallel \vec{w} \parallel  
8. $`\parallel k \vec{v} \parallel = |k| \parallel \vec{v} \parallel `$
9. $`\vec{v} \parallel \vec{w} \iff \vec{v}= k \vec{w}`$
10. Conmutativa :$`\vec{u} + \vec{w} = \vec{w} + \vec{u}`$
11. Asociativa : $`(\vec{u} + \vec{w})+\vec{v} = \vec{u} + \vec{w} + \vec{v}`$
12. Distributiva : $`\alpha (\vec{u}+\vec{v}) = \alpha \vec{u} + \alpha \vec{v}`$

### Vector Determinado por dos puntos 
_
$`A=(x_{a},y_{a},z_{a}) y B=(x_{b},y_{b},z_{b})`$
 <br>
$`\overrightarrow{\rm AB} = (x_{b}-x_{a} , y_{b}-y_{a} , z_{b}-z_{a}) `$

### Distancia entre dos puntos 

$$`d(a,b)= \parallel \overrightarrow{\rm ab} \parallel= \sqrt{(X_{b}-X_{a})^2 +(Y_{b}-Y_{a})^2 + (Z_{b}-Z_{a})^2} `$$

### Expresiones Canonicas 

* $` \overline{\rm i} = (1,0,0)`$
* $` \overline{\rm k} = (0,1,0)`$
* $` \overline{\rm j} = (0,0,1)`$ 

### Versor asociado a un vector 
> Vector unitario !
>
$`\vec{v} = \frac{\vec{v}}{\parallel\vec{v}\parallel} `$

### Producto Escalar 
$`\vec{u} . \vec{v} = \parallel\vec{u}\parallel . \parallel \vec{v} \parallel . \sin\theta`$ 
* Propiedades 
	* Conmutativa 
	* Distributiva
	* Distributividad con un escalar 
	* $`\vec{v} . \vec{v} = (\| \vec{v}\|)^2 `$


### Perpendicularidad 

$`\boxed{\vec{u}\perp\vec{v} \iff \vec{u} . \vec{v} = 0} `$ 

### Proyeccion de un vector en la direccion de otro 

$`\overrightarrow{proy_v}\: \vec{u} = \frac{\vec{u}{.}\vec{v}}{{\parallel\vec{v}\parallel}^2} . \vec{v} `$

### Producto Vectorial
$`\vec{u} x \vec{v} = \vec{w} \land \vec{w}\perp\vec{v} \land \vec{w}\perp\vev{u} `$
* No Es conmutativo
* Nos da un nuevo vector que es ortogonal a ambos $`\vec{u} x \vec{v} = \vec{w}`$ 
* $`\vec{v} x \vec{v} = \vec{0}`$

### Parallel 

 $`\boxed{\vec{u}\parallel\vec{v} \iff \vec{u}x\vec{v}=\vec{0}\iff\vec{u}= k . \vec{v}}`$

### Como hacer Producto Vectorial 

$`\vec{u}x\vec{v}= \matrix{I&J&K \\		
											U_x&U_y&U_z \\
											V_x&V_y&V_z} `$
</br>
$$\vec{u}x\vec{v}=(U_y \: V_z - U_z \: V_y , -(U_x \: V_z - U_z \: V_x) , U_x \: V_y - U_y \: V_x)$$

### Area del Paralelogramo 

$`\|\vec{u}\| \|\vec{v}\| \sin{\theta}=\|\vec{u} x \vec{v}\| `$

### Coplanaridad  

$`(\vec{u}x\vec{v}).\vec{w}=0`$


## Plano y recta 
### Planos coordenadas 
* xy ---> k=(0,0,1)
* yz ---> i=(1,0,0)
* xz ---> j=(0,1,0)
### Ecuacion implicita 
$`ax+by+cz+d=0`$
Donde (a,b,c) son el $`\vec{n}`$ normal y para sacar d se tiene que reemplazar las componentes (x,y,z) por un punto de paso
### Ecuacion Segmentaria del plano
$`\frac{a}{-d} x + \frac{b}{-d} y + \frac{c}{-d} z = 1 `$ 

### Ecuacion Parametrica 
$`(x,y,z) = (x_0,y_0,z_0)+\alpha(v_1,v_2,v_3)+\beta(u_1,u_2,u_3)`$ 
### Pasajes entre ecuaciones 

1. De implicita a segmentaria es simple despejando d y dividiendo por el mismo .
2. De Parametrica a Implicita 
	1. Los vectores que acompanian a alpha y beta hago producto vectorial 
	2. Luego los uso con la formula de implicita 
	3. Despejo d con el punto que tengo . 

### Planos Paralelos 

$`\pi_1\|\pi_2 \iff \vec{n_1} = k \vec{n_2}`$ 

### Planos Perpendiculares 
$`\pi_1\perp\pi_2 \iff \vec{n_1}.\vec{n_2}=0`$

### Distancia de un punto a un plano  
$`a x + b y +c z + d =0 \land A(x_a,y_a,z_a)`$</br>
$`dist(a,\pi)=\|\vec{proy_{\vec{n}}}(\vec{pa}\|)`$</br>
Donde p es un punto del plano
$`\vec{proy_{\vec{n}}}(\vec{pa})= \frac{\vec{pa}.\vec{n}}{\|\vec{n}\|}`$

### Distancia Entre planos paralelos 

> Elegimos un punto y hacemos lo mismo que antes (un item)

### Haz de planos no paralelos   
$`r=\pi_1\cap\pi_2
* Pueden venir de las siguientes maneras:
	* Ecuacion Vectorial 
		* $`(x,y,z) = (x_o,y_o,z_o) + \alpha(v_1,v_2,v_3) `$
	* Ecuacion Parametrica 
		* $`\left\{ \begin{array}{c} 
	X=x_0 + \alpha . v_1 \\
	Y=y_0 + \alpha . v_2  \\
  Z=z_0 + \alpha . v_2
\end{array}
\right. `$

### Recta definida como interseccion de dos planos (no paralelos) 

$`\left\{ \begin{array}{c} 
\pi_1:a_1.X + b_1.Y+c_1.Z+d_1=0\\
\pi_2:a_2.X+b_2.Y+c_2.Z+d_2=0   
\end{array}
\right.
`$
### Interseccion de recta y plano 

$`\boxed{r \cap \pi = {P}}`$ </br>
$`\boxed{r \| \pi = \varnothing}`$ </br>
$`\boxed{r \subset \pi =r}`$ </br>

1. Ecuaciones parametricas de la recta 
2. Reemplazamos en el plano 
3. Luego Reemplazamos alpha en la recta , obtenemos punto de interseccion 
> Si da $`\alpha`$ igual a un valor entonces hay un punto de interseccion , si da 0=0 entonces significa que la recta esta contenida en el plano y encambio si da un absurdo significa que no se tocan.

Ejemplo : 
$` 2x-3y+z+1=0 `$ 
$`(x,y,z)=(0,1,3) + \alpha (1,0,1)`$
$`\left\{ \begin{array}{c}
 x=\alpha\\
 y= 1 \\
 z= 3+\alpha
 \end{array}
 \right.`$ 
$`2.\alpha-3+(3+\alpha)+1=0 \Rightarrow \alpha=\frac{-1}{3}`$ 

### Angulo entre Recta y Plano 

### Interseccion de Rectas en $`(\R)^4`$ 

1. Concurrentes 
	* $` r_1\cap r_2 = {P}`$
2. Paralelas 
	* $` r_1\|r_2\iff \vec{v_1} = k . \vec{v_2}`$
3. Alabeadas 
	* $` r_1 \nparallel r_2 \land r_1 \cap r_2 = \varnothing `$ 


## Espacios vectoriales 

> Es un conjunto no vacio V de vectories , en el que se han definido dos operaciones (suma y producto escalar) , tienen que cumplir con los 10 axiomas \


1. $` u + v \in V`$ 
2. $` u + v = v + u`$ 
3. $` (u+v)+w = u+(v+w) `$
4. $` \exists 0_v \in V \Rightarrow v+0_v = v `$
5. $` v \in V \exists (-v)\in V \Rightarrow v + (-v) = 0_v`$
6. $` \alpha \cdot v \in V 	`$
7. $` \alpha \cdot (u+v)=\alpha u + \alpha v `$
8. $` (\alpha + \beta ) v = \alpha v + \beta v `$
9. $` \alpha (\beta v) =  (\alpha   \beta ) v`$
10. $` 1 v=v`$ 


### Propiedades espacios vectoriales 

1. $`0.u = 0_v `$ 
2. $`\alpha 0_v = 0_v `$
3. $`(-\alpha)u = -(\alpha u) `$
4. $`(\alpha u)=0_v \Rightarrow \alpha = 0 \lor u= 0_v  `$ 

### Subespacios Vectoriales 

> Para ser subespacio en si tiene que ser un espacio vectorial con las mismas operaciones que el espacio vectorial padre(del cual trata de ser subespacio) 

### Condiciones Necesaria y suficiente para que sea subespacio (cnys)

> Sea W un subconjunto de un espacio vectorial V ( $` W \subseteq V `$ )
<br>

* $`0_v \in W `$
* $` (u \land v \in W ) \Rightarrow (u+v)\in W `$
* $` u \in W \land k (escalar) \Rightarrow (ku)\in W `$

### Subespacios Triviales 

* si V es un subespacio entonces V es un subespacio en si mismo 
* El conjunto $` 0_v `$  que contiene solo al vector nulo del espacio tambien es un subepacio de V.       

### Conjunto Generador 

> si todo vector de V puede expresarse como combinacion lineal de v1,v2, ,vnentonces se dice que {v1,v2, ,vn} es un conjunto generador de V 

> Aca faltan ejemplos !!

### Subespacio Generado 

1. Vector nulo puede expresarse como combinacion de dichos vectores
2. Si sumamos dos combinaciones linales de los vectores obtenemos otra combinacion lineal 
3. Si multiplicamos un escalar por una combinacion lineal de los vectores dados obtenemos una combinacion lineal de dichos vectores 

> Llamamos subespacio generado al conjunto de tdas las combinaciones lineales de estos vectores . gen{v1,v2, ,vr} 

### Independencia lineal y dependencia lineal 

> si la unica solucion es la trivial entonces el li y si hay mas de una entonces es ld 
* Propiedades 
	* Tiene que ser el $`0_v`$
	* Si contiene al nulo es LD

### Base y Dimesion de un espacio vectorial 

> Se denomina base si es li y generea al espacio V 
>
> La dimension de un espacio vectorial es la cantidad de vectores que componen una base V
>
> dimension del vector nulo es 0 
