# Unidad 5 

## Transformaciones lineales (TF) 

Una transformacion lineal es una funcion, por ende tiene dominio y codominio. Pero son *Espacios vectoriales*.

* $`F: V \to W `$ es una tranformacion lineal si y solo si : 
  1. $` F(u+v)=F(u)+F(v) \forall u, v \in V`$
  2. $` F(k.v)= k.F(v) \forall v \in V, \forall k \in \R

## Propiedades de TF
 
1. $` T(0_{v}) = 0_{w}`$ 
2. $` T(-v) = -T(v) 
3. $` F(\alpha_{1} . v_{1} + \alpha_{2} . v_{2}=\alpha F(v_{1} + \alpha_{2} .F(v_{2})`$ 

## Nucleo , imagen y teoremas de las dimensiones 

### Nucleo
Llamamos nucleo de F al conjunto de vectores del dominio cuya imagen F es el nulo de del codominio

$$` Nu(F)= {v \in V | F(v) = 0_{w} } `$$ 

### Imagen
Llamamos imagen de F al conjunto de vectores de W que son imagen de algun vector de V 

$$` Im(F)= {w \in W | w = F(v), v \in V} `$$ 

### Teorema de las dimensiones 

$` F : V \to W `$
$$` dim(V) = dim(Nu(F)+ dim(Im(F)) `$$

los transformados de una base generan la imagen 

## Clasificacion de las transformaciones lineales 

1. Monomorfismo (inyectiva) 
  * $`Nu(F) = {0_{v}}`$
  * dim(Nu) = 0  
2. Epimorfismo (sobreyectiva)
  * $`Im(F) = W`$ 
3. Isomorfismo (biyectiva)
  * Si cumple con ambas 


## Teorema fundamental de las transformaciones lineales 

