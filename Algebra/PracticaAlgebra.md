# Practica de Algebra 
El destino de este archivo es :
	* Escribir los ejercicios que considero importante con una resolucion 
	* Puede ser generica o no 
	* Pueden ser que esten todos los ejercicios como no

* Ejercicios Resueltos : 
	* Guia 1 (Recta y plano)
		* Ex.23 
	* Guia 2


## Guia 1 

### Ex.23 

> Halle las ecuaciones de los tres planos que incluyen a la recta $`r:(x,y,z) = (2,-2,3) + \lambda(1,2,-1) , con \lambda \in \R `$ y que son perpendiculares a cada uno de los planos coordenadas 

* Planos 
	* xy ---> k=(0,0,1)
	* yz ---> i=(1,0,0)
	* xz ---> j=(0,1,0)
(1) $`\vec{v_r}\perp\vec{n}`$</br> 
(2) $`\vec{n}\perp\vec{k} `$ </br>
(3) $`\vec{n}\| \vec{v_r} x \vec{w}`$</br> 
Del (3) saco un plano de la forma:
(4) $`\pi:x+y+z+d=0`$
(5) Remplazo con un punto de la recta y vuelvo a hacer pasos anteriores con los otros planos .
