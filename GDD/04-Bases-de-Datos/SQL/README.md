# SQL

## Pasos

* Se empieza por el from 
* Asegurarse que no cambie la atomicidad 
* El group by son todas las cosas no calculadas 

## Ejercicio 1 

> Mostrar el código, razón social de todos los clientes cuyo límite de crédito sea mayor o
igual a $ 1000 ordenado por código de cliente.

```SQL
SELECT clie_codigo, clie_razon_social
From [2023-Gestion].dbo.Cliente
where clie_limite_credito >= 1000; 
```

## Ejercicio 2 

> Mostrar el código, detalle de todos los artículos vendidos en el año 2012 ordenados por cantidad vendida.

```
 SELECT
    p.prod_codigo,
    p.prod_detalle,
    SUM(i.item_cantidad) AS cantidad_vendida
FROM
    Factura f
JOIN
    Item_Factura i ON f.fact_tipo = i.item_tipo
                   AND f.fact_sucursal = i.item_sucursal
                   AND f.fact_numero = i.item_numero
JOIN
    Producto p ON i.item_producto = p.prod_codigo
WHERE
    YEAR(f.fact_fecha) = 2012
GROUP BY
    p.prod_codigo,
    p.prod_detalle
ORDER BY
    cantidad_vendida DESC;
```
### Resuelto en clase 



```
select prod_codigo, prod_detalle ,sum(item_cantidad)  
from item_factura 
join producto on prod_codigo = itemproducto 
group by prod_codigo, prod_detalle 
ordery by sum(item_cantidad) 

```

el join se puede poner todo junto ` factura on fact_tipo+fact_sucursal+fact_numero = item_tipo+item_sucursal+item_numero`


### Otra forma 

```
 SELECT
    p.prod_codigo,
    p.prod_detalle,
FROM
    Factura f
JOIN
    Item_Factura i ON f.fact_tipo = i.item_tipo
                   AND f.fact_sucursal = i.item_sucursal
                   AND f.fact_numero = i.item_numero
JOIN
    Producto p ON i.item_producto = p.prod_codigo
WHERE
    YEAR(f.fact_fecha) = 2012
GROUP BY
    p.prod_codigo,
    p.prod_detalle
ORDER BY
    cSUM(i.item_cantidad) DESC;
```

## Ejercicio 3

> Realizar una consulta que muestre código de producto, nombre de producto y el stock total, sin importar en que deposito se encuentre, los datos deben ser ordenados por nombre del artículo de menor a mayor.

```
SELECT p.prod_codigo,p.prod_detalle,sum(s.stoc_cantidad) as cantidad
from Producto p
inner join stock s on p.prod_codigo = s.stoc_producto
inner join DEPOSITO d on d.depo_codigo = s.stoc_deposito
group by p.prod_codigo,p.prod_detalle
order by p.prod_detalle ASC
```

### Resuelto en clase 

```
select prod_codigo,prod_detalle, sum(isnull(stoc_cantidad ,0))
from producto 
left join stock on prod_codigo = stoc_producto 
order by 2 
```

## Ejercicio 4 

> Realizar una consulta que muestre para todos los artículos código, detalle y cantidad de artículos que lo componen. Mostrar solo aquellos artículos para los cuales el stock promedio por depósito sea mayor a 100.

### Resuelto en clase 

```
select prod_codigo, prod_detalle, count(distinct comp_componente)
from producto 
left join composicion on prod_codigo = comp_producto
join stock on stoc_producto = prod_codigo 
group by prod_codigo, prod_detalle 
having avg(stoc_cantidad) > 100
order by count(comp_componente) dec 
```
Otra forma sacando el join de stock 

```
select prod_codigo, prod_detalle, count( comp_componente)
from producto 
left join composicion on prod_codigo = comp_producto
group by prod_codigo, prod_detalle 
having prod_codigo in ( select stock_producto from stock group by stock_producto having avg(stoc_cantidad) > 100)
order by count(comp_componente) desc
```

## Ejercicio 5 

```

select p.prod_codigo,
	   p.prod_detalle,
	   sum(i.item_cantidad) AS cantidad_vendida
from Factura f
join Item_Factura i on f.fact_tipo = i.item_tipo
					and f.fact_sucursal = i.item_sucursal
					and f.fact_numero = i.item_numero
join Producto p on i.item_producto = p.prod_codigo
where YEAR(f.fact_fecha) = 2012
group by
	p.prod_codigo,
	p.prod_detalle
having sum(i.item_cantidad) > (
								select sum(i2.item_cantidad)
								from Factura f2
								join Item_Factura i2 on f2.fact_tipo = i2.item_tipo
													and f2.fact_sucursal = i2.item_sucursal
													and f2.fact_numero = i2.item_numero
								join Producto p2 on i2.item_producto = p2.prod_codigo
								where YEAR(f2.fact_fecha) = 2011 and i2.item_producto = p.prod_codigo

							   )
```

## Ejercicio 6 

```
select p.prod_codigo,s.stoc_cantidad,d.depo_codigo,r.rubr_detalle
from stock s
join Producto p on p.prod_codigo = s.stoc_producto
join DEPOSITO d on d.depo_codigo = s.stoc_deposito
join Rubro r  on r.rubr_id = p.prod_rubro
where d.depo_codigo = '00'
group by
	p.prod_codigo,
	d.depo_codigo,
	r.rubr_detalle,
	s.stoc_cantidad
having s.stoc_cantidad > (
							select s2.stoc_cantidad
							from STOCK s2
							join DEPOSITO d2 on d2.depo_codigo = s2.stoc_deposito
							join Producto p2 on s2.stoc_producto = p2.prod_codigo
							where p2.prod_codigo = '00000000' and d2.depo_codigo = '00'
							)
```
