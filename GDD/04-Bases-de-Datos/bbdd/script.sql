USE [master]
GO
/****** Object:  Database [GD1C2023]    Script Date: 9/8/2024 21:04:24 ******/
CREATE DATABASE [GD1C2023]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GD1C2023', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\GD1C2023.mdf' , SIZE = 777216KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GD1C2023_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\GD1C2023_log.ldf' , SIZE = 16576KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GD1C2023] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GD1C2023].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GD1C2023] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GD1C2023] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GD1C2023] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GD1C2023] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GD1C2023] SET ARITHABORT OFF 
GO
ALTER DATABASE [GD1C2023] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GD1C2023] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [GD1C2023] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GD1C2023] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GD1C2023] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GD1C2023] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GD1C2023] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GD1C2023] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GD1C2023] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GD1C2023] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GD1C2023] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GD1C2023] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GD1C2023] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GD1C2023] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GD1C2023] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GD1C2023] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GD1C2023] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GD1C2023] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GD1C2023] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GD1C2023] SET  MULTI_USER 
GO
ALTER DATABASE [GD1C2023] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GD1C2023] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GD1C2023] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GD1C2023] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [GD1C2023]
GO
/****** Object:  Schema [gd_esquema]    Script Date: 9/8/2024 21:04:24 ******/
CREATE SCHEMA [gd_esquema]
GO
/****** Object:  Schema [INTER_DATA]    Script Date: 9/8/2024 21:04:24 ******/
CREATE SCHEMA [INTER_DATA]
GO
/****** Object:  StoredProcedure [INTER_DATA].[migracion]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migracion]
AS
EXEC INTER_DATA.migrar_provincia
EXEC INTER_DATA.migrar_localidad
EXEC INTER_DATA.migrar_direccion
EXEC INTER_DATA.migrar_tipo_movilidad
EXEC INTER_DATA.migrar_tipo_paquete
EXEC INTER_DATA.migrar_paquete

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_cupon_cupon]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_cupon_cupon]
AS
INSERT INTO INTER_DATA.cupon(Numero,Monto,Fecha_Alta,Fecha_Vencimiento,Tipo_Cupon_id_Tipo_Cupon)
	SELECT 
		DISTINCT CUPON_NRO,
		CUPON_MONTO,
		CUPON_FECHA_ALTA,
		CUPON_FECHA_VENCIMIENTO,
		(SELECT id_Tipo_Cupon
		 FROM INTER_DATA.Tipo_Cupon TC
		 WHERE TC.Tipo = M.CUPON_TIPO
			)
			
	FROM gd_esquema.Maestra M
	WHERE 
		CUPON_NRO		            IS NOT NULL
		AND CUPON_MONTO             IS NOT NULL
		AND CUPON_FECHA_ALTA        IS NOT NULL
		AND CUPON_FECHA_VENCIMIENTO IS NOT NULL
		AND M.CUPON_TIPO != 'Devoluci�n'

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_cupon_reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_cupon_reclamo]
AS
INSERT INTO INTER_DATA.cupon(Numero, Monto, Fecha_Alta, Fecha_Vencimiento,Tipo_Cupon_id_Tipo_Cupon)
	SELECT 
		DISTINCT M.CUPON_RECLAMO_NRO,
				 M.CUPON_RECLAMO_MONTO,
				 M.CUPON_RECLAMO_FECHA_ALTA,
				 M.CUPON_RECLAMO_FECHA_VENCIMIENTO,
				 (SELECT id_Tipo_Cupon
				  FROM INTER_DATA.Tipo_Cupon TC
				  WHERE M.CUPON_RECLAMO_TIPO = TC.Tipo
				  )
	FROM gd_esquema.Maestra M
	WHERE CUPON_RECLAMO_TIPO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_cupon_x_local]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_cupon_x_local] 
AS
INSERT INTO INTER_DATA.Cupon_X_Local(Cupon_Numero_Cupon,Local_id_Local)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			WHERE C.Numero = M.CUPON_RECLAMO_NRO
			OR C.Numero = M.CUPON_NRO
		) ,
		(SELECT id_local
			FROM INTER_DATA.Local L
			WHERE M.LOCAL_DESCRIPCION = L.Descripcion
			AND M.LOCAL_NOMBRE = L.Nombre
		)
	FROM gd_esquema.Maestra M
	WHERE CUPON_NRO IS NOT NULL 
	OR CUPON_RECLAMO_NRO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_Cupon_X_Pedido]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_Cupon_X_Pedido]  
AS
INSERT INTO INTER_DATA.Cupon_X_Pedido(Cupon_Numero,Pedido_Numero)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			WHERE C.Numero = M.CUPON_NRO
		) ,
		(SELECT Numero
			FROM INTER_DATA.Pedido P
			WHERE P.Numero = M.PEDIDO_NRO
		)
	FROM gd_esquema.Maestra M
	WHERE CUPON_NRO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_cupon_x_reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_cupon_x_reclamo] 
AS
INSERT INTO INTER_DATA.Cupon_X_Reclamo(Cupon_Numero_Cupon,Reclamo_Numero_Reclamo)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			JOIN INTER_DATA.Tipo_Cupon TC ON C.Tipo_Cupon_id_Tipo_Cupon = TC.id_Tipo_Cupon
			WHERE C.Numero = M.CUPON_RECLAMO_NRO
			AND TC.Tipo = M.CUPON_RECLAMO_TIPO
		) ,
		(SELECT Numero
			FROM INTER_DATA.Reclamo P
			WHERE P.Numero = M.RECLAMO_NRO
		)
	FROM gd_esquema.Maestra M
	WHERE CUPON_RECLAMO_NRO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_cupon_x_usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_cupon_x_usuario] 
AS
INSERT INTO INTER_DATA.Cupon_X_Usuario(Cupon_Numero_Cupon,Usuario_id_Usuario)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			WHERE C.Numero = M.CUPON_RECLAMO_NRO
			OR C.Numero = M.CUPON_NRO
		) ,
		(SELECT id_usuario
			 FROM INTER_DATA.Usuario U
			 WHERE  U.Apellido = M.USUARIO_APELLIDO
			 AND    U.DNI = M.USUARIO_DNI
			 AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
			)
	FROM gd_esquema.Maestra M
	WHERE CUPON_RECLAMO_NRO IS NOT NULL
	OR CUPON_NRO IS NOT NULL
	AND M.CUPON_TIPO != 'Devoluci�n'

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion]
AS
CREATE TABLE INTER_DATA.aux (
	  id_direccion INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Calle NVARCHAR(50) NOT NULL,
	  Numero DECIMAL(18,0)  NOT NULL,
	  Localidad_id_Localidad INT
)
EXEC INTER_DATA.migrar_direccion_usuarios
EXEC INTER_DATA.migrar_direccion_local
EXEC INTER_DATA.migrar_direccion_operador
EXEC INTER_DATA.migrar_direccion_repartidor
EXEC INTER_DATA.migrar_direccion_destino
EXEC INTER_DATA.migrar_direccion_origen
INSERT INTO INTER_DATA.Direccion(Calle, Numero, Localidad_id_Localidad)
		SELECT 
			DISTINCT Calle, 
			Numero, 
			Localidad_id_Localidad
		FROM INTER_DATA.aux
		ORDER BY Calle, Numero
DROP TABLE INTER_DATA.aux

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_destino]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_destino]
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
	SELECT 
		DISTINCT INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_DEST) AS Calle,
		INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_DEST) AS Numero, 
		(SELECT id_Localidad 
		 FROM INTER_DATA.Localidad L 
		 JOIN INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
		 WHERE M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
		)
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_DIR_DEST IS NOT NULL
	GROUP BY M.ENVIO_MENSAJERIA_DIR_DEST, M.ENVIO_MENSAJERIA_LOCALIDAD, M.ENVIO_MENSAJERIA_PROVINCIA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_local]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_local]
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
SELECT DISTINCT INTER_DATA.get_calle(M.LOCAL_DIRECCION) AS Calle,
				INTER_DATA.get_numero(M.LOCAL_DIRECCION) AS Numero, 
				(SELECT id_Localidad 
				 FROM INTER_DATA.Localidad L 
				 JOIN INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
				 WHERE M.LOCAL_LOCALIDAD = L.Localidad AND M.LOCAL_PROVINCIA = P.Nombre
				)
	FROM gd_esquema.Maestra M
	WHERE M.LOCAL_DIRECCION IS NOT NULL
	GROUP BY M.LOCAL_DIRECCION, M.LOCAL_LOCALIDAD, M.LOCAL_PROVINCIA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_operador]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_operador]
AS
INSERT INTO INTER_DATA.aux(Calle, Numero)
SELECT DISTINCT INTER_DATA.get_calle(OPERADOR_RECLAMO_DIRECCION) AS Calle,
				INTER_DATA.get_numero(OPERADOR_RECLAMO_DIRECCION) AS Numero
	FROM gd_esquema.Maestra
	WHERE OPERADOR_RECLAMO_DIRECCION IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_origen]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_origen]
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
	SELECT 
		DISTINCT INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_ORIG) AS Calle,
		INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_ORIG) AS Numero, 
		(SELECT id_Localidad 
		 FROM INTER_DATA.Localidad L 
		 JOIN INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
		 WHERE M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
		)
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_DIR_ORIG IS NOT NULL
	GROUP BY M.ENVIO_MENSAJERIA_DIR_ORIG, M.ENVIO_MENSAJERIA_LOCALIDAD, M.ENVIO_MENSAJERIA_PROVINCIA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_repartidor]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_repartidor]
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
	SELECT 
		DISTINCT INTER_DATA.get_calle(REPARTIDOR_DIRECION) AS Calle,
		INTER_DATA.get_numero(REPARTIDOR_DIRECION) AS Numero,
		(SELECT id_Localidad FROM INTER_DATA.Localidad L
		JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
		WHERE
			(CASE
				WHEN M.ENVIO_MENSAJERIA_FECHA >= M.PEDIDO_FECHA THEN M.ENVIO_MENSAJERIA_LOCALIDAD
				ELSE M.LOCAL_LOCALIDAD
			END
		) = L.Localidad
		AND 
			(CASE
				WHEN M.ENVIO_MENSAJERIA_FECHA >= M.PEDIDO_FECHA THEN M.ENVIO_MENSAJERIA_PROVINCIA
				ELSE M.LOCAL_PROVINCIA
			END
		) = P.Nombre
				
		)
	FROM gd_esquema.Maestra M
	WHERE M.REPARTIDOR_DIRECION IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_usuario]
AS
INSERT INTO INTER_DATA.Direccion_Usuario(Nombre,Usuario_id_usuario,Direccion_id_direccion)
	SELECT 
		DISTINCT DIRECCION_USUARIO_NOMBRE,
		(SELECT id_usuario
		 FROM INTER_DATA.Usuario U
		 WHERE  U.Apellido = M.USUARIO_APELLIDO
		 AND    U.DNI = M.USUARIO_DNI
		 AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
			),
		(SELECT id_direccion
				FROM INTER_DATA.Direccion D
				JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
				JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
				WHERE INTER_DATA.get_calle(M.DIRECCION_USUARIO_DIRECCION) = D.Calle
				AND INTER_DATA.get_numero(M.DIRECCION_USUARIO_DIRECCION) = D.Numero
				AND M.DIRECCION_USUARIO_LOCALIDAD = L.Localidad
				AND M.DIRECCION_USUARIO_PROVINCIA = P.Nombre
			)
	FROM gd_esquema.Maestra M
	WHERE 
		DIRECCION_USUARIO_NOMBRE		IS NOT NULL 

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_direccion_usuarios]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_direccion_usuarios]
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
SELECT DISTINCT INTER_DATA.get_calle(M.DIRECCION_USUARIO_DIRECCION) AS Calle,
				INTER_DATA.get_numero(M.DIRECCION_USUARIO_DIRECCION) AS Numero, 
				(SELECT	id_Localidad 
				 FROM	INTER_DATA.Localidad L 
				 JOIN	INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
				 WHERE	M.DIRECCION_USUARIO_LOCALIDAD = L.Localidad AND M.DIRECCION_USUARIO_PROVINCIA = P.Nombre
				)
	FROM gd_esquema.Maestra M
	WHERE M.DIRECCION_USUARIO_DIRECCION IS NOT NULL
	GROUP BY M.DIRECCION_USUARIO_DIRECCION, M.DIRECCION_USUARIO_LOCALIDAD, M.DIRECCION_USUARIO_PROVINCIA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_envio]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_envio]
AS
CREATE TABLE INTER_DATA.aux (
	  id_envio INT IDENTITY(1,1) PRIMARY KEY	NOT NULL,
	  Precio_Envio DECIMAL(18,2)				NOT NULL,
	  Propina DECIMAL(18,2)						NOT NULL,
	  Tiempo_Estimado DECIMAL(18,2)				NOT NULL,
	  Fecha_Entrega DATETIME					NOT NULL,
	  Direccion_id_direccion_destino INT		NOT NULL,
	  Repartidor_id_repartidor INT				NOT NULL
)
EXEC INTER_DATA.migrar_envio_mensajeria
EXEC INTER_DATA.migrar_envio_pedido
INSERT INTO INTER_DATA.Envio(Precio_Envio, Propina, Tiempo_Estimado, Fecha_Entrega, Direccion_id_direccion_destino, Repartidor_id_repartidor)
		SELECT 
			DISTINCT Precio_Envio, 
			Propina, Tiempo_Estimado, 
			Fecha_Entrega, 
			Direccion_id_direccion_destino, 
			Repartidor_id_repartidor 
		FROM INTER_DATA.aux
		ORDER BY Precio_Envio
DROP TABLE INTER_DATA.aux

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_envio_mensajeria]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_envio_mensajeria]
AS
INSERT INTO INTER_DATA.aux(Precio_Envio, Propina, Tiempo_Estimado, Fecha_Entrega, Direccion_id_direccion_destino, Repartidor_id_repartidor)
	SELECT DISTINCT M.ENVIO_MENSAJERIA_PRECIO_ENVIO,
				 M.ENVIO_MENSAJERIA_PROPINA,
				 M.ENVIO_MENSAJERIA_TIEMPO_ESTIMADO,
				 M.ENVIO_MENSAJERIA_FECHA_ENTREGA,
				 (SELECT DISTINCT id_direccion
					  FROM INTER_DATA.Direccion D
					  JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
					  JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
					  WHERE INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Calle
					  AND INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Numero
					  AND M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad
					  AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
				 ),
				 (SELECT DISTINCT id_repartidor
				  FROM INTER_DATA.Repartidor R
				  WHERE M.REPARTIDOR_APELLIDO = R.Apellido
					AND M.REPARTIDOR_DNI =		R.DNI
					AND M.REPARTIDOR_FECHA_NAC= R.Fecha_Nac
				 )
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_PRECIO_ENVIO		IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_PROPINA			IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_TIEMPO_ESTIMADO	IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_FECHA_ENTREGA		IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_DIR_DEST			IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_envio_pedido]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_envio_pedido]
AS
INSERT INTO INTER_DATA.aux(Precio_Envio, Propina, Tiempo_Estimado, Fecha_Entrega, Direccion_id_direccion_destino, Repartidor_id_repartidor)
	SELECT DISTINCT M.PEDIDO_PRECIO_ENVIO,
				 M.PEDIDO_PROPINA,
				 M.PEDIDO_TIEMPO_ESTIMADO_ENTREGA,
				 M.PEDIDO_FECHA_ENTREGA,
				 (SELECT DISTINCT id_direccion
				  FROM INTER_DATA.Direccion D
				  JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
				  JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
				  WHERE INTER_DATA.get_calle(M.DIRECCION_USUARIO_DIRECCION) = D.Calle
				  AND INTER_DATA.get_numero(M.DIRECCION_USUARIO_DIRECCION) = D.Numero
				  AND M.DIRECCION_USUARIO_LOCALIDAD = L.Localidad
				  AND M.DIRECCION_USUARIO_PROVINCIA = P.Nombre
				 ),
				 (SELECT DISTINCT id_repartidor
					FROM INTER_DATA.Repartidor R
					WHERE M.REPARTIDOR_NOMBRE    =	R.Nombre
					  AND M.REPARTIDOR_APELLIDO  =	R.Apellido
					  AND M.REPARTIDOR_DNI       =	R.DNI
					  AND M.REPARTIDOR_TELEFONO  =	R.Telefono
					  AND M.REPARTIDOR_EMAIL     =	R.Mail
					  AND M.REPARTIDOR_FECHA_NAC =	R.Fecha_Nac
				 )
	FROM gd_esquema.Maestra M
	WHERE M.PEDIDO_PRECIO_ENVIO				IS NOT NULL
	  AND M.PEDIDO_PROPINA					IS NOT NULL
	  AND M.PEDIDO_TIEMPO_ESTIMADO_ENTREGA	IS NOT NULL
	  AND M.PEDIDO_FECHA_ENTREGA			IS NOT NULL
	  AND M.DIRECCION_USUARIO_DIRECCION		IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_estado_reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_estado_reclamo]
AS
INSERT INTO INTER_DATA.Estado_Reclamo(Estado)
	SELECT 
		DISTINCT(RECLAMO_ESTADO) 
	FROM gd_esquema.Maestra
	WHERE RECLAMO_ESTADO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_horario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_horario]
AS
INSERT INTO INTER_DATA.Horario(Hora_Apertura, Hora_CIerre, Dia)
SELECT DISTINCT
	HORARIO_LOCAL_HORA_APERTURA, 
	HORARIO_LOCAL_HORA_CIERRE,
	HORARIO_LOCAL_DIA 
FROM gd_esquema.Maestra M
WHERE HORARIO_LOCAL_DIA�IS�NOT�NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_horario_x_local]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_horario_x_local]
AS
INSERT INTO INTER_DATA.Horario_X_Local(Local_id_local, Horario_id_horario)
	SELECT DISTINCT(
		SELECT id_local 
			FROM INTER_DATA.Local L 
				WHERE L.Nombre = M.LOCAL_NOMBRE), 
		(SELECT id_horario 
			FROM INTER_DATA.Horario H 
				WHERE H.Dia = HORARIO_LOCAL_DIA 
					AND H.Hora_Apertura = HORARIO_LOCAL_HORA_APERTURA 
					AND H.Hora_CIerre = HORARIO_LOCAL_HORA_CIERRE)
	FROM gd_esquema.Maestra M
	WHERE M.LOCAL_NOMBRE IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_local_tipo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_local_tipo]
AS
INSERT INTO INTER_DATA.Local_Tipo(Tipo)
	SELECT DISTINCT Local_Tipo 
		FROM gd_esquema.Maestra
			WHERE LOCAL_TIPO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_localidad]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_localidad]
AS
CREATE TABLE INTER_DATA.aux (
	  id_Localidad INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Localidad NVARCHAR(255) NOT NULL,
	  Provincia_id_Provincia INT  NOT NULL
)
EXEC INTER_DATA.migrar_localidad_usuario
EXEC INTER_DATA.migrar_localidad_envio_mensajeria
INSERT INTO INTER_DATA.Localidad(Localidad, Provincia_id_Provincia)
		SELECT DISTINCT Localidad, Provincia_id_Provincia FROM INTER_DATA.aux
			ORDER BY Localidad
DROP TABLE INTER_DATA.aux

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_localidad_envio_mensajeria]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_localidad_envio_mensajeria]
AS
INSERT INTO INTER_DATA.aux(Localidad, Provincia_id_Provincia)
	SELECT 
		M.ENVIO_MENSAJERIA_LOCALIDAD,
		(SELECT id_Provincia 
		 FROM	INTER_DATA.Provincia P
		 WHERE	P.Nombre = M.ENVIO_MENSAJERIA_PROVINCIA
		)
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_LOCALIDAD IS NOT NULL
	GROUP BY M.ENVIO_MENSAJERIA_LOCALIDAD, M.ENVIO_MENSAJERIA_PROVINCIA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_localidad_usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_localidad_usuario]
AS
INSERT INTO INTER_DATA.aux(Localidad, Provincia_id_Provincia)
	SELECT 
		M.DIRECCION_USUARIO_LOCALIDAD, 
		(SELECT id_Provincia 
		 FROM	INTER_DATA.Provincia P
		 WHERE	P.Nombre = M.DIRECCION_USUARIO_PROVINCIA
		)
	FROM gd_esquema.Maestra M
	WHERE M.DIRECCION_USUARIO_LOCALIDAD IS NOT NULL
	GROUP BY M.DIRECCION_USUARIO_LOCALIDAD, M.DIRECCION_USUARIO_PROVINCIA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_mensajeria]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_mensajeria]
AS
INSERT INTO INTER_DATA.Mensajeria(Numero, KM, Observaciones, Precio_Seguro, Total, Fecha, Calificacion, Paquete_id_Paquete, Usuario_id_usuario, Envio_id_envio, Tipo_Estado_id_Tipo_Estado, Tipo_Medio_Pago_id_tipo_medio_pago, Direccion_id_direccion_origen)
	SELECT DISTINCT M.ENVIO_MENSAJERIA_NRO,
				 M.ENVIO_MENSAJERIA_KM,
				 M.ENVIO_MENSAJERIA_OBSERV,
				 M.ENVIO_MENSAJERIA_PRECIO_SEGURO,
				 M.ENVIO_MENSAJERIA_TOTAL,
				 M.ENVIO_MENSAJERIA_FECHA,
				 M.ENVIO_MENSAJERIA_CALIFICACION,
				 (SELECT DISTINCT id_Paquete
					  FROM INTER_DATA.Paquete P
					  JOIN INTER_DATA.Tipo_Paquete TP ON P.Tipo_Paquete_id_tipo_paquete = TP.id_tipo_paquete
					  WHERE M.ENVIO_MENSAJERIA_VALOR_ASEGURADO = P.Valor_Asegurado
						AND M.PAQUETE_TIPO = TP.Nombre
				 ),
				 (SELECT DISTINCT id_usuario
					  FROM INTER_DATA.Usuario U
					  WHERE U.DNI = M.USUARIO_DNI
						AND    U.Fecha_Registro = M.USUARIO_FECHA_REGISTRO
				 ),
				 (SELECT DISTINCT id_envio
					  FROM INTER_DATA.Envio ENV
					  WHERE M.ENVIO_MENSAJERIA_FECHA_ENTREGA = ENV.Fecha_Entrega
						AND M.ENVIO_MENSAJERIA_PRECIO_ENVIO = ENV.Precio_Envio
						AND M.ENVIO_MENSAJERIA_PROPINA = ENV.Propina
						AND M.ENVIO_MENSAJERIA_TIEMPO_ESTIMADO = ENV.Tiempo_Estimado
				 ),
				 (SELECT DISTINCT id_Tipo_Estado
					  FROM INTER_DATA.Tipo_Estado E
					  WHERE M.ENVIO_MENSAJERIA_ESTADO = E.Nombre
				 ),
				 (SELECT DISTINCT id_tipo_medio_pago
					FROM INTER_DATA.Tipo_Medio_Pago MP
					WHERE M.MEDIO_PAGO_TIPO = MP.Nombre
					AND MP.Tarjeta_Usuario_Numero = MEDIO_PAGO_NRO_TARJETA
				 ),
				 (SELECT DISTINCT id_direccion
					  FROM INTER_DATA.Direccion D
					  JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
					  JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
					  WHERE INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Calle
					  AND INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Numero
					  AND M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad
					  AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
				 )
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_NRO				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_KM					IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_OBSERV				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_PRECIO_SEGURO		IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_TOTAL				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_FECHA				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_CALIFICACION		IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_operador]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_operador]
AS
INSERT INTO INTER_DATA.OPERADOR(Nombre, Apellido, DNI, Telefono, Mail,Fecha_Nac, Direccion_id_direccion)
	SELECT DISTINCT M.OPERADOR_RECLAMO_NOMBRE,
					M.OPERADOR_RECLAMO_APELLIDO,
					M.OPERADOR_RECLAMO_DNI,
					M.OPERADOR_RECLAMO_TELEFONO,
					M.OPERADOR_RECLAMO_MAIL,
					M.OPERADOR_RECLAMO_FECHA_NAC,
					(SELECT DISTINCT id_direccion
					 FROM INTER_DATA.Direccion A
					 WHERE INTER_DATA.get_calle(M.OPERADOR_RECLAMO_DIRECCION) = A.Calle
					 AND INTER_DATA.get_numero(M.OPERADOR_RECLAMO_DIRECCION) = A.numero
					 AND A.Localidad_id_Localidad IS NULL
					)
	FROM gd_esquema.Maestra M
	WHERE M.OPERADOR_RECLAMO_NOMBRE		IS NOT NULL
	  AND M.OPERADOR_RECLAMO_APELLIDO	IS NOT NULL
	  AND M.OPERADOR_RECLAMO_DNI		IS NOT NULL
	  AND M.OPERADOR_RECLAMO_TELEFONO	IS NOT NULL
	  AND M.OPERADOR_RECLAMO_MAIL		IS NOT NULL
	  AND M.OPERADOR_RECLAMO_FECHA_NAC	IS NOT NULL
	  AND M.OPERADOR_RECLAMO_DIRECCION	IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_paquete]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_paquete]
AS
INSERT INTO INTER_DATA.Paquete(Valor_Asegurado, Tipo_Paquete_id_tipo_paquete)
	SELECT 
		DISTINCT M.ENVIO_MENSAJERIA_VALOR_ASEGURADO,
		(SELECT id_tipo_paquete
			FROM INTER_DATA.Tipo_Paquete P
			WHERE P.Nombre = M.PAQUETE_TIPO
		) 
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_VALOR_ASEGURADO IS NOT NULL
	ORDER BY M.ENVIO_MENSAJERIA_VALOR_ASEGURADO

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_pedido]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_pedido]
AS
INSERT INTO INTER_DATA.Pedido(Numero,Fecha,Tarifa_Servicio,Total_Productos,Observaciones,Calificacion,Total_Cupones,Total_Servicio,Usuario_id_usuario,Local_id_local,Envio_id_envio,Tipo_Estado_id_Tipo_Estado,Tipo_Medio_Pago_id_tipo_medio_pago)
	SELECT DISTINCT
		PEDIDO_NRO,
		PEDIDO_FECHA,
		PEDIDO_TARIFA_SERVICIO,
		PEDIDO_TOTAL_PRODUCTOS,
		PEDIDO_OBSERV,
		PEDIDO_CALIFICACION,
		PEDIDO_TOTAL_CUPONES,
		PEDIDO_TOTAL_SERVICIO,
		(SELECT id_usuario
			 FROM INTER_DATA.Usuario U
			 WHERE  U.Apellido = M.USUARIO_APELLIDO
			 AND    U.DNI = M.USUARIO_DNI
			 AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
		),
		(SELECT id_local
			FROM INTER_DATA.Local L
			WHERE M.LOCAL_DESCRIPCION = L.Descripcion
			AND M.LOCAL_NOMBRE = L.Nombre
		) ,
		(SELECT DISTINCT id_envio
					  FROM INTER_DATA.Envio ENV
					  WHERE M.PEDIDO_FECHA_ENTREGA = ENV.Fecha_Entrega
						AND M.PEDIDO_PRECIO_ENVIO = ENV.Precio_Envio
						AND M.PEDIDO_PROPINA = ENV.Propina
						AND M.PEDIDO_TIEMPO_ESTIMADO_ENTREGA = ENV.Tiempo_Estimado
				 ),
		(SELECT DISTINCT id_Tipo_Estado
					  FROM INTER_DATA.Tipo_Estado E
					  WHERE M.PEDIDO_ESTADO = E.Nombre
				 ),
		(SELECT DISTINCT id_tipo_medio_pago
					FROM INTER_DATA.Tipo_Medio_Pago MP
					JOIN INTER_DATA.Tarjeta_Usuario TU ON MP.Tarjeta_Usuario_Numero = TU.Numero
					WHERE M.MEDIO_PAGO_TIPO = MP.Nombre
					AND TU.Numero = MEDIO_PAGO_NRO_TARJETA
				 ) 
		
	FROM gd_esquema.Maestra M
	WHERE PEDIDO_NRO				IS NOT NULL 
		AND PEDIDO_FECHA			IS NOT NULL  
		AND PEDIDO_TARIFA_SERVICIO	IS NOT NULL  
		AND PEDIDO_TOTAL_PRODUCTOS	IS NOT NULL  
		AND PEDIDO_OBSERV			IS NOT NULL  
		AND PEDIDO_CALIFICACION		IS NOT NULL  
		AND PEDIDO_TOTAL_CUPONES	IS NOT NULL  
		AND PEDIDO_TOTAL_SERVICIO	IS NOT NULL 
		

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_producto]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_producto]
AS
INSERT INTO INTER_DATA.Producto(Codigo, Nombre, Descripcion, Precio, Local_id_local)
SELECT DISTINCT
	PRODUCTO_LOCAL_CODIGO, 
	PRODUCTO_LOCAL_NOMBRE,
	PRODUCTO_LOCAL_DESCRIPCION, 
	PRODUCTO_LOCAL_PRECIO, (
		SELECT id_local 
		FROM INTER_DATA.Local L
		WHERE L.Nombre = M.LOCAL_NOMBRE)  
FROM gd_esquema.Maestra M
WHERE PRODUCTO_LOCAL_CODIGO�IS�NOT�NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_provincia]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_provincia]
AS
INSERT INTO INTER_DATA.Provincia(Nombre)
	SELECT 
		DISTINCT(DIRECCION_USUARIO_PROVINCIA) 
	FROM gd_esquema.Maestra
	WHERE DIRECCION_USUARIO_PROVINCIA IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_reclamo]
AS
INSERT INTO INTER_DATA.reclamo(Numero,Fecha,Descripcion,Fecha_Solucion,Solucion,Calificacion,Pedido_Numero,Estado_Reclamo_id_Estado_Reclamo,Tipo_Reclamo_id_Tipo_Reclamo,Operador_id_operador)
	SELECT DISTINCT
		RECLAMO_NRO,
		RECLAMO_FECHA,
		RECLAMO_DESCRIPCION,
		RECLAMO_FECHA_SOLUCION,
		RECLAMO_SOLUCION,
		RECLAMO_CALIFICACION,
		(SELECT Numero
			FROM INTER_DATA.Pedido P
			WHERE P.Numero = M.PEDIDO_NRO
		) ,
		(SELECT id_Estado_Reclamo
			FROM INTER_DATA.Estado_Reclamo ER
			WHERE ER.Estado = M.RECLAMO_ESTADO
		) ,
		(SELECT id_Tipo_Reclamo
			FROM INTER_DATA.Tipo_Reclamo TR
			WHERE TR.Tipo = M.RECLAMO_TIPO
		) ,
		(SELECT id_operador
			FROM INTER_DATA.Operador O
			WHERE O.DNI = M.OPERADOR_RECLAMO_DNI
			AND O.Nombre = M.OPERADOR_RECLAMO_NOMBRE
			AND O.Apellido = M.OPERADOR_RECLAMO_APELLIDO
			AND O.Telefono = M.OPERADOR_RECLAMO_TELEFONO
			AND O.Fecha_Nac = M.OPERADOR_RECLAMO_FECHA_NAC
			AND O.Mail = M.OPERADOR_RECLAMO_MAIL
		)
	FROM gd_esquema.Maestra M
	WHERE RECLAMO_NRO			IS NOT NULL 
	AND RECLAMO_FECHA			IS NOT NULL 
	AND RECLAMO_DESCRIPCION		IS NOT NULL 
	AND RECLAMO_FECHA_SOLUCION	IS NOT NULL 
	AND RECLAMO_SOLUCION		IS NOT NULL 
	AND RECLAMO_CALIFICACION	IS NOT NULL 


GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_repartidor]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_repartidor]
AS
INSERT INTO INTER_DATA.Repartidor(Nombre, Apellido, DNI, Telefono, Mail,Fecha_Nac, Direccion_id_direccion, Tipo_Movilidad_id_Tipo_Movilidad)
	SELECT DISTINCT M.REPARTIDOR_NOMBRE,
					M.REPARTIDOR_APELLIDO,
					M.REPARTIDOR_DNI,
					M.REPARTIDOR_TELEFONO,
					M.REPARTIDOR_EMAIL,
					M.REPARTIDOR_FECHA_NAC,
					(SELECT DISTINCT id_direccion
						FROM INTER_DATA.Direccion A
						WHERE INTER_DATA.get_calle(M.REPARTIDOR_DIRECION) = A.Calle
						AND INTER_DATA.get_numero(M.REPARTIDOR_DIRECION) = A.Numero
						AND A.Localidad_id_Localidad IS NULL
					),
					(SELECT id_Tipo_Movilidad
						FROM INTER_DATA.Tipo_Movilidad TM
						WHERE M.REPARTIDOR_TIPO_MOVILIDAD = TM.Tipo_Movilidad
					)
	FROM gd_esquema.Maestra M
	WHERE M.REPARTIDOR_NOMBRE		IS NOT NULL
	AND M.REPARTIDOR_APELLIDO		IS NOT NULL
	AND M.REPARTIDOR_DNI			IS NOT NULL
	AND M.REPARTIDOR_TELEFONO		IS NOT NULL
	AND M.REPARTIDOR_EMAIL			IS NOT NULL
	AND M.REPARTIDOR_FECHA_NAC		IS NOT NULL
	AND M.REPARTIDOR_DIRECION		IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tarjeta_usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tarjeta_usuario]
AS
INSERT INTO INTER_DATA.Tarjeta_Usuario(Numero, Marca_Tarjeta,Tipo,Usuario_id_usuario)
	SELECT 
	DISTINCT MEDIO_PAGO_NRO_TARJETA,
			 MARCA_TARJETA,
			 MEDIO_PAGO_TIPO,
			 (SELECT id_usuario
				FROM INTER_DATA.Usuario U
				WHERE  U.Apellido = M.USUARIO_APELLIDO
				AND	   U.DNI = M.USUARIO_DNI
				AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
			) 
	FROM gd_esquema.Maestra M
	WHERE 
		MEDIO_PAGO_NRO_TARJETA			IS NOT NULL 
		AND MARCA_TARJETA				IS NOT NULL 
		AND MEDIO_PAGO_TIPO				IS NOT NULL 
	ORDER BY MEDIO_PAGO_NRO_TARJETA

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tipo_cupon]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tipo_cupon]
AS
INSERT INTO INTER_DATA.Tipo_Cupon(Tipo)
	SELECT 
		DISTINCT(CUPON_TIPO) 
	FROM gd_esquema.Maestra
	WHERE CUPON_TIPO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tipo_estado]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tipo_estado]
AS
INSERT INTO INTER_DATA.Tipo_Estado(Nombre)
	SELECT 
		DISTINCT PEDIDO_ESTADO
	FROM gd_esquema.Maestra M
	WHERE 
		PEDIDO_ESTADO	IS NOT NULL 

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tipo_medio_pago]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tipo_medio_pago]
AS
INSERT INTO INTER_DATA.Tipo_Medio_Pago(Nombre,Tarjeta_Usuario_Numero)
	SELECT 
		DISTINCT MEDIO_PAGO_TIPO,		
		(
			SELECT T.Numero
			FROM INTER_DATA.Tarjeta_Usuario T
			WHERE T.Numero = M.MEDIO_PAGO_NRO_TARJETA
			AND	  T.Marca_Tarjeta  = M.MARCA_TARJETA	
		)
	FROM gd_esquema.Maestra M
	WHERE MEDIO_PAGO_TIPO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tipo_movilidad]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tipo_movilidad]
AS
INSERT INTO INTER_DATA.Tipo_Movilidad(Tipo_Movilidad)
	SELECT 
		DISTINCT(REPARTIDOR_TIPO_MOVILIDAD) 
	FROM gd_esquema.Maestra
	WHERE REPARTIDOR_TIPO_MOVILIDAD IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tipo_paquete]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tipo_paquete]
AS
INSERT INTO INTER_DATA.Tipo_Paquete(Nombre, Alto_MAX, Ancho_MAX, Largo_MAX, Peso_MAX, Tipo_Precio)
	SELECT 
		DISTINCT PAQUETE_TIPO, PAQUETE_ALTO_MAX, PAQUETE_ANCHO_MAX, PAQUETE_LARGO_MAX, PAQUETE_PESO_MAX, PAQUETE_TIPO_PRECIO 
	FROM gd_esquema.Maestra
	WHERE PAQUETE_TIPO IS NOT NULL
	AND PAQUETE_ALTO_MAX IS NOT NULL
	AND PAQUETE_ANCHO_MAX IS NOT NULL
	AND PAQUETE_LARGO_MAX IS NOT NULL
	AND PAQUETE_PESO_MAX IS NOT NULL
	AND PAQUETE_TIPO_PRECIO IS NOT NULL
	ORDER BY PAQUETE_TIPO

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_tipo_reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_tipo_reclamo]
AS
INSERT INTO INTER_DATA.Tipo_Reclamo(Tipo)
	SELECT 
		DISTINCT(RECLAMO_TIPO) 
	FROM gd_esquema.Maestra
	WHERE RECLAMO_TIPO IS NOT NULL

GO
/****** Object:  StoredProcedure [INTER_DATA].[migrar_usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [INTER_DATA].[migrar_usuario]
AS
INSERT INTO INTER_DATA.Usuario( Nombre, Apellido, DNI, Telefono, Mail,Fecha_Nac, Fecha_Registro)
	SELECT 
		DISTINCT USUARIO_NOMBRE,
				 USUARIO_APELLIDO,
				 USUARIO_DNI,
				 USUARIO_TELEFONO,
				 USUARIO_MAIL,
				 USUARIO_FECHA_NAC,
				 USUARIO_FECHA_REGISTRO
	FROM gd_esquema.Maestra
	WHERE USUARIO_NOMBRE			IS NOT NULL 
		AND USUARIO_APELLIDO		IS NOT NULL 
		AND USUARIO_DNI				IS NOT NULL 
		AND USUARIO_TELEFONO		IS NOT NULL
		AND USUARIO_MAIL			IS NOT NULL
		AND USUARIO_FECHA_NAC		IS NOT NULL
		AND USUARIO_FECHA_REGISTRO	IS NOT NULL
	ORDER BY USUARIO_APELLIDO, USUARIO_DNI, USUARIO_FECHA_NAC

GO
/****** Object:  UserDefinedFunction [INTER_DATA].[get_calle]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [INTER_DATA].[get_calle](@Direccion NVARCHAR(255))
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN LEFT(@Direccion, LEN(@Direccion)-LEN(INTER_DATA.get_numero(@Direccion)))
END

GO
/****** Object:  UserDefinedFunction [INTER_DATA].[get_numero]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [INTER_DATA].[get_numero](@Direccion NVARCHAR(255))
RETURNS DECIMAL(18, 0)
AS
BEGIN
	RETURN SUBSTRING(RIGHT(@Direccion, 4), PATINDEX('%[0-9]%', RIGHT(@Direccion, 4)), LEN(RIGHT(@Direccion,�4)))
END

GO
/****** Object:  Table [gd_esquema].[Maestra]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gd_esquema].[Maestra](
	[USUARIO_NOMBRE] [nvarchar](255) NULL,
	[USUARIO_APELLIDO] [nvarchar](255) NULL,
	[USUARIO_DNI] [decimal](18, 0) NULL,
	[USUARIO_FECHA_REGISTRO] [datetime2](3) NULL,
	[USUARIO_TELEFONO] [decimal](18, 0) NULL,
	[USUARIO_MAIL] [nvarchar](255) NULL,
	[USUARIO_FECHA_NAC] [date] NULL,
	[ENVIO_MENSAJERIA_NRO] [decimal](18, 0) NULL,
	[ENVIO_MENSAJERIA_DIR_ORIG] [nvarchar](255) NULL,
	[ENVIO_MENSAJERIA_DIR_DEST] [nvarchar](255) NULL,
	[ENVIO_MENSAJERIA_LOCALIDAD] [nvarchar](255) NULL,
	[ENVIO_MENSAJERIA_PROVINCIA] [nvarchar](255) NULL,
	[ENVIO_MENSAJERIA_KM] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_VALOR_ASEGURADO] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_PRECIO_ENVIO] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_PRECIO_SEGURO] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_PROPINA] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_TOTAL] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_OBSERV] [nvarchar](255) NULL,
	[ENVIO_MENSAJERIA_FECHA] [datetime] NULL,
	[ENVIO_MENSAJERIA_FECHA_ENTREGA] [datetime] NULL,
	[ENVIO_MENSAJERIA_TIEMPO_ESTIMADO] [decimal](18, 2) NULL,
	[ENVIO_MENSAJERIA_CALIFICACION] [decimal](18, 0) NULL,
	[REPARTIDOR_NOMBRE] [nvarchar](255) NULL,
	[REPARTIDOR_APELLIDO] [nvarchar](255) NULL,
	[REPARTIDOR_DNI] [decimal](18, 0) NULL,
	[REPARTIDOR_TELEFONO] [decimal](18, 0) NULL,
	[REPARTIDOR_DIRECION] [nvarchar](255) NULL,
	[REPARTIDOR_EMAIL] [nvarchar](255) NULL,
	[REPARTIDOR_FECHA_NAC] [date] NULL,
	[REPARTIDOR_TIPO_MOVILIDAD] [nvarchar](50) NULL,
	[PAQUETE_TIPO] [nvarchar](50) NULL,
	[PAQUETE_ALTO_MAX] [decimal](18, 2) NULL,
	[PAQUETE_ANCHO_MAX] [decimal](18, 2) NULL,
	[PAQUETE_LARGO_MAX] [decimal](18, 2) NULL,
	[PAQUETE_PESO_MAX] [decimal](18, 2) NULL,
	[PAQUETE_TIPO_PRECIO] [decimal](18, 2) NULL,
	[MEDIO_PAGO_NRO_TARJETA] [nvarchar](50) NULL,
	[MEDIO_PAGO_TIPO] [nvarchar](50) NULL,
	[MARCA_TARJETA] [nvarchar](100) NULL,
	[ENVIO_MENSAJERIA_ESTADO] [nvarchar](50) NULL,
	[PEDIDO_NRO] [decimal](18, 0) NULL,
	[PEDIDO_TOTAL_PRODUCTOS] [decimal](18, 2) NULL,
	[PEDIDO_PRECIO_ENVIO] [decimal](18, 2) NULL,
	[PEDIDO_TARIFA_SERVICIO] [decimal](18, 2) NULL,
	[PEDIDO_PROPINA] [decimal](18, 2) NULL,
	[PEDIDO_TOTAL_CUPONES] [decimal](18, 2) NULL,
	[PEDIDO_TOTAL_SERVICIO] [decimal](18, 2) NULL,
	[PEDIDO_OBSERV] [nvarchar](255) NULL,
	[PEDIDO_FECHA] [datetime] NULL,
	[PEDIDO_FECHA_ENTREGA] [datetime] NULL,
	[PEDIDO_TIEMPO_ESTIMADO_ENTREGA] [decimal](18, 2) NULL,
	[PEDIDO_CALIFICACION] [decimal](18, 0) NULL,
	[DIRECCION_USUARIO_NOMBRE] [nvarchar](50) NULL,
	[DIRECCION_USUARIO_DIRECCION] [nvarchar](255) NULL,
	[DIRECCION_USUARIO_LOCALIDAD] [nvarchar](255) NULL,
	[DIRECCION_USUARIO_PROVINCIA] [nvarchar](255) NULL,
	[LOCAL_NOMBRE] [nvarchar](100) NULL,
	[LOCAL_DESCRIPCION] [nvarchar](255) NULL,
	[LOCAL_DIRECCION] [nvarchar](255) NULL,
	[LOCAL_TIPO] [nvarchar](50) NULL,
	[LOCAL_LOCALIDAD] [nvarchar](255) NULL,
	[LOCAL_PROVINCIA] [nvarchar](255) NULL,
	[HORARIO_LOCAL_HORA_APERTURA] [decimal](18, 0) NULL,
	[HORARIO_LOCAL_HORA_CIERRE] [decimal](18, 0) NULL,
	[HORARIO_LOCAL_DIA] [nvarchar](50) NULL,
	[PEDIDO_ESTADO] [nvarchar](50) NULL,
	[PRODUCTO_LOCAL_CODIGO] [nvarchar](50) NULL,
	[PRODUCTO_LOCAL_NOMBRE] [nvarchar](50) NULL,
	[PRODUCTO_LOCAL_DESCRIPCION] [nvarchar](255) NULL,
	[PRODUCTO_LOCAL_PRECIO] [decimal](18, 2) NULL,
	[PRODUCTO_CANTIDAD] [decimal](18, 0) NULL,
	[CUPON_NRO] [decimal](18, 0) NULL,
	[CUPON_MONTO] [decimal](18, 2) NULL,
	[CUPON_FECHA_ALTA] [datetime] NULL,
	[CUPON_FECHA_VENCIMIENTO] [datetime] NULL,
	[CUPON_TIPO] [nvarchar](50) NULL,
	[RECLAMO_NRO] [decimal](18, 0) NULL,
	[RECLAMO_FECHA] [datetime] NULL,
	[RECLAMO_DESCRIPCION] [nvarchar](255) NULL,
	[RECLAMO_FECHA_SOLUCION] [datetime] NULL,
	[RECLAMO_SOLUCION] [nvarchar](255) NULL,
	[RECLAMO_CALIFICACION] [decimal](18, 0) NULL,
	[RECLAMO_TIPO] [nvarchar](50) NULL,
	[RECLAMO_ESTADO] [nvarchar](50) NULL,
	[OPERADOR_RECLAMO_NOMBRE] [nvarchar](255) NULL,
	[OPERADOR_RECLAMO_APELLIDO] [nvarchar](255) NULL,
	[OPERADOR_RECLAMO_DNI] [decimal](18, 0) NULL,
	[OPERADOR_RECLAMO_TELEFONO] [decimal](18, 0) NULL,
	[OPERADOR_RECLAMO_DIRECCION] [nvarchar](255) NULL,
	[OPERADOR_RECLAMO_MAIL] [nvarchar](255) NULL,
	[OPERADOR_RECLAMO_FECHA_NAC] [date] NULL,
	[CUPON_RECLAMO_NRO] [decimal](18, 0) NULL,
	[CUPON_RECLAMO_MONTO] [decimal](18, 2) NULL,
	[CUPON_RECLAMO_FECHA_ALTA] [datetime] NULL,
	[CUPON_RECLAMO_FECHA_VENCIMIENTO] [datetime] NULL,
	[CUPON_RECLAMO_TIPO] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Cupon]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Cupon](
	[Numero] [decimal](18, 0) NOT NULL,
	[Monto] [decimal](18, 2) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Fecha_Vencimiento] [datetime] NOT NULL,
	[Usuario_id_usuario] [int] NOT NULL,
	[Local_id_local] [int] NOT NULL,
	[Tipo_Cupon_id_Tipo_Cupon] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Cupon_Reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Cupon_Reclamo](
	[Numero] [decimal](18, 0) NOT NULL,
	[Monto] [decimal](18, 2) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Fecha_Vencimiento] [datetime] NOT NULL,
	[Tipo_Cupon_Reclamo_id_Tipo_Cupon_Reclamo] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Cupon_X_Pedido]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Cupon_X_Pedido](
	[id_Cupon_X_Pedido] [int] IDENTITY(1,1) NOT NULL,
	[Cupon_Numero] [decimal](18, 0) NOT NULL,
	[Pedido_Numero] [decimal](18, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Cupon_X_Pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Direccion]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Direccion](
	[id_direccion] [int] IDENTITY(1,1) NOT NULL,
	[Calle] [nvarchar](50) NOT NULL,
	[Numero] [decimal](18, 0) NOT NULL,
	[Localidad_id_Localidad] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_direccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Direccion_Usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Direccion_Usuario](
	[id_direccion_usuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Usuario_id_usuario] [int] NOT NULL,
	[Direccion_id_direccion] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_direccion_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Envio]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Envio](
	[id_envio] [int] IDENTITY(1,1) NOT NULL,
	[Precio_Envio] [decimal](18, 2) NOT NULL,
	[Propina] [decimal](18, 2) NOT NULL,
	[Tiempo_Estimado] [decimal](18, 2) NOT NULL,
	[Fecha_Entrega] [datetime] NOT NULL,
	[Direccion_id_direccion_destino] [int] NOT NULL,
	[Repartidor_id_repartidor] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_envio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Estado_Reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Estado_Reclamo](
	[id_Estado_Reclamo] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Estado_Reclamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Horario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Horario](
	[id_horario] [int] IDENTITY(1,1) NOT NULL,
	[Hora_Apertura] [decimal](18, 0) NOT NULL,
	[Hora_CIerre] [decimal](18, 0) NOT NULL,
	[Dia] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_horario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Horario_X_Local]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Horario_X_Local](
	[id_Horario_X_Local] [int] IDENTITY(1,1) NOT NULL,
	[Local_id_local] [int] NOT NULL,
	[Horario_id_horario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Horario_X_Local] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Item_Pedido]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Item_Pedido](
	[id_Item_Pedido] [int] IDENTITY(1,1) NOT NULL,
	[Pedido_Numero] [decimal](18, 0) NOT NULL,
	[Producto_Codigo] [nvarchar](50) NOT NULL,
	[Cantidad] [decimal](18, 0) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Item_Pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Local]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Local](
	[id_local] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
	[Descripcion] [nvarchar](255) NOT NULL,
	[Local_Tipo_id_Local_Tipo] [int] NOT NULL,
	[Local_Tipo_id_Local_Categoria] [int] NOT NULL,
	[Direccion_id_direccion] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_local] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Local_Categoria]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Local_Categoria](
	[id_Local_Categoria] [int] IDENTITY(1,1) NOT NULL,
	[Categoria] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Local_Categoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Local_Tipo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Local_Tipo](
	[id_Local_Tipo] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Local_Tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Localidad]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Localidad](
	[id_Localidad] [int] IDENTITY(1,1) NOT NULL,
	[Localidad] [nvarchar](255) NOT NULL,
	[Provincia_id_Provincia] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Localidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Mensajeria]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Mensajeria](
	[Numero] [decimal](18, 0) NOT NULL,
	[KM] [decimal](18, 2) NOT NULL,
	[Observaciones] [nvarchar](255) NOT NULL,
	[Precio_Seguro] [decimal](18, 2) NOT NULL,
	[Total] [decimal](18, 2) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Calificacion] [decimal](18, 0) NOT NULL,
	[Paquete_id_Paquete] [int] NOT NULL,
	[Tipo_Medio_Pago_id_tipo_medio_pago] [int] NOT NULL,
	[Tipo_Estado_id_Tipo_Estado] [int] NOT NULL,
	[Usuario_id_usuario] [int] NOT NULL,
	[Envio_id_envio] [int] NOT NULL,
	[Direccion_id_direccion_origen] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Operador]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Operador](
	[id_operador] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
	[Apellido] [nvarchar](255) NOT NULL,
	[DNI] [decimal](18, 0) NOT NULL,
	[Telefono] [decimal](18, 0) NOT NULL,
	[Mail] [nvarchar](255) NOT NULL,
	[Fecha_Nac] [date] NOT NULL,
	[Direccion_id_direccion] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_operador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Paquete]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Paquete](
	[id_Paquete] [int] IDENTITY(1,1) NOT NULL,
	[Valor_Asegurado] [decimal](18, 2) NOT NULL,
	[Tipo_Paquete_id_tipo_paquete] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Paquete] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Pedido]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Pedido](
	[Numero] [decimal](18, 0) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Tarifa_Servicio] [decimal](18, 2) NOT NULL,
	[Total_Productos] [decimal](18, 2) NOT NULL,
	[Observaciones] [nvarchar](255) NOT NULL,
	[Calificacion] [decimal](18, 0) NOT NULL,
	[Total_Cupones] [decimal](18, 2) NOT NULL,
	[Total_Servicio] [decimal](18, 2) NOT NULL,
	[Usuario_id_usuario] [int] NOT NULL,
	[Local_id_local] [int] NOT NULL,
	[Envio_id_envio] [int] NOT NULL,
	[Tipo_Estado_id_Tipo_Estado] [int] NOT NULL,
	[Tipo_Medio_Pago_id_tipo_medio_pago] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Producto]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Producto](
	[Codigo] [nvarchar](50) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](255) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Local_id_local] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Provincia]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Provincia](
	[id_Provincia] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Provincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Reclamo](
	[Numero] [decimal](18, 0) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Descripcion] [nvarchar](255) NOT NULL,
	[Fecha_Solucion] [datetime] NOT NULL,
	[Solucion] [nvarchar](255) NOT NULL,
	[Calificacion] [decimal](18, 0) NOT NULL,
	[Pedido_Numero] [decimal](18, 0) NOT NULL,
	[Estado_Reclamo_id_Estado_Reclamo] [int] NOT NULL,
	[Tipo_Reclamo_id_Tipo_Reclamo] [int] NOT NULL,
	[Operador_id_operador] [int] NOT NULL,
	[Cupon_Reclamo_Numero] [decimal](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Repartidor]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Repartidor](
	[id_repartidor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
	[Apellido] [nvarchar](255) NOT NULL,
	[DNI] [decimal](18, 0) NOT NULL,
	[Telefono] [decimal](18, 0) NOT NULL,
	[Mail] [nvarchar](255) NOT NULL,
	[Fecha_Nac] [date] NOT NULL,
	[Direccion_id_direccion] [int] NOT NULL,
	[Tipo_Movilidad_id_Tipo_Movilidad] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_repartidor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tarjeta_Usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tarjeta_Usuario](
	[Numero] [nvarchar](50) NOT NULL,
	[Marca_Tarjeta] [nvarchar](100) NOT NULL,
	[Tipo] [nvarchar](50) NOT NULL,
	[Usuario_id_usuario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Cupon]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Cupon](
	[id_Tipo_Cupon] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Tipo_Cupon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Cupon_Reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Cupon_Reclamo](
	[id_Tipo_Cupon_Reclamo] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Tipo_Cupon_Reclamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Estado]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Estado](
	[id_Tipo_Estado] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Tipo_Estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Medio_Pago]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Medio_Pago](
	[id_tipo_medio_pago] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Tarjeta_Usuario_Numero] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tipo_medio_pago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Movilidad]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Movilidad](
	[id_Tipo_Movilidad] [int] IDENTITY(1,1) NOT NULL,
	[Tipo_Movilidad] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Tipo_Movilidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Paquete]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Paquete](
	[id_tipo_paquete] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Alto_MAX] [decimal](18, 2) NOT NULL,
	[Ancho_MAX] [decimal](18, 2) NOT NULL,
	[Largo_MAX] [decimal](18, 2) NOT NULL,
	[Peso_MAX] [decimal](18, 2) NOT NULL,
	[Tipo_Precio] [decimal](18, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tipo_paquete] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Tipo_Reclamo]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Tipo_Reclamo](
	[id_Tipo_Reclamo] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Tipo_Reclamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [INTER_DATA].[Usuario]    Script Date: 9/8/2024 21:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTER_DATA].[Usuario](
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
	[Apellido] [nvarchar](255) NOT NULL,
	[DNI] [decimal](18, 0) NOT NULL,
	[Telefono] [decimal](18, 0) NOT NULL,
	[Mail] [nvarchar](255) NOT NULL,
	[Fecha_Nac] [date] NOT NULL,
	[Fecha_Registro] [datetime2](3) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [CUPON]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [CUPON] ON [INTER_DATA].[Cupon]
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DIRECCIONES]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [DIRECCIONES] ON [INTER_DATA].[Direccion]
(
	[Calle] ASC,
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LOCALES]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [LOCALES] ON [INTER_DATA].[Local]
(
	[Nombre] ASC,
	[Local_Tipo_id_Local_Tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LOCALIDAD]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [LOCALIDAD] ON [INTER_DATA].[Localidad]
(
	[Localidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OPERADOR]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [OPERADOR] ON [INTER_DATA].[Operador]
(
	[Apellido] ASC,
	[DNI] ASC,
	[Fecha_Nac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PAQUETE]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [PAQUETE] ON [INTER_DATA].[Paquete]
(
	[Valor_Asegurado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PEDIDO]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [PEDIDO] ON [INTER_DATA].[Pedido]
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PROVINCIA]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [PROVINCIA] ON [INTER_DATA].[Provincia]
(
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [REPARTIDOR]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [REPARTIDOR] ON [INTER_DATA].[Repartidor]
(
	[Apellido] ASC,
	[DNI] ASC,
	[Fecha_Nac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TARJETA_USUARIO]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [TARJETA_USUARIO] ON [INTER_DATA].[Tarjeta_Usuario]
(
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TIPO_MEDIO_PAGO]    Script Date: 9/8/2024 21:04:24 ******/
CREATE NONCLUSTERED INDEX [TIPO_MEDIO_PAGO] ON [INTER_DATA].[Tipo_Medio_Pago]
(
	[Tarjeta_Usuario_Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [INTER_DATA].[Cupon]  WITH CHECK ADD  CONSTRAINT [fk_Cupon_Local] FOREIGN KEY([Local_id_local])
REFERENCES [INTER_DATA].[Local] ([id_local])
GO
ALTER TABLE [INTER_DATA].[Cupon] CHECK CONSTRAINT [fk_Cupon_Local]
GO
ALTER TABLE [INTER_DATA].[Cupon]  WITH CHECK ADD  CONSTRAINT [fk_Cupon_Tipo_Cupon] FOREIGN KEY([Tipo_Cupon_id_Tipo_Cupon])
REFERENCES [INTER_DATA].[Tipo_Cupon] ([id_Tipo_Cupon])
GO
ALTER TABLE [INTER_DATA].[Cupon] CHECK CONSTRAINT [fk_Cupon_Tipo_Cupon]
GO
ALTER TABLE [INTER_DATA].[Cupon]  WITH CHECK ADD  CONSTRAINT [fk_Cupon_Usuario] FOREIGN KEY([Usuario_id_usuario])
REFERENCES [INTER_DATA].[Usuario] ([id_usuario])
GO
ALTER TABLE [INTER_DATA].[Cupon] CHECK CONSTRAINT [fk_Cupon_Usuario]
GO
ALTER TABLE [INTER_DATA].[Cupon_Reclamo]  WITH CHECK ADD  CONSTRAINT [fk_Cupon_Reclamo_Tipo_Cupon_Reclamo] FOREIGN KEY([Tipo_Cupon_Reclamo_id_Tipo_Cupon_Reclamo])
REFERENCES [INTER_DATA].[Tipo_Cupon_Reclamo] ([id_Tipo_Cupon_Reclamo])
GO
ALTER TABLE [INTER_DATA].[Cupon_Reclamo] CHECK CONSTRAINT [fk_Cupon_Reclamo_Tipo_Cupon_Reclamo]
GO
ALTER TABLE [INTER_DATA].[Cupon_X_Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Cupon_X_Pedido_Cupon] FOREIGN KEY([Cupon_Numero])
REFERENCES [INTER_DATA].[Cupon] ([Numero])
GO
ALTER TABLE [INTER_DATA].[Cupon_X_Pedido] CHECK CONSTRAINT [fk_Cupon_X_Pedido_Cupon]
GO
ALTER TABLE [INTER_DATA].[Cupon_X_Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Cupon_X_Pedido_Pedido] FOREIGN KEY([Pedido_Numero])
REFERENCES [INTER_DATA].[Pedido] ([Numero])
GO
ALTER TABLE [INTER_DATA].[Cupon_X_Pedido] CHECK CONSTRAINT [fk_Cupon_X_Pedido_Pedido]
GO
ALTER TABLE [INTER_DATA].[Direccion]  WITH CHECK ADD  CONSTRAINT [fk_Direccion_Localidad] FOREIGN KEY([Localidad_id_Localidad])
REFERENCES [INTER_DATA].[Localidad] ([id_Localidad])
GO
ALTER TABLE [INTER_DATA].[Direccion] CHECK CONSTRAINT [fk_Direccion_Localidad]
GO
ALTER TABLE [INTER_DATA].[Direccion_Usuario]  WITH CHECK ADD  CONSTRAINT [fk_Direccion_Usuario_Direccion] FOREIGN KEY([Direccion_id_direccion])
REFERENCES [INTER_DATA].[Direccion] ([id_direccion])
GO
ALTER TABLE [INTER_DATA].[Direccion_Usuario] CHECK CONSTRAINT [fk_Direccion_Usuario_Direccion]
GO
ALTER TABLE [INTER_DATA].[Direccion_Usuario]  WITH CHECK ADD  CONSTRAINT [fk_Direccion_Usuario_Usuario] FOREIGN KEY([Usuario_id_usuario])
REFERENCES [INTER_DATA].[Usuario] ([id_usuario])
GO
ALTER TABLE [INTER_DATA].[Direccion_Usuario] CHECK CONSTRAINT [fk_Direccion_Usuario_Usuario]
GO
ALTER TABLE [INTER_DATA].[Envio]  WITH CHECK ADD  CONSTRAINT [fk_Envio_Direccion_Destino] FOREIGN KEY([Direccion_id_direccion_destino])
REFERENCES [INTER_DATA].[Direccion] ([id_direccion])
GO
ALTER TABLE [INTER_DATA].[Envio] CHECK CONSTRAINT [fk_Envio_Direccion_Destino]
GO
ALTER TABLE [INTER_DATA].[Envio]  WITH CHECK ADD  CONSTRAINT [fk_Envio_Repartidor] FOREIGN KEY([Repartidor_id_repartidor])
REFERENCES [INTER_DATA].[Repartidor] ([id_repartidor])
GO
ALTER TABLE [INTER_DATA].[Envio] CHECK CONSTRAINT [fk_Envio_Repartidor]
GO
ALTER TABLE [INTER_DATA].[Horario_X_Local]  WITH CHECK ADD  CONSTRAINT [fk_Horario_X_Local_Horario] FOREIGN KEY([Horario_id_horario])
REFERENCES [INTER_DATA].[Horario] ([id_horario])
GO
ALTER TABLE [INTER_DATA].[Horario_X_Local] CHECK CONSTRAINT [fk_Horario_X_Local_Horario]
GO
ALTER TABLE [INTER_DATA].[Horario_X_Local]  WITH CHECK ADD  CONSTRAINT [fk_Horario_X_Local_Local] FOREIGN KEY([Local_id_local])
REFERENCES [INTER_DATA].[Local] ([id_local])
GO
ALTER TABLE [INTER_DATA].[Horario_X_Local] CHECK CONSTRAINT [fk_Horario_X_Local_Local]
GO
ALTER TABLE [INTER_DATA].[Item_Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Item_Pedido_Pedido] FOREIGN KEY([Pedido_Numero])
REFERENCES [INTER_DATA].[Pedido] ([Numero])
GO
ALTER TABLE [INTER_DATA].[Item_Pedido] CHECK CONSTRAINT [fk_Item_Pedido_Pedido]
GO
ALTER TABLE [INTER_DATA].[Item_Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Item_Pedido_Producto] FOREIGN KEY([Producto_Codigo])
REFERENCES [INTER_DATA].[Producto] ([Codigo])
GO
ALTER TABLE [INTER_DATA].[Item_Pedido] CHECK CONSTRAINT [fk_Item_Pedido_Producto]
GO
ALTER TABLE [INTER_DATA].[Local]  WITH CHECK ADD  CONSTRAINT [fk_Local_Direccion] FOREIGN KEY([Direccion_id_direccion])
REFERENCES [INTER_DATA].[Direccion] ([id_direccion])
GO
ALTER TABLE [INTER_DATA].[Local] CHECK CONSTRAINT [fk_Local_Direccion]
GO
ALTER TABLE [INTER_DATA].[Local]  WITH CHECK ADD  CONSTRAINT [fk_Local_Local_Categoria] FOREIGN KEY([Local_Tipo_id_Local_Categoria])
REFERENCES [INTER_DATA].[Local_Categoria] ([id_Local_Categoria])
GO
ALTER TABLE [INTER_DATA].[Local] CHECK CONSTRAINT [fk_Local_Local_Categoria]
GO
ALTER TABLE [INTER_DATA].[Local]  WITH CHECK ADD  CONSTRAINT [fk_Local_Local_Tipo] FOREIGN KEY([Local_Tipo_id_Local_Tipo])
REFERENCES [INTER_DATA].[Local_Tipo] ([id_Local_Tipo])
GO
ALTER TABLE [INTER_DATA].[Local] CHECK CONSTRAINT [fk_Local_Local_Tipo]
GO
ALTER TABLE [INTER_DATA].[Localidad]  WITH CHECK ADD  CONSTRAINT [fk_Localidad_Provincia] FOREIGN KEY([Provincia_id_Provincia])
REFERENCES [INTER_DATA].[Provincia] ([id_Provincia])
GO
ALTER TABLE [INTER_DATA].[Localidad] CHECK CONSTRAINT [fk_Localidad_Provincia]
GO
ALTER TABLE [INTER_DATA].[Mensajeria]  WITH CHECK ADD  CONSTRAINT [fk_Mensajeria_Direccion] FOREIGN KEY([Direccion_id_direccion_origen])
REFERENCES [INTER_DATA].[Direccion] ([id_direccion])
GO
ALTER TABLE [INTER_DATA].[Mensajeria] CHECK CONSTRAINT [fk_Mensajeria_Direccion]
GO
ALTER TABLE [INTER_DATA].[Mensajeria]  WITH CHECK ADD  CONSTRAINT [fk_Mensajeria_Envio] FOREIGN KEY([Envio_id_envio])
REFERENCES [INTER_DATA].[Envio] ([id_envio])
GO
ALTER TABLE [INTER_DATA].[Mensajeria] CHECK CONSTRAINT [fk_Mensajeria_Envio]
GO
ALTER TABLE [INTER_DATA].[Mensajeria]  WITH CHECK ADD  CONSTRAINT [fk_Mensajeria_Paquete] FOREIGN KEY([Paquete_id_Paquete])
REFERENCES [INTER_DATA].[Paquete] ([id_Paquete])
GO
ALTER TABLE [INTER_DATA].[Mensajeria] CHECK CONSTRAINT [fk_Mensajeria_Paquete]
GO
ALTER TABLE [INTER_DATA].[Mensajeria]  WITH CHECK ADD  CONSTRAINT [fk_Mensajeria_Tipo_Estado] FOREIGN KEY([Tipo_Estado_id_Tipo_Estado])
REFERENCES [INTER_DATA].[Tipo_Estado] ([id_Tipo_Estado])
GO
ALTER TABLE [INTER_DATA].[Mensajeria] CHECK CONSTRAINT [fk_Mensajeria_Tipo_Estado]
GO
ALTER TABLE [INTER_DATA].[Mensajeria]  WITH CHECK ADD  CONSTRAINT [fk_Mensajeria_Tipo_Medio_Pago] FOREIGN KEY([Tipo_Medio_Pago_id_tipo_medio_pago])
REFERENCES [INTER_DATA].[Tipo_Medio_Pago] ([id_tipo_medio_pago])
GO
ALTER TABLE [INTER_DATA].[Mensajeria] CHECK CONSTRAINT [fk_Mensajeria_Tipo_Medio_Pago]
GO
ALTER TABLE [INTER_DATA].[Mensajeria]  WITH CHECK ADD  CONSTRAINT [fk_Mensajeria_Usuario] FOREIGN KEY([Usuario_id_usuario])
REFERENCES [INTER_DATA].[Usuario] ([id_usuario])
GO
ALTER TABLE [INTER_DATA].[Mensajeria] CHECK CONSTRAINT [fk_Mensajeria_Usuario]
GO
ALTER TABLE [INTER_DATA].[Operador]  WITH CHECK ADD  CONSTRAINT [fk_Operador_Direccion] FOREIGN KEY([Direccion_id_direccion])
REFERENCES [INTER_DATA].[Direccion] ([id_direccion])
GO
ALTER TABLE [INTER_DATA].[Operador] CHECK CONSTRAINT [fk_Operador_Direccion]
GO
ALTER TABLE [INTER_DATA].[Paquete]  WITH CHECK ADD  CONSTRAINT [fk_Paquete_id_Tipo_Paquete] FOREIGN KEY([Tipo_Paquete_id_tipo_paquete])
REFERENCES [INTER_DATA].[Tipo_Paquete] ([id_tipo_paquete])
GO
ALTER TABLE [INTER_DATA].[Paquete] CHECK CONSTRAINT [fk_Paquete_id_Tipo_Paquete]
GO
ALTER TABLE [INTER_DATA].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Pedido_Envio] FOREIGN KEY([Envio_id_envio])
REFERENCES [INTER_DATA].[Envio] ([id_envio])
GO
ALTER TABLE [INTER_DATA].[Pedido] CHECK CONSTRAINT [fk_Pedido_Envio]
GO
ALTER TABLE [INTER_DATA].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Pedido_Local] FOREIGN KEY([Local_id_local])
REFERENCES [INTER_DATA].[Local] ([id_local])
GO
ALTER TABLE [INTER_DATA].[Pedido] CHECK CONSTRAINT [fk_Pedido_Local]
GO
ALTER TABLE [INTER_DATA].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Pedido_Tipo_Estado] FOREIGN KEY([Tipo_Estado_id_Tipo_Estado])
REFERENCES [INTER_DATA].[Tipo_Estado] ([id_Tipo_Estado])
GO
ALTER TABLE [INTER_DATA].[Pedido] CHECK CONSTRAINT [fk_Pedido_Tipo_Estado]
GO
ALTER TABLE [INTER_DATA].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Pedido_Tipo_Medio_Pago] FOREIGN KEY([Tipo_Medio_Pago_id_tipo_medio_pago])
REFERENCES [INTER_DATA].[Tipo_Medio_Pago] ([id_tipo_medio_pago])
GO
ALTER TABLE [INTER_DATA].[Pedido] CHECK CONSTRAINT [fk_Pedido_Tipo_Medio_Pago]
GO
ALTER TABLE [INTER_DATA].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_Pedido_Usuario] FOREIGN KEY([Usuario_id_usuario])
REFERENCES [INTER_DATA].[Usuario] ([id_usuario])
GO
ALTER TABLE [INTER_DATA].[Pedido] CHECK CONSTRAINT [fk_Pedido_Usuario]
GO
ALTER TABLE [INTER_DATA].[Producto]  WITH CHECK ADD  CONSTRAINT [fk_Producto_Local] FOREIGN KEY([Local_id_local])
REFERENCES [INTER_DATA].[Local] ([id_local])
GO
ALTER TABLE [INTER_DATA].[Producto] CHECK CONSTRAINT [fk_Producto_Local]
GO
ALTER TABLE [INTER_DATA].[Reclamo]  WITH CHECK ADD  CONSTRAINT [fk_Reclamo_Cupon_Reclamo] FOREIGN KEY([Cupon_Reclamo_Numero])
REFERENCES [INTER_DATA].[Cupon_Reclamo] ([Numero])
GO
ALTER TABLE [INTER_DATA].[Reclamo] CHECK CONSTRAINT [fk_Reclamo_Cupon_Reclamo]
GO
ALTER TABLE [INTER_DATA].[Reclamo]  WITH CHECK ADD  CONSTRAINT [fk_Reclamo_Estado_Reclamo] FOREIGN KEY([Estado_Reclamo_id_Estado_Reclamo])
REFERENCES [INTER_DATA].[Estado_Reclamo] ([id_Estado_Reclamo])
GO
ALTER TABLE [INTER_DATA].[Reclamo] CHECK CONSTRAINT [fk_Reclamo_Estado_Reclamo]
GO
ALTER TABLE [INTER_DATA].[Reclamo]  WITH CHECK ADD  CONSTRAINT [fk_Reclamo_Operador] FOREIGN KEY([Operador_id_operador])
REFERENCES [INTER_DATA].[Operador] ([id_operador])
GO
ALTER TABLE [INTER_DATA].[Reclamo] CHECK CONSTRAINT [fk_Reclamo_Operador]
GO
ALTER TABLE [INTER_DATA].[Reclamo]  WITH CHECK ADD  CONSTRAINT [fk_Reclamo_Pedido] FOREIGN KEY([Pedido_Numero])
REFERENCES [INTER_DATA].[Pedido] ([Numero])
GO
ALTER TABLE [INTER_DATA].[Reclamo] CHECK CONSTRAINT [fk_Reclamo_Pedido]
GO
ALTER TABLE [INTER_DATA].[Reclamo]  WITH CHECK ADD  CONSTRAINT [fk_Reclamo_Tipo_Reclamo] FOREIGN KEY([Tipo_Reclamo_id_Tipo_Reclamo])
REFERENCES [INTER_DATA].[Tipo_Reclamo] ([id_Tipo_Reclamo])
GO
ALTER TABLE [INTER_DATA].[Reclamo] CHECK CONSTRAINT [fk_Reclamo_Tipo_Reclamo]
GO
ALTER TABLE [INTER_DATA].[Repartidor]  WITH CHECK ADD  CONSTRAINT [fk_Repartidor_Direccion] FOREIGN KEY([Direccion_id_direccion])
REFERENCES [INTER_DATA].[Direccion] ([id_direccion])
GO
ALTER TABLE [INTER_DATA].[Repartidor] CHECK CONSTRAINT [fk_Repartidor_Direccion]
GO
ALTER TABLE [INTER_DATA].[Repartidor]  WITH CHECK ADD  CONSTRAINT [fk_Repartidor_Tipo_Movilidad] FOREIGN KEY([Tipo_Movilidad_id_Tipo_Movilidad])
REFERENCES [INTER_DATA].[Tipo_Movilidad] ([id_Tipo_Movilidad])
GO
ALTER TABLE [INTER_DATA].[Repartidor] CHECK CONSTRAINT [fk_Repartidor_Tipo_Movilidad]
GO
ALTER TABLE [INTER_DATA].[Tarjeta_Usuario]  WITH CHECK ADD  CONSTRAINT [fk_Tarjeta_Usuario_Usuario] FOREIGN KEY([Usuario_id_usuario])
REFERENCES [INTER_DATA].[Usuario] ([id_usuario])
GO
ALTER TABLE [INTER_DATA].[Tarjeta_Usuario] CHECK CONSTRAINT [fk_Tarjeta_Usuario_Usuario]
GO
ALTER TABLE [INTER_DATA].[Tipo_Medio_Pago]  WITH CHECK ADD  CONSTRAINT [fk_Tipo_Medio_Pago_Tarjeta_Usuario] FOREIGN KEY([Tarjeta_Usuario_Numero])
REFERENCES [INTER_DATA].[Tarjeta_Usuario] ([Numero])
GO
ALTER TABLE [INTER_DATA].[Tipo_Medio_Pago] CHECK CONSTRAINT [fk_Tipo_Medio_Pago_Tarjeta_Usuario]
GO
USE [master]
GO
ALTER DATABASE [GD1C2023] SET  READ_WRITE 
GO
