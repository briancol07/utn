# SQL Bolt 

## Lesson 1 

```
SELECT title from movies 
SELECT director from movies
SELECT title,director from movies 
SELECT title,year from movies
SELECT * from movies 
```


## Lesson 2 

```
SELECT * FROM movies where id =6;
SELECT * FROM movies where year between 2000 and 2010
SELECT * FROM movies where year not between 2000 and 2010
SELECT title,year FROM movies where id <=5

```

## Lesson 3


```
SELECT * FROM movies where title like 'Toy Story%'
SELECT * FROM movies where director is 'John Lasseter'
SELECT * FROM movies where director is not 'John Lasseter'
SELECT * FROM movies where title like 'Wall-%'

```

## Lesson 4 


```
SELECT distinct(director) FROM movies order by director asc ;
SELECT * FROM movies order by year desc  limit 4  OFFSET 0;
SELECT * FROM movies order by Title asc limit 5 ;
SELECT * FROM movies order by Title asc limit 5 offset 5;
```
## Review 

```
SELECT * FROM north_american_cities where Country = "Canada" ;
SELECT * FROM north_american_cities where Country = "United States" order by latitude desc ;
SELECT city, longitude FROM north_american_cities WHERE longitude < -87.629798 ORDER BY longitude ASC;
SELECT * FROM north_american_cities where country = "Mexico" limit 2 offset 1;
SELECT city, population FROM north_american_cities WHERE country LIKE "United States" ORDER BY population DESC LIMIT 2 OFFSET 2;
```

## Lesson 6 

```
SELECT * FROM movies INNER JOIN Boxoffice on movie_id = id;
SELECT * FROM movies INNER JOIN Boxoffice on movie_id = id where international_sales > domestic_sales;;
SELECT * FROM movies INNER JOIN Boxoffice on movie_id = id order by ratingk desc;
```
## Lesson 7 
