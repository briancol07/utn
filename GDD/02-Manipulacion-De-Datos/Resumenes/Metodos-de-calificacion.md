# Metodos de calificacion 

## Objetivo 

Dado un conjunto de valores desordenados del tipo {a1,a2,..,an} devolver un conjunto ordenado de menor a mayor 

## Registros
 
Cada registro contiene una clave (key) que es el valor a ser ordenado y el resto del registro contiene los datos 

Para numeros solo no es tan util sino mas para letras 

## Estabilidad 

Si mantiene el orden relativo que tenian originalmente los elementos con claves iguales 

![Estabilidad](../img/estabilidad.png)

## In situ 

Los metodos in situ son los que transforman una estructura de datos usando una cantidad extra de memoira siendo esta pequenia y constante . Sobreescribiendola por la salida a medida que se ejecuta el algoritmo

Utilizar la misma estructura disminuye los tiempos de ejecucion.

## Interna y externa

* Metodo Interno: si el archivo a ordenar cabe en memoria.
* Metodo Externo: Si ordenamos archivos desde un disco u otro dispostivo que no es memoria.

## Complejidad

* La computacion que estudia la complejidad de ejecutar un algorimo
* La clase de complejidad "P" es el conjunto de los problemas de decision que pueden ser resueltos en una maquina determinista en tiempo a lo sumo polinomico
* La clase de complejidad "NP" es el conjunto de los problemas de decision que pueden ser resueltos por una maquina no determinista en tiempo mayor que polinomico
* Se describe como **O** ( funcion ) al orden de complejidad donde funcion es la funcion matematica que acota el comportamiento del algoritmo ne funcion del tiempo y cantidad de elementos 

### Complejidad 

* La computadora solo puede realizar operaciones matematicas y comparaciones por su caracteristica booleana.
* Para evaluar la complejidad de un algoritmo determinado se evaluan principalmnete la cantidad de comparaciones realizadas

## Bubble Sort

![Bubble-sort](../img/Bubble-sort.png)

* Consiste en hacer pasadas sobre los datos, donde cada paso, los elementos adyacentes son comparados e intercambiados si es necesario.
* Peor caso $`O(n^2)`$ Mejor caso $`O(n)`$
* Lento 

## Selection sort 

![Selection-sort](../img/Selection-sort.png)


* Busca el elemento mas chico del array y se lo intercambia hasta la primer posicion , luego el segundo y asi sucesivamente* Para Archivos con registros grandes y claves pequenias 
* No tiene corte , su orden para el peor caso $`O(n^2)`$

## Insertion sort 

![Insertion-sort](../img/Insertion-sort.png)

* Se basa en ordenamiento parcial, en la cual hay un marcador que apunta a una posicion donde a su izquierda se considera que estan los elementos parcialmente ordenados.
* Comienza ocn un elemento para insertarlo en su lugar en el grupo parcialmente ordenado.
* Tiempo de ejecucion es $`O(n^2)`$

## Shell sort 

![Shell-sort](../img/Shell-sort.png)

* Tiempo de ejecucion es $`O(n^{\frac 3 2})`$ y $`O(n^{\frac 7 6})`$
* Compara los elementos mas lejanos y luego va comparando elementos mas cercanos para fianlmente realizar un insertion sort 

## Merge sort 

![merge1](../img/merge1.png)
![merge2](../img/merge2.png)

* Utiliza divide y venceras 
* Tiempo de ejecucion es $`O(n \log n)`$
* Accede de forma secuencial a los elementos 
* Pasos 
  * Dividir la secuencia en dos subsecuencias mas pequenias 
  * Ordenar recursivamente las dos subsecuencias 
  * Fusionar las subsecuencias ordenadas para obtener el resultado final 

## Quick sort

![quic1](../img/quick1.png)
![quic2](../img/quick2.png)

* Tiempo de ejecucion es $`O(n \log n)`$ y $`O(n^2)`$ como en el peor de los casos 
* Tambien usa divide y vencearas pero este en dos subproblemas luego juntarlos y obtener el resultado final 
* Pasos 
  * Elegir un elemento como pivote
  * Comparar todos los elementos con el pivote gerendo dos subconjuntos , izq menores y derecha mayores 
 
![quick3](../img/quick3.png)

### Bsort - Meansort

> Ambos son variantes del quicksort

* Bsort : Solo cambia la eleccion del pivote, en este caso es el elemento central 
* Meansort: Solo cambia la eleccion del pivote que es el mas proximo a la media 

## Heap sort 

* Se basa en una estructura de datos llamada heap.
* Implementar una cola de prioridad. 
  * Heap -> arbol binario completo 
  * Cada nodo debe ser mayor o igual a las claves de sus hijos.
  * La clave mas grande esta en la raiz 
* Tiempo de ejecucion es $`O(n \log n)`$ ( nunca lo supera ) 
* No requiere espacio de memoria adicional ( in-situ ) 
* Dos fases 
  1. Con los elementos a ordenar construye el heap 
  2. Se desarma dicho heap y de esa forma obtendremos lso valores ordenados 

## Metodos calificados 

![calificacion](../img/calificacion.png)
