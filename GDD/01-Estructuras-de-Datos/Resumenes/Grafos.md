# Grafos 

> Conjunto de vertices y arcos que los relacionan, concepto matematico 

$`G = (V,A)`$ 

* V son los vertices 
* A son las aristas 

## Componentes 

* Nodos : Son denominados vertices, dado que en un modelo computacional de grafos pueden incluir muchos valores 
* Relaciones : Son las aristas o arcos dado que lo que hacen es relacionar nodos 
* Grado : Cantidad de arcos que salen o entran en un vertice 
  * Positivo : Arcos salientes 
  * Negativo : Arcos entrantes 

## Objetivos 

Modelizar un problema especifico a traves de un modelo abstracto donde los elementos que participan en el problema son los vertices y las relaciones que pueden existir entre estos participantes son los arcos 

> Estructuras abstractas , solo sirven como modelo 

## Representacion 

> Llevarlo a la realidad 

* Estatica : Utilizando estructuras computacionales rigidas, utilizando concepto de contiguidad como los vectores y matrices.
  * Matriz de adyacencia 
  * Matriz de incidencia 
* Dinamica: El espacio utilizado por la representacion va cambiando en funcion de como va cambiando el grafo 
  * Lista de adyacencia 

## Caracterizacion 

* Grafo libre : No existen arcos ( todos los vertices son aislados ) 
* Grafo Completo : Conectado con todos los vertices incluido si mismo 
* Grafo Regular : Si todos los vertices tienen el mismo grado .
* Grafo simple : Si a lo sumo un arco une dos vertices cualquiera .
* Grafo Complejo : Mas de un arco que vincule vertices 
* Grafo Conexo : Si todo par de vertices esta conectado por un camino ( una coneccion entre todos los nodos ) 
* Grafo no Conexo : Un grupo de vertices no esta conectado con el resto 
* Grafo complementario : Son las aristas que no estan en uno usando los mismos vertices .

## Clasificacion 

* Dirigidos : Cuando las relaciones tienen sentido 
* No Dirigidos : Cuando no tienen sentido
* Restricciones 
  * Grafos restrictos : No deben cumplir con Reflexividad , simetria y transitividad 
  * Grafos Irrestrictos : No se les aplica ninguna restriccion a la relacion que se modela 

## Caminos y pasos 

* Camino : Cuando existe vinculacion directa o indirecta
* Paso : Cuando existe un camino entre ambos pero con un sentido preestablecido 
* Ciclo : Es un paso o camino donde el origen y el destino son iguales.

## Busqueda 

* Busqueda en profundidad ( Depth First ) : Avanzar en profundidad es sin mantener orden va avanzando hasta que no puede mas y ahi empieza a retroceder para tomar otra relacion 
* Busqueda en Anchura ( Breath First ) : Evalua todos los destinos de todos los arcos que parten del vertice de origen. antes de evaluar el siguiente 

## Estructuras de datos 

> Una estructura de datos es un grafor dirigido y restricto con las caracteristicas de unicidad en sus relaciones, cada nodo solo puede terner un nodo predecesor a el 

Son utilizadas para modelar problemas reales al igual que los grafos, la ventaja comparativa es que debido a las limitaciones de las mismas. 

### Clasificacion 

* Biunivocas ( univocas en ambos sentidos) ( Pilas , colas , listas) y univocas ( Arboles ) solo tienen un predecesor pero muchos sucesores 

* Listas : Tienen una dinamica abierta, 
  * Lista lineal : ultimo puntero a null 
  * Lista circular : ultimo apunta al primero 
  * Lista Doblemente enlazada : Apuntan al anterior y al siguiente 
* Pila : LIFO (Last input first output)
* Cola : FIFO (First input first output) Orden y jerarquia 
* Arboles : 
  * Grado es la cantidad de sucesores que puede tener cada nodo ( en las otras estructuras es 1 ) 
