# Arbol 

## Index 

* [## Caracteristicas ](#caracteristicas)
* [## Busqueda ](#busqueda)
  * [### Arbol binario de busqueda ( ABB ) ](#abb)
    * [#### Barridos ](#Barridos)
  * [### Arbol de Expresion ](#expresion)

* Grado : Es por definicion la maxima cantidad de hijos o subarboles que puede tener cada nodo
* Nivel : Es la posicion en la que se encuentra cada nodo con respecto a la raiz del mismo considerando que la raiz se encuentra en nivel 0 
* Profundidad : Cantidad niveles que cuenta el mismo 
* Representacion Computacional 
  * Estatica 
    * Se representa a traves de un vector
  * Dinamica 
    * Conjunto de nodos de igual tipo vinculados entre si a traves de punteros o links 
 
## Caracteristicas <a name="caracteristicas"></a>

* Completo: Todos los nodos del arbol cumplen el grado o son hojas 
* Balanceado : Si todos los subarboles desde la raiz pesan lo mismo o sea que tienen la misma cantidad de elementos o menos 1 
* Perfectamente Balanceado : Cuando esta balanceado en todos sus niveles 
* Crecimientto : Es exponencia en funcion del grado del mismo, cada nivel puede crecer segun el grado definido
  * $` max_{elementos} = Grado^{niveles} - 1 `$

## Busqueda <a name="busqueda"></a>

Si se quiere encontrar un elemento en dicho arbol, la busqueda se realiza por niveles y no por elementos de forma tal que para encontrar cualquier elemento a lo sumo realizaremos tantas preguntas como niveles tenga el arbol .

$` niveles = \log(elementos) + 1 - grado `$
$` niveles > \log(elementos)`$

### Arbol binario de busqueda ( ABB ) <a name="abb"></a>

Es un arbol de grado 2 diesniado par buscar como metodo alternativo a una lista representada por un vector, donde los elementos menores se ingresan a la izquierda y los mayores a la derecha .

#### Barridos <a name="Barridos"></a>

Un Barrido de un arbol es la forma de leer el mismo, si biex existe una forma de recorrer un arbol que es de arriba hacia abajo de izq a der por convencion occidental, existen 3 forma s de leer 

* PreOrden : El nodo se lee apenas se llega al mismo 
* PostOrden : El nodo se lee cuand ose va del mismo y no se va regresar 
* Inorden : El nodo se lee cuando se cambia de rama en el arbol 

Ejemplo 

* Preorden : 8-3-7-4-5-22-14-13-11-9-45
* PostOrden: 5-4-7-3-9-11-13-14-45-22-8
* Inorden: 3-4-5-7-8-9-11-13-14-22-45

### Arbol de Expresion <a name="expresion"></a>

Una expresion puede representarse y resolverse a partir de un arbol , ejemplo: `3+5*8–4*`
* Inorder = notacion infija : `3 + 5 * 8 – 4 * 2`
* PostOrden = postfijo o polaca inversa : `358*+42*-`

![Arbol](../img/arbol-expresion.png)



