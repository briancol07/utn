# Introduccion

Una base de datos es un conjunto de datos estructurados y definidos a traves de un proceso especifico, que busca evitar la redundancia y que se almacenara en algun medio de almacenamiento masivo. 

## Sistema de gestion de BBDD

SGBD o DBMS ( Data base management system), motod de bbdd. Es una capa de sfotware que controla todos los accesos a la base de datos. 
Las instrucciones se agrupan en: 

* DDL ( Lenguaje de definicion de datos ) 
* DML ( Lenguaje de manipulacion de datos ) 
* DCL ( Lenguaje de control de datos 

### DDL

Es el conjunto de ordenes que permiten definir la estructura de una base datos 

### DML 

Las instrucciones que conforman este grupo son las que estan incluidas en las aplicaciones y se usan para alterar el contenido de un archivo de datos.

La diferencia respexto al DDL que actuaba sobre la estructura para almacenar, mientras que estas ordenes afectan al contenido de los archivos de datos 

### DCL

Son ordenes que se utilizan para implementar seguridad en la base de datos como por ejempli indicar privilegios, insertar modificar etc, tiene cada usuario respecto a los distintos objetos de la bbdd 
