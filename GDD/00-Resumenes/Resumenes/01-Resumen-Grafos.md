# Grafos

* [## Nodo ](#nodo)
  * [### Grado ](#grado)
* [## Representacion de grafos ](#representacion)
  * [### Estatico VS dinamico ](#vs)
* [## Caracteristicas de los grafos ](#caracteristicas)
  * [### Clasificacion ](#clasificacion)
* [## Camino Paso y ciclo ](#cpc)
* [## Algoritmo dijkstra ](#dijkstra)

```
G = (V,A)

----   a1    ----
|V1|  ---->  |V2|
----         ----

```

## Nodo <a name="nodo"></a>

* Tipos 
  * Simples 
  * Compuestos

### Grado <a name="grado"></a>

Cantidad de aristas que salen o entran 

* Positivo : Salen 
* Negativo : Entran 

## Representacion de grafos <a name="representacion"></a>
 
* Estatica 
  * Espacio fijo (No cambia)
  * De un principio se ven todas las relaciones 
  * Matriz
    * Adyacencia
    * Incidencia 
* Dinamico 
  * Va cambiando segun el grafo (y cantidad de elementos)
  * Listas de adyacencia 
  * Pfaltz 
 
### Estatico VS dinamico <a name="vs"></a>

* Espacio Ocupado : 
  * Depende de cuanto se reserve en los estaticos 
* Velocidad : 
  * Estaticas suelen ser mas rapidas por el acceso que es directo 

## Caracteristicas de los grafos <a name="caracteristicas"></a>

* Grafo libre : Todos los vertices son aristas 
* Completo : Todas las aristas posibles 
* Regular : Cada vertice tiene el mismo grado
* Simple : Solo hay una arista que une dos vertices 
* Complejo : Puede existir mas de una arco que une dos vertices cualesquiera
* Conexo : Existe almenos 1 conexion entre los nodso 
* No Conexo : Un grupo de vertices no esta conectado con los otros 

### Clasificacion <a name="clasificacion"></a>

* Direcion aristas 
  * Dirigidos 
  * No dirigidos
* Segun Restricciones 
  * Restrictos (No cumplen con)
    * Reflexibilidad
    * Simetria 
    * Transitividad 
  * Irrestrictos (Ninguna restriccion)

## Camino Paso y ciclo <a name="cpc"></a>

* Camino : Vinculacion directa o no entre nodos
* Paso : Existe un camino pero con sentido
* Ciclo : Paso o camino donde el origen y el destino son iguales 

## Algoritmo dijkstra <a name="dijkstra"></a>

Nos permite calcula la distancia minima para llegar de un nodo origen a todos los nodo destino 

* Algoritmos utilizados
  * Busqueda en profundidad (depth first)
    * Va recorriendo 1 por 1 
    * Si no llega al nodo que deberia llegar vuelve un nodo atras y vuelve a probar 
    * Encuentra camino posible no el mas optimo 
  * Buqueda en anchura 
    * Evalua todos los arcos (antes de pasar al siguiente)
    * No requiere retroceder 
    * Busca el mas corto 



