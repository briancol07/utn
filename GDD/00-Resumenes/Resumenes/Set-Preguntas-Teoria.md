# Preguntas de Gestión de Datos

## Grafos <a name="grafos"></a>

1. ¿Cómo se define un grafo en términos de conjuntos de vértices y aristas?
2. ¿Cuál es la diferencia entre un nodo simple y un nodo compuesto?
3. ¿Cómo se calcula el grado positivo y negativo de un nodo en un grafo dirigido?
4. ¿Cuáles son las ventajas y desventajas de la representación estática y dinámica de grafos?
5. ¿En qué casos se recomienda utilizar una matriz de adyacencia en lugar de una lista de adyacencia?
6. ¿Cómo funciona el algoritmo de Dijkstra y para qué se utiliza?
7. ¿Cuál es la diferencia entre la búsqueda en profundidad y la búsqueda en anchura?
8. ¿Cómo se construye la matriz de Floyd-Warshall y qué representa?
9. Explica qué es un grafo conexo y un grafo no conexo con ejemplos.
10. ¿Qué diferencia hay entre un grafo dirigido y uno no dirigido?

## Estructuras de Datos <a name="ED"></a>

1. ¿Qué características diferencian a una pila de una cola?
2. ¿Cómo funciona una lista doblemente enlazada y cuáles son sus ventajas?
3. Explica la diferencia entre listas lineales y listas circulares.
4. ¿Cuál es la diferencia fundamental entre una estructura de datos biunívoca y una unívoca?
5. ¿Por qué una pila sigue la estrategia LIFO y una cola la estrategia FIFO?
6. ¿En qué situaciones se recomienda el uso de listas enlazadas sobre arreglos?
7. ¿Cuáles son las principales operaciones que se pueden realizar en una pila?
8. ¿Cómo se implementa una cola con dos pilas?
9. Explica cómo funcionan las listas de adyacencia en la representación de grafos.
10. ¿Qué estructuras de datos se utilizan para modelar árboles y grafos en bases de datos?

## Árboles <a name="arboles"></a>
1. ¿Qué condiciones debe cumplir un grafo para ser considerado un árbol?
2. ¿Cómo se define el grado de un nodo en un árbol?
3. Explica la diferencia entre la profundidad y el nivel de un árbol.
4. ¿Qué características definen un árbol balanceado y un árbol lleno?
5. ¿Cómo funciona la representación estática de árboles en un vector?
6. ¿Cómo se determina el hijo izquierdo y derecho de un nodo en una representación de árbol en memoria?
7. Explica los tres métodos de recorrido de árboles en profundidad (preorden, inorden, postorden).
8. ¿Cómo funciona la transformación de Knuth y para qué se utiliza?
9. ¿Qué es un árbol de expresión algebraica y cómo se recorre?
10. ¿Cómo se implementa un árbol en una base de datos relacional?

## Árboles de Búsqueda <a name="busqueda"></a>

1. ¿Qué es un árbol binario de búsqueda (ABB) y qué condiciones debe cumplir?
2. ¿Cómo se realiza una búsqueda en un ABB y cuál es su complejidad en el mejor y peor caso?
3. ¿Cómo se balancea un árbol binario de búsqueda?
4. ¿Cuál es la diferencia entre un árbol balanceado y un árbol AVL?
5. ¿Qué tipos de rotaciones existen para balancear un árbol binario de búsqueda?
6. ¿Cómo se realiza una inserción en un ABB y qué problemas pueden surgir?
7. ¿Cuáles son los tres casos de eliminación de un nodo en un ABB?
8. ¿Por qué es importante mantener balanceado un ABB?
9. ¿Cuál es la ventaja de un ABB sobre una lista ordenada?
10. ¿En qué casos un ABB puede degenerar en una lista enlazada?

## Compresión de Datos (Huffman) <a name="huffman"></a>

1. ¿Cuál es la diferencia entre compresión con pérdida y sin pérdida?
2. ¿Cómo se construye el árbol de Huffman?
3. ¿Qué estructura de datos se utiliza para almacenar la información en la compresión de Huffman?
4. ¿Por qué Huffman asigna códigos de longitud variable a los caracteres?
5. ¿Cómo se obtiene el código de un carácter en Huffman?
6. ¿Qué información se almacena en la cabecera de un archivo comprimido con Huffman?
7. ¿Cómo se reconstruye el archivo original a partir de su versión comprimida?
8. ¿Por qué Huffman es más eficiente en textos con caracteres de frecuencia variable?
9. ¿Cómo se implementa Huffman en bases de datos?
10. ¿Cuál es la ventaja de Huffman sobre otros métodos de compresión?

## Métodos de Ordenamiento <a name="ordenamietno"></a>

1. ¿Cuál es la diferencia entre un ordenamiento estable e inestable?
2. ¿Cómo funciona el algoritmo de Merge Sort y cuál es su complejidad?
3. ¿Cuál es el peor caso del algoritmo Insertion Sort?
4. ¿Cómo funciona Quick Sort y qué papel juega el pivote?
5. ¿Qué es una estrategia in situ en ordenamiento?
6. ¿Cuál es la principal desventaja de Bubble Sort?
7. ¿Por qué Heap Sort utiliza un árbol binario para ordenar datos?
8. ¿En qué se diferencia Shell Sort de otros métodos de ordenamiento?
9. ¿Cuál es la complejidad promedio de Quick Sort?
10. ¿Qué métodos de ordenamiento permiten ejecución en paralelo?

## Hashing <a name="hashing"></a>

1. ¿Qué es una función de hash y para qué se utiliza?
2. ¿Cómo se maneja una colisión en una tabla hash?
3. Explica la diferencia entre hashing estático y dinámico.
4. ¿Cómo funciona la función de re-hash y en qué casos se usa?
5. ¿Qué características debe cumplir una buena función de hash?
6. ¿Por qué hashing no es útil para búsquedas por rango?
7. ¿Cuál es la diferencia entre hashing abierto y cerrado?
8. ¿Cómo se implementa un índice de base de datos con hashing?
9. ¿Qué problemas puede generar el clustering en hashing?
10. ¿Cuál es la relación entre hashing y estructuras de datos como listas enlazadas?

## Árbol B <a name="arbol-b"></a>

1. ¿Qué es un árbol B y para qué se usa en bases de datos?
2. ¿Cuál es la diferencia entre un ABB y un árbol B?
3. ¿Cómo se estructura un nodo en un árbol B?
4. ¿Por qué los árboles B se usan en almacenamiento en disco?
5. ¿Cómo funciona la inserción en un árbol B?
6. ¿Qué ocurre cuando un nodo se llena en un árbol B?
7. ¿Cómo se realiza una búsqueda en un árbol B?
8. ¿Qué ventajas tiene un árbol B sobre un ABB en términos de acceso a disco?
9. ¿Cómo se mantiene balanceado un árbol B?
10. ¿Cuál es la diferencia entre un árbol B y un árbol B+?

----

## Respuestas <a name="rta"></a>

### Grafos <a name="rG"></a>

1. Un grafo se define como G = (V, A), donde V es el conjunto de vértices o nodos y A es el conjunto de aristas o conexiones entre los vértices.
2. Un nodo simple contiene un único valor, mientras que un nodo compuesto tiene un conjunto de valores que lo caracterizan.
3. El grado positivo es la cantidad de aristas que salen de un nodo, mientras que el grado negativo es la cantidad de aristas que llegan a un nodo.
4.  Res
  * Estática: Ocupa un espacio fijo, puede ser más rápida en accesos, pero desperdicia espacio en grafos dispersos.
  * Dinámica: Ajusta su tamaño según el grafo, pero es más lenta en accesos debido al uso de punteros.
5. Se recomienda cuando el grafo es denso (muchas aristas en relación con los nodos) o cuando se necesita acceso rápido a la existencia de aristas.
6. Se utiliza para encontrar la ruta más corta desde un nodo origen a todos los demás en un grafo con pesos no negativos. Se basa en seleccionar el nodo con menor costo acumulado y actualizar los costos de sus vecinos.
7. Res 
  * Búsqueda en profundidad (DFS): Explora lo más lejos posible por cada camino antes de retroceder.
  * Búsqueda en anchura (BFS): Explora todos los vecinos de un nodo antes de avanzar a los siguientes niveles.
8. Es una matriz cuadrada donde cada celda (i, j) representa la distancia más corta entre los nodos i y j. Se construye iterativamente considerando caminos intermedios.
9. Un grafo conexo tiene un camino entre cualquier par de nodos. Ejemplo: una red de carreteras donde todas las ciudades están conectadas. Un grafo no conexo tiene al menos un par de nodos sin camino entre ellos. Ejemplo: dos islas sin puentes entre ellas.
10. Un grafo dirigido tiene aristas con dirección, es decir, (A → B) no implica (B → A). Un grafo no dirigido tiene aristas bidireccionales, es decir, (A — B) significa que el camino es en ambas direcciones.


