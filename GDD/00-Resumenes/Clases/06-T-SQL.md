# T-SQL (17-09-25)


> Crear objetos y se incorporan al compilarlo

* PL-SQL
  * Oracle 
  * Program language 
* SQL 
  * Interpreter 
* capa de presentacion
  * Dominio : Logica 
  * Persistencia : GDD

## Funcion <a name="funcion"></a>

Una funcion siempre retorna un valor o sino seria un proceudre (no returns)

## Vistas <a name="vistas"></a>

Posibilidad de ver info de determinada forma `Persistir un select` (tabla virtual)

> Tabla virtual

Usar algo frecuentemente, son un conjunto de datos sacados de la tabla (no tiene datos ) sino como acceder 

Interpretar (motor): entonces ejecucion va a ser mas rapido  para consultar datos 

Si no tiene referencia la tabla tiene referencia y delte borra todos , y el insert en todas 

No puede tener order by amenos que haya top, varaible tipo tabla, lo que trae en memoria la query -> data set (matriz)

### Vista materializada <a name="materializada "></a>

Muchas consultas y poca actualizacion 

No recomendable en datawarehouse o olap.

* Limitacion 
  * No pueden tener algo que se vaya a ejecutar 
  * No outter join
  * No count -> solo count big
  * Crear un indice cluster -> Acceso rapido 

## Snapshot <a name="snapshot"></a>

* Igual que la vista pero se materializa 
* No actualiza, datos duplicados 


## Trigger <a name="trigger"></a>

* Clave especial 
* No retorna 
* No recibe parametros 
* Se ejecuta cuando pasa algo -> automatico 

Construir un sistema completo es distinto a evento , que es un suceso que puede modificar ( en un motor puede crear eliminar o update )
