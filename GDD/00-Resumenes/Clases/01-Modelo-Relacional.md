# Modelo relacional


> Modelo jerarquico de persistencia 

* Padres y hijos 
* Red con los padres 

## Los conceptos y ejemplos 

* Herramientas 
  * Casos de uso 

## Partes 

1. Relacion entre entidades 
  * Objeto -> Fisico 
  * Agrupamientos de elementos 
    * Unificacion y pertenencia 
2. Tupla 
  * Generico 
  * Conjunto de atributos no vacios 
    * Caracteristicas del elemento del dominio 

* Reducir al minimo occurrencia de cuotas.
* Mas chico el dominio mas facil manejarlo

## Valor NULL

* Desconocido 
* Cuando no se asigna un valor por defecto 

## PK 

> Primary Key 

* Identifica univocamente a esa tupla 
* Atributo o conjunto de atributos 

## FK 

> Foreign Key 

* Atributo dentro de tupla que otro lo tiene como PK

## Integridad Referencial 

> No puedo borrar tupla la cual tiene referencias 

* No hay referencias cruzadas rotas 
* Independiente de la implementacion -> Puesta en practica 
* No hay orden de prioridad 

## Normalizacion 

> Concepto : lo comun 

* Estandarizar : un esquema estandard 
* Iterativos 
  * 1era forma norma (FN) 
    * PK 
    * No tiene atributos calculables 
    * No tiene atributos repetidos ni compuestos 
  * 2da FN 
    * Atributos no clave dependen de la pk 
  * 3ra FN 
    * Todos los atributos no clave no dependen de algun atributo no clave 

### Ventajas 

* Quitar redundancias 
* Estandarizar 
* Estable 

### Desventaja

* Velocidad 
* Performance 

## Lenguaje SQL

* DDL 
  * Definir BBDD ( alta baja y modificacion ) 
  * Create 
  * Alter 
  * Drop
* DML 
  * Manipular informacion 
* DCL 
  * Asignar permisos 
* Tipos de datos tipados 

### DDL 









