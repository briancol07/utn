# Lenguaje SQL 

## Data modeling language ( DML ) 

* Alta baja y modificacion fila 
* SELECT : Consulta sobre datos 
* INSERT : insertar valores en una tabla 
* UPDATE : Modificar valores de una o varias 
  * Necesita where 
* DELETE : Eliminar filas 
  * Necesita where 
``` SQL
-- Mismo orden 
INSERT into tabla (col1, col2, col3)
VALUES (val1, val2, val3)

INSERT tabla VALUES (val1, val2, val3)
```

## SELECT 

```
SELECT
FROM 
WHERE ( condicion )
GROUP BY 
HAVING ( condicion )
ORDER BY 
```

* Teoria de conjuntos
* Producto cartesiano 

El select arranca en el from ( que seria el universo ) 

## Where 

* Algebra relacional
* Condicion de filtro
* Devolver true or false 
* Este en el universo del from 

## Between 

* between v1 and v2 ( > < ) 
* like : parecido
  * es como igual pero no es igual
  * `%a`
  * `_a`
 
> El `+` es polimorfico

## GROUP BY 

* Funciones de grupo 
  * COUNT()
  * SUM()
  * MIN()
  * MAX()
  * AVG()
* Corte de control 
* Having es el where del group by

> DISTINCT : Son distintos, no trae duplicados 

## HAVING 

* Solo que este implicito en group by 
* Lo mismo que where mas las funciones de grupo 
* Depende del group by y el universo que haya 

## ORDER BY

* Ordenar nivel fila (DESC, ASC)
* Siempre que este universo from 
