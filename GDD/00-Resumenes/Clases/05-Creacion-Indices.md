# Creacion Indices (16-09-25)

## Index 

* [## Busqueda Secuencial ](#busqueda)
* [## Fragmentacion ](#fragmentacion)
* [## Tipos de acceso ](#tipos)
* [## Metodos para armar ](#metodos)
  * [### Hashing ](#hashing)
  * [### Hashing encadenamiento ](#encadenamiento)
* [## Arbol m-ario ](#arbolem)
  * [### Insercion ](#insercion)

Crear una estructura adicional a la table que permita mantener los datos ordenados en funcion de alguna clave (PK o indice) 

* Fisica y no logica 
* Clave + posicion 

> Objetivo: Tener datos ordendor y ser perfomante , un almacenamiento secuencial 

## Busqueda Secuencial <a name="busqueda"></a>

ir preguntando de uno a uno , el acceso es directo, al a posicion donde estan 

Valores relativos : donde empieza (algo)

Estructura adiciona a la tabla de la cual se ve a lo que genera un espacio adicional y la necesidadde incorporar y eliminar valores en ambos sitios 

## Fragmentacion <a name="fragmentacion"></a>

* Partido 
* separar

`F_seek` ir a una posicion del archivo , indice de referencia a la tabla 

## Tipos de acceso <a name="tipos"></a>

* Secuencial 
* Secuencial indexado 
  * En funcion a alguna clave
* Directo o random 
  * Sin hacer alguna tipo de recorrido 

## Metodos para armar <a name="metodos"></a>

### Hashing <a name="hashing"></a>

> Asegurar integridad 

Tabla + funcion(hash) -> Dispersion

```
   
--in--> f(x) --out--> string 
```

`f(x)` en este caso es una funicon de hash que procesa 

```
Din --> H ---> Dout
```
* D = Data , si `Din > Dout` hay colisiones 
* H es una funcion y determina como puede ser 

Distribuir uniformemente las claves, asi no hay tanta colision, no se puede evitar 

### Hashing encadenamiento <a name="encadenamiento"></a>

> Salvar colision 

Se agrega una columna mas para las colisiones y asi creo una lista

* Sondeo lineal 
  * No crea otra lista, usa la misma tabla buscando un lugar 
* Sondeo Cuadratico 
  * Para los que hay acumulacion
  * Trabaja por complemento
* Hashing Doble 
  * Aplica otra funcion de hash si entro por primera o segunda 

## Arbol m-ario <a name="arbolem"></a>

> Grado alto  ( O (log n) ) 

* Crecimiento mas potente en los niveles 
* Mas potente en el acceso (50 - 2000)
* Indices fisicos 
* Tabla estructurada como arbol
  * Nodo hoja 
    * vector: claves ordenadas direccion relativa (Parecido tabla hash)
  * Nodo Raiz/rama 
    * Tiene un puntero al hijo 
    * Menores iguales a la clave 
    * Clave + puntero
  * Nodos Sueltos 

### Insercion <a name="insercion"></a>

Desde la raiz y hacemos una busqueda , Primer elemento tipo hoja, arranca hasta que la hoja requiera padre 

* Split: 
  * Dividir: separar en 2 : hoja ariba y hoja abajo crear otro nodo
  * Padre: Tiene el mayor de los nodos + punteros 

Va a estar en disco (SO), lo que mas tarda es acceso a disco, tamanio de cluster y nodo.

> Indice Numerico > indice alfanumerico

