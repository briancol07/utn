# Teoria-Clase-9-9-24

## Index 

* [## Clasificar](### Clasificar)
* [## Complejidad](### Complejidad)
* [## O(F)](### O(F))
* [## Search and SORT ( libro )](### Search and SORT ( libro ))

Clasificar != ordenar 

> Ordenamiento: Mantener ordenado, a lo largo del tiempo 

## Clasificar <a name="clasificar"></a>

Tomar un conjunto de elementos y ordenarlo por un criterio ( en un tiempo ) pierde orden. Todo referenciado por una parte 

* Insitu: en el lugar, valores y datos ordenados en el lugar que tengo 
  * Memoria ram o disco 
  * Interna 
  * externa 

## Complejidad <a name="complejidad"></a>

Para la compu ejecutar el algoritmo 
* Determinisitico 
* `p`: resuelto y resolucion posible 
* `np`: No hay solucion posible en un tiempo determinado 
* Establecer orden de complejidad : en el tiempo de acuerdo a cantidad de elementos 

> Regla de 3: Proporcionalidad , linealidad 

## O(F) <a name="o"></a>

Comportamiento de la funcion 
Orden complejidad > a linealidad 
Cantidad de comparaciones, le lleva mas a la maquina 

## Search and SORT ( libro ) <a name="search"></a>

* Bubble Sort 
  * orden n2
  * acotadada -> inerno 
* Selection sort 
  * Orden n2 
* Insertion sort 
  * n2 peor situacion 
  * Menos comparaciones que antes 
* Shell sort 
  * Comparaciones cada 4 elementos 
  * Interno
* Merge Sort 
  * Separa y junta 
  * Divide y venceras 
  * Externo 
* Quick sort 
  * trata como un arbol 
  * n log n y peor n2
  * bsort 
  * meansort 
* Heap Sort 
  * Arbol completo y balanceado ( binario )
