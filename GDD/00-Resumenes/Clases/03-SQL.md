# SQL - Clase - 3-9-24

* Join implicito sin join pero poniendo dos tablas 
* where -> Filtrar select 

## JOIN 

* INNER: lo mismo
* LEFT : Mandatoria tabla izq 
  * Pone nulls
* RIGHT: Mandatoria tabla der 
  * Pone nulls

## Comando seleccion multiple

* Case : Cuando no s quiere mostrar un valor crudo 

## TOP n 

* Las n primeras filas
* Acompaniado de un order 

## Distinct 

contar valores distintos 

## Select dentro de otro select 

* Subconsultas 
* Select [    ] 
  * Que espera? un valor unico? 
  * Select : 1 fila y 1 columna , sino rompe la gramatica 

``` SQL
SELECT (
        SELECT 
        FROM 
        )
FROM
```
> Select en el from esta mal 

## UNION 

* Permite unir selects 
* UNION
* UNION ALL: si hay duplicados muestra los dos 
* Misma cantidad de columnas y tipos 
* Disyuntiva : si hay una parecida no va 
* Order by va al final 
* Resuelve un select y luego el otro , ahi termina y unifica los dos 

