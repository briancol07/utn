# Preguntas Teoricas 

1. Un vector es una estructura de datos basica
  * V 
  * F 
2. Para poder realizar un UNION entre dos consultas, las mismas deben tener la misma cantidad de columnas, en el mismo orden y con los mismos tipos de datos 
  * V
  * F
3. Los arboles B garantizan mayor velocidad de Hashing para el acceso de datos 
  * V
  * F
4. La ejecucion sin filas de resultado de una query dentro de un trigger genera la cancelacion de la transaccion
  * V
  * F
5. Cualquier archivo, independientemente de su formato y tipo puede ser compactado por algun metodo de compactacion, disminuyendo aunque sea un caracter de su medida
  * V
  * F
6. Una subconsulta en el having, siempre debe retornar una fila y una columna
  * V
  * F
7. Un arbol B es la tecnica de creacion de indices que debe utilizarse cuando no se tiene informacion del archivo a mantener ordenado
  * V
  * F
8. Nunca es posible ejecutar la operacion de insert sobre una vista
  * V
  * F
9. La cantidad de nodos de un arbol de expresion siempre es par 
  * V
  * F
10. Un data mart es un datawarehouse afectado a un departamento o sector de la empresa
  * V
  * F
11. Cualquier Arbol puede ser representado computacionalmente mediante un vector? 
  * V
  * F 
12. Los hypercubos y multicubos, son distintas tecnicas para almacenar informacion en las bases de datos multidimensionales
  * V
  * F 
13. Un arbol B es la tecnica de creacion de indices que debe utilizarse cuando no se tiene informacion del archivo a mantener ordenado
  * V
  * F
14. Un vector es una estructura de datos basicas
  * V
  * F
15. Para poder realizar un UNION enre dos consultas, las mismas deben tener la misma cantidad de columna, en el mismo orden y con los mismos tipos de datos 
  * V
  * F
16. Un vector puede representar computacionalmente cualquier grafo irrestricto
  * V
  * F
17. El comando check es una restriccion al model
  * V
  * F
18. Cual es el objetivo de los grafos?
19. Explique en que casos la presencia de un indice para una tabla en una base de datos relacional podria generar mayor caida de performance que la ventaja aportada 
20. Indique las caracteristicas y funcionalidades de los algoritmos basados en arboles de decision
21. Que representa en concepto de integridad de un DBMS. Especifique lo descripto con un ejemplo

## Repuestas 

1. Falso, es una representacion computacional
2. Verdadero 
3. Falso 
4. Falso , Retorna null 
5. Verdadero, Siempre also se va a repetir sino el archivo solo mediria 256 caracteres 
6. Falso 
7. Verdadero, Porque no se puede estimar el espacio a ocupar ni el dominio de entrada de las claves 
8. Falso, Se pueden insertar, borrar y modificar valores
9. Falso
10. Verdadero 
11. Verdadero, Debido a que es su representacion estatica y la informacion se guarda por niveles sin importar el grado 
12. Verdadero, Son diferentes formas de almacenar las dimensiones 
13. Verdadero, por que no se pueden estimar el espacio a ocupar ni el dominio de entrada de las claves 
14. Falso, Es una representacion computacional
15. El proposito principal de los grafos es representar de manera abstracta y modelar un problema particular. En este modelo , se identifican los elementos involucrados en el problema, representados como vertices y se describen las posibles relaciones entre estos elementos mediante arcos.
16. Verdadero
17. Falso
18. Verdadero
19. RTA:
  * Cuando se realixan varias actualizaciones a una tabla con indices, es necesario actualizar estos cada vez que se modifica la tabla ( En sistemas OLAP, el mantenimiento de los indices ante actualizaciones incrementales constantes es muy costos) 
  * Cuando poseemos tablas chicas la presencia de indices no siempre es mjor ya que al ser una tabla chica realizar una busqued secuencial es mas perforante que el proceso de la creacion y busqueda mediante indices 
  * Cuando se utilizan indices con funciones de hash puede ser que se generen colisiones teniendo que buscar metodos para solucionar los mismos
20. Un arbol de decision ofrece ventajas como la clasificacion de nuevos casos, interpretacion facil, comprension del conocimiento, explicacion del comportamiento y reduccion de variables. Una de su funcionalidad la utilizamos en data mining para automatizar modelos de decisiones
21. La integridad asegura que solo se comienzan aquellas operaciones que se puedan terminar para asi llevar la base de datos de un estado valido a otro, previniendo asi inconsistencias . Un ejemplo es cuando utilizamos transacciones ( Stored procedures ) En SQL, que ante un fallo en la transaccion, la misma. no genera cambios en nuestra base de datos .
  * Esto  es la consistencia la integridad apunta a la integridad en las referencias cruzadas como por ejemplo la integridad relacional
