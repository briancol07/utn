# TP - 2C - 2024

Una plataforma de market place que permite conectar vendedores y clientes

* A traves de publicaciones 
* Gestionar ventas 
* Pagos 
* Envios 

## Etapas 

1. Migracion de los datos 
  * Reformular disenio bbdd y procesos 
2. Implementacion de un segundo modelo 
  * Procedimientos y vistas 

## Scripts de entrega

* script\_creacion\_inicial.sql
* script\_creacion\_BI.sql

## EQUIPO 

* Equipo: SQLITO
* Grupo; 26
* Curso: k3521
* Integrantes: 
  * Colman,Brian-1634689 bcolman@frba.utn.edu.ar
  * Jaralampidis,Federico-2037397 
  * Haite Gulotta,Gustavo-2037154
  * Bencina,Morena-1772922

## DER

![DER](./TP2C2024-K3521-SQLito-26/der.jpg)
![DER-BI](./TP2C2024-K3521-SQLito-26/Der_BI.jpg)

## Data 

script\_creacion\_BI.sql
script\_creacion\_inicial.sql

