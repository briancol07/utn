-- GD2C2024
-- Nombre del esquema igual nombre del grupo registrado
-- Mayusculas, sin espacio y separado por guiones bajos 

USE GD2C2024
GO


----------------------------
--- Eliminacion de TODO ----
----------------------------

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Envio' )
  DROP TABLE SQLITO.Envio
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Usudom' )
  DROP TABLE SQLITO.Usudom
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'TipoEnvio' )
 DROP TABLE SQLITO.TipoEnvio
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Concepto' )
  DROP TABLE SQLITO.Concepto
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'DetalleFactura' )
  DROP TABLE SQLITO.DetalleFactura
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Factura' )
 DROP TABLE SQLITO.Factura
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Pago' )
 DROP TABLE SQLITO.Pago 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'DetalleDePago' )
 DROP TABLE SQLITO.DetalleDePago
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'MedioDePago' )
 DROP TABLE SQLITO.MedioDePago
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'DetalleVenta' )
 DROP TABLE SQLITO.DetalleVenta
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Venta' )
 DROP TABLE SQLITO.Venta 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Publicacion' )
 DROP TABLE SQLITO.Publicacion
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Producto' )
 DROP TABLE SQLITO.Producto 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Almacen' )
 DROP TABLE SQLITO.Almacen 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Domicilio' )
  DROP TABLE SQLITO.Domicilio
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Localidad' )
  DROP TABLE SQLITO.Localidad
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Provincia' )
  DROP TABLE SQLITO.Provincia
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Usuario' )
  DROP TABLE SQLITO.Usuario
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Pago' )
  DROP TABLE SQLITO.Pago
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Cliente' )
 DROP TABLE SQLITO.Cliente 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Vendedor' )
 DROP TABLE SQLITO.Vendedor
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Subrubro' )
 DROP TABLE SQLITO.Subrubro 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Rubro' )
 DROP TABLE SQLITO.Rubro
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Modelo' )
 DROP TABLE SQLITO.Modelo
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'Marca' )
 DROP TABLE SQLITO.Marca
GO

---- Procedure 

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_marca' AND type='p')
	DROP PROCEDURE SQLITO.migrar_marca
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_modelo' AND type='p')
	DROP PROCEDURE SQLITO.migrar_modelo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_rubro' AND type='p')
	DROP PROCEDURE SQLITO.migrar_rubro
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_subrubro' AND type='p')
	DROP PROCEDURE SQLITO.migrar_subrubro
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_producto' AND type='p')
	DROP PROCEDURE SQLITO.migrar_producto
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_medio_de_pago' AND type='p')
	DROP PROCEDURE SQLITO.migrar_medio_de_pago
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipoEnvio' AND type='p')
	DROP PROCEDURE SQLITO.migrar_tipoEnvio
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_vendedor' AND type='p')
	DROP PROCEDURE SQLITO.migrar_vendedor
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cliente' AND type='p')
	DROP PROCEDURE SQLITO.migrar_cliente
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_provincia' AND type='p')
	DROP PROCEDURE SQLITO.migrar_provincia
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_localidad' AND type='p')
	DROP PROCEDURE SQLITO.migrar_localidad
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_domicilio' AND type='p')
	DROP PROCEDURE SQLITO.migrar_domicilio
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_usuario' AND type='p')
	DROP PROCEDURE SQLITO.migrar_usuario
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_pago' AND type='p')
	DROP PROCEDURE SQLITO.migrar_pago
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_domicilios_usuarios' AND type='p')
	DROP PROCEDURE SQLITO.migrar_domicilios_usuarios
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_venta' AND type='p')
	DROP PROCEDURE SQLITO.migrar_venta
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_publicacion' AND type='p')
	DROP PROCEDURE SQLITO.migrar_publicacion
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_almacen' AND type='p')
	DROP PROCEDURE SQLITO.migrar_almacen
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_envio' AND type='p')
	DROP PROCEDURE SQLITO.migrar_envio
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_concepto' AND type='p')
	DROP PROCEDURE SQLITO.migrar_concepto
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_detalleDePago' AND type='p')
	DROP PROCEDURE SQLITO.migrar_detalleDePago
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_factura' AND type='p')
	DROP PROCEDURE SQLITO.migrar_factura
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'migrar_detalleVenta' AND type='p')
  DROP PROCEDURE SQLITO.migrar_detalleVenta
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_detalle_factura' AND type='p')
	DROP PROCEDURE SQLITO.migrar_detalle_factura
GO

IF EXISTS ( SELECT name FROM sys.schemas WHERE name = 'SQLITO' )
  DROP SCHEMA SQLITO
GO
---------------------- ESQUEMA -------------
-----------------------------------
--- Creacion Tablas y esquemas ----
-----------------------------------

CREATE SCHEMA SQLITO
GO 
-----------------------------
--- Creacion Table: Marca  --
-----------------------------
CREATE TABLE SQLITO.Marca(
  marc_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  marc_detalle NVARCHAR(50) NOT NULL,
)

--------------------------------
--- Creacion Table: Modelo  ----
--------------------------------
CREATE TABLE SQLITO.Modelo(
  mod_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  mod_detalle NVARCHAR(50) NOT NULL,
  mod_marc_id INT NOT NULL
  CONSTRAINT fk_mod_marc
  FOREIGN KEY (mod_marc_id) REFERENCES SQLITO.Marca (marc_id)
)
--------------------------------
--- Creacion Table: Rubro  ----
--------------------------------
CREATE TABLE SQLITO.Rubro(
  rub_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  rub_detalle NVARCHAR(50) NOT NULL ,
)
--------------------------------
--- Creacion Table: Subrubro  --
--------------------------------
CREATE TABLE SQLITO.Subrubro (
  srub_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  srub_detalle NVARCHAR(50) NOT NULL ,
  srub_rub INT NOT NULL,
  CONSTRAINT fk_srub_rub
  FOREIGN KEY (srub_rub) REFERENCES SQLITO.Rubro (rub_id),
)


--------------------------------
--- Creacion Table: Vendedor  --
--------------------------------
CREATE TABLE SQLITO.Vendedor (
  vend_codigo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  vend_razon_social NVARCHAR(50),
  vend_cuit NVARCHAR(50),
)
-------------------------------
--- Creacion Table: Cliente  --
-------------------------------
CREATE TABLE SQLITO.Cliente (
  clie_codigo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  clie_nombre NVARCHAR(50) NOT NULL ,
  clie_apellido NVARCHAR(50) NOT NULL ,
  clie_fecha_nacimiento DATE,
  clie_dni DECIMAL(18,0),
)
-------------------------------
--- Creacion Table: Usuario  --
-------------------------------
CREATE TABLE SQLITO.Usuario(
  usu_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  usu_vend_codigo INT,
  usu_clie_codigo INT,
  usu_nombre NVARCHAR(50) NOT NULL ,
  usu_psw NVARCHAR(50) NOT NULL ,
  usu_fecha_creacion DATE,
  usu_mail NVARCHAR(50) NULL,
  CONSTRAINT fk_usu_vend_codigo
  FOREIGN KEY (usu_vend_codigo) REFERENCES SQLITO.Vendedor(vend_codigo),
  CONSTRAINT fk_usu_clie_codigo
  FOREIGN KEY (usu_clie_codigo) REFERENCES SQLITO.Cliente(clie_codigo)
)
--------------------------------
--- Creacion Table: Provincia --
--------------------------------
CREATE TABLE SQLITO.Provincia(
  prov_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  prov_nombre NVARCHAR(50) NOT NULL ,
)

---------------------------------
--- Creacion Table: Localidad  --
---------------------------------
CREATE TABLE SQLITO.Localidad(
  loc_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  loc_prov_id INT NOT NULL,
  loc_nombre NVARCHAR(50) NOT NULL ,
  CONSTRAINT fk_loc_prov_id
  FOREIGN KEY (loc_prov_id) REFERENCES SQLITO.Provincia(prov_id)
)
---------------------------------
--- Creacion Table: Domicilio  --
---------------------------------
CREATE TABLE SQLITO.Domicilio(
  dom_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  dom_loc_id INT NOT NULL,
  dom_calle NVARCHAR(50) NOT NULL ,
  dom_nro_calle DECIMAL(18,0),
  dom_piso DECIMAL(18,0),
  dom_depto NVARCHAR(50), -- Hay que ver si se pueda bajar 
  dom_cp NVARCHAR(50),
  CONSTRAINT fk_dom_loc_id 
  FOREIGN KEY (dom_loc_id ) REFERENCES SQLITO.Localidad(loc_id)
)
-------------------------------
--- Creacion Table: Almacen  --
-------------------------------
CREATE TABLE SQLITO.Almacen (
	alma_codigo INT PRIMARY KEY NOT NULL,
	alma_detalle NVARCHAR(50) NULL ,
	alma_dom_id INT NOT NULL,
	alma_costo_dia_al DECIMAL(18,2) 
	CONSTRAINT fk_alma_dom_id 
	FOREIGN KEY (alma_dom_id) REFERENCES SQLITO.Domicilio (dom_id)
)
--------------------------------
--- Creacion Table: Producto  --
--------------------------------
CREATE TABLE SQLITO.Producto (
  prod_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  prod_codigo nvarchar(50) NOT NULL,
  prod_descuento TINYINT,
  prod_sub_rubro INT NOT NULL,
  prod_marca INT NOT NULL ,
  prod_modelo INT NOT NULL ,
  prod_descripcion NVARCHAR(100) NOT NULL ,
  prod_precio DECIMAL(18,2),
  CONSTRAINT fk_prod_sub_rubro
  FOREIGN KEY (prod_sub_rubro) REFERENCES SQLITO.Subrubro (srub_id),
  CONSTRAINT fk_prod_marca
  FOREIGN KEY (prod_marca) REFERENCES SQLITO.Marca (marc_id),
  CONSTRAINT fk_prod_modelo
  FOREIGN KEY (prod_modelo) REFERENCES SQLITO.Modelo(mod_id),
)
-----------------------------------
--- Creacion Table: Publicacion  --
-----------------------------------
CREATE TABLE SQLITO.Publicacion(
  pub_codigo NVARCHAR(50) PRIMARY KEY NOT NULL,
  pub_descripcion NVARCHAR(50),
  pub_fecha_inicio DATETIME,
  pub_fecha_fin DATETIME,
  pub_vend_usuario INT NOT NULL,
  pub_costo DECIMAL(18,2),
  pub_porc_venta DECIMAL(18,2),
  pub_precio_unitario DECIMAL(18,2),
  pub_stock DECIMAL(18,0),
  pub_almacen INT NOT NULL,
  pub_prod_id INT NOT NULL,
  CONSTRAINT fk_pub_vend_usuario
  FOREIGN KEY (pub_vend_usuario) REFERENCES SQLITO.Usuario(usu_id),
  CONSTRAINT fk_pub_almacen
  FOREIGN KEY (pub_almacen) REFERENCES SQLITO.Almacen(alma_codigo),
  CONSTRAINT pub_prod_id
  FOREIGN KEY (pub_prod_id) REFERENCES SQLITO.Producto(prod_id),
)
----------------------------
--- Creacion Table: Venta --
----------------------------
CREATE TABLE SQLITO.Venta (
  vent_cod INT PRIMARY KEY NOT NULL,
  vent_usu_cliente INT NOT NULL,
  vent_fecha_hora DATE,
  vent_importe_total DECIMAL(18,2)
  CONSTRAINT fk_vent_usu_cliente
  FOREIGN KEY (vent_usu_cliente) REFERENCES SQLITO.Usuario(usu_id)
)

------------------------------------
--- Creacion Table: DetalleVenta  --
------------------------------------
CREATE TABLE SQLITO.DetalleVenta(
  det_vent_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  det_vent_pub_codigo NVARCHAR(50) NOT NULL,
  det_vent_cantidad DECIMAL(18,0),
  det_vent_precio_unitario DECIMAL(18,2),
  det_vent_vent_nro INT NOT NULL,
  CONSTRAINT fk_det_vent_pub_codigo
  FOREIGN KEY (det_vent_pub_codigo) REFERENCES SQLITO.Publicacion(pub_codigo),
  CONSTRAINT fk_det_vent_vent_nro 
  FOREIGN KEY (det_vent_vent_nro) REFERENCES SQLITO.Venta(vent_cod)
)
------------------------------------
--- Creacion Table: MediosDePago  --
------------------------------------

CREATE TABLE SQLITO.MedioDePago(
  mdp_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  mdp_tipo NVARCHAR(50) NOT NULL,
  mdp_detalle NVARCHAR(50) NOT NULL ,
)
-------------------------------------
--- Creacion Table: DetalleDePago  --
-------------------------------------
CREATE TABLE SQLITO.DetalleDePago(
  det_pago_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  det_pago_mdp_id INT NOT NULL,
  det_pago_numero DECIMAL(18,0),
  det_pago_nro_tarjeta NVARCHAR(50), 
  det_pago_fecha_venc DATE,
  det_pago_cuotas DECIMAL(18,0),
  det_pago_importe DECIMAL(18,2),
  CONSTRAINT fk_det_pago_mdp_id
  FOREIGN KEY (det_pago_mdp_id) REFERENCES SQLITO.MedioDePago(mdp_id)
)

----------------------------
--- Creacion Table: Pago  --
----------------------------
CREATE TABLE SQLITO.Pago (
  pago_numero INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  pago_venta_nro INT NOT NULL,
  pago_importe DECIMAL(18,2),
  pago_fecha date,
  CONSTRAINT fk_pago_venta_nro
  FOREIGN KEY (pago_venta_nro) REFERENCES SQLITO.Venta(vent_cod)
)

-------------------------------
--- Creacion Table: Factura  --
-------------------------------
CREATE TABLE SQLITO.Factura(
  fac_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  fac_numero DECIMAL(18,0)  NOT NULL,
  fac_fecha DATE,
  fac_usu_id INT NOT NULL,
  fac_total DECIMAL(18,2),
  CONSTRAINT fk_fac_usu_id
  FOREIGN KEY (fac_usu_id) REFERENCES SQLITO.Usuario(usu_id),
)
--------------------------------------
--- Creacion Table: DetalleFactura  --
--------------------------------------
CREATE TABLE SQLITO.DetalleFactura(
  det_fact_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  det_fact_pub_codigo NVARCHAR(50) NOT NULL,
  det_fact_precio DECIMAL(18,2),
  det_fact_cant DECIMAL(18,0),
  det_fact_subtotal  DECIMAL(18,2),
  det_fact_concepto NVARCHAR(100) NOT NULL ,
  det_fact_fac_id INT NOT NULL,
  CONSTRAINT fk_det_fact_pub_codigo
  FOREIGN KEY (det_fact_pub_codigo) REFERENCES SQLITO.Publicacion(pub_codigo),
  CONSTRAINT fk_det_fact_fac_id
  FOREIGN KEY (det_fact_fac_id) REFERENCES SQLITO.Factura(fac_id)
)
--------------------------------
--- Creacion Table: Concepto  --
--------------------------------
CREATE TABLE SQLITO.Concepto(
  concp_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  concp_nombre NVARCHAR(50),
  concp_descripcion NVARCHAR(50) NOT NULL,
)
---------------------------------
--- Creacion Table: TipoEnvio  --
---------------------------------
CREATE TABLE SQLITO.TipoEnvio(
  tipo_env_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  tipo_env_detalle NVARCHAR(50) NOT NULL ,
)
--------------------------------
--- Creacion Table: UsuDom  --
--------------------------------
CREATE TABLE SQLITO.UsuDom(
  usu_dom_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  usu_dom_usu_id INT NOT NULL,
  usu_dom_dom_id INT NOT NULL,
  CONSTRAINT fk_usu_dom_usu_id
  FOREIGN KEY (usu_dom_usu_id) REFERENCES SQLITO.Usuario(usu_id),
  CONSTRAINT fk_usu_dom_dom_id
  FOREIGN KEY (usu_dom_dom_id) REFERENCES SQLITO.Domicilio(dom_id)
)
-----------------------------
--- Creacion Table: Envio  --
-----------------------------
CREATE TABLE SQLITO.Envio(
  env_nro INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  env_vent_numero INT NOT NULL,
  env_usu_dom_envio INT NOT NULL,
  env_tipo_envio INT NOT NULL,
  env_fecha_programada DATE,
  env_inicio_hs_entrega DECIMAL(18,0),
  env_fin_hs_entrega DECIMAL(18,0),
  env_fecha_hs_entregado DATETIME,
  env_estado SMALLINT,
  env_costo DECIMAL(18,2),
  CONSTRAINT fk_env_vent_numero
  FOREIGN KEY (env_vent_numero) REFERENCES SQLITO.Venta(vent_cod),
  CONSTRAINT fk_env_domicilio_envio
  FOREIGN KEY (env_usu_dom_envio) REFERENCES SQLITO.UsuDom(usu_dom_id),
  CONSTRAINT fk_env_tipo_envio
  FOREIGN KEY (env_tipo_envio) REFERENCES SQLITO.TipoEnvio(tipo_env_id)
)

-----------------------
--- Creacion Indices --
-----------------------

CREATE INDEX Producto ON SQLITO.Producto(prod_sub_rubro,prod_marca,prod_modelo,prod_descripcion,prod_precio)
GO
CREATE INDEX Subrubro ON SQLITO.Subrubro(srub_detalle,srub_rub)
GO
CREATE INDEX Cliente ON SQLITO.Cliente(clie_nombre,clie_apellido,clie_fecha_nacimiento,clie_dni)
GO
CREATE INDEX Marca ON SQLITO.Marca(marc_id,marc_detalle)
GO
CREATE INDEX Publicacion ON SQLITO.Publicacion(pub_codigo,pub_descripcion,pub_fecha_inicio, pub_fecha_fin,pub_vend_usuario,pub_costo,pub_porc_venta,pub_precio_unitario,pub_stock)
GO
CREATE INDEX Almacen ON SQLITO.Almacen(alma_codigo,alma_detalle,alma_dom_id,alma_costo_dia_al)
GO
CREATE INDEX Domicilio ON SQLITO.Domicilio(dom_id,dom_loc_id,dom_calle,dom_nro_calle,dom_piso,dom_depto,dom_cp)
GO
CREATE INDEX Usuario ON SQLITO.Usuario(usu_id,usu_vend_codigo,usu_nombre,usu_psw,usu_fecha_creacion,usu_mail)
GO
CREATE INDEX Venta ON SQLITO.Venta(vent_cod,vent_usu_cliente,vent_fecha_hora,vent_importe_total)
GO
CREATE INDEX Localidad ON SQLITO.Localidad(loc_id,loc_prov_id,loc_nombre)
GO
CREATE INDEX Provincia ON SQLITO.Provincia (prov_id,prov_nombre)
GO

-------------------------
--- Creacion Funciones --
-------------------------

-------------------------------
--- Creacion Store Procedure --
-------------------------------
CREATE PROCEDURE SQLITO.migrar_marca
AS
INSERT INTO SQLITO.Marca(marc_detalle)
	SELECT 
		DISTINCT (PRODUCTO_MARCA)
	FROM gd_esquema.Maestra
	WHERE PRODUCTO_MARCA	IS NOT NULL
	ORDER BY PRODUCTO_MARCA
GO

CREATE PROC SQLITO.migrar_modelo
AS
INSERT INTO SQLITO.Modelo(mod_marc_id,mod_detalle)
	SELECT DISTINCT
		marc_id,
		PRODUCTO_MOD_DESCRIPCION
	FROM gd_esquema.Maestra
	join SQLITO.Marca
	on marc_detalle = PRODUCTO_MARCA
	WHERE PRODUCTO_MOD_CODIGO IS NOT NULL
		AND PRODUCTO_MOD_DESCRIPCION IS NOT NULL
	ORDER BY PRODUCTO_MOD_DESCRIPCION
GO

CREATE PROC SQLITO.migrar_rubro
AS
INSERT INTO SQLITO.Rubro(rub_detalle)
	SELECT DISTINCT 
		PRODUCTO_RUBRO_DESCRIPCION
	FROM gd_esquema.Maestra
	WHERE PRODUCTO_RUBRO_DESCRIPCION IS NOT NULL
GO

CREATE PROC SQLITO.migrar_subrubro
AS
INSERT INTO SQLITO.Subrubro(srub_detalle,srub_rub)
	SELECT DISTINCT 
		PRODUCTO_SUB_RUBRO,
		rub_id
	FROM gd_esquema.Maestra
	join SQLITO.Rubro
	on PRODUCTO_RUBRO_DESCRIPCION = rub_detalle
	WHERE PRODUCTO_SUB_RUBRO IS NOT NULL
GO
-- Tipo envio 
CREATE PROC SQLITO.migrar_tipoEnvio
AS
INSERT INTO SQLITO.tipoEnvio(tipo_env_detalle)
	SELECT DISTINCT ENVIO_TIPO
	FROM gd_esquema.Maestra
	where ENVIO_TIPO IS NOT NULL
GO

-- Cliente 

CREATE PROC SQLITO.migrar_cliente
AS
INSERT INTO SQLITO.Cliente(clie_nombre,clie_apellido,clie_fecha_nacimiento,clie_dni)
	SELECT DISTINCT CLIENTE_NOMBRE,
                  CLIENTE_APELLIDO,
                  CLIENTE_FECHA_NAC,
                  CLIENTE_DNI
	FROM gd_esquema.Maestra
	WHERE CLIENTE_NOMBRE IS NOT NULL 
        AND CLIENTE_APELLIDO IS NOT NULL
        AND CLIENTE_FECHA_NAC IS NOT NULL
        AND CLIENTE_DNI IS NOT NULL
GO

-- Vendedor

CREATE PROC SQLITO.migrar_vendedor
AS
INSERT INTO SQLITO.Vendedor(vend_razon_social,
							vend_cuit)
	SELECT DISTINCT VENDEDOR_RAZON_SOCIAL,
    VENDEDOR_CUIT
	FROM gd_esquema.Maestra
	WHERE VENDEDOR_RAZON_SOCIAL IS NOT NULL
GO
		
CREATE PROC SQLITO.migrar_medio_de_pago
AS
INSERT INTO SQLITO.MedioDePago(mdp_tipo,mdp_detalle)
	SELECT DISTINCT 
		PAGO_TIPO_MEDIO_PAGO,
		PAGO_MEDIO_PAGO
	FROM gd_esquema.Maestra
	WHERE PAGO_MEDIO_PAGO IS NOT NULL
GO

CREATE PROC SQLITO.migrar_Provincia
AS
BEGIN
INSERT INTO SQLITO.Provincia(prov_nombre)
	SELECT DISTINCT  ALMACEN_PROVINCIA 
	FROM gd_esquema.Maestra
	WHERE ALMACEN_PROVINCIA IS NOT NULL

INSERT INTO SQLITO.Provincia(prov_nombre)
	SELECT DISTINCT  CLI_USUARIO_DOMICILIO_PROVINCIA 
	FROM gd_esquema.Maestra
	WHERE CLI_USUARIO_DOMICILIO_PROVINCIA IS NOT NULL and CLI_USUARIO_DOMICILIO_PROVINCIA NOT IN (SELECT prov_nombre FROM SQLITO.Provincia)
INSERT INTO SQLITO.Provincia(prov_nombre)
	SELECT DISTINCT  VEN_USUARIO_DOMICILIO_PROVINCIA 
	FROM gd_esquema.Maestra
	WHERE VEN_USUARIO_DOMICILIO_PROVINCIA IS NOT NULL and VEN_USUARIO_DOMICILIO_PROVINCIA NOT IN (SELECT prov_nombre FROM SQLITO.Provincia)
END
GO

CREATE PROC SQLITO.migrar_localidad
AS 
BEGIN
INSERT INTO SQLITO.Localidad(loc_nombre,loc_prov_id)
	select distinct ALMACEN_Localidad,prov_id
FROM gd_esquema.Maestra
	JOIN SQLITO.Provincia on prov_nombre = ALMACEN_PROVINCIA
	WHERE ALMACEN_Localidad IS NOT NULL
union
SELECT DISTINCT CLI_USUARIO_DOMICILIO_LOCALIDAD,
					prov_id
	FROM gd_esquema.Maestra
	JOIN SQLITO.Provincia on prov_nombre = CLI_USUARIO_DOMICILIO_PROVINCIA
	WHERE CLI_USUARIO_DOMICILIO_LOCALIDAD IS NOT NULL
union
SELECT DISTINCT VEN_USUARIO_DOMICILIO_LOCALIDAD,
					prov_id
	FROM gd_esquema.Maestra
	JOIN SQLITO.Provincia on prov_nombre = VEN_USUARIO_DOMICILIO_PROVINCIA
	WHERE VEN_USUARIO_DOMICILIO_LOCALIDAD IS NOT NULL
END
GO

CREATE PROC SQLITO.migrar_domicilio
AS
BEGIN
INSERT INTO SQLITO.Domicilio(dom_calle,
							 dom_cp,
							 dom_loc_id,
							 dom_nro_calle
							 )

	SELECT DISTINCT ALMACEN_CALLE,
					ALMACEN_CODIGO,
					loc_id,
					ALMACEN_NRO_CALLE
	FROM gd_esquema.Maestra 
	JOIN SQLITO.Provincia on ALMACEN_PROVINCIA = prov_nombre
	JOIN SQLITO.Localidad on ALMACEN_Localidad = loc_nombre and loc_prov_id = prov_id

INSERT INTO SQLITO.Domicilio(dom_calle,dom_cp,dom_depto,dom_loc_id,dom_nro_calle,dom_piso)
SELECT DISTINCT CLI_USUARIO_DOMICILIO_CALLE,
					CLI_USUARIO_DOMICILIO_CP,
					CLI_USUARIO_DOMICILIO_DEPTO,
					loc_id,
					CLI_USUARIO_DOMICILIO_NRO_CALLE,
					CLI_USUARIO_DOMICILIO_PISO
	FROM gd_esquema.Maestra
	JOIN SQLITO.Provincia on CLI_USUARIO_DOMICILIO_PROVINCIA = prov_nombre
	JOIN SQLITO.Localidad on CLI_USUARIO_DOMICILIO_LOCALIDAD = loc_nombre and loc_prov_id = prov_id
union
SELECT DISTINCT VEN_USUARIO_DOMICILIO_CALLE,
					VEN_USUARIO_DOMICILIO_CP,
					VEN_USUARIO_DOMICILIO_DEPTO,
					loc_id,
					VEN_USUARIO_DOMICILIO_NRO_CALLE,
					VEN_USUARIO_DOMICILIO_PISO
	FROM gd_esquema.Maestra
	JOIN SQLITO.Provincia on VEN_USUARIO_DOMICILIO_PROVINCIA = prov_nombre
	JOIN SQLITO.Localidad on VEN_USUARIO_DOMICILIO_LOCALIDAD = loc_nombre and loc_prov_id = prov_id
END
GO

CREATE PROC SQLITO.migrar_usuario
AS
BEGIN
INSERT INTO SQLITO.Usuario(usu_clie_codigo,
						   usu_nombre,
						   usu_psw,
						   usu_fecha_creacion,
						   usu_mail)
	SELECT DISTINCT clie_codigo,
				    CLI_USUARIO_NOMBRE,
					CLI_USUARIO_PASS,
					CLI_USUARIO_FECHA_CREACION,
					CLIENTE_MAIL
	FROM gd_esquema.Maestra
	join SQLITO.Cliente
	on clie_nombre = CLIENTE_NOMBRE and 
		clie_apellido = CLIENTE_APELLIDO and 
		clie_dni = CLIENTE_DNI and
		clie_fecha_nacimiento = CLIENTE_FECHA_NAC
	where CLI_USUARIO_NOMBRE IS NOT NULL


	
INSERT INTO SQLITO.Usuario(usu_vend_codigo,
						   usu_nombre,usu_psw,
						   usu_fecha_creacion,
						   usu_mail
						   )	
	SELECT DISTINCT vend_codigo,
					VEN_USUARIO_NOMBRE,
					VEN_USUARIO_PASS,
					VEN_USUARIO_FECHA_CREACION,
					VENDEDOR_MAIL
	FROM gd_esquema.Maestra
	join SQLITO.Vendedor
	on vend_razon_social = VENDEDOR_RAZON_SOCIAL and
		vend_cuit = VENDEDOR_CUIT
	where VEN_USUARIO_NOMBRE IS NOT NULL

END
GO

CREATE PROC SQLITO.migrar_domicilios_usuarios
AS
BEGIN
INSERT INTO SQLITO.UsuDom(usu_dom_dom_id,
						  usu_dom_usu_id)
	SELECT DISTINCT dom_id, 
					usu_id
	FROM gd_esquema.Maestra
	JOIN SQLITO.Usuario on CLI_USUARIO_NOMBRE = usu_nombre and CLI_USUARIO_PASS = usu_psw and
						   CLI_USUARIO_FECHA_CREACION = usu_fecha_creacion and CLIENTE_MAIL = usu_mail
	join SQLITO.Provincia on CLI_USUARIO_DOMICILIO_PROVINCIA = prov_nombre
	join SQLITO.Localidad on CLI_USUARIO_DOMICILIO_LOCALIDAD = loc_nombre and loc_prov_id = prov_id
	JOIN SQLITO.Domicilio on CLI_USUARIO_DOMICILIO_CALLE = dom_calle and CLI_USUARIO_DOMICILIO_CP = dom_cp and
							 CLI_USUARIO_DOMICILIO_DEPTO = dom_depto and CLI_USUARIO_DOMICILIO_NRO_CALLE = dom_nro_calle
							 and CLI_USUARIO_DOMICILIO_PISO = dom_piso
	where usu_clie_codigo IS NOT NULL

INSERT INTO SQLITO.UsuDom(usu_dom_dom_id,usu_dom_usu_id)
	SELECT DISTINCT dom_id, 
					usu_id
	FROM gd_esquema.Maestra
	JOIN SQLITO.Usuario on VEN_USUARIO_NOMBRE = usu_nombre and VEN_USUARIO_PASS = usu_psw and
						   VEN_USUARIO_FECHA_CREACION = usu_fecha_creacion and VENDEDOR_MAIL = usu_mail
	join SQLITO.Provincia on VEN_USUARIO_DOMICILIO_PROVINCIA = prov_nombre
	join SQLITO.Localidad on VEN_USUARIO_DOMICILIO_LOCALIDAD = loc_nombre and loc_prov_id = prov_id
	JOIN SQLITO.Domicilio on VEN_USUARIO_DOMICILIO_CALLE = dom_calle and VEN_USUARIO_DOMICILIO_CP = dom_cp and
							 VEN_USUARIO_DOMICILIO_DEPTO = dom_depto and VEN_USUARIO_DOMICILIO_NRO_CALLE = dom_nro_calle
							 and VEN_USUARIO_DOMICILIO_PISO = dom_piso and loc_id = dom_loc_id
	where usu_vend_codigo IS NOT NULL
END
GO

CREATE PROC SQLITO.migrar_venta
AS
INSERT INTO SQLITO.Venta(vent_cod,
						 vent_usu_cliente,
						 vent_fecha_hora,
						 vent_importe_total
						 )
	SELECT DISTINCT VENTA_CODIGO,
					u.usu_id,
					VENTA_FECHA,
					VENTA_TOTAL
	FROM gd_esquema.Maestra M
	JOIN SQLITO.Cliente on clie_dni = CLIENTE_DNI and clie_apellido = CLIENTE_APELLIDO -- 103592
	JOIN SQLITO.Usuario u on u.usu_id = Cliente.clie_codigo
	where VENTA_CODIGO IS NOT NULL
GO

CREATE PROC SQLITO.migrar_pago
AS
INSERT INTO SQLITO.Pago(pago_venta_nro,
						pago_importe,
						pago_fecha)
	SELECT DISTINCT vent_cod,
					M.PAGO_IMPORTE,
					M.PAGO_FECHA
	FROM gd_esquema.Maestra M
	join SQLITO.Venta on VENTA_CODIGO = vent_cod
GO

CREATE PROC SQLITO.migrar_producto
AS
INSERT INTO SQLITO.Producto(prod_codigo,
							prod_sub_rubro,
							prod_marca,
							prod_modelo,
							prod_descripcion,
							prod_precio)
	SELECT DISTINCT
		PRODUCTO_CODIGO,
		srub_id,
		marc_id,
		mod_id,
		PRODUCTO_DESCRIPCION,
		PRODUCTO_PRECIO
	FROM gd_esquema.Maestra
	join SQLITO.Subrubro
	on PRODUCTO_SUB_RUBRO = srub_detalle
	join SQLITO.Marca
	on PRODUCTO_MARCA = marc_detalle
	join SQLITO.Modelo
	on PRODUCTO_MOD_DESCRIPCION = mod_detalle	
	WHERE PRODUCTO_CODIGO IS NOT NULL
GO

--- PUBLICACION

CREATE PROC SQLITO.migrar_publicacion
AS
INSERT INTO SQLITO.Publicacion(pub_codigo,
							   pub_descripcion,
							   pub_fecha_inicio,
							   pub_fecha_fin,
							   pub_vend_usuario,
							   pub_costo,
							   pub_porc_venta,
							   pub_precio_unitario,
							   pub_stock,
							   pub_almacen,
							   pub_prod_id)

	SELECT DISTINCT PUBLICACION_CODIGO,
                    PUBLICACION_DESCRIPCION,
                    PUBLICACION_FECHA,
                    PUBLICACION_FECHA_V,
                    usu_id,
                    PUBLICACION_COSTO,
                    PUBLICACION_PORC_VENTA,
                    PUBLICACION_PRECIO,
                    PUBLICACION_STOCK,
					alma_codigo,
					prod_id
    FROM gd_esquema.Maestra
	join SQLITO.Marca on PRODUCTO_MARCA = marc_detalle
	join SQLITO.Modelo on PRODUCTO_MOD_DESCRIPCION = mod_detalle and marc_id = mod_marc_id
	join SQLITO.Rubro on PRODUCTO_RUBRO_DESCRIPCION = rub_detalle
	join SQLITO.Subrubro on PRODUCTO_SUB_RUBRO = srub_detalle and srub_rub = rub_id
	join SQLITO.Producto on PRODUCTO_CODIGO = prod_codigo and prod_marca = marc_id and prod_modelo = mod_id and prod_sub_rubro = srub_id and PRODUCTO_PRECIO = prod_precio
	JOIN SQLITO.Vendedor on VENDEDOR_CUIT = vend_cuit and VENDEDOR_RAZON_SOCIAL = vend_razon_social
    JOIN SQLITO.Usuario on usu_vend_codigo = vend_codigo
	JOIN SQLITO.Almacen on ALMACEN_CODIGO = alma_codigo
    WHERE PUBLICACION_CODIGO IS NOT NULL
GO

CREATE PROC SQLITO.migrar_almacen
AS
INSERT INTO SQLITO.Almacen(	alma_codigo,
							alma_dom_id,
							alma_costo_dia_al
							)
	SELECT DISTINCT ALMACEN_CODIGO,
					dom_id,
					ALMACEN_COSTO_DIA_AL
	FROM gd_esquema.Maestra
	JOIN SQLITO.Domicilio on dom_nro_calle = ALMACEN_NRO_CALLE and dom_calle = ALMACEN_CALLE
	WHERE ALMACEN_CODIGO IS NOT NULL
GO

CREATE PROC SQLITO.migrar_envio
AS
INSERT INTO SQLITO.Envio(env_vent_numero,
						 env_usu_dom_envio,
						 env_tipo_envio,
						 env_fecha_programada,
						 env_inicio_hs_entrega,
						 env_fin_hs_entrega,
						 env_fecha_hs_entregado,
						 env_costo)
	SELECT DISTINCT vent_cod,
					usu_dom_id,
					tipo_env_id,
					ENVIO_FECHA_PROGAMADA,
					ENVIO_HORA_INICIO,
					ENVIO_HORA_FIN_INICIO,
					ENVIO_FECHA_ENTREGA,
					ENVIO_COSTO
	FROM gd_esquema.Maestra
	JOIN SQLITO.Venta on VENTA_CODIGO = vent_cod
	JOIN SQLITO.TipoEnvio on ENVIO_TIPO = tipo_env_detalle
	join SQLITO.Provincia on CLI_USUARIO_DOMICILIO_PROVINCIA = prov_nombre
	join SQLITO.Localidad on CLI_USUARIO_DOMICILIO_LOCALIDAD = loc_nombre and loc_prov_id = prov_id
	JOIN SQLITO.Domicilio ON CLI_USUARIO_DOMICILIO_CALLE = dom_calle and
							 CLI_USUARIO_DOMICILIO_CP = dom_cp and
							 CLI_USUARIO_DOMICILIO_DEPTO = dom_depto and
							 CLI_USUARIO_DOMICILIO_NRO_CALLE = dom_nro_calle and
							 CLI_USUARIO_DOMICILIO_PISO = dom_piso 
							 and dom_loc_id = loc_id
	JOIN SQLITO.Usuario on usu_nombre = CLI_USUARIO_NOMBRE and usu_psw = CLI_USUARIO_PASS
	JOIN SQLITO.UsuDom on usu_dom_dom_id = dom_id and usu_id = usu_dom_usu_id
GO

CREATE PROC SQLITO.migrar_concepto
AS
INSERT INTO SQLITO.Concepto(concp_descripcion)
	SELECT DISTINCT FACTURA_DET_TIPO 
	FROM gd_esquema.Maestra 
	WHERE FACTURA_DET_TIPO IS NOT NULL
GO

CREATE PROC SQLITO.migrar_detalleVenta
AS
INSERT INTO SQLITO.DetalleVenta(det_vent_pub_codigo,
                                det_vent_cantidad,
                                det_vent_precio_unitario,
                                det_vent_vent_nro)
  SELECT DISTINCT
    pub_codigo,
    VENTA_DET_CANT,
    VENTA_DET_PRECIO,
    vent_cod
  FROM gd_esquema.Maestra
  JOIN SQLITO.Venta ON Venta.vent_cod = VENTA_CODIGO
  JOIN SQLITO.Publicacion ON Publicacion.pub_codigo = PUBLICACION_CODIGO

GO


CREATE PROC SQLITO.migrar_detalleDePago
AS
INSERT INTO SQLITO.DetalleDePago(det_pago_mdp_id,
								 det_pago_numero,
								 det_pago_nro_tarjeta,
								 det_pago_fecha_venc,
								 det_pago_cuotas,
								 det_pago_importe
								)
	SELECT DISTINCT 
				mdp_id,
				pago_numero,
				PAGO_NRO_TARJETA,
				PAGO_FECHA_VENC_TARJETA,
				PAGO_CANT_CUOTAS,
				M.PAGO_IMPORTE
	FROM gd_esquema.Maestra M
	JOIN SQLITO.MedioDePago on mdp_detalle = PAGO_MEDIO_PAGO
	JOIN SQLITO.Venta on vent_cod = VENTA_CODIGO
	JOIN SQLITO.Pago p on  p.pago_venta_nro = vent_cod
GO

CREATE PROC SQLITO.migrar_factura
AS
INSERT INTO SQLITO.Factura(fac_numero,
						   fac_fecha,
						   fac_total,
						   fac_usu_id)

	SELECT DISTINCT FACTURA_NUMERO,
					FACTURA_FECHA,
					FACTURA_TOTAL,
					usu_id
	FROM gd_esquema.Maestra
	JOIN SQLITO.Publicacion on PUBLICACION_CODIGO = pub_codigo
	JOIN SQLITO.Usuario on pub_vend_usuario = usu_id
	WHERE FACTURA_NUMERO IS NOT NULL

GO

CREATE PROC SQLITO.migrar_detalle_factura
AS
INSERT INTO SQLITO.DetalleFactura(det_fact_fac_id,
								  det_fact_concepto,
								  det_fact_precio,
								  det_fact_cant,
								  det_fact_pub_codigo,
								  det_fact_subtotal)
	SELECT DISTINCT fac_id,
					concp_id,
					FACTURA_DET_PRECIO,
					FACTURA_DET_CANTIDAD,
					pub_codigo,
					FACTURA_DET_SUBTOTAL
	FROM gd_esquema.Maestra
	JOIN SQLITO.Publicacion on PUBLICACION_CODIGO = pub_codigo
	JOIN SQLITO.Factura on FACTURA_NUMERO = fac_numero
	JOIN SQLITO.Concepto on FACTURA_DET_TIPO = concp_descripcion
	WHERE FACTURA_NUMERO IS NOT NULL
GO

EXEC SQLITO.migrar_marca
EXEC SQLITO.migrar_modelo
EXEC SQLITO.migrar_rubro
EXEC SQLITO.migrar_tipoEnvio
EXEC SQLITO.migrar_vendedor
EXEC SQLITO.migrar_cliente
EXEC SQLITO.migrar_subrubro
EXEC SQLITO.migrar_medio_de_pago
EXEC SQLITO.migrar_provincia
EXEC SQLITO.migrar_localidad
EXEC SQLITO.migrar_domicilio
EXEC SQLITO.migrar_usuario
EXEC SQLITO.migrar_venta
EXEC SQLITO.migrar_pago
EXEC SQLITO.migrar_domicilios_usuarios
EXEC SQLITO.migrar_almacen
EXEC SQLITO.migrar_producto
EXEC SQLITO.migrar_publicacion
EXEC SQLITO.migrar_envio
EXEC SQLITO.migrar_concepto
EXEC SQLITO.migrar_detalleDePago
EXEC SQLITO.migrar_factura
EXEC SQLITO.migrar_detalleVenta
EXEC SQLITO.migrar_detalle_factura