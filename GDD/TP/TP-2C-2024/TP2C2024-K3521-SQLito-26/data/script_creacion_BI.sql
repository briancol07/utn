USE GD2C2024
GO

----------------------------------------------------------
---- Eliminacin de Tablas, Funciones, etc existentes ----
----------------------------------------------------------

-- TABLAS 

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_H_Factura')
  DROP TABLE SQLITO.BI_H_Factura
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_H_Envio')
  DROP TABLE SQLITO.BI_H_Envio 
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_H_Pago')
  DROP TABLE SQLITO.BI_H_Pago
GO
IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_H_Venta')
  DROP TABLE SQLITO.BI_H_Venta 
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_H_Publicacion')
  DROP TABLE SQLITO.BI_H_Publicacion
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_rango_etario')
  DROP TABLE SQLITO.BI_D_rango_etario
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_rango_horario')
  DROP TABLE SQLITO.BI_D_rango_horario
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_ubicacion')
  DROP TABLE SQLITO.BI_D_ubicacion  
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_Marca')
  DROP TABLE SQLITO.BI_D_Marca
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_TipoEnvio')
  DROP TABLE SQLITO.BI_D_TipoEnvio
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_Sub_rubro')
  DROP TABLE SQLITO.BI_D_Sub_rubro
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_Tiempo')
  DROP TABLE SQLITO.BI_D_Tiempo
GO

IF EXISTS ( SELECT name FROM sys.tables WHERE name = 'BI_D_MedioDePago')
  DROP TABLE SQLITO.BI_D_MedioDePago
GO


---- FUNCIONES 

IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'BI_get_rango_horario')
	DROP FUNCTION SQLITO.BI_get_rango_horario
GO
IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'BI_Rango_Etario_Cliente')
	DROP FUNCTION SQLITO.BI_Rango_Etario_Cliente 
GO

-- PROCEDURES
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_medios_de_pago' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_medios_de_pago
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_ubicacion' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_ubicacion
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_marca' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_marca
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tipo_envio' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_tipo_envio
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_Sub_rubro' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_Sub_rubro
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_rango_etario' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_rango_etario
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_rango_horario' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_rango_horario
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tiempo' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_tiempo
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_ventas' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_ventas
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_publicacion' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_publicacion
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_factura' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_factura
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_envio' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_envio
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_pagos' AND type='p')
	DROP PROCEDURE SQLITO.migrar_BI_pagos
GO
-- VISTAS
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_promedio_tiempo_publicaciones')
	DROP VIEW SQLITO.v_promedio_tiempo_publicaciones
GO
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_promedio_stock_inicial')
	DROP VIEW SQLITO.v_promedio_stock_inicial
GO
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_porcentaje_cumplimientos_envios')
	DROP VIEW SQLITO.v_porcentaje_cumplimientos_envios
GO
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_localidades_con_mayor_costo_envio')
	DROP VIEW SQLITO.v_localidades_con_mayor_costo_envio
GO
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_porcentaje_facturacion_concepto')
	DROP VIEW SQLITO.v_porcentaje_facturacion_concepto
GO
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_facturacion_provincia')
	DROP VIEW SQLITO.v_facturacion_provincia
GO
IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_venta_promedio_mensual')
	DROP VIEW SQLITO.v_venta_promedio_mensual
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_Pago_Cuotas')
	DROP VIEW SQLITO.v_Pago_Cuotas
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'v_rendimiento_rubros')
	DROP VIEW SQLITO.v_rendimiento_rubros
GO



----------------------------------------------------------
------------- Creacion de tablas  --------------
----------------------------------------------------------

----------------------------------------------------------
------------- Table SQLITO.BI_rango_etario  ----------
----------------------------------------------------------

CREATE TABLE SQLITO.BI_D_rango_etario (
	rango_etario_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	rango_etario_detalle NVARCHAR(6) NOT NULL,
)
GO

----------------------------------------------------------
------------- Table SQLITO.BI_rango_horario  ----------
----------------------------------------------------------

CREATE TABLE SQLITO.BI_D_rango_horario(
	rango_horario_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	rango_horario_minimo DECIMAL(3,0) NOT NULL ,
	rango_horario_maximo DECIMAL(3,0) NOT NULL,
)
GO

----------------------------------------------------------
------------- Table SQLITO.Marca  --------
----------------------------------------------------------

CREATE TABLE SQLITO.BI_D_Marca(
	marc_id INT PRIMARY KEY NOT NULL,
	marc_detalle NVARCHAR(50)NOT NULL,

)
GO

----------------------------------------------------------
------------- Table SQLITO.Sub_rubro  --------
----------------------------------------------------------

CREATE TABLE SQLITO.BI_D_Sub_rubro (
	srub_id INT PRIMARY KEY NOT NULL,
	srub_detalle NVARCHAR(50) NOT NULL,
	srub_rub_nombre NVARCHAR(50) NOT NULL,
)
GO
----------------------------------------------------------
------------- Table SQLITO.BI_Tiempo --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_D_Tiempo (
	tiemp_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	tiemp_anio	DECIMAL(5,0) NOT NULL,
	tiemp_mes	DECIMAL(3,0) NOT NULL,
	tiemp_cuatrimestre  DECIMAL(2,0) NOT NULL,
)
GO

----------------------------------------------------------
------------- Table SQLITO.MedioDePago --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_D_MedioDePago (
	mdp_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	mdp_detalle NVARCHAR(50) NOT NULL,
	mdp_cuotas DECIMAL(18,0) NOT NULL
)
GO

----------------------------------------------------------
------------- Table SQLITO.Ubicacion --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_D_Ubicacion (
	ubi_id INT PRIMARY KEY NOT NULL,
	ubi_prov NVARCHAR(50) NOT NULL,
	ubi_loc NVARCHAR(50) NOT NULL,
)
GO

----------------------------------------------------------
------------- Table SQLITO.BI_TipoEnvio (D) --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_D_TipoEnvio (
	tipo_env_id INT PRIMARY KEY NOT NULL,
	tipo_env_detalle NVARCHAR(50) NOT NULL,
)
GO

----------------------------------------------------------
------------- Table SQLITO.Publicacion (H)--------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_H_Publicacion (
	pub_marc_id INT NOT NULL,
	pub_srub_id INT NOT NULL,
	pub_tiemp_id INT NOT NULL, 
	pub_cantidad_stocks_inicial DECIMAL(18,2),
	pub_cantidad_publicaciones DECIMAL(18,0),
	pub_duracion_en_dias DECIMAL(18,2),
	CONSTRAINT fk_pub_marc_id
	FOREIGN KEY (pub_marc_id) REFERENCES SQLITO.BI_D_marca (marc_id),
	CONSTRAINT fk_pub_srub_id
	FOREIGN KEY (pub_srub_id) REFERENCES SQLITO.BI_D_Sub_rubro (srub_id),
	CONSTRAINT fk_pub_tiemp_id
	FOREIGN KEY (pub_tiemp_id) REFERENCES SQLITO.BI_D_tiempo (tiemp_id),
)
GO

----------------------------------------------------------
------------- Table SQLITO.Venta (H) --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_H_Venta(
	vent_srub_id INT NOT NULL,
	vent_tiemp_id INT NOT NULL,
	vent_ubi_almacen_id INT NOT NULL,
	vent_ubi_cliente_id INT NOT NULL,
	vent_retario_id INT NOT NULL,
	vent_importe_total DECIMAL(18,2) NOT NULL,
	vent_cant_ventas DECIMAL(18,0) NOT NULL
	CONSTRAINT fk_vent_srub_id
	FOREIGN KEY (vent_srub_id) REFERENCES SQLITO.BI_D_Sub_rubro (srub_id),
	CONSTRAINT fk_vent_tiemp_id
	FOREIGN KEY (vent_tiemp_id) REFERENCES SQLITO.BI_D_tiempo (tiemp_id),
	CONSTRAINT vent_ubi_almacen_id 
	FOREIGN KEY (vent_ubi_almacen_id) REFERENCES SQLITO.BI_D_ubicacion (ubi_id),
	CONSTRAINT vent_ubi_cliente_id 
	FOREIGN KEY (vent_ubi_cliente_id) REFERENCES SQLITO.BI_D_ubicacion (ubi_id),
	CONSTRAINT fk_vent_retario_id
	FOREIGN KEY (vent_retario_id) REFERENCES SQLITO.BI_D_rango_etario (rango_etario_id),
)
GO
----------------------------------------------------------
------------- Table SQLITO.Pago (H) --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_H_Pago(
	pag_tiemp_id INT NOT NULL,
	pag_ubi_cliente_id INT NOT NULL,
	pag_mdp_id INT NOT NULL,
	pag_total DECIMAL(18,2) NOT NULL,
	CONSTRAINT fk_pag_tiemp_id
	FOREIGN KEY (pag_tiemp_id) REFERENCES SQLITO.BI_D_tiempo (tiemp_id),
	CONSTRAINT fk_pag_ubi_cliente_id 
	FOREIGN KEY (pag_ubi_cliente_id) REFERENCES SQLITO.BI_D_ubicacion (ubi_id),
	CONSTRAINT fk_pag_mdp_id
	FOREIGN KEY (pag_mdp_id ) REFERENCES SQLITO.BI_D_MedioDePago (mdp_id),
)
GO

----------------------------------------------------------
------------- Table SQLITO.BI_Envio (H) --------
----------------------------------------------------------

CREATE TABLE SQLITO.BI_H_Envio (
	env_ubi_cliente_id INT NOT NULL,
	env_ubi_almacen_id INT NOT NULL,
	env_tiemp_id INT NOT NULL,
	env_tipo_env_id INT NOT NULL,
	env_cumplidos DECIMAL(18,0),
	env_cantidad DECIMAL(18,0) NOT NULL,
	env_costo DECIMAL(18,2) NOT NULL,
	CONSTRAINT env_ubi_cliente_id 
	FOREIGN KEY (env_ubi_cliente_id) REFERENCES SQLITO.BI_D_ubicacion (ubi_id),
	CONSTRAINT env_ubi_almacen_id 
	FOREIGN KEY (env_ubi_almacen_id) REFERENCES SQLITO.BI_D_ubicacion (ubi_id),
	CONSTRAINT fk_env_tiemp_id
	FOREIGN KEY (env_tiemp_id) REFERENCES SQLITO.BI_D_tiempo (tiemp_id),
	CONSTRAINT fk_env_tipo_env_id
	FOREIGN KEY (env_tipo_env_id) REFERENCES SQLITO.BI_D_TipoEnvio (tipo_env_id),
)
GO

----------------------------------------------------------
------------- Table SQLITO.BI_Factura  (H) --------
----------------------------------------------------------
CREATE TABLE SQLITO.BI_H_Factura (
	fact_ubi_id INT NOT NULL,
	fact_tiemp_id INT NOT NULL,
	fact_cantidad_facturada DECIMAL(18,2),
	fact_concepto_facturado DECIMAL(18,2),
	CONSTRAINT fk_fact_env_ubi_id
	FOREIGN KEY (fact_ubi_id) REFERENCES SQLITO.BI_D_ubicacion (ubi_id),
	CONSTRAINT fk_fact_tiemp_id
	FOREIGN KEY (fact_tiemp_id) REFERENCES SQLITO.BI_D_tiempo (tiemp_id),
)
GO


-------------------------------------------------------------------------


----------------------------------------------------------
------------- Creacion de Funciones  --------------
----------------------------------------------------------
-- Rango etario 
CREATE FUNCTION SQLITO.BI_Rango_Etario_Cliente (@fechaNacimiento DATE)
RETURNS NVARCHAR(50)
AS
BEGIN
    DECLARE @edad INT;
    DECLARE @rango NVARCHAR(10);

    -- Calcular la edad
    SET @edad = DATEDIFF(YEAR, @fechaNacimiento, GETDATE());
    IF (MONTH(@fechaNacimiento) > MONTH(GETDATE())) OR 
       (MONTH(@fechaNacimiento) = MONTH(GETDATE()) AND DAY(@fechaNacimiento) > DAY(GETDATE()))
    BEGIN
        SET @edad = @edad - 1;
    END

    -- Determinar el rango etario
    IF @edad < 25
        SET @rango = '< 25';
    ELSE IF @edad BETWEEN 25 AND 35
        SET @rango = '25-35';
    ELSE IF @edad BETWEEN 36 AND 50
        SET @rango = '36-50';
    ELSE
        SET @rango = '> 50';

    RETURN @rango;
END;
GO

------------------------------------------------------
----------------- Procedure MARCA (D) --------
-------------------------------------------------------

CREATE PROCEDURE SQLITO.migrar_BI_Marca
AS
INSERT INTO SQLITO.BI_D_marca ( 
	marc_id,
	marc_detalle
) 
select marc_id, marc_detalle
from SQLITO.marca

GO

------------------------------------------------------
----------------- Procedure  dimension_medioDePago(D) 
-------------------------------------------------------

CREATE PROCEDURE SQLITO.migrar_BI_medios_de_pago
AS
INSERT INTO SQLITO.BI_D_MedioDePago (
	BI_MedioDePago.mdp_detalle,
	BI_MedioDePago.mdp_cuotas
)
SELECT distinct SQLITO.MedioDePago.mdp_detalle,
								  det_pago_cuotas
	FROM SQLITO.MedioDePago
	JOIN SQLITO.DetalleDePago
		ON SQLITO.MedioDePago.mdp_id = SQLITO.DetalleDePago.det_pago_mdp_id
GO

------------------------------------------------------
----------------- Procedure  dimension_ubicacion(D) 
-------------------------------------------------------
CREATE PROC	SQLITO.migrar_BI_ubicacion
AS
INSERT INTO SQLITO.BI_D_Ubicacion(ubi_id,
								 ubi_prov,
								 ubi_loc)
	select 	loc_id,
			prov_nombre,
			loc_nombre
	from SQLITO.Localidad join SQLITO.Provincia on prov_id = loc_prov_id
GO

------------------------------------------------------
----------------- Procedure  dimension_tipo_envio(D) 
------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_tipo_envio
AS
INSERT INTO SQLITO.BI_D_TipoEnvio(tipo_env_id,
								  tipo_env_detalle)
select tipo_env_id,
	   tipo_env_detalle 
from SQLITO.TipoEnvio
GO

------------------------------------------------------
----------------- Procedure  dimension_subrubro_rubro(D) 
------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_Sub_rubro
AS
INSERT INTO SQLITO.BI_D_Sub_rubro(srub_id,
								 srub_detalle,
								 srub_rub_nombre)
select srub_id,
	   srub_detalle,
	   rub_detalle 
from SQLITO.Subrubro 
join SQLITO.Rubro on srub_rub = rub_id
GO

------------------------------------------------------
----------------- Procedure  dimension_rango_etario(D) 
------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_rango_etario
AS
INSERT INTO SQLITO.BI_D_rango_etario(rango_etario_detalle) 
	VALUES	('< 25'),
			('25-35'),
			('36-50'),
			('> 50')
GO

------------------------------------------------------
----------------- Procedure  dimension_rango_horario(D) 
------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_rango_horario
AS
INSERT INTO SQLITO.BI_D_rango_horario(rango_horario_minimo,
									  rango_horario_maximo) 
VALUES (0,6),
	  (6,12),
	  (12,18), 
	  (18,24)
GO

-------------------------------------------------------
----------------- Procedure  dimension_tiempo(D) -----
-------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_tiempo
AS
INSERT INTO SQLITO.BI_D_Tiempo(tiemp_anio,
							   tiemp_mes,
							   tiemp_cuatrimestre)
	select distinct YEAR(fac_fecha),
					MONTH(fac_fecha),
					DATEPART(QUARTER,fac_fecha) 
	from SQLITO.Factura
	union
	select distinct YEAR(vent_fecha_hora),
					MONTH(vent_fecha_hora),
					DATEPART(QUARTER,vent_fecha_hora) 
	from SQLITO.Venta
	union
	select distinct YEAR(pub_fecha_inicio),
					MONTH(pub_fecha_inicio),
					DATEPART(QUARTER,pub_fecha_inicio) 
	from SQLITO.Publicacion
	union
	select distinct YEAR(pub_fecha_fin),
					MONTH(pub_fecha_fin),
					DATEPART(QUARTER,pub_fecha_fin) 
	from SQLITO.Publicacion
	union
	select distinct YEAR(det_pago_fecha_venc),
					MONTH(det_pago_fecha_venc),
					DATEPART(QUARTER,det_pago_fecha_venc) 
	from SQLITO.DetalleDePago
	union
	select distinct YEAR(env_fecha_programada),
					MONTH(env_fecha_programada),
					DATEPART(QUARTER,env_fecha_programada) 
	from SQLITO.Envio
	union
	select distinct YEAR(env_fecha_hs_entregado),
					MONTH(env_fecha_hs_entregado),
					DATEPART(QUARTER,env_fecha_hs_entregado) 
	from SQLITO.Envio
GO

------------------------------------------------------
-------------- Procedure  hecho_ventas ----------
------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_ventas
AS
INSERT INTO SQLITO.BI_H_venta(	vent_srub_id,
								vent_tiemp_id,
								vent_ubi_almacen_id,
								vent_ubi_cliente_id,
								vent_retario_id ,
								vent_importe_total,
								vent_cant_ventas
								)

select distinct srub_id,
tiemp_id,
ubi_alm.ubi_id,
ubi_c.ubi_id,
rango_etario_id,
SUM(vent_importe_total), 
COUNT(*)
from SQLITO.Venta
join SQLITO.DetalleVenta on det_vent_vent_nro = vent_cod
join SQLITO.Publicacion on det_vent_pub_codigo = pub_codigo
join SQLITO.Producto on pub_prod_id = prod_id
join SQLITO.BI_D_Sub_rubro on prod_sub_rubro = srub_id
join SQLITO.BI_D_Tiempo on YEAR(vent_fecha_hora) = tiemp_anio and month(vent_fecha_hora) = tiemp_mes
join SQLITO.Almacen on pub_almacen = alma_codigo
join SQLITO.Domicilio dom_alm on dom_alm.dom_id = alma_dom_id
join SQLITO.BI_D_Ubicacion ubi_alm on ubi_alm.ubi_id = dom_alm.dom_loc_id
join SQLITO.Envio on env_vent_numero = vent_cod
join SQLITO.UsuDom on usu_dom_id = env_usu_dom_envio
join SQLITO.Domicilio dom_c on dom_c.dom_id = usu_dom_dom_id
join SQLITO.BI_D_Ubicacion ubi_c on ubi_c.ubi_id = dom_c.dom_loc_id
join SQLITO.Usuario on vent_usu_cliente = usu_id
join SQLITO.Cliente on usu_clie_codigo = clie_codigo
join SQLITO.BI_D_rango_etario on rango_etario_detalle = SQLITO.BI_Rango_Etario_Cliente(clie_fecha_nacimiento)
group by srub_id,
tiemp_id,
ubi_alm.ubi_id,
ubi_c.ubi_id,
rango_etario_id
GO	
------------------------------------------------------
-------------- Procedure  dimension_pago ----------
------------------------------------------------------
CREATE PROC SQLITO.migrar_BI_pagos
AS
INSERT INTO SQLITO.BI_H_Pago(	pag_tiemp_id,
								pag_ubi_cliente_id,
								pag_mdp_id,
								pag_total
								)
select tiemp_id,ubi_id,bi_mdp.mdp_id,sum(pago_importe)
from SQLITO.Pago
join SQLITO.BI_D_Tiempo on YEAR(pago_fecha) = tiemp_anio and MONTH(pago_fecha) = tiemp_mes
join SQLITO.Venta on vent_cod = pago_venta_nro
join SQLITO.Envio on env_vent_numero = vent_cod
join SQLITO.UsuDom on usu_dom_id = env_usu_dom_envio
join SQLITO.Domicilio on dom_id = usu_dom_dom_id
join SQLITO.BI_D_Ubicacion on ubi_id = dom_loc_id
join SQLITO.DetalleDePago on det_pago_numero = pago_numero
join SQLITO.MedioDePago mdp on det_pago_mdp_id = mdp.mdp_id
join SQLITO.BI_D_MedioDePago bi_mdp on det_pago_cuotas = bi_mdp.mdp_cuotas and mdp.mdp_detalle = bi_mdp.mdp_detalle
group by tiemp_id,ubi_id,bi_mdp.mdp_id 
GO
-------------------------------------------------------
----------------- Procedure publicacion(H) ------------
-------------------------------------------------------

CREATE PROC SQLITO.migrar_BI_publicacion
AS
INSERT INTO SQLITO.BI_H_Publicacion(pub_marc_id,
									pub_srub_id,
									pub_tiemp_id,
									pub_cantidad_stocks_inicial,
									pub_duracion_en_dias,
									pub_cantidad_publicaciones)
select marc_id,
	   srub_id,
	   t1.tiemp_id,
	   sum(pub_stock) + sum(isnull(det_vent_cantidad,0)), 
	   avg(DATEDIFF(DAY, pub_fecha_inicio, pub_fecha_fin)),
	   count(*)
from SQLITO.Publicacion
join SQLITO.DetalleVenta on pub_codigo = det_vent_pub_codigo
join SQLITO.Producto on pub_prod_id = prod_id
join SQLITO.BI_D_Marca on prod_marca = marc_id
join SQLITO.BI_D_Sub_rubro on srub_id = prod_sub_rubro
join SQLITO.BI_D_Tiempo t1 on YEAR(pub_fecha_inicio) = t1.tiemp_anio and MONTH(pub_fecha_inicio) = t1.tiemp_mes
group by marc_id,srub_id,tiemp_id
GO

-------------------------------------------------------
----------------- Procedure factura(H) ----------------
-------------------------------------------------------
CREATE PROC SQLITO.migrar_BI_factura
AS
INSERT INTO SQLITO.BI_H_Factura(fact_ubi_id,
									fact_tiemp_id,
									fact_concepto_facturado,
									fact_cantidad_facturada)
select distinct ubi_id,
	tiemp_id,
	sum(det_fact_precio),
	sum(fac_total)
from SQLITO.Factura
join SQLITO.DetalleFactura on det_fact_fac_id = fac_id
join SQLITO.BI_D_Tiempo on YEAR(fac_fecha) = tiemp_anio and MONTH(fac_fecha) = tiemp_mes
join SQLITO.Usuario on usu_id = fac_usu_id
join SQLITO.UsuDom on usu_id = usu_dom_usu_id
join SQLITO.Domicilio on usu_dom_dom_id = dom_id
join SQLITO.BI_D_Ubicacion on dom_loc_id = ubi_id
group by ubi_id,
		 tiemp_id
GO

CREATE PROC SQLITO.migrar_BI_envio
AS
INSERT INTO SQLITO.BI_H_Envio(env_tiemp_id,
							  env_ubi_cliente_id,
							  env_ubi_almacen_id,
							  env_tipo_env_id,
							  env_costo,
							  env_cantidad,
							  env_cumplidos)
select tiemp_id,
		ubi_c.ubi_id,
		ubi_a.ubi_id,
		tipo_env_id,
		sum(env_costo),
		count(*),
	SUM(CASE 
        WHEN DATEDIFF(DAY, env_fecha_programada, env_fecha_hs_entregado) = 0 
		AND (datepart(hour, env_fecha_hs_entregado) BETWEEN env_inicio_hs_entrega AND env_fin_hs_entrega)THEN 1
        ELSE 0 
    END)
from SQLITO.Envio 
join SQLITO.BI_D_TipoEnvio on tipo_env_id = env_tipo_envio
join SQLITO.BI_D_Tiempo on tiemp_anio = YEAR(env_fecha_programada) and MONTH(env_fecha_programada) = tiemp_mes
join SQLITO.UsuDom on env_usu_dom_envio = usu_dom_id
join SQLITO.Domicilio dom_c on usu_dom_dom_id = dom_c.dom_id
join SQLITO.BI_D_Ubicacion ubi_c on ubi_c.ubi_id = dom_loc_id
join SQLITO.Venta on env_vent_numero = vent_cod
join SQLITO.DetalleVenta on det_vent_vent_nro = vent_cod
join SQLITO.Publicacion on det_vent_pub_codigo = pub_codigo
join SQLITO.Almacen on alma_codigo = pub_almacen
join SQLITO.Domicilio dom_a on dom_a.dom_id = alma_dom_id
join SQLITO.BI_D_Ubicacion ubi_a on ubi_a.ubi_id = dom_a.dom_loc_id
group by tiemp_id,
		ubi_c.ubi_id,
		ubi_a.ubi_id,
		tipo_env_id
GO

------------------------------------------------------
----------------- EXECUTESS -------------------------
------------------------------------------------------

EXEC SQLITO.migrar_BI_ubicacion
EXEC SQLITO.migrar_BI_Marca
EXEC SQLITO.migrar_BI_tipo_envio
EXEC SQLITO.migrar_BI_Sub_rubro
EXEC SQLITO.migrar_BI_medios_de_pago
EXEC SQLITO.migrar_BI_rango_etario
EXEC SQLITO.migrar_BI_rango_horario
EXEC SQLITO.migrar_BI_tiempo
EXEC SQLITO.migrar_BI_ventas
EXEC SQLITO.migrar_BI_factura
EXEC SQLITO.migrar_BI_envio
EXEC SQLITO.migrar_BI_publicacion
EXEC SQLITO.migrar_BI_pagos
GO

----------------------------------------------------------
------------- Creacion de Vistas  ------------------------
----------------------------------------------------------

-------------------------------------------------------
---------- Promedio de tiempo de publicaciones --------
-------------------------------------------------------

CREATE VIEW SQLITO.v_promedio_tiempo_publicaciones
AS
	select tiemp_anio,
		   tiemp_cuatrimestre,
		   srub_detalle,
		   avg(pub_duracion_en_dias) as promedio_dias
	from SQLITO.BI_H_Publicacion
	join SQLITO.BI_D_Sub_rubro on pub_srub_id = srub_id
	join SQLITO.BI_D_Tiempo on tiemp_id = pub_tiemp_id
	group by srub_detalle,
		  tiemp_anio,
		  tiemp_cuatrimestre
GO

-------------------------------------------------------
------------- Promedio de Stock Inicial ---------------
-------------------------------------------------------

CREATE VIEW SQLITO.v_promedio_stock_inicial
AS
	select tiemp_anio,
		marc_detalle,
		sum(pub_cantidad_stocks_inicial)/sum(pub_cantidad_publicaciones) as promedio_stock_inicial
	from SQLITO.BI_H_Publicacion
	join SQLITO.BI_D_Marca on marc_id = pub_marc_id
	join SQLITO.BI_D_Tiempo on tiemp_id = pub_tiemp_id
	group by tiemp_anio,
			marc_detalle
GO
-------------------------------------------------------
---------------VISTA 3 Venta Promedio Mensual -------------
-------------------------------------------------------


CREATE VIEW SQLITO.v_venta_promedio_mensual
AS
SELECT 
    ubi.ubi_prov AS Provincia,
	ti.tiemp_anio AS ANIO,
	ti.tiemp_mes AS MES,
    SUM(v.vent_importe_total) / sum(v.vent_cant_ventas) AS Promedio_ventas
FROM 
    SQLITO.BI_H_Venta V
join SQLITO.BI_D_Ubicacion ubi on ubi.ubi_id = v.vent_ubi_almacen_id
join SQLITO.BI_D_Tiempo ti on ti.tiemp_id = v.vent_tiemp_id
group by ubi.ubi_prov,
		 ti.tiemp_anio,
		 ti.tiemp_mes
GO
-------------------------------------------------------
---------------VISTA 4 Rendimiento de rubros ----------
-------------------------------------------------------
CREATE VIEW SQLITO.v_rendimiento_rubros
AS
SELECT  t.tiemp_anio, 
		t.tiemp_cuatrimestre,
		ubi.ubi_loc,
		re.rango_etario_detalle,
		srb.srub_rub_nombre
FROM SQLITO.BI_H_Venta v
join SQLITO.BI_D_Tiempo t on t.tiemp_id = v.vent_tiemp_id
join SQLITO.BI_D_rango_etario re on re.rango_etario_id = v.vent_retario_id
join SQLITO.BI_D_Ubicacion ubi on ubi.ubi_id = v.vent_ubi_cliente_id
join SQLITO.BI_D_Sub_rubro srb on srb.srub_id = v.vent_srub_id
where srb.srub_rub_nombre in (
		SELECT TOP 5 sr2.srub_rub_nombre
		from SQLITO.BI_H_Venta v2
		join SQLITO.BI_D_Sub_rubro sr2 on sr2.srub_id = v2.vent_srub_id
		where t.tiemp_id = v2.vent_tiemp_id and
				ubi.ubi_id = v2.vent_ubi_cliente_id and 
				re.rango_etario_id = v2.vent_retario_id
		group by sr2.srub_rub_nombre
		order by sum(v2.vent_importe_total)  DESC
		)
group by t.tiemp_anio, 
		 t.tiemp_cuatrimestre,
		 ubi.ubi_loc,
		 re.rango_etario_detalle,
		 srb.srub_rub_nombre
GO
-------------------------------------------------------
---------------VISTA 6 Pago en cuotas  -------------
-------------------------------------------------------
CREATE VIEW SQLITO.v_Pago_Cuotas
AS
SELECT ti.tiemp_anio,
	   ti.tiemp_mes, 
	   mdp_detalle,
	   ubi.ubi_loc
FROM SQLITO.BI_H_Pago
join SQLITO.BI_D_Tiempo ti on ti.tiemp_id = pag_tiemp_id
join SQLITO.BI_D_Ubicacion ubi on ubi.ubi_id = pag_ubi_cliente_id
join SQLITO.BI_D_MedioDePago mdp1 on mdp_id = pag_mdp_id
WHERE mdp_cuotas > 1 and
	ubi.ubi_id in (select top 3 ubi2.ubi_id
					from SQLITO.BI_H_Pago p2
					join SQLITO.BI_D_Ubicacion ubi2 on ubi2.ubi_id = p2.pag_ubi_cliente_id
					join SQLITO.BI_D_MedioDePago mdp2 on mdp2.mdp_id = p2.pag_mdp_id
					where ti.tiemp_id = p2.pag_tiemp_id and
					mdp2.mdp_detalle = mdp1.mdp_detalle
					group by ubi2.ubi_id
					order by sum(p2.pag_total) desc)
group by	ti.tiemp_anio,
			ti.tiemp_mes, 
			mdp_detalle, 
			ubi.ubi_loc
GO
-------------------------------------------------------
------ Porcentaje de cumplimiento de envíos -----------
-------------------------------------------------------

CREATE VIEW SQLITO.v_porcentaje_cumplimientos_envios
AS
	select ubi_prov,
		   tiemp_anio,
		   tiemp_mes,
		   (sum(isnull(env_cumplidos,0))/sum(isnull(env_cantidad,0))) * 100 as porcentaje_cumplimientos_envios
	from SQLITO.BI_H_Envio
	join SQLITO.BI_D_Tiempo on env_tiemp_id = tiemp_id
	join SQLITO.BI_D_Ubicacion on env_ubi_almacen_id = ubi_id
	group by ubi_prov,
			tiemp_anio,
			tiemp_mes
GO

-------------------------------------------------------
------ Localidades con mayor costo de envío -----------
-------------------------------------------------------

CREATE VIEW SQLITO.v_localidades_con_mayor_costo_envio
AS
	select top 5 ubi_loc
	from SQLITO.BI_H_Envio
	join SQLITO.BI_D_Ubicacion on env_ubi_cliente_id = ubi_id
	group by ubi_loc
	order by sum(env_costo) desc
GO

-------------------------------------------------------
-------- Porcentaje de facturación por concepto -------
-------------------------------------------------------

CREATE VIEW SQLITO.v_porcentaje_facturacion_concepto
AS
	select tiemp_anio,
		   tiemp_mes,
		   (sum(fact_concepto_facturado)/sum(fact_cantidad_facturada))*100 as porcentaje_concepto
	from SQLITO.BI_H_Factura
	join SQLITO.BI_D_Tiempo on tiemp_id = fact_tiemp_id
	group by tiemp_anio,
			 tiemp_mes
GO

-------------------------------------------------------
--------------- Facturación por provincia -------------
-------------------------------------------------------

CREATE VIEW SQLITO.v_facturacion_provincia
AS
	select tiemp_anio,
		   tiemp_cuatrimestre,
		   ubi_prov,
		   sum(fact_cantidad_facturada) as monto_facturado
	from SQLITO.BI_H_Factura
	join SQLITO.BI_D_Tiempo on tiemp_id = fact_tiemp_id
	join SQLITO.BI_D_Ubicacion on ubi_id = fact_ubi_id
	group by tiemp_anio,
		   tiemp_cuatrimestre,
		   ubi_prov
GO