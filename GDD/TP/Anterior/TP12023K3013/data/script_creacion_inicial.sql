USE GD1C2023
GO

----------------------------------------------------------
---- Eliminaci�n de Tablas, Funciones, etc existentes ----
----------------------------------------------------------

IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'get_numero')
	DROP FUNCTION INTER_DATA.get_numero
GO

IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'get_calle')
	DROP FUNCTION INTER_DATA.get_calle
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Cupon_X_Pedido')
	DROP TABLE INTER_DATA.Cupon_X_Pedido
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Cupon_X_Reclamo')
	DROP TABLE INTER_DATA.Cupon_X_Reclamo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Cupon_X_Local')
	DROP TABLE INTER_DATA.Cupon_X_Local
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Cupon_X_Usuario')
	DROP TABLE INTER_DATA.Cupon_X_Usuario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Horario_X_Local')
	DROP TABLE INTER_DATA.Horario_X_Local
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Item_Pedido')
	DROP TABLE INTER_DATA.Item_Pedido
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Reclamo')
	DROP TABLE INTER_DATA.Reclamo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Cupon')
	DROP TABLE INTER_DATA.Cupon
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tipo_Cupon')
	DROP TABLE INTER_DATA.Tipo_Cupon
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Operador')
	DROP TABLE INTER_DATA.Operador
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tipo_Reclamo')
	DROP TABLE INTER_DATA.Tipo_Reclamo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Estado_Reclamo')
	DROP TABLE INTER_DATA.Estado_Reclamo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Producto')
	DROP TABLE INTER_DATA.Producto
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Horario')
	DROP TABLE INTER_DATA.Horario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Pedido')
	DROP TABLE INTER_DATA.Pedido
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Local')
	DROP TABLE INTER_DATA.Local
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Local_Categoria')
	DROP TABLE INTER_DATA.Local_Categoria
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Local_Tipo')
	DROP TABLE INTER_DATA.Local_Tipo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Direccion_Usuario')
	DROP TABLE INTER_DATA.Direccion_Usuario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Mensajeria')
	DROP TABLE INTER_DATA.Mensajeria
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Envio')
	DROP TABLE INTER_DATA.Envio
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Repartidor')
	DROP TABLE INTER_DATA.Repartidor
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tipo_Movilidad')
	DROP TABLE INTER_DATA.Tipo_Movilidad
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Direccion')
	DROP TABLE INTER_DATA.Direccion
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Localidad')
	DROP TABLE INTER_DATA.Localidad
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Provincia')
	DROP TABLE INTER_DATA.Provincia
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tipo_Estado')
	DROP TABLE INTER_DATA.Tipo_Estado
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tipo_Medio_Pago')
	DROP TABLE INTER_DATA.Tipo_Medio_Pago
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tarjeta_Usuario')
	DROP TABLE INTER_DATA.Tarjeta_Usuario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Paquete')
	DROP TABLE INTER_DATA.Paquete
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Tipo_Paquete')
	DROP TABLE INTER_DATA.Tipo_Paquete
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'Usuario')
	DROP TABLE INTER_DATA.Usuario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_usuario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_usuario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipo_paquete' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tipo_paquete
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_paquete' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_paquete
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tarjeta_usuario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tarjeta_usuario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipo_medio_pago' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tipo_medio_pago
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipo_estado' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tipo_estado
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_provincia' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_provincia
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_localidad_usuario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_localidad_usuario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_localidad_envio_mensajeria' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_localidad_envio_mensajeria
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_localidad' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_localidad
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_usuarios' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_usuarios
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_local' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_local
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_operador' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_operador
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_repartidor' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_repartidor
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_destino' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_destino
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_origen' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_origen
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipo_movilidad' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tipo_movilidad
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_repartidor' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_repartidor
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_envio_mensajeria' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_envio_mensajeria
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_envio_pedido' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_envio_pedido
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_envio' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_envio
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_mensajeria' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_mensajeria
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_direccion_usuario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_direccion_usuario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_local_categoria' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_local_categoria
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_local_tipo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_local_tipo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_local' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_local
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_pedido' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_pedido

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_horario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_horario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_producto' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_producto
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_estado_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_estado_reclamo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipo_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tipo_reclamo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_operador' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_operador
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_tipo_cupon' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_tipo_cupon
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cupon_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_cupon_reclamo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cupon_cupon' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_cupon_cupon
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_reclamo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cupon_x_usuario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_cupon_x_usuario 
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cupon_x_local' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_cupon_x_local 
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cupon_x_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_cupon_x_reclamo 
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_item_pedido' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_item_pedido
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_horario_x_local' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_horario_x_local
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_cupon_X_Pedido' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_Cupon_X_Pedido  
GO
GO

IF EXISTS (SELECT name FROM sys.schemas WHERE name = 'INTER_DATA')
	DROP SCHEMA INTER_DATA
GO

----------------------------------------------------------
-------------- Creaci�n de Tablas y Schema ---------------
----------------------------------------------------------

CREATE SCHEMA INTER_DATA
GO

----------------------------------------------------------
---------------- Table INTER_DATA.Usuario ----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Usuario (
	  id_usuario INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(255) NOT NULL,
	  Apellido NVARCHAR(255) NOT NULL,
	  DNI DECIMAL(18,0)  NOT NULL,
	  Telefono DECIMAL(18,0)  NOT NULL,
	  Mail NVARCHAR(255) NOT NULL,
	  Fecha_Nac DATE NOT NULL,
	  Fecha_Registro DATETIME2(3) NOT NULL,
)


----------------------------------------------------------
------------- Table INTER_DATA.Tipo_Paquete --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tipo_Paquete (
	  id_tipo_paquete INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Nombre NVARCHAR(50) NOT NULL,
	  Alto_MAX DECIMAL(18,2)  NOT NULL,
	  Ancho_MAX DECIMAL(18,2)  NOT NULL,
	  Largo_MAX DECIMAL(18,2)  NOT NULL,
	  Peso_MAX DECIMAL(18,2)  NOT NULL,
	  Tipo_Precio DECIMAL(18,2)  NOT NULL,
)


----------------------------------------------------------
---------------- Table INTER_DATA.Paquete ----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Paquete (
	  id_Paquete INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Valor_Asegurado DECIMAL(18,2)  NOT NULL,
	  Tipo_Paquete_id_tipo_paquete INT  NOT NULL,
	  CONSTRAINT fk_Paquete_id_Tipo_Paquete
	  FOREIGN KEY (Tipo_Paquete_id_tipo_paquete) REFERENCES INTER_DATA.Tipo_Paquete (id_tipo_paquete)
)


----------------------------------------------------------
------------ Table INTER_DATA.Tarjeta_Usuario ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tarjeta_Usuario (
	  Numero NVARCHAR(50) PRIMARY KEY NOT NULL ,
	  Marca_Tarjeta NVARCHAR(100) NOT NULL,
	  Tipo NVARCHAR(50) NOT NULL,
	  Usuario_id_usuario INT  NOT NULL,
	  CONSTRAINT fk_Tarjeta_Usuario_Usuario
	  FOREIGN KEY (Usuario_id_usuario) REFERENCES INTER_DATA.Usuario (id_usuario),
)


----------------------------------------------------------
------------ Table INTER_DATA.Tipo_Medio_Pago ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tipo_Medio_Pago (
	  id_tipo_medio_pago INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(50) NOT NULL,
	  Tarjeta_Usuario_Numero NVARCHAR(50),
	  CONSTRAINT fk_Tipo_Medio_Pago_Tarjeta_Usuario
	  FOREIGN KEY (Tarjeta_Usuario_Numero) REFERENCES INTER_DATA.Tarjeta_Usuario (Numero),
)


----------------------------------------------------------
-------------- Table INTER_DATA.Tipo_Estado --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tipo_Estado (
	  id_Tipo_Estado INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(50) NOT NULL
)


----------------------------------------------------------
--------------- Table INTER_DATA.Provincia ---------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Provincia (
	  id_Provincia INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(255) NOT NULL
)


----------------------------------------------------------
--------------- Table INTER_DATA.Localidad ---------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Localidad (
	  id_Localidad INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Localidad NVARCHAR(255) NOT NULL,
	  Provincia_id_Provincia INT  NOT NULL,
	  CONSTRAINT fk_Localidad_Provincia
	  FOREIGN KEY (Provincia_id_Provincia) REFERENCES INTER_DATA.Provincia (id_Provincia)
)


----------------------------------------------------------
--------------- Table INTER_DATA.Direccion ---------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Direccion (
	  id_direccion INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Calle NVARCHAR(50) NOT NULL,
	  Numero DECIMAL(18,0)  NOT NULL,
	  Localidad_id_Localidad INT,
	  CONSTRAINT fk_Direccion_Localidad
	  FOREIGN KEY (Localidad_id_Localidad) REFERENCES INTER_DATA.Localidad (id_Localidad)
)


----------------------------------------------------------
------------- Table INTER_DATA.Tipo_Movilidad ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tipo_Movilidad (
	  id_Tipo_Movilidad INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Tipo_Movilidad NVARCHAR(50) NOT NULL
)


----------------------------------------------------------
--------------- Table INTER_DATA.Repartidor --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Repartidor (
	  id_repartidor INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Nombre NVARCHAR(255) NOT NULL,
	  Apellido NVARCHAR(255) NOT NULL,
	  DNI DECIMAL(18,0)  NOT NULL,
	  Telefono DECIMAL(18,0)  NOT NULL,
	  Mail NVARCHAR(255) NOT NULL,
	  Fecha_Nac DATE NOT NULL,
	  Direccion_id_direccion INT  NOT NULL,
	  Tipo_Movilidad_id_Tipo_Movilidad INT  NOT NULL,
	  CONSTRAINT fk_Repartidor_Direccion
	  FOREIGN KEY (Direccion_id_direccion) REFERENCES INTER_DATA.Direccion (id_direccion),
	  CONSTRAINT fk_Repartidor_Tipo_Movilidad
	  FOREIGN KEY (Tipo_Movilidad_id_Tipo_Movilidad) REFERENCES INTER_DATA.Tipo_Movilidad (id_Tipo_Movilidad)
)


----------------------------------------------------------
----------------- Table INTER_DATA.Envio -----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Envio (
	  id_envio INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Precio_Envio DECIMAL(18,2)  NOT NULL,
	  Propina DECIMAL(18,2)  NOT NULL,
	  Tiempo_Estimado DECIMAL(18,2)  NOT NULL,
	  Fecha_Entrega DATETIME NOT NULL,
	  Direccion_id_direccion_destino INT  NOT NULL,
	  Repartidor_id_repartidor INT  NOT NULL,
	  CONSTRAINT fk_Envio_Direccion_Destino
	  FOREIGN KEY (Direccion_id_direccion_destino) REFERENCES INTER_DATA.Direccion (id_direccion),
	  CONSTRAINT fk_Envio_Repartidor
	  FOREIGN KEY (Repartidor_id_repartidor) REFERENCES INTER_DATA.Repartidor (id_repartidor)
)


----------------------------------------------------------
--------------- Table INTER_DATA.Mensajeria --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Mensajeria (
	  Numero DECIMAL(18,0) PRIMARY KEY NOT NULL,
	  KM DECIMAL(18,2)  NOT NULL,
	  Observaciones NVARCHAR(255) NOT NULL,
	  Precio_Seguro DECIMAL(18,2)  NOT NULL,
	  Total DECIMAL(18,2)  NOT NULL,
	  Fecha DATETIME NOT NULL,
	  Calificacion DECIMAL(18,0)  NOT NULL,
	  Paquete_id_Paquete INT NOT NULL,
	  Usuario_id_usuario INT  NOT NULL,
	  Tipo_Estado_id_Tipo_Estado INT  NOT NULL,
	  Envio_id_envio INT  NOT NULL,
	  Tipo_Medio_Pago_id_tipo_medio_pago INT  NOT NULL,
	  Direccion_id_direccion_origen INT  NOT NULL,
	  CONSTRAINT fk_Mensajeria_Paquete
	  FOREIGN KEY (Paquete_id_Paquete) REFERENCES INTER_DATA.Paquete(id_Paquete),
	  CONSTRAINT fk_Mensajeria_Tipo_Medio_Pago
	  FOREIGN KEY (Tipo_Medio_Pago_id_tipo_medio_pago) REFERENCES INTER_DATA.Tipo_Medio_Pago(id_tipo_medio_pago),
	  CONSTRAINT fk_Mensajeria_Tipo_Estado
	  FOREIGN KEY (Tipo_Estado_id_Tipo_Estado) REFERENCES INTER_DATA.Tipo_Estado(id_Tipo_Estado),
	  CONSTRAINT fk_Mensajeria_Usuario
	  FOREIGN KEY (Usuario_id_usuario) REFERENCES INTER_DATA.Usuario(id_usuario),
	  CONSTRAINT fk_Mensajeria_Envio
	  FOREIGN KEY (Envio_id_envio) REFERENCES INTER_DATA.Envio(id_envio),
	  CONSTRAINT fk_Mensajeria_Direccion
	  FOREIGN KEY (Direccion_id_direccion_origen) REFERENCES INTER_DATA.Direccion(id_direccion)
)


----------------------------------------------------------
----------- Table INTER_DATA.Direccion_Usuario -----------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Direccion_Usuario (
	  id_direccion_usuario INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(50) NOT NULL,
	  Usuario_id_usuario INT  NOT NULL,
	  Direccion_id_direccion INT  NOT NULL,
	  CONSTRAINT fk_Direccion_Usuario_Usuario
	  FOREIGN KEY (Usuario_id_usuario) REFERENCES INTER_DATA.Usuario (id_usuario),
	  CONSTRAINT fk_Direccion_Usuario_Direccion
	  FOREIGN KEY (Direccion_id_direccion) REFERENCES INTER_DATA.Direccion (id_direccion)
)


----------------------------------------------------------
-------------- Table INTER_DATA.Local_Tipo ---------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Local_Tipo (
	  id_Local_Tipo INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Tipo NVARCHAR(50) NOT NULL,
)


----------------------------------------------------------
------------ Table INTER_DATA.Local_Categoria ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Local_Categoria (
	  id_Local_Categoria INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Categoria NVARCHAR(50) NOT NULL,
)


----------------------------------------------------------
----------------- Table INTER_DATA.Local -----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Local (
	  id_local INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(100) NOT NULL,
	  Descripcion NVARCHAR(255) NOT NULL,
	  Local_Tipo_id_Local_Tipo INT  NOT NULL,
	  Local_Categoria_id_Local_Categoria INT,
	  Direccion_id_direccion INT  NOT NULL,
	  CONSTRAINT fk_Local_Tipo
	  FOREIGN KEY (Local_Tipo_id_Local_Tipo) REFERENCES INTER_DATA.Local_Tipo (id_Local_Tipo),
	  CONSTRAINT fk_Local_Categoria
	  FOREIGN KEY (Local_Categoria_id_Local_Categoria) REFERENCES INTER_DATA.Local_Categoria (id_Local_Categoria),
	  CONSTRAINT fk_Local_Direccion
	  FOREIGN KEY (Direccion_id_direccion) REFERENCES INTER_DATA.Direccion (id_direccion)
)


----------------------------------------------------------
----------------- Table INTER_DATA.Pedido ----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Pedido (
	  Numero DECIMAL(18,0) PRIMARY KEY NOT NULL,
	  Fecha DATETIME NOT NULL,
	  Tarifa_Servicio DECIMAL(18,2)  NOT NULL,
	  Total_Productos DECIMAL(18,2)  NOT NULL,
	  Observaciones NVARCHAR(255) NOT NULL,
	  Calificacion DECIMAL(18,0)  NOT NULL,
	  Total_Cupones DECIMAL(18,2)  NOT NULL,
	  Total_Servicio DECIMAL(18,2)  NOT NULL,
	  Usuario_id_usuario INT  NOT NULL,
	  Local_id_local INT  NOT NULL,
	  Envio_id_envio INT  NOT NULL,
	  Tipo_Estado_id_Tipo_Estado INT  NOT NULL,
	  Tipo_Medio_Pago_id_tipo_medio_pago INT  NOT NULL,
	  CONSTRAINT fk_Pedido_Usuario
	  FOREIGN KEY (Usuario_id_usuario) REFERENCES INTER_DATA.Usuario (id_usuario),
	  CONSTRAINT fk_Pedido_Local
	  FOREIGN KEY (Local_id_local) REFERENCES INTER_DATA.Local (id_local),
	  CONSTRAINT fk_Pedido_Envio
	  FOREIGN KEY (Envio_id_envio) REFERENCES INTER_DATA.Envio (id_envio),
	  CONSTRAINT fk_Pedido_Tipo_Estado
	  FOREIGN KEY (Tipo_Estado_id_Tipo_Estado) REFERENCES INTER_DATA.Tipo_Estado (id_Tipo_Estado),
	  CONSTRAINT fk_Pedido_Tipo_Medio_Pago
	  FOREIGN KEY (Tipo_Medio_Pago_id_tipo_medio_pago) REFERENCES INTER_DATA.Tipo_Medio_Pago (id_tipo_medio_pago)
)


----------------------------------------------------------
---------------- Table INTER_DATA.Horario ----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Horario (
	  id_horario INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Hora_Apertura DECIMAL(18,0)  NOT NULL,
	  Hora_CIerre DECIMAL(18,0)  NOT NULL,
	  Dia NVARCHAR(50) NOT NULL,
)


----------------------------------------------------------
--------------- Table INTER_DATA.Producto ----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Producto (
	  id_producto INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Codigo NVARCHAR(50) NOT NULL,
	  Nombre NVARCHAR(50) NOT NULL,
	  Descripcion NVARCHAR(255) NOT NULL,
	  Precio DECIMAL(18,2)  NOT NULL,
	  Local_id_local INT  NOT NULL,
	  CONSTRAINT fk_Producto_Local
	  FOREIGN KEY (Local_id_local) REFERENCES INTER_DATA.Local (id_local)
)


----------------------------------------------------------
------------- Table INTER_DATA.Estado_Reclamo ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Estado_Reclamo (
	  id_Estado_Reclamo INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Estado NVARCHAR(50) NOT NULL,
)


----------------------------------------------------------
-------------- Table INTER_DATA.Tipo_Reclamo -------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tipo_Reclamo (
	  id_Tipo_Reclamo INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Tipo NVARCHAR(50) NOT NULL,
)


----------------------------------------------------------
---------------- Table INTER_DATA.Operador ---------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Operador (
	  id_operador INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Nombre NVARCHAR(255) NOT NULL,
	  Apellido NVARCHAR(255) NOT NULL,
	  DNI DECIMAL(18,0)  NOT NULL,
	  Telefono DECIMAL(18,0)  NOT NULL,
	  Mail NVARCHAR(255) NOT NULL,
	  Fecha_Nac DATE NOT NULL,
	  Direccion_id_direccion INT  NOT NULL,
	  CONSTRAINT fk_Operador_Direccion
	  FOREIGN KEY (Direccion_id_direccion) REFERENCES INTER_DATA.Direccion (id_direccion)
)


----------------------------------------------------------
-------------- Table INTER_DATA.Tipo_Cupon ---------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Tipo_Cupon (
	  id_Tipo_Cupon INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Tipo NVARCHAR(50) NOT NULL,
)


----------------------------------------------------------
----------------- Table INTER_DATA.Cupon -----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Cupon (
	  Numero DECIMAL(18,0) PRIMARY KEY NOT NULL,
	  Monto DECIMAL(18,2)  NOT NULL,
	  Fecha_Alta DATETIME NOT NULL,
	  Fecha_Vencimiento DATETIME NOT NULL,
	  Tipo_Cupon_id_Tipo_Cupon INT  NOT NULL,
	  CONSTRAINT fk_Tipo_Cupon
	  FOREIGN KEY (Tipo_Cupon_id_Tipo_Cupon) REFERENCES INTER_DATA.Tipo_Cupon (id_Tipo_Cupon)
)


----------------------------------------------------------
---------------- Table INTER_DATA.Reclamo ----------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Reclamo (
	  Numero DECIMAL(18,0) PRIMARY KEY NOT NULL,
	  Fecha DATETIME NOT NULL,
	  Descripcion NVARCHAR(255) NOT NULL,
	  Fecha_Solucion DATETIME NOT NULL,
	  Solucion NVARCHAR(255) NOT NULL,
	  Calificacion DECIMAL(18,0)  NOT NULL,
	  Pedido_Numero DECIMAL(18,0)  NOT NULL,
	  Estado_Reclamo_id_Estado_Reclamo INT  NOT NULL,
	  Tipo_Reclamo_id_Tipo_Reclamo INT  NOT NULL,
	  Operador_id_operador INT  NOT NULL,
	  CONSTRAINT fk_Reclamo_Pedido
	  FOREIGN KEY (Pedido_Numero) REFERENCES INTER_DATA.Pedido (Numero),
	  CONSTRAINT fk_Reclamo_Estado_Reclamo
	  FOREIGN KEY (Estado_Reclamo_id_Estado_Reclamo) REFERENCES INTER_DATA.Estado_Reclamo (id_Estado_Reclamo),
	  CONSTRAINT fk_Reclamo_Tipo_Reclamo
	  FOREIGN KEY (Tipo_Reclamo_id_Tipo_Reclamo) REFERENCES INTER_DATA.Tipo_Reclamo (id_Tipo_Reclamo),
	  CONSTRAINT fk_Reclamo_Operador
	  FOREIGN KEY (Operador_id_operador) REFERENCES INTER_DATA.Operador (id_operador),
)


----------------------------------------------------------
------------ Table INTER_DATA.Cupon_X_Usuario ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Cupon_X_Usuario (
	  id_cupon_x_usuario INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Usuario_id_Usuario INT NOT NULL,
	  Cupon_Numero_Cupon DECIMAL(18,0) NOT NULL,
	  CONSTRAINT fk_Usuario_id_Usuario
	  FOREIGN KEY (Usuario_id_Usuario) REFERENCES INTER_DATA.Usuario (id_Usuario),
	  CONSTRAINT fk_Cupon_Numero_Cupon
	  FOREIGN KEY (Cupon_Numero_Cupon) REFERENCES INTER_DATA.Cupon (Numero)
)


----------------------------------------------------------
------------- Table INTER_DATA.Cupon_X_Local -------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Cupon_X_Local (
	  id_cupon_x_local INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Local_id_Local INT NOT NULL,
	  Cupon_Numero_Cupon DECIMAL(18,0) NOT NULL,
	  CONSTRAINT fk_Local_id_Local
	  FOREIGN KEY (Local_id_Local) REFERENCES INTER_DATA.Local (id_Local),
	  CONSTRAINT fk_Cupon_X_Local
	  FOREIGN KEY (Cupon_Numero_Cupon) REFERENCES INTER_DATA.Cupon (Numero)
)


----------------------------------------------------------
------------ Table INTER_DATA.Cupon_X_Reclamo ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Cupon_X_Reclamo (
	  id_cupon_x_reclamo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Reclamo_Numero_Reclamo DECIMAL(18,0) NOT NULL,
	  Cupon_Numero_Cupon DECIMAL(18,0) NOT NULL,
	  CONSTRAINT fk_Reclamo_Numero_Reclamo
	  FOREIGN KEY (Reclamo_Numero_Reclamo) REFERENCES INTER_DATA.Reclamo (Numero),
	  CONSTRAINT fk_Cupon_X_Reclamo
	  FOREIGN KEY (Cupon_Numero_Cupon) REFERENCES INTER_DATA.Cupon (Numero)
)


----------------------------------------------------------
-------------- Table INTER_DATA.Item_Pedido --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Item_Pedido (
	  id_Item_Pedido INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Pedido_Numero DECIMAL(18,0) NOT NULL,
	  Producto_Codigo INT NOT NULL,
	  Cantidad DECIMAL(18,0)  NOT NULL,
	  Precio DECIMAL(18,2)  NOT NULL,
	  CONSTRAINT fk_Item_Pedido_Pedido
	  FOREIGN KEY (Pedido_Numero) REFERENCES INTER_DATA.Pedido (Numero),
	  CONSTRAINT fk_Item_Pedido_Producto
	  FOREIGN KEY (Producto_Codigo) REFERENCES INTER_DATA.Producto (id_producto)
)


----------------------------------------------------------
------------ Table INTER_DATA.Horario_X_Local ------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Horario_X_Local (
	  id_Horario_X_Local INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Local_id_local INT NOT NULL,
	  Horario_id_horario INT NOT NULL,
	  CONSTRAINT fk_Horario_X_Local_Local
	  FOREIGN KEY (Local_id_local) REFERENCES INTER_DATA.Local (id_local),
	  CONSTRAINT fk_Horario_X_Local_Horario
	  FOREIGN KEY (Horario_id_horario) REFERENCES INTER_DATA.Horario (id_horario)
)


----------------------------------------------------------
------------ Table INTER_DATA.Cupon_X_Pedido -------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.Cupon_X_Pedido (
	  id_Cupon_X_Pedido INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	  Cupon_Numero DECIMAL(18,0) NOT NULL,
	  Pedido_Numero DECIMAL(18,0) NOT NULL,
	  CONSTRAINT fk_Cupon_X_Pedido_Cupon
	  FOREIGN KEY (Cupon_Numero) REFERENCES INTER_DATA.Cupon (Numero),
	  CONSTRAINT fk_Cupon_X_Pedido_Pedido
	  FOREIGN KEY (Pedido_Numero) REFERENCES INTER_DATA.Pedido (Numero)
)

/* ---------------------------------------------------------------- */


----------------------------------------------------------------------------
---------------------------- Cracion de Indices ----------------------------
----------------------------------------------------------------------------


CREATE INDEX USUARIO ON INTER_DATA.Usuario(Apellido, DNI, Fecha_Nac)
GO

CREATE INDEX PAQUETE ON INTER_DATA.Paquete(Valor_Asegurado)
GO

CREATE INDEX TARJETA_USUARIO ON INTER_DATA.Tarjeta_Usuario(Numero)
GO

CREATE INDEX TIPO_MEDIO_PAGO ON INTER_DATA.Tipo_Medio_Pago(Tarjeta_Usuario_Numero)
GO

CREATE INDEX PROVINCIA ON INTER_DATA.Provincia(Nombre)
GO

CREATE INDEX LOCALIDAD ON INTER_DATA.Localidad(Localidad)
GO

CREATE INDEX DIRECCIONES ON INTER_DATA.Direccion(Calle, Numero)
GO

CREATE INDEX REPARTIDOR ON INTER_DATA.Repartidor(Apellido, DNI, Fecha_Nac)
GO

CREATE INDEX CUPON ON INTER_DATA.Cupon(Numero)
GO

CREATE INDEX LOCALES ON INTER_DATA.Local(Nombre, Local_Tipo_id_Local_Tipo)
GO

CREATE INDEX PEDIDO ON INTER_DATA.Pedido(Numero)
GO

CREATE INDEX OPERADOR ON INTER_DATA.Operador(Apellido, DNI, Fecha_Nac)
GO

/* ---------------------------------------------------------------- */


----------------------------------------------------------------------------
------------------------------ Funciones -----------------------------------
----------------------------------------------------------------------------


/* FUNCIONES PARA OBTENER LA CALLE Y EL NUMERO DE LA DIRECCION */

IF EXISTS (SELECT name, type FROM sysobjects WHERE name='get_numero' AND type='fn')
	DROP FUNCTION INTER_DATA.get_numero
GO

CREATE FUNCTION INTER_DATA.get_numero(@Direccion NVARCHAR(255))
RETURNS DECIMAL(18, 0)
AS
BEGIN
	RETURN SUBSTRING(RIGHT(@Direccion, 4), PATINDEX('%[0-9]%', RIGHT(@Direccion, 4)), LEN(RIGHT(@Direccion,�4)))
END
GO

IF EXISTS (SELECT name, type FROM sysobjects WHERE name='get_calle' AND type='fn')
	DROP FUNCTION INTER_DATA.get_calle
GO

CREATE FUNCTION INTER_DATA.get_calle(@Direccion NVARCHAR(255))
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN LEFT(@Direccion, LEN(@Direccion)-LEN(INTER_DATA.get_numero(@Direccion)))
END
GO

/* ---------------------------------------------------------------- */


----------------------------------------------------------------------------
----------------- Stored Procedures para la Migracion ----------------------
----------------------------------------------------------------------------


/* SP USUARIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_usuario
AS
INSERT INTO INTER_DATA.Usuario( Nombre, Apellido, DNI, Telefono, Mail,Fecha_Nac, Fecha_Registro)
	SELECT 
		DISTINCT USUARIO_NOMBRE,
				 USUARIO_APELLIDO,
				 USUARIO_DNI,
				 USUARIO_TELEFONO,
				 USUARIO_MAIL,
				 USUARIO_FECHA_NAC,
				 USUARIO_FECHA_REGISTRO
	FROM gd_esquema.Maestra
	WHERE USUARIO_NOMBRE			IS NOT NULL 
		AND USUARIO_APELLIDO		IS NOT NULL 
		AND USUARIO_DNI				IS NOT NULL 
		AND USUARIO_TELEFONO		IS NOT NULL
		AND USUARIO_MAIL			IS NOT NULL
		AND USUARIO_FECHA_NAC		IS NOT NULL
		AND USUARIO_FECHA_REGISTRO	IS NOT NULL
	ORDER BY USUARIO_APELLIDO, USUARIO_DNI, USUARIO_FECHA_NAC
GO

/* ---------------------------------------------------------------- */


/* SP TIPO_PAQUETE */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tipo_paquete
AS
INSERT INTO INTER_DATA.Tipo_Paquete(Nombre, Alto_MAX, Ancho_MAX, Largo_MAX, Peso_MAX, Tipo_Precio)
	SELECT 
		DISTINCT PAQUETE_TIPO, PAQUETE_ALTO_MAX, PAQUETE_ANCHO_MAX, PAQUETE_LARGO_MAX, PAQUETE_PESO_MAX, PAQUETE_TIPO_PRECIO 
	FROM gd_esquema.Maestra
	WHERE PAQUETE_TIPO IS NOT NULL
	AND PAQUETE_ALTO_MAX IS NOT NULL
	AND PAQUETE_ANCHO_MAX IS NOT NULL
	AND PAQUETE_LARGO_MAX IS NOT NULL
	AND PAQUETE_PESO_MAX IS NOT NULL
	AND PAQUETE_TIPO_PRECIO IS NOT NULL
	ORDER BY PAQUETE_TIPO
GO

/* ---------------------------------------------------------------- */


/* SP PAQUETE */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_paquete
AS
INSERT INTO INTER_DATA.Paquete(Valor_Asegurado, Tipo_Paquete_id_tipo_paquete)
	SELECT 
		DISTINCT M.ENVIO_MENSAJERIA_VALOR_ASEGURADO,
		(SELECT id_tipo_paquete
			FROM INTER_DATA.Tipo_Paquete P
			WHERE P.Nombre = M.PAQUETE_TIPO
		) 
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_VALOR_ASEGURADO IS NOT NULL
	ORDER BY M.ENVIO_MENSAJERIA_VALOR_ASEGURADO
GO

/* ---------------------------------------------------------------- */


/* SP TARJETA_USUARIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tarjeta_usuario
AS
INSERT INTO INTER_DATA.Tarjeta_Usuario(Numero, Marca_Tarjeta,Tipo,Usuario_id_usuario)
	SELECT 
	DISTINCT MEDIO_PAGO_NRO_TARJETA,
			 MARCA_TARJETA,
			 MEDIO_PAGO_TIPO,
			 (SELECT id_usuario
				FROM INTER_DATA.Usuario U
				WHERE  U.Apellido = M.USUARIO_APELLIDO
				AND	   U.DNI = M.USUARIO_DNI
				AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
			) 
	FROM gd_esquema.Maestra M
	WHERE 
		MEDIO_PAGO_NRO_TARJETA			IS NOT NULL 
		AND MARCA_TARJETA				IS NOT NULL 
		AND MEDIO_PAGO_TIPO				IS NOT NULL 
	ORDER BY MEDIO_PAGO_NRO_TARJETA
GO


/* ---------------------------------------------------------------- */


/* SP TIPO_MEDIO_PAGO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tipo_medio_pago
AS
INSERT INTO INTER_DATA.Tipo_Medio_Pago(Nombre,Tarjeta_Usuario_Numero)
	SELECT 
		DISTINCT MEDIO_PAGO_TIPO,		
		(
			SELECT T.Numero
			FROM INTER_DATA.Tarjeta_Usuario T
			WHERE T.Numero = M.MEDIO_PAGO_NRO_TARJETA
			AND	  T.Marca_Tarjeta  = M.MARCA_TARJETA	
		)
	FROM gd_esquema.Maestra M
	WHERE MEDIO_PAGO_TIPO IS NOT NULL
GO


/* ---------------------------------------------------------------- */


/* SP TIPO_ESTADO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tipo_estado
AS
INSERT INTO INTER_DATA.Tipo_Estado(Nombre)
	SELECT 
		DISTINCT PEDIDO_ESTADO
	FROM gd_esquema.Maestra M
	WHERE 
		PEDIDO_ESTADO	IS NOT NULL 
GO


/* ---------------------------------------------------------------- */


/* SP PROVINCIA */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_provincia
AS
INSERT INTO INTER_DATA.Provincia(Nombre)
	SELECT 
		DISTINCT(DIRECCION_USUARIO_PROVINCIA) 
	FROM gd_esquema.Maestra
	WHERE DIRECCION_USUARIO_PROVINCIA IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP LOCALIDAD_USUARIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_localidad_usuario
AS
INSERT INTO INTER_DATA.aux(Localidad, Provincia_id_Provincia)
	SELECT 
		M.DIRECCION_USUARIO_LOCALIDAD, 
		(SELECT id_Provincia 
		 FROM	INTER_DATA.Provincia P
		 WHERE	P.Nombre = M.DIRECCION_USUARIO_PROVINCIA
		)
	FROM gd_esquema.Maestra M
	WHERE M.DIRECCION_USUARIO_LOCALIDAD IS NOT NULL
	GROUP BY M.DIRECCION_USUARIO_LOCALIDAD, M.DIRECCION_USUARIO_PROVINCIA
GO

/* ---------------------------------------------------------------- */


/* SP LOCALIDAD_ENVIO_MENSAJERIA */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_localidad_envio_mensajeria
AS
INSERT INTO INTER_DATA.aux(Localidad, Provincia_id_Provincia)
	SELECT 
		M.ENVIO_MENSAJERIA_LOCALIDAD,
		(SELECT id_Provincia 
		 FROM	INTER_DATA.Provincia P
		 WHERE	P.Nombre = M.ENVIO_MENSAJERIA_PROVINCIA
		)
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_LOCALIDAD IS NOT NULL
	GROUP BY M.ENVIO_MENSAJERIA_LOCALIDAD, M.ENVIO_MENSAJERIA_PROVINCIA
GO

/* ---------------------------------------------------------------- */


/* SP LOCALIDAD */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_localidad
AS
CREATE TABLE INTER_DATA.aux (
	  id_Localidad INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Localidad NVARCHAR(255) NOT NULL,
	  Provincia_id_Provincia INT  NOT NULL
)
EXEC INTER_DATA.migrar_localidad_usuario
EXEC INTER_DATA.migrar_localidad_envio_mensajeria
INSERT INTO INTER_DATA.Localidad(Localidad, Provincia_id_Provincia)
		SELECT DISTINCT Localidad, Provincia_id_Provincia FROM INTER_DATA.aux
			ORDER BY Localidad
DROP TABLE INTER_DATA.aux
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_USUARIOS */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_usuarios
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
SELECT DISTINCT INTER_DATA.get_calle(M.DIRECCION_USUARIO_DIRECCION) AS Calle,
				INTER_DATA.get_numero(M.DIRECCION_USUARIO_DIRECCION) AS Numero, 
				(SELECT	id_Localidad 
				 FROM	INTER_DATA.Localidad L 
				 JOIN	INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
				 WHERE	M.DIRECCION_USUARIO_LOCALIDAD = L.Localidad AND M.DIRECCION_USUARIO_PROVINCIA = P.Nombre
				)
	FROM gd_esquema.Maestra M
	WHERE M.DIRECCION_USUARIO_DIRECCION IS NOT NULL
	GROUP BY M.DIRECCION_USUARIO_DIRECCION, M.DIRECCION_USUARIO_LOCALIDAD, M.DIRECCION_USUARIO_PROVINCIA
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_LOCAL */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_local
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
SELECT DISTINCT INTER_DATA.get_calle(M.LOCAL_DIRECCION) AS Calle,
				INTER_DATA.get_numero(M.LOCAL_DIRECCION) AS Numero, 
				(SELECT id_Localidad 
				 FROM INTER_DATA.Localidad L 
				 JOIN INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
				 WHERE M.LOCAL_LOCALIDAD = L.Localidad AND M.LOCAL_PROVINCIA = P.Nombre
				)
	FROM gd_esquema.Maestra M
	WHERE M.LOCAL_DIRECCION IS NOT NULL
	GROUP BY M.LOCAL_DIRECCION, M.LOCAL_LOCALIDAD, M.LOCAL_PROVINCIA
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_OPERADOR */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_operador
AS
INSERT INTO INTER_DATA.aux(Calle, Numero)
SELECT DISTINCT INTER_DATA.get_calle(OPERADOR_RECLAMO_DIRECCION) AS Calle,
				INTER_DATA.get_numero(OPERADOR_RECLAMO_DIRECCION) AS Numero
	FROM gd_esquema.Maestra
	WHERE OPERADOR_RECLAMO_DIRECCION IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_REPARTIDOR */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_repartidor
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
	SELECT 
		DISTINCT INTER_DATA.get_calle(REPARTIDOR_DIRECION) AS Calle,
		INTER_DATA.get_numero(REPARTIDOR_DIRECION) AS Numero,
		(SELECT id_Localidad FROM INTER_DATA.Localidad L
		JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
		WHERE
			(CASE
				WHEN M.ENVIO_MENSAJERIA_FECHA >= M.PEDIDO_FECHA THEN M.ENVIO_MENSAJERIA_LOCALIDAD
				ELSE M.LOCAL_LOCALIDAD
			END
		) = L.Localidad
		AND 
			(CASE
				WHEN M.ENVIO_MENSAJERIA_FECHA >= M.PEDIDO_FECHA THEN M.ENVIO_MENSAJERIA_PROVINCIA
				ELSE M.LOCAL_PROVINCIA
			END
		) = P.Nombre
				
		)
	FROM gd_esquema.Maestra M
	WHERE M.REPARTIDOR_DIRECION IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_DESTINO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_destino
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
	SELECT 
		DISTINCT INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_DEST) AS Calle,
		INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_DEST) AS Numero, 
		(SELECT id_Localidad 
		 FROM INTER_DATA.Localidad L 
		 JOIN INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
		 WHERE M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
		)
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_DIR_DEST IS NOT NULL
	GROUP BY M.ENVIO_MENSAJERIA_DIR_DEST, M.ENVIO_MENSAJERIA_LOCALIDAD, M.ENVIO_MENSAJERIA_PROVINCIA
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_ORIGEN */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_origen
AS
INSERT INTO INTER_DATA.aux(Calle, Numero, Localidad_id_Localidad)
	SELECT 
		DISTINCT INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_ORIG) AS Calle,
		INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_ORIG) AS Numero, 
		(SELECT id_Localidad 
		 FROM INTER_DATA.Localidad L 
		 JOIN INTER_DATA.Provincia P ON L.Provincia_id_Provincia = P.id_Provincia
		 WHERE M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
		)
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_DIR_ORIG IS NOT NULL
	GROUP BY M.ENVIO_MENSAJERIA_DIR_ORIG, M.ENVIO_MENSAJERIA_LOCALIDAD, M.ENVIO_MENSAJERIA_PROVINCIA
GO

/* ---------------------------------------------------------------- */


/* DIRECCIONES */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion
AS
CREATE TABLE INTER_DATA.aux (
	  id_direccion INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
	  Calle NVARCHAR(50) NOT NULL,
	  Numero DECIMAL(18,0)  NOT NULL,
	  Localidad_id_Localidad INT
)
EXEC INTER_DATA.migrar_direccion_usuarios
EXEC INTER_DATA.migrar_direccion_local
EXEC INTER_DATA.migrar_direccion_operador
EXEC INTER_DATA.migrar_direccion_repartidor
EXEC INTER_DATA.migrar_direccion_destino
EXEC INTER_DATA.migrar_direccion_origen
INSERT INTO INTER_DATA.Direccion(Calle, Numero, Localidad_id_Localidad)
		SELECT 
			DISTINCT Calle, 
			Numero, 
			Localidad_id_Localidad
		FROM INTER_DATA.aux
		ORDER BY Calle, Numero
DROP TABLE INTER_DATA.aux
GO

/* ---------------------------------------------------------------- */


/* SP TIPO_MOVILIDAD */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tipo_movilidad
AS
INSERT INTO INTER_DATA.Tipo_Movilidad(Tipo_Movilidad)
	SELECT 
		DISTINCT(REPARTIDOR_TIPO_MOVILIDAD) 
	FROM gd_esquema.Maestra
	WHERE REPARTIDOR_TIPO_MOVILIDAD IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP REPARTIDOR */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_repartidor
AS
INSERT INTO INTER_DATA.Repartidor(Nombre, Apellido, DNI, Telefono, Mail,Fecha_Nac, Direccion_id_direccion, Tipo_Movilidad_id_Tipo_Movilidad)
	SELECT DISTINCT M.REPARTIDOR_NOMBRE,
					M.REPARTIDOR_APELLIDO,
					M.REPARTIDOR_DNI,
					M.REPARTIDOR_TELEFONO,
					M.REPARTIDOR_EMAIL,
					M.REPARTIDOR_FECHA_NAC,
					(SELECT DISTINCT id_direccion
						FROM INTER_DATA.Direccion A
						WHERE INTER_DATA.get_calle(M.REPARTIDOR_DIRECION) = A.Calle
						AND INTER_DATA.get_numero(M.REPARTIDOR_DIRECION) = A.Numero
						AND A.Localidad_id_Localidad IS NULL
					),
					(SELECT id_Tipo_Movilidad
						FROM INTER_DATA.Tipo_Movilidad TM
						WHERE M.REPARTIDOR_TIPO_MOVILIDAD = TM.Tipo_Movilidad
					)
	FROM gd_esquema.Maestra M
	WHERE M.REPARTIDOR_NOMBRE		IS NOT NULL
	AND M.REPARTIDOR_APELLIDO		IS NOT NULL
	AND M.REPARTIDOR_DNI			IS NOT NULL
	AND M.REPARTIDOR_TELEFONO		IS NOT NULL
	AND M.REPARTIDOR_EMAIL			IS NOT NULL
	AND M.REPARTIDOR_FECHA_NAC		IS NOT NULL
	AND M.REPARTIDOR_DIRECION		IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP ENVIO_MENSAJERIA*/ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_envio_mensajeria
AS
INSERT INTO INTER_DATA.aux(Precio_Envio, Propina, Tiempo_Estimado, Fecha_Entrega, Direccion_id_direccion_destino, Repartidor_id_repartidor)
	SELECT DISTINCT M.ENVIO_MENSAJERIA_PRECIO_ENVIO,
				 M.ENVIO_MENSAJERIA_PROPINA,
				 M.ENVIO_MENSAJERIA_TIEMPO_ESTIMADO,
				 M.ENVIO_MENSAJERIA_FECHA_ENTREGA,
				 (SELECT DISTINCT id_direccion
					  FROM INTER_DATA.Direccion D
					  JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
					  JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
					  WHERE INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Calle
					  AND INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Numero
					  AND M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad
					  AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
				 ),
				 (SELECT DISTINCT id_repartidor
				  FROM INTER_DATA.Repartidor R
				  WHERE M.REPARTIDOR_APELLIDO = R.Apellido
					AND M.REPARTIDOR_DNI =		R.DNI
					AND M.REPARTIDOR_FECHA_NAC= R.Fecha_Nac
				 )
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_PRECIO_ENVIO		IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_PROPINA			IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_TIEMPO_ESTIMADO	IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_FECHA_ENTREGA		IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_DIR_DEST			IS NOT NULL
GO

/* ---------------------------------------------------------------- */

/* SP ENVIO_PEDIDO*/ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_envio_pedido
AS
INSERT INTO INTER_DATA.aux(Precio_Envio, Propina, Tiempo_Estimado, Fecha_Entrega, Direccion_id_direccion_destino, Repartidor_id_repartidor)
	SELECT DISTINCT M.PEDIDO_PRECIO_ENVIO,
				 M.PEDIDO_PROPINA,
				 M.PEDIDO_TIEMPO_ESTIMADO_ENTREGA,
				 M.PEDIDO_FECHA_ENTREGA,
				 (SELECT DISTINCT id_direccion
				  FROM INTER_DATA.Direccion D
				  JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
				  JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
				  WHERE INTER_DATA.get_calle(M.DIRECCION_USUARIO_DIRECCION) = D.Calle
				  AND INTER_DATA.get_numero(M.DIRECCION_USUARIO_DIRECCION) = D.Numero
				  AND M.DIRECCION_USUARIO_LOCALIDAD = L.Localidad
				  AND M.DIRECCION_USUARIO_PROVINCIA = P.Nombre
				 ),
				 (SELECT DISTINCT id_repartidor
					FROM INTER_DATA.Repartidor R
					WHERE M.REPARTIDOR_NOMBRE    =	R.Nombre
					  AND M.REPARTIDOR_APELLIDO  =	R.Apellido
					  AND M.REPARTIDOR_DNI       =	R.DNI
					  AND M.REPARTIDOR_TELEFONO  =	R.Telefono
					  AND M.REPARTIDOR_EMAIL     =	R.Mail
					  AND M.REPARTIDOR_FECHA_NAC =	R.Fecha_Nac
				 )
	FROM gd_esquema.Maestra M
	WHERE M.PEDIDO_PRECIO_ENVIO				IS NOT NULL
	  AND M.PEDIDO_PROPINA					IS NOT NULL
	  AND M.PEDIDO_TIEMPO_ESTIMADO_ENTREGA	IS NOT NULL
	  AND M.PEDIDO_FECHA_ENTREGA			IS NOT NULL
	  AND M.DIRECCION_USUARIO_DIRECCION		IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP ENVIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_envio
AS
CREATE TABLE INTER_DATA.aux (
	  id_envio INT IDENTITY(1,1) PRIMARY KEY	NOT NULL,
	  Precio_Envio DECIMAL(18,2)				NOT NULL,
	  Propina DECIMAL(18,2)						NOT NULL,
	  Tiempo_Estimado DECIMAL(18,2)				NOT NULL,
	  Fecha_Entrega DATETIME					NOT NULL,
	  Direccion_id_direccion_destino INT		NOT NULL,
	  Repartidor_id_repartidor INT				NOT NULL
)
EXEC INTER_DATA.migrar_envio_mensajeria
EXEC INTER_DATA.migrar_envio_pedido
INSERT INTO INTER_DATA.Envio(Precio_Envio, Propina, Tiempo_Estimado, Fecha_Entrega, Direccion_id_direccion_destino, Repartidor_id_repartidor)
		SELECT 
			DISTINCT Precio_Envio, 
			Propina, Tiempo_Estimado, 
			Fecha_Entrega, 
			Direccion_id_direccion_destino, 
			Repartidor_id_repartidor 
		FROM INTER_DATA.aux
		ORDER BY Precio_Envio
DROP TABLE INTER_DATA.aux
GO

/* ---------------------------------------------------------------- */


/* SP MENSAJERIA */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_mensajeria
AS
INSERT INTO INTER_DATA.Mensajeria(Numero, KM, Observaciones, Precio_Seguro, Total, Fecha, Calificacion, Paquete_id_Paquete, Usuario_id_usuario, Envio_id_envio, Tipo_Estado_id_Tipo_Estado, Tipo_Medio_Pago_id_tipo_medio_pago, Direccion_id_direccion_origen)
	SELECT DISTINCT M.ENVIO_MENSAJERIA_NRO,
				 M.ENVIO_MENSAJERIA_KM,
				 M.ENVIO_MENSAJERIA_OBSERV,
				 M.ENVIO_MENSAJERIA_PRECIO_SEGURO,
				 M.ENVIO_MENSAJERIA_TOTAL,
				 M.ENVIO_MENSAJERIA_FECHA,
				 M.ENVIO_MENSAJERIA_CALIFICACION,
				 (SELECT DISTINCT id_Paquete
					  FROM INTER_DATA.Paquete P
					  JOIN INTER_DATA.Tipo_Paquete TP ON P.Tipo_Paquete_id_tipo_paquete = TP.id_tipo_paquete
					  WHERE M.ENVIO_MENSAJERIA_VALOR_ASEGURADO = P.Valor_Asegurado
						AND M.PAQUETE_TIPO = TP.Nombre
				 ),
				 (SELECT DISTINCT id_usuario
					  FROM INTER_DATA.Usuario U
					  WHERE U.DNI = M.USUARIO_DNI
						AND    U.Fecha_Registro = M.USUARIO_FECHA_REGISTRO
				 ),
				 (SELECT DISTINCT id_envio
					  FROM INTER_DATA.Envio ENV
					  WHERE M.ENVIO_MENSAJERIA_FECHA_ENTREGA = ENV.Fecha_Entrega
						AND M.ENVIO_MENSAJERIA_PRECIO_ENVIO = ENV.Precio_Envio
						AND M.ENVIO_MENSAJERIA_PROPINA = ENV.Propina
						AND M.ENVIO_MENSAJERIA_TIEMPO_ESTIMADO = ENV.Tiempo_Estimado
				 ),
				 (SELECT DISTINCT id_Tipo_Estado
					  FROM INTER_DATA.Tipo_Estado E
					  WHERE M.ENVIO_MENSAJERIA_ESTADO = E.Nombre
				 ),
				 (SELECT DISTINCT id_tipo_medio_pago
					FROM INTER_DATA.Tipo_Medio_Pago MP
					WHERE M.MEDIO_PAGO_TIPO = MP.Nombre
					AND MP.Tarjeta_Usuario_Numero = MEDIO_PAGO_NRO_TARJETA
				 ),
				 (SELECT DISTINCT id_direccion
					  FROM INTER_DATA.Direccion D
					  JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
					  JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
					  WHERE INTER_DATA.get_calle(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Calle
					  AND INTER_DATA.get_numero(M.ENVIO_MENSAJERIA_DIR_ORIG) = D.Numero
					  AND M.ENVIO_MENSAJERIA_LOCALIDAD = L.Localidad
					  AND M.ENVIO_MENSAJERIA_PROVINCIA = P.Nombre
				 )
	FROM gd_esquema.Maestra M
	WHERE M.ENVIO_MENSAJERIA_NRO				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_KM					IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_OBSERV				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_PRECIO_SEGURO		IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_TOTAL				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_FECHA				IS NOT NULL
	  AND M.ENVIO_MENSAJERIA_CALIFICACION		IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP DIRECCION_USUARIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_direccion_usuario
AS
INSERT INTO INTER_DATA.Direccion_Usuario(Nombre,Usuario_id_usuario,Direccion_id_direccion)
	SELECT 
		DISTINCT DIRECCION_USUARIO_NOMBRE,
		(SELECT id_usuario
		 FROM INTER_DATA.Usuario U
		 WHERE  U.Apellido = M.USUARIO_APELLIDO
		 AND    U.DNI = M.USUARIO_DNI
		 AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
			),
		(SELECT id_direccion
				FROM INTER_DATA.Direccion D
				JOIN INTER_DATA.Localidad L ON L.id_Localidad = D.Localidad_id_Localidad
				JOIN INTER_DATA.Provincia P ON P.id_Provincia = L.Provincia_id_Provincia
				WHERE INTER_DATA.get_calle(M.DIRECCION_USUARIO_DIRECCION) = D.Calle
				AND INTER_DATA.get_numero(M.DIRECCION_USUARIO_DIRECCION) = D.Numero
				AND M.DIRECCION_USUARIO_LOCALIDAD = L.Localidad
				AND M.DIRECCION_USUARIO_PROVINCIA = P.Nombre
			)
	FROM gd_esquema.Maestra M
	WHERE 
		DIRECCION_USUARIO_NOMBRE		IS NOT NULL 
GO


/* ---------------------------------------------------------------- */


/* SP LOCAL_TIPO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_local_tipo
AS
INSERT INTO INTER_DATA.Local_Tipo(Tipo)
	SELECT DISTINCT Local_Tipo 
		FROM gd_esquema.Maestra
			WHERE LOCAL_TIPO IS NOT NULL
GO


/* ---------------------------------------------------------------- */

/* ---------------------------------------------------------------- */


/* SP LOCAL_CATEGORIA */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_local_categoria
AS
INSERT INTO INTER_DATA.Local_Categoria(Categoria)
	VALUES ('parrilla')
INSERT INTO INTER_DATA.Local_Categoria(Categoria)
	VALUES('kiosco')
GO

/* ---------------------------------------------------------------- */


/* SP LOCAL */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_local
AS
INSERT INTO INTER_DATA.Local(Nombre, Descripcion, Local_Tipo_id_Local_Tipo, Local_Categoria_id_Local_Categoria, Direccion_id_direccion)
SELECT DISTINCT
	LOCAL_NOMBRE, 
	LOCAL_DESCRIPCION,(
		SELECT id_Local_Tipo 
		FROM INTER_DATA.Local_Tipo T 
		WHERE M.LOCAL_TIPO = T.Tipo),
		(
		SELECT id_Local_Tipo 
		FROM INTER_DATA.Local_Tipo T 
		WHERE M.LOCAL_TIPO = T.Tipo)
		,(
		SELECT id_direccion 
		FROM INTER_DATA.Direccion D JOIN INTER_DATA.Localidad L ON D.Localidad_id_Localidad = L.id_Localidad
		WHERE INTER_DATA.get_calle(M.LOCAL_DIRECCION) = D.Calle 
		AND INTER_DATA.get_numero(M.LOCAL_DIRECCION) = D.Numero 
		AND L.Localidad = M.LOCAL_LOCALIDAD) 
FROM gd_esquema.Maestra M
WHERE M.LOCAL_NOMBRE�IS�NOT�NULL
GO

/* ---------------------------------------------------------------- */


/* SP PEDIDO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_pedido
AS
INSERT INTO INTER_DATA.Pedido(Numero,Fecha,Tarifa_Servicio,Total_Productos,Observaciones,Calificacion,Total_Cupones,Total_Servicio,Usuario_id_usuario,Local_id_local,Envio_id_envio,Tipo_Estado_id_Tipo_Estado,Tipo_Medio_Pago_id_tipo_medio_pago)
	SELECT DISTINCT
		PEDIDO_NRO,
		PEDIDO_FECHA,
		PEDIDO_TARIFA_SERVICIO,
		PEDIDO_TOTAL_PRODUCTOS,
		PEDIDO_OBSERV,
		PEDIDO_CALIFICACION,
		PEDIDO_TOTAL_CUPONES,
		PEDIDO_TOTAL_SERVICIO,
		(SELECT id_usuario
			 FROM INTER_DATA.Usuario U
			 WHERE  U.Apellido = M.USUARIO_APELLIDO
			 AND    U.DNI = M.USUARIO_DNI
			 AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
		),
		(SELECT id_local
			FROM INTER_DATA.Local L
			WHERE M.LOCAL_DESCRIPCION = L.Descripcion
			AND M.LOCAL_NOMBRE = L.Nombre
		) ,
		(SELECT DISTINCT id_envio
					  FROM INTER_DATA.Envio ENV
					  WHERE M.PEDIDO_FECHA_ENTREGA = ENV.Fecha_Entrega
						AND M.PEDIDO_PRECIO_ENVIO = ENV.Precio_Envio
						AND M.PEDIDO_PROPINA = ENV.Propina
						AND M.PEDIDO_TIEMPO_ESTIMADO_ENTREGA = ENV.Tiempo_Estimado
				 ),
		(SELECT DISTINCT id_Tipo_Estado
					  FROM INTER_DATA.Tipo_Estado E
					  WHERE M.PEDIDO_ESTADO = E.Nombre
				 ),
		(SELECT DISTINCT id_tipo_medio_pago
					FROM INTER_DATA.Tipo_Medio_Pago MP
					JOIN INTER_DATA.Tarjeta_Usuario TU ON MP.Tarjeta_Usuario_Numero = TU.Numero
					WHERE M.MEDIO_PAGO_TIPO = MP.Nombre
					AND TU.Numero = MEDIO_PAGO_NRO_TARJETA
				 ) 
		
	FROM gd_esquema.Maestra M
	WHERE PEDIDO_NRO				IS NOT NULL 
		AND PEDIDO_FECHA			IS NOT NULL  
		AND PEDIDO_TARIFA_SERVICIO	IS NOT NULL  
		AND PEDIDO_TOTAL_PRODUCTOS	IS NOT NULL  
		AND PEDIDO_OBSERV			IS NOT NULL  
		AND PEDIDO_CALIFICACION		IS NOT NULL  
		AND PEDIDO_TOTAL_CUPONES	IS NOT NULL  
		AND PEDIDO_TOTAL_SERVICIO	IS NOT NULL 
		
GO

/* ---------------------------------------------------------------- */


/* SP HORARIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_horario
AS
INSERT INTO INTER_DATA.Horario(Hora_Apertura, Hora_CIerre, Dia)
SELECT DISTINCT
	HORARIO_LOCAL_HORA_APERTURA, 
	HORARIO_LOCAL_HORA_CIERRE,
	HORARIO_LOCAL_DIA 
FROM gd_esquema.Maestra M
WHERE HORARIO_LOCAL_DIA�IS�NOT�NULL
GO

/* ---------------------------------------------------------------- */


/* SP PRODUCTO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_producto
AS
INSERT INTO INTER_DATA.Producto(Codigo, Nombre, Descripcion, Precio, Local_id_local)
SELECT DISTINCT
	PRODUCTO_LOCAL_CODIGO, 
	PRODUCTO_LOCAL_NOMBRE,
	PRODUCTO_LOCAL_DESCRIPCION, 
	PRODUCTO_LOCAL_PRECIO, (
		SELECT id_local 
		FROM INTER_DATA.Local L
		WHERE L.Nombre = M.LOCAL_NOMBRE)  
FROM gd_esquema.Maestra M
WHERE PRODUCTO_LOCAL_CODIGO�IS�NOT�NULL
GO

/* ---------------------------------------------------------------- */


/* SP ESTADO_RECLAMO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_estado_reclamo
AS
INSERT INTO INTER_DATA.Estado_Reclamo(Estado)
	SELECT 
		DISTINCT(RECLAMO_ESTADO) 
	FROM gd_esquema.Maestra
	WHERE RECLAMO_ESTADO IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP TIPO_RECLAMO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tipo_reclamo
AS
INSERT INTO INTER_DATA.Tipo_Reclamo(Tipo)
	SELECT 
		DISTINCT(RECLAMO_TIPO) 
	FROM gd_esquema.Maestra
	WHERE RECLAMO_TIPO IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP OPERADOR */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_operador
AS
INSERT INTO INTER_DATA.OPERADOR(Nombre, Apellido, DNI, Telefono, Mail,Fecha_Nac, Direccion_id_direccion)
	SELECT DISTINCT M.OPERADOR_RECLAMO_NOMBRE,
					M.OPERADOR_RECLAMO_APELLIDO,
					M.OPERADOR_RECLAMO_DNI,
					M.OPERADOR_RECLAMO_TELEFONO,
					M.OPERADOR_RECLAMO_MAIL,
					M.OPERADOR_RECLAMO_FECHA_NAC,
					(SELECT DISTINCT id_direccion
					 FROM INTER_DATA.Direccion A
					 WHERE INTER_DATA.get_calle(M.OPERADOR_RECLAMO_DIRECCION) = A.Calle
					 AND INTER_DATA.get_numero(M.OPERADOR_RECLAMO_DIRECCION) = A.numero
					 AND A.Localidad_id_Localidad IS NULL
					)
	FROM gd_esquema.Maestra M
	WHERE M.OPERADOR_RECLAMO_NOMBRE		IS NOT NULL
	  AND M.OPERADOR_RECLAMO_APELLIDO	IS NOT NULL
	  AND M.OPERADOR_RECLAMO_DNI		IS NOT NULL
	  AND M.OPERADOR_RECLAMO_TELEFONO	IS NOT NULL
	  AND M.OPERADOR_RECLAMO_MAIL		IS NOT NULL
	  AND M.OPERADOR_RECLAMO_FECHA_NAC	IS NOT NULL
	  AND M.OPERADOR_RECLAMO_DIRECCION	IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP TIPO_CUPON */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_tipo_cupon
AS
INSERT INTO INTER_DATA.Tipo_Cupon(Tipo)
	SELECT 
		DISTINCT(CUPON_TIPO) 
	FROM gd_esquema.Maestra
	WHERE CUPON_TIPO IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP CUPON_RECLAMO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_cupon_reclamo
AS
INSERT INTO INTER_DATA.cupon(Numero, Monto, Fecha_Alta, Fecha_Vencimiento,Tipo_Cupon_id_Tipo_Cupon)
	SELECT 
		DISTINCT M.CUPON_RECLAMO_NRO,
				 M.CUPON_RECLAMO_MONTO,
				 M.CUPON_RECLAMO_FECHA_ALTA,
				 M.CUPON_RECLAMO_FECHA_VENCIMIENTO,
				 (SELECT id_Tipo_Cupon
				  FROM INTER_DATA.Tipo_Cupon TC
				  WHERE M.CUPON_RECLAMO_TIPO = TC.Tipo
				  )
	FROM gd_esquema.Maestra M
	WHERE CUPON_RECLAMO_TIPO IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP CUPON_CUPON */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_cupon_cupon
AS
INSERT INTO INTER_DATA.cupon(Numero,Monto,Fecha_Alta,Fecha_Vencimiento,Tipo_Cupon_id_Tipo_Cupon)
	SELECT 
		DISTINCT CUPON_NRO,
		CUPON_MONTO,
		CUPON_FECHA_ALTA,
		CUPON_FECHA_VENCIMIENTO,
		(SELECT id_Tipo_Cupon
		 FROM INTER_DATA.Tipo_Cupon TC
		 WHERE TC.Tipo = M.CUPON_TIPO
			)
			
	FROM gd_esquema.Maestra M
	WHERE 
		CUPON_NRO		            IS NOT NULL
		AND CUPON_MONTO             IS NOT NULL
		AND CUPON_FECHA_ALTA        IS NOT NULL
		AND CUPON_FECHA_VENCIMIENTO IS NOT NULL
		AND M.CUPON_TIPO != 'Devoluci�n'
GO

/* ---------------------------------------------------------------- */


/* SP RECLAMO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_reclamo
AS
INSERT INTO INTER_DATA.reclamo(Numero,Fecha,Descripcion,Fecha_Solucion,Solucion,Calificacion,Pedido_Numero,Estado_Reclamo_id_Estado_Reclamo,Tipo_Reclamo_id_Tipo_Reclamo,Operador_id_operador)
	SELECT DISTINCT
		RECLAMO_NRO,
		RECLAMO_FECHA,
		RECLAMO_DESCRIPCION,
		RECLAMO_FECHA_SOLUCION,
		RECLAMO_SOLUCION,
		RECLAMO_CALIFICACION,
		(SELECT Numero
			FROM INTER_DATA.Pedido P
			WHERE P.Numero = M.PEDIDO_NRO
		) ,
		(SELECT id_Estado_Reclamo
			FROM INTER_DATA.Estado_Reclamo ER
			WHERE ER.Estado = M.RECLAMO_ESTADO
		) ,
		(SELECT id_Tipo_Reclamo
			FROM INTER_DATA.Tipo_Reclamo TR
			WHERE TR.Tipo = M.RECLAMO_TIPO
		) ,
		(SELECT id_operador
			FROM INTER_DATA.Operador O
			WHERE O.DNI = M.OPERADOR_RECLAMO_DNI
			AND O.Nombre = M.OPERADOR_RECLAMO_NOMBRE
			AND O.Apellido = M.OPERADOR_RECLAMO_APELLIDO
			AND O.Telefono = M.OPERADOR_RECLAMO_TELEFONO
			AND O.Fecha_Nac = M.OPERADOR_RECLAMO_FECHA_NAC
			AND O.Mail = M.OPERADOR_RECLAMO_MAIL
		)
	FROM gd_esquema.Maestra M
	WHERE RECLAMO_NRO			IS NOT NULL 
	AND RECLAMO_FECHA			IS NOT NULL 
	AND RECLAMO_DESCRIPCION		IS NOT NULL 
	AND RECLAMO_FECHA_SOLUCION	IS NOT NULL 
	AND RECLAMO_SOLUCION		IS NOT NULL 
	AND RECLAMO_CALIFICACION	IS NOT NULL 

GO


/* ---------------------------------------------------------------- */


/* SP CUPON_X_USUARIO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_cupon_x_usuario 
AS
INSERT INTO INTER_DATA.Cupon_X_Usuario(Cupon_Numero_Cupon,Usuario_id_Usuario)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			WHERE C.Numero = M.CUPON_RECLAMO_NRO
			OR C.Numero = M.CUPON_NRO
		) ,
		(SELECT id_usuario
			 FROM INTER_DATA.Usuario U
			 WHERE  U.Apellido = M.USUARIO_APELLIDO
			 AND    U.DNI = M.USUARIO_DNI
			 AND    U.Fecha_Nac = M.USUARIO_FECHA_NAC
			)
	FROM gd_esquema.Maestra M
	WHERE CUPON_RECLAMO_NRO IS NOT NULL
	OR CUPON_NRO IS NOT NULL
	AND M.CUPON_TIPO != 'Devoluci�n'
GO

/* ---------------------------------------------------------------- */


/* SP CUPON_X_LOCAL */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_cupon_x_local 
AS
INSERT INTO INTER_DATA.Cupon_X_Local(Cupon_Numero_Cupon,Local_id_Local)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			WHERE C.Numero = M.CUPON_RECLAMO_NRO
			OR C.Numero = M.CUPON_NRO
		) ,
		(SELECT id_local
			FROM INTER_DATA.Local L
			WHERE M.LOCAL_DESCRIPCION = L.Descripcion
			AND M.LOCAL_NOMBRE = L.Nombre
		)
	FROM gd_esquema.Maestra M
	WHERE CUPON_NRO IS NOT NULL 
	OR CUPON_RECLAMO_NRO IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP CUPON_X_RECLAMO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_cupon_x_reclamo 
AS
INSERT INTO INTER_DATA.Cupon_X_Reclamo(Cupon_Numero_Cupon,Reclamo_Numero_Reclamo)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			JOIN INTER_DATA.Tipo_Cupon TC ON C.Tipo_Cupon_id_Tipo_Cupon = TC.id_Tipo_Cupon
			WHERE C.Numero = M.CUPON_RECLAMO_NRO
			AND TC.Tipo = M.CUPON_RECLAMO_TIPO
		) ,
		(SELECT Numero
			FROM INTER_DATA.Reclamo P
			WHERE P.Numero = M.RECLAMO_NRO
		)
	FROM gd_esquema.Maestra M
	WHERE CUPON_RECLAMO_NRO IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP ITEM_PEDIDO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_item_pedido
AS
INSERT INTO INTER_DATA.item_pedido(Pedido_Numero,Producto_Codigo,Cantidad,Precio)
	SELECT DISTINCT
		(SELECT DISTINCT Numero
			FROM INTER_DATA.Pedido P
			WHERE P.Numero = M.PEDIDO_NRO
		) ,
		(SELECT DISTINCT id_producto
			FROM INTER_DATA.Producto PROD
			JOIN INTER_DATA.Local L ON L.id_local = PROD.Local_id_local
			WHERE PROD.Codigo = M.PRODUCTO_LOCAL_CODIGO
			AND L.Nombre = M.LOCAL_NOMBRE
		),
		PRODUCTO_CANTIDAD,
		PRODUCTO_LOCAL_PRECIO
	FROM gd_esquema.Maestra M
	WHERE PRODUCTO_CANTIDAD			IS NOT NULL 
	  AND PRODUCTO_LOCAL_PRECIO		IS NOT NULL
		
GO

/* ---------------------------------------------------------------- */


/* SP HORARIO_X_LOCAL */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_horario_x_local
AS
INSERT INTO INTER_DATA.Horario_X_Local(Local_id_local, Horario_id_horario)
	SELECT DISTINCT(
		SELECT id_local 
			FROM INTER_DATA.Local L 
				WHERE L.Nombre = M.LOCAL_NOMBRE), 
		(SELECT id_horario 
			FROM INTER_DATA.Horario H 
				WHERE H.Dia = HORARIO_LOCAL_DIA 
					AND H.Hora_Apertura = HORARIO_LOCAL_HORA_APERTURA 
					AND H.Hora_CIerre = HORARIO_LOCAL_HORA_CIERRE)
	FROM gd_esquema.Maestra M
	WHERE M.LOCAL_NOMBRE IS NOT NULL
GO

/* ---------------------------------------------------------------- */


/* SP CUPON_X_PEDIDO */ /* LISTO */

CREATE PROCEDURE INTER_DATA.migrar_Cupon_X_Pedido  
AS
INSERT INTO INTER_DATA.Cupon_X_Pedido(Cupon_Numero,Pedido_Numero)
	SELECT DISTINCT
		 (SELECT Numero
			FROM INTER_DATA.Cupon C
			WHERE C.Numero = M.CUPON_NRO
		) ,
		(SELECT Numero
			FROM INTER_DATA.Pedido P
			WHERE P.Numero = M.PEDIDO_NRO
		)
	FROM gd_esquema.Maestra M
	WHERE CUPON_NRO IS NOT NULL
GO


EXEC INTER_DATA.migrar_usuario
EXEC INTER_DATA.migrar_tipo_paquete
EXEC INTER_DATA.migrar_paquete
EXEC INTER_DATA.migrar_tarjeta_usuario
EXEC INTER_DATA.migrar_tipo_medio_pago
EXEC INTER_DATA.migrar_tipo_estado
EXEC INTER_DATA.migrar_provincia
EXEC INTER_DATA.migrar_localidad
EXEC INTER_DATA.migrar_direccion
EXEC INTER_DATA.migrar_tipo_movilidad
EXEC INTER_DATA.migrar_repartidor
EXEC INTER_DATA.migrar_envio
EXEC INTER_DATA.migrar_mensajeria
EXEC INTER_DATA.migrar_direccion_usuario
EXEC INTER_DATA.migrar_local_tipo
EXEC INTER_DATA.migrar_local_categoria
EXEC INTER_DATA.migrar_local
EXEC INTER_DATA.migrar_pedido
EXEC INTER_DATA.migrar_horario
EXEC INTER_DATA.migrar_producto
EXEC INTER_DATA.migrar_estado_reclamo
EXEC INTER_DATA.migrar_tipo_reclamo
EXEC INTER_DATA.migrar_operador
EXEC INTER_DATA.migrar_tipo_cupon
EXEC INTER_DATA.migrar_cupon_reclamo
EXEC INTER_DATA.migrar_cupon_cupon
EXEC INTER_DATA.migrar_reclamo
EXEC INTER_DATA.migrar_cupon_x_usuario
EXEC INTER_DATA.migrar_cupon_x_local
EXEC INTER_DATA.migrar_cupon_x_reclamo
EXEC INTER_DATA.migrar_item_pedido
EXEC INTER_DATA.migrar_horario_x_local
EXEC INTER_DATA.migrar_cupon_x_pedido