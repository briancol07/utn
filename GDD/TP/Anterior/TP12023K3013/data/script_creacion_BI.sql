USE GD1C2023
GO

-- estado_reclamo(D)
-- rango_etario(D)
-- estado_envio(D)
-- tipo_movilidad(D)
-- tipo_paque(D)
-- reclamos(H)
-- envios(H)
-- local(D)
-- tiempo(D)
-- dia_de_la_semana(D)
-- rango_horario(D)
-- pedidos(H)
-- ubicacion(D)
-- estado_pedido(D)
-- tipo_medio_de_pago(D)

----------------------------------------------------------
---- Eliminaci�n de Tablas, Funciones, etc existentes ----
----------------------------------------------------------


IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'BI_get_rango_horario')
	DROP FUNCTION INTER_DATA.BI_get_rango_horario
GO

IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'BI_get_edad')
	DROP FUNCTION INTER_DATA.BI_get_edad
GO

IF EXISTS (SELECT name FROM sys.objects WHERE type_desc = 'SQL_SCALAR_FUNCTION' AND name = 'BI_get_rango_etario')
	DROP FUNCTION INTER_DATA.BI_get_rango_etario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_pedidos')
	DROP TABLE INTER_DATA.BI_pedidos
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_reclamos')
	DROP TABLE INTER_DATA.BI_reclamos
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_envios')
	DROP TABLE INTER_DATA.BI_envios
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_estado_reclamo')
	DROP TABLE INTER_DATA.BI_estado_Reclamo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_tipo_reclamo')
	DROP TABLE INTER_DATA.BI_tipo_Reclamo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_rango_etario')
	DROP TABLE INTER_DATA.BI_rango_etario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_estado_envio')
	DROP TABLE INTER_DATA.BI_estado_envio
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_tipo_movilidad')
	DROP TABLE INTER_DATA.BI_tipo_movilidad
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_tipo_paquete')
	DROP TABLE INTER_DATA.BI_tipo_paquete
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_tipo_envio')
	DROP TABLE INTER_DATA.BI_tipo_envio
GO


IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_local')
	DROP TABLE INTER_DATA.BI_local
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_tiempo')
	DROP TABLE INTER_DATA.BI_tiempo
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_dia_de_la_semana')
	DROP TABLE INTER_DATA.BI_dia_de_la_semana
GO
IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_rango_horario')
	DROP TABLE INTER_DATA.BI_rango_horario
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_ubicacion')
	DROP TABLE INTER_DATA.BI_ubicacion
GO
IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_estado_pedido')
	DROP TABLE INTER_DATA.BI_estado_pedido
GO
IF EXISTS (SELECT name FROM sys.tables WHERE name = 'BI_tipo_medio_de_pago')
	DROP TABLE INTER_DATA.BI_tipo_medio_de_pago
GO


IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tipo_movilidad' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_tipo_movilidad
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_estado_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_estado_reclamo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tipo_reclamo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_tipo_reclamo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_rango_etario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_rango_etario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_estado_envio' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_estado_envio
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tipo_paquete' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_tipo_paquete
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_local' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_local
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tiempo' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_tiempo
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_dia_de_la_semana' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_dia_de_la_semana
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_rango_horario' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_rango_horario
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_ubicacion' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_ubicacion
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_estado_pedido' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_estado_pedido
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tipo_medio_de_pago' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_tipo_medio_de_pago
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_tipo_envio' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_tipo_envio
GO


IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_pedidos' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_pedidos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_reclamos' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_reclamos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name='migrar_BI_envios' AND type='p')
	DROP PROCEDURE INTER_DATA.migrar_BI_envios
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_MayorCantidadPedidos')
DROP VIEW INTER_DATA.V_MayorCantidadPedidos
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_MontoTotalNoCobrado')
DROP VIEW INTER_DATA.V_MontoTotalNoCobrado
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_PromedioEnviosPorLocalidad')
DROP VIEW INTER_DATA.V_PromedioEnviosPorLocalidad
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_DesvioPromedioTiempoEntrega')
DROP VIEW INTER_DATA.V_DesvioPromedioTiempoEntrega
GO


IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_MontoTotalCuponesPorRangoEtario')
DROP VIEW INTER_DATA.V_MontoTotalCuponesPorRangoEtario
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_PromedioCalificacionMensualPorLocal')
DROP VIEW INTER_DATA.V_PromedioCalificacionMensualPorLocal
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_PorcentajeEntregasMensuales')
DROP VIEW INTER_DATA.V_PorcentajeEntregasMensuales
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_PromedioValorAsegurado')
DROP VIEW INTER_DATA.V_PromedioValorAsegurado
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_CantidadReclamosMensualesPorLocal')
DROP VIEW INTER_DATA.V_CantidadReclamosMensualesPorLocal
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_TiempoPromedioResolucionReclamos')
DROP VIEW INTER_DATA.V_TiempoPromedioResolucionReclamos
GO

IF EXISTS (SELECT name FROM sys.objects WHERE name = 'V_MontoMensualCuponesReclamos')
DROP VIEW INTER_DATA.V_MontoMensualCuponesReclamos
GO


----------------------------------------------------------
------------- Table INTER_DATA.BI_estado_reclamo --------------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_estado_reclamo(
	estado_reclamo_id INT PRIMARY KEY NOT NULL,
	estado_reclamo NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_tipo_reclamo --------------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_tipo_reclamo(
	tipo_reclamo_id INT PRIMARY KEY NOT NULL,
	tipo_reclamo NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_rango_etario  ----------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_rango_etario (
	rango_etario_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	rango_etario NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_estado_envio------------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_estado_envio(
	estado_envio_id INT PRIMARY KEY NOT NULL,
	estado_envio NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_tipo_movilidad  --------------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_tipo_movilidad (
	tipo_movilidad_id INT PRIMARY KEY NOT NULL,
	tipo_movilidad NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_tipo_paquete   --------------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_tipo_paquete(
	tipo_paquete_id INT PRIMARY KEY NOT NULL,
	tipo_paquete NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_ubicacion       --------
----------------------------------------------------------
CREATE TABLE INTER_DATA.BI_ubicacion(
	ubicacion_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,	
	provincia NVARCHAR(255),
	localidad NVARCHAR(255),
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_estado_pedido  --------
----------------------------------------------------------
CREATE TABLE INTER_DATA.BI_estado_pedido(
	estado_pedido_id INT PRIMARY KEY NOT NULL,
	estado_pedido NVARCHAR(255)
)

----------------------------------------------------------
-----------Table INTER_DATA.BI_tipo_medio_de_pago--------
----------------------------------------------------------
CREATE TABLE INTER_DATA.BI_tipo_medio_de_pago(
	tipo_medio_de_pago_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	tipo_medio_de_pago NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_local  --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.BI_local(
	local_id INT PRIMARY KEY NOT NULL,
	local_provincia NVARCHAR(255) NOT NULL,
	local_localidad NVARCHAR(255) NOT NULL ,
	local_tipo NVARCHAR(255) NOT NULL, 
	local_categoria NVARCHAR(255) NOT NULL ,
)
----------------------------------------------------------
------------- Table INTER_DATA.BI_tiempo --------------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_tiempo(
	tiempo_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	tiempo_mes NVARCHAR(255),
	tiempo_anio NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_dia_de_la_semana--------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_dia_de_la_semana(
	dia_de_la_semana_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	dia_de_la_semana NVARCHAR(255) 
)


----------------------------------------------------------
------------- Table INTER_DATA.BI_tipo_envio--------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_tipo_envio(
	tipo_envio_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	tipo_envio NVARCHAR(255) 
)


----------------------------------------------------------
------------- Table INTER_DATA.BI_rango_horario  --------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_rango_horario(
	rango_horario_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	rango_horario NVARCHAR(255)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_reclamos  --------------
----------------------------------------------------------
CREATE TABLE INTER_DATA.BI_reclamos(
	 local_id INT  NOT NULL,
	 CONSTRAINT fk_reclamos_local
	 FOREIGN KEY (local_id) REFERENCES INTER_DATA.BI_local(local_id),

	 tiempo_id INT NOT NULL,
	 CONSTRAINT fk_reclamos_tiempo
	 FOREIGN KEY (tiempo_id) REFERENCES INTER_DATA.BI_tiempo(tiempo_id),

	 dia_de_la_semana_id INT NOT NULL,
	 CONSTRAINT fk_reclamos_dia_semana
	 FOREIGN KEY (dia_de_la_semana_id) REFERENCES INTER_DATA.BI_dia_de_la_semana(dia_de_la_semana_id),

	 rango_horario_id INT NOT NULL, 
	 CONSTRAINT fk_reclamos_rango_horario
	 FOREIGN KEY (rango_horario_id) REFERENCES INTER_DATA.BI_rango_horario(rango_horario_id),

	 estado_reclamo_id INT NOT NULL,
	 CONSTRAINT fk_reclamos_estado
	 FOREIGN KEY (estado_reclamo_id) REFERENCES INTER_DATA.BI_estado_reclamo(estado_reclamo_id),

	 tipo_reclamo_id INT NOT NULL,
	 CONSTRAINT fk_reclamos_tipo
	 FOREIGN KEY (tipo_reclamo_id) REFERENCES INTER_DATA.BI_tipo_reclamo(tipo_reclamo_id),

	 rango_etario_id INT NOT NULL,
	 CONSTRAINT fk_reclamos_rango_etario
	 FOREIGN KEY (rango_etario_id) REFERENCES INTER_DATA.BI_rango_etario(rango_etario_id),

	 reclamos_cantidad INT,
	 reclamos_tiempo_promedio_resolucion INT,
	 reclamo_total_cupones INT,

	 PRIMARY KEY (local_id, tiempo_id, dia_de_la_semana_id, rango_horario_id, estado_reclamo_id, tipo_reclamo_id, rango_etario_id)
)

----------------------------------------------------------
------------- Table INTER_DATA.BI_envios   ----
----------------------------------------------------------
CREATE TABLE INTER_DATA.BI_envios(
	ubicacion_id INT  NOT NULL,
	CONSTRAINT fk_envios_ubicacion
	FOREIGN KEY (ubicacion_id) REFERENCES INTER_DATA.BI_ubicacion(ubicacion_id),

	tiempo_id INT NOT NULL,
	CONSTRAINT fk_envios_tiempo
	FOREIGN KEY (tiempo_id) REFERENCES INTER_DATA.BI_tiempo(tiempo_id),

	dia_de_la_semana_id INT  NOT NULL,
	CONSTRAINT fk_envios_dia_semana
	FOREIGN KEY (dia_de_la_semana_id) REFERENCES INTER_DATA.BI_dia_de_la_semana(dia_de_la_semana_id),

	rango_horario_id INT  NOT NULL,
	CONSTRAINT fk_envios_rango_horario
	FOREIGN KEY (rango_horario_id) REFERENCES INTER_DATA.BI_rango_horario(rango_horario_id),

	rango_etario_id INT  NOT NULL,
	CONSTRAINT fk_envios_rango_etario
	FOREIGN KEY (rango_etario_id) REFERENCES INTER_DATA.BI_rango_etario(rango_etario_id),

	estado_envio_id INT  NOT NULL,
	CONSTRAINT fk_envios_estado_envio
	FOREIGN KEY (estado_envio_id) REFERENCES INTER_DATA.BI_estado_envio(estado_envio_id),

	tipo_movilidad_id INT  NOT NULL,
	CONSTRAINT fk_envios_tipo_movilidad
	FOREIGN KEY (tipo_movilidad_id) REFERENCES INTER_DATA.BI_tipo_movilidad(tipo_movilidad_id),

	tipo_paquete_id INT  NOT NULL,
	CONSTRAINT fk_envios_tipo_paquete
	FOREIGN KEY (tipo_paquete_id) REFERENCES INTER_DATA.BI_tipo_paquete(tipo_paquete_id),

	tipo_envio_id INT  NOT NULL,
	CONSTRAINT fk_envios_tipo_envio
	FOREIGN KEY (tipo_envio_id) REFERENCES INTER_DATA.BI_tipo_envio(tipo_envio_id),

	envios_cantidad INT,
	envios_total INT,
	envios_total_asegurado INT,
	envios_desvio_promedio INT
	
	PRIMARY KEY (ubicacion_id, tiempo_id, dia_de_la_semana_id, rango_horario_id, rango_etario_id, estado_envio_id, tipo_movilidad_id, tipo_paquete_id, tipo_envio_id) 
)


----------------------------------------------------------
------------- Table INTER_DATA.BI_pedidos--------
----------------------------------------------------------

CREATE TABLE INTER_DATA.BI_pedidos(
	local_id INT NOT NULL,
	CONSTRAINT fk_pedidos_local
	FOREIGN KEY (local_id) REFERENCES INTER_DATA.BI_local(local_id),

	dia_de_la_semana_id INT NOT NULL,
	CONSTRAINT fk_pedidos_dia_semana_id
	FOREIGN KEY (dia_de_la_semana_id) REFERENCES INTER_DATA.BI_dia_de_la_semana(dia_de_la_semana_id),

	tiempo_id INT NOT NULL, 
	CONSTRAINT fk_pedidos_tiempo
	FOREIGN KEY (tiempo_id) REFERENCES INTER_DATA.BI_tiempo(tiempo_id),

	rango_horario_id INT NOT NULL,
	CONSTRAINT fk_pedidos_rango_horario
	FOREIGN KEY (rango_horario_id) REFERENCES INTER_DATA.BI_rango_horario(rango_horario_id),

	rango_etario_id INT  NOT NULL,
	CONSTRAINT fk_pedidos_rango_etario
	FOREIGN KEY (rango_etario_id) REFERENCES INTER_DATA.BI_rango_etario(rango_etario_id),

	tipo_medio_de_pago_id INT NOT NULL,
	CONSTRAINT fk_pedidos_tipo_pago
	FOREIGN KEY (tipo_medio_de_pago_id) REFERENCES INTER_DATA.BI_tipo_medio_de_pago(tipo_medio_de_pago_id),

	estado_pedido_id INT NOT NULL,
	CONSTRAINT fk_pedidos_estado
	FOREIGN KEY (estado_pedido_id) REFERENCES INTER_DATA.BI_estado_pedido(estado_pedido_id),

	pedidos_cantidad INT,
	pedidos_total INT,
	pedidos_total_cupones INT,
	pedidos_promedio_calificacion INT,
	
	PRIMARY KEY (local_id, dia_de_la_semana_id, tiempo_id, rango_horario_id, rango_etario_id, tipo_medio_de_pago_id, estado_pedido_id)
)

----------------------------------------------------------
------------- SPs DE MIGRACION ---------------------------
----------------------------------------------------------

GO
CREATE FUNCTION INTER_DATA.BI_get_rango_horario(@hora Datetime)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @returnvalue NVARCHAR(255)

	SET @returnvalue = '0:00 - 8:00'

	IF(@hora >= 8 AND @hora < 10)
	BEGIN
		SET @returnvalue = '8:00 - 10:00'
	END
	ELSE IF(@hora >= 10 AND @hora < 12)
	BEGIN
		SET @returnvalue = '10:00 - 12:00'
	END
	ELSE IF(@hora >= 12 AND @hora < 14)
	BEGIN
		SET @returnvalue = '12:00 - 14:00'
	END
	ELSE IF(@hora >= 14 AND @hora < 16)
	BEGIN
		SET @returnvalue = '14:00 - 16:00'
	END
	ELSE IF(@hora >= 16 AND @hora < 18)
	BEGIN
		SET @returnvalue = '16:00 - 18:00'
	END
	ELSE IF(@hora >= 18 AND @hora < 20)
	BEGIN
		SET @returnvalue = '18:00 - 20:00'
	END
	ELSE IF(@hora >= 20 AND @hora < 22)
	BEGIN
		SET @returnvalue = '20:00 - 22:00'
	END
	ELSE IF(@hora >= 22 AND @hora < 0)
	BEGIN
		SET @returnvalue = '22:00 - 0:00'
	END

	RETURN @returnvalue
END
GO


CREATE FUNCTION INTER_DATA.BI_get_edad(@fdnac Datetime)
RETURNS INT
AS
BEGIN
	DECLARE @edad INT

	IF(MONTH(@fdnac) != MONTH(GETDATE()))
		SET @edad = DATEDIFF(MONTH, @fdnac, GETDATE())/12
	ELSE IF(DAY(@edad) > DAY(GETDATE()))
		SET @edad = (DATEDIFF(MONTH, @fdnac, GETDATE())/12)-1
	ELSE
	BEGIN
		SET @edad = DATEDIFF(MONTH, @fdnac, GETDATE())/12
	END
	
	RETURN @edad
END
GO


CREATE FUNCTION INTER_DATA.BI_get_rango_etario(@fdnac Datetime)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @rango_etario NVARCHAR(255)
	DECLARE @edad INT

	SET @edad = INTER_DATA.BI_get_edad(@fdnac)

	IF(@edad < 25)
	BEGIN
		SET @rango_etario = '<25'
	END
	ELSE IF(@edad >= 25 AND @edad < 35)
	BEGIN
		SET @rango_etario = '25 - 35'
	END
	ELSE IF(@edad >= 35 AND @edad < 55)
	BEGIN
		SET @rango_etario = '35 - 55'
	END
	ELSE
		SET @rango_etario = '>55'
	
	RETURN @rango_etario
END
GO


------------------------------------------------------
----------------- -- tipo_movilidad(D)----------------------
-------------------------------------------------------
GO
CREATE PROCEDURE INTER_DATA.migrar_BI_tipo_movilidad
AS
INSERT INTO INTER_DATA.BI_tipo_movilidad(tipo_movilidad_id, tipo_movilidad)
	SELECT 
		DISTINCT id_Tipo_Movilidad, Tipo_Movilidad
	FROM INTER_DATA.Tipo_Movilidad
	WHERE Tipo_Movilidad IS NOT NULL 
GO


---------------------------------------------------------------
----------------- migrar_estado_reclamo (D)--------------------
---------------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_estado_reclamo
AS
INSERT INTO INTER_DATA.BI_estado_reclamo(estado_reclamo_id, estado_reclamo)
	SELECT DISTINCT
		id_Estado_Reclamo, Estado
	FROM INTER_DATA.estado_reclamo
	WHERE Estado IS NOT NULL 
GO

---------------------------------------------------------------
----------------- migrar_tipo_reclamo (D)--------------------
---------------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_tipo_reclamo
AS
INSERT INTO INTER_DATA.BI_tipo_reclamo(tipo_reclamo_id, tipo_reclamo)
	SELECT DISTINCT
		id_Tipo_Reclamo, Tipo
	FROM INTER_DATA.Tipo_Reclamo
GO

------------------------------------------------------
----------------- rango_etario(D) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_rango_etario
AS
INSERT INTO INTER_DATA.BI_rango_etario(rango_etario)
	SELECT DISTINCT INTER_DATA.BI_get_rango_etario(Fecha_Nac) FROM INTER_DATA.Operador
GO

------------------------------------------------------
----------------- tipo_envio(D) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_tipo_envio
AS
INSERT INTO INTER_DATA.BI_tipo_envio(tipo_envio) VALUES('Pedido')
INSERT INTO INTER_DATA.BI_tipo_envio(tipo_envio) VALUES('Mensajeria')
GO

------------------------------------------------------
----------------- estado_envio(D)----------------------
-------------------------------------------------------

CREATE PROCEDURE INTER_DATA.migrar_BI_estado_envio
AS
INSERT INTO INTER_DATA.BI_estado_envio(estado_envio_id, estado_envio)
	SELECT DISTINCT
		id_Tipo_Estado,
		Nombre
	FROM INTER_DATA.Tipo_Estado
	WHERE Nombre IS NOT NULL 
GO

------------------------------------------------------
----------------- -- tipo_paquete(D) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_tipo_paquete
AS
INSERT INTO INTER_DATA.BI_tipo_paquete(tipo_paquete_id, tipo_paquete)
	SELECT DISTINCT
		id_tipo_paquete, Nombre
	FROM INTER_DATA.tipo_paquete
	WHERE Nombre IS NOT NULL
INSERT INTO INTER_DATA.BI_tipo_paquete(tipo_paquete_id, tipo_paquete)
	VALUES(4, 'Pedido') 
GO

------------------------------------------------------
----------------- -- local(D) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_local
AS
INSERT INTO INTER_DATA.BI_local(local_id,
								local_provincia,
								local_localidad,
								local_tipo,
								locaL_categoria)
	SELECT
		L.id_local,
		P.Nombre,
		Loc.Localidad,
		T.Tipo,
		C.Categoria

	FROM INTER_DATA.Local L JOIN INTER_DATA.Direccion D 
	ON L.Direccion_id_direccion = D.id_direccion
	JOIN INTER_DATA.Localidad Loc
	ON D.Localidad_id_Localidad = Loc.id_Localidad
	JOIN INTER_DATA.Provincia P
	ON P.id_Provincia = Loc.Provincia_id_Provincia
	JOIN INTER_DATA.Local_Tipo T 
	ON L.Local_Tipo_id_Local_Tipo = T.id_Local_Tipo
	JOIN INTER_DATA.Local_Categoria C
	ON L.Local_Categoria_id_Local_Categoria = C.id_Local_Categoria 

	WHERE L.id_local	IS NOT NULL 
GO

------------------------------------------------------
----------------- -- tiempo(D)----------------------
-------------------------------------------------------

CREATE PROCEDURE INTER_DATA.migrar_BI_tiempo
AS
INSERT INTO INTER_DATA.BI_tiempo(tiempo_mes,
								 tiempo_anio)
	SELECT MONTH(Fecha), YEAR(Fecha)
	FROM INTER_DATA.Pedido 
	UNION
	SELECT MONTH(Fecha_Entrega), YEAR(Fecha_Entrega)
	FROM INTER_DATA.Envio 
	UNION
	SELECT MONTH(Fecha), YEAR(Fecha)
	FROM INTER_DATA.Reclamo
GO

------------------------------------------------------
----------------- dia_de_la_semana(D) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_dia_de_la_semana
AS
INSERT INTO INTER_DATA.BI_dia_de_la_semana(dia_de_la_semana)
	SELECT DISTINCT DATENAME(WEEKDAY, Fecha) 
	FROM INTER_DATA.Pedido
GO

------------------------------------------------------
----------------- -- rango_horario(D) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_rango_horario
AS
INSERT INTO INTER_DATA.BI_rango_horario(rango_horario)
	SELECT
	INTER_DATA.BI_get_rango_horario(DATEPART(HOUR, Fecha)) 
	FROM INTER_DATA.Pedido
	UNION
	SELECT
	INTER_DATA.BI_get_rango_horario(DATEPART(HOUR, Fecha_Entrega)) 
	FROM INTER_DATA.Envio 
	UNION
	SELECT
	INTER_DATA.BI_get_rango_horario(DATEPART(HOUR, Fecha)) 
	FROM INTER_DATA.Reclamo
GO

------------------------------------------------------
----------------- -- -- ubicacion(D)----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_ubicacion
AS
INSERT INTO INTER_DATA.BI_ubicacion(provincia,
									localidad )
	SELECT DISTINCT
		P.Nombre,
		L.Localidad
		
	FROM INTER_DATA.Envio E
	JOIN INTER_DATA.Direccion D
	ON E.Direccion_id_direccion_destino = D.id_direccion
	JOIN INTER_DATA.Localidad L
	ON D.Localidad_id_Localidad = L.id_Localidad
	JOIN INTER_DATA.Provincia P
	ON P.id_Provincia = L.Provincia_id_Provincia
GO

------------------------------------------------------
----------------- -- -- estado_pedido(D)----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_estado_pedido
AS
INSERT INTO INTER_DATA.BI_estado_pedido(estado_pedido_id,
										estado_pedido)
	SELECT
		id_Tipo_Estado,
		Nombre
	FROM INTER_DATA.Tipo_Estado
GO

------------------------------------------------------
----------------- ---- tipo_medio_de_pago(D)----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_tipo_medio_de_pago
AS
INSERT INTO INTER_DATA.BI_tipo_medio_de_pago(tipo_medio_de_pago)
	SELECT DISTINCT
		Nombre
	FROM INTER_DATA.Tipo_Medio_Pago 
GO


------------------------------------------------------
----------------- -- pedidos(H)----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_pedidos
AS
INSERT INTO INTER_DATA.BI_pedidos(local_id,
								  dia_de_la_semana_id,
								  tiempo_id,
								  rango_horario_id,
								  rango_etario_id,
								  tipo_medio_de_pago_id,
								  estado_pedido_id,
								  pedidos_cantidad,
								  pedidos_total,
								  pedidos_total_cupones,
								  pedidos_promedio_calificacion)
	SELECT 
		P.Local_id_local,
		D.dia_de_la_semana_id,
		T.tiempo_id,
		R.rango_horario_id,
		RE.rango_etario_id,
		BITMP.tipo_medio_de_pago_id,
		P.Tipo_Estado_id_Tipo_Estado,
		COUNT(P.Local_id_local) AS Cantidad,
		SUM(P.Total_Productos) AS Total,
		SUM(P.Total_Cupones) AS TotalCupones,
		AVG(P.Calificacion) AS PromedioCalificacion
	FROM INTER_DATA.Pedido P
	JOIN INTER_DATA.BI_dia_de_la_semana D
	ON D.dia_de_la_semana = DATENAME(WEEKDAY, P.Fecha)
	JOIN INTER_DATA.BI_tiempo T
	ON T.tiempo_mes = MONTH(P.Fecha) AND T.tiempo_anio = YEAR(P.Fecha)
	JOIN INTER_DATA.BI_rango_horario R
	ON R.rango_horario = INTER_DATA.BI_get_rango_horario(DATEPART(HOUR, P.Fecha))
	JOIN INTER_DATA.Usuario US
	ON US.id_usuario = P.Usuario_id_usuario
	JOIN INTER_DATA.BI_rango_etario RE
	ON RE.rango_etario = INTER_DATA.BI_get_rango_etario(US.Fecha_Nac)
	JOIN INTER_DATA.Tipo_Medio_Pago TMP
	ON TMP.id_tipo_medio_pago = P.Tipo_Medio_Pago_id_tipo_medio_pago
	JOIN INTER_DATA.BI_tipo_medio_de_pago BITMP
	ON BITMP.tipo_medio_de_pago = TMP.Nombre
	GROUP BY 
		P.Local_id_local,
		D.dia_de_la_semana_id,
		T.tiempo_id,
		R.rango_horario_id,
		RE.rango_etario_id,
		BITMP.tipo_medio_de_pago_id,
		P.Tipo_Estado_id_Tipo_Estado
GO

------------------------------------------------------
----------------- -- envios(H) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_envios
AS
INSERT INTO INTER_DATA.BI_envios(ubicacion_id,    --PRIMERO INSERTO LOS ENVIOS DE MENSAJERIA
								 tiempo_id,
								 dia_de_la_semana_id,
								 rango_horario_id,
								 estado_envio_id,
								 rango_etario_id,
								 tipo_movilidad_id,
								 tipo_paquete_id,
								 tipo_envio_id,
								 envios_cantidad,
								 envios_total,
								 envios_total_asegurado,
								 envios_desvio_promedio)
	SELECT
		U.ubicacion_id,
		T.tiempo_id,
		DS.dia_de_la_semana_id,
		R.rango_horario_id,
		M.Tipo_Estado_id_Tipo_Estado,
		RE.rango_etario_id,
		RP.Tipo_Movilidad_id_Tipo_Movilidad,
		PQ.Tipo_Paquete_id_tipo_paquete,
		2,
		COUNT(ubicacion_id),
		SUM(E.Precio_Envio),
		SUM(PQ.Valor_Asegurado),
		AVG((DATEPART(MINUTE, E.Fecha_Entrega)) - (DATEPART(MINUTE, M.Fecha))/E.Tiempo_Estimado)
		
	FROM INTER_DATA.Envio E
	JOIN INTER_DATA.Direccion D
	ON E.Direccion_id_direccion_destino = D.id_direccion
	JOIN INTER_DATA.Localidad L
	ON D.Localidad_id_Localidad = L.id_Localidad
	JOIN INTER_DATA.Provincia P
	ON P.id_Provincia = L.Provincia_id_Provincia
	JOIN INTER_DATA.BI_ubicacion U
	ON U.localidad = L.Localidad AND U.provincia = P.Nombre
	JOIN INTER_DATA.BI_tiempo T
	ON T.tiempo_mes = MONTH(E.Fecha_Entrega) AND T.tiempo_anio = YEAR(E.Fecha_Entrega)
	JOIN INTER_DATA.BI_dia_de_la_semana DS
	ON DS.dia_de_la_semana = DATENAME(WEEKDAY, E.Fecha_Entrega)
	JOIN INTER_DATA.BI_rango_horario R
	ON R.rango_horario = INTER_DATA.BI_get_rango_horario(E.Fecha_Entrega)
	JOIN INTER_DATA.Mensajeria M
	ON M.Envio_id_envio = E.id_envio
	JOIN INTER_DATA.Repartidor RP
	ON E.Repartidor_id_repartidor = RP.id_repartidor
	JOIN INTER_DATA.BI_rango_etario RE
	ON RE.rango_etario = INTER_DATA.BI_get_rango_etario(RP.Fecha_Nac)
	JOIN INTER_DATA.Paquete PQ
	ON PQ.id_Paquete = M.Paquete_id_Paquete
	GROUP BY
		U.ubicacion_id,
		T.tiempo_id,
		DS.dia_de_la_semana_id,
		R.rango_horario_id,
		M.Tipo_Estado_id_Tipo_Estado,
		RE.rango_etario_id,
		RP.Tipo_Movilidad_id_Tipo_Movilidad,
		PQ.Tipo_Paquete_id_tipo_paquete

	INSERT INTO INTER_DATA.BI_envios(ubicacion_id,   --INSERTO LOS ENVIOS DE PEDIDOS
								 tiempo_id,
								 dia_de_la_semana_id,
								 rango_horario_id,
								 estado_envio_id,
								 rango_etario_id,
								 tipo_movilidad_id,
								 tipo_paquete_id,
								 tipo_envio_id,
								 envios_cantidad,
								 envios_total,
								 envios_total_asegurado,
								 envios_desvio_promedio)
	SELECT
		U.ubicacion_id,
		T.tiempo_id,
		DS.dia_de_la_semana_id,
		R.rango_horario_id,
		PD.Tipo_Estado_id_Tipo_Estado,
		RE.rango_etario_id,
		RP.Tipo_Movilidad_id_Tipo_Movilidad,
		4,
		1,
		COUNT(ubicacion_id),
		SUM(E.Precio_Envio),
		NULL,
		AVG((DATEPART(MINUTE, E.Fecha_Entrega)) - (DATEPART(MINUTE, PD.Fecha))/E.Tiempo_Estimado)
		
	FROM INTER_DATA.Envio E
	JOIN INTER_DATA.Direccion D
	ON E.Direccion_id_direccion_destino = D.id_direccion
	JOIN INTER_DATA.Localidad L
	ON D.Localidad_id_Localidad = L.id_Localidad
	JOIN INTER_DATA.Provincia P
	ON P.id_Provincia = L.Provincia_id_Provincia
	JOIN INTER_DATA.BI_ubicacion U
	ON U.localidad = L.Localidad AND U.provincia = P.Nombre
	JOIN INTER_DATA.BI_tiempo T
	ON T.tiempo_mes = MONTH(E.Fecha_Entrega) AND T.tiempo_anio = YEAR(E.Fecha_Entrega)
	JOIN INTER_DATA.BI_dia_de_la_semana DS
	ON DS.dia_de_la_semana = DATENAME(WEEKDAY, E.Fecha_Entrega)
	JOIN INTER_DATA.BI_rango_horario R
	ON R.rango_horario = INTER_DATA.BI_get_rango_horario(E.Fecha_Entrega)
	JOIN INTER_DATA.Pedido PD
	ON PD.Envio_id_envio = E.id_envio
	JOIN INTER_DATA.Repartidor RP
	ON E.Repartidor_id_repartidor = RP.id_repartidor
	JOIN INTER_DATA.BI_rango_etario RE
	ON RE.rango_etario = INTER_DATA.BI_get_rango_etario(RP.Fecha_Nac)
	GROUP BY
		U.ubicacion_id,
		T.tiempo_id,
		DS.dia_de_la_semana_id,
		R.rango_horario_id,
		PD.Tipo_Estado_id_Tipo_Estado,
		RE.rango_etario_id,
		RP.Tipo_Movilidad_id_Tipo_Movilidad
GO

------------------------------------------------------
----------------- -- reclamos(H) ----------------------
-------------------------------------------------------
CREATE PROCEDURE INTER_DATA.migrar_BI_reclamos
AS
INSERT INTO INTER_DATA.BI_reclamos( local_id,
									tiempo_id,
									dia_de_la_semana_id,
									rango_horario_id,
									estado_reclamo_id,
									tipo_reclamo_id,
									rango_etario_id,
									reclamos_cantidad,
									reclamos_tiempo_promedio_resolucion,
									reclamo_total_cupones)
	SELECT
		P.Local_id_local,
		T.tiempo_id,
		D.dia_de_la_semana_id,
		RH.rango_horario_id,
		R.Estado_Reclamo_id_Estado_Reclamo,
		R.Tipo_Reclamo_id_Tipo_Reclamo,
		RE.rango_etario_id,
		COUNT(P.Local_id_local),
		AVG(DATEPART(MINUTE, R.Fecha_Solucion - R.Fecha)),
		SUM(CP.Monto)
	FROM INTER_DATA.Reclamo R
	JOIN INTER_DATA.Pedido P
	ON P.Numero = R.Pedido_Numero
	JOIN INTER_DATA.BI_tiempo T
	ON T.tiempo_mes = MONTH(R.Fecha) AND T.tiempo_anio = YEAR(R.Fecha)
	JOIN INTER_DATA.BI_dia_de_la_semana D
	ON D.dia_de_la_semana = DATENAME(WEEKDAY, R.Fecha)
	JOIN INTER_DATA.BI_rango_horario RH
	ON RH.rango_horario = INTER_DATA.BI_get_rango_horario(DATEPART(HOUR, R.Fecha))
	JOIN INTER_DATA.Operador OP
	ON OP.id_operador = R.Operador_id_operador
	JOIN INTER_DATA.BI_rango_etario RE
	ON RE.rango_etario = INTER_DATA.BI_get_rango_etario(OP.Fecha_Nac)
	JOIN INTER_DATA.Cupon_X_Reclamo CR
	ON CR.Reclamo_Numero_Reclamo = R.Numero
	JOIN INTER_DATA.Cupon CP
	ON CP.Numero = CR.Cupon_Numero_Cupon
	GROUP BY 
		P.Local_id_local,
		T.tiempo_id,
		D.dia_de_la_semana_id,
		RH.rango_horario_id,
		R.Estado_Reclamo_id_Estado_Reclamo,
		R.Tipo_Reclamo_id_Tipo_Reclamo,
		RE.rango_etario_id

GO

/*
SELECT CR.Reclamo_Numero_Reclamo, C.Monto
FROM INTER_DATA.Cupon_X_Reclamo CR
JOIN INTER_DATA.Cupon C
ON CR.Cupon_Numero_Cupon = C.Numero


SELECT
	T.tiempo_anio,
	T.tiempo_mes,
	SUM(R.reclamo_total_cupones) AS 'Total Mensual de Cupones'
FROM INTER_DATA.BI_reclamos R
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = R.tiempo_id
GROUP BY
	T.tiempo_anio,
	T.tiempo_mes

SELECT
	PEDIDO_NRO,
	PAQUETE_TIPO
FROM gd_esquema.Maestra
WHERE PEDIDO_NRO IS NOT NULL
*/


EXEC INTER_DATA.migrar_BI_tipo_movilidad
EXEC INTER_DATA.migrar_BI_estado_reclamo
EXEC INTER_DATA.migrar_BI_tipo_reclamo
EXEC INTER_DATA.migrar_BI_rango_etario
EXEC INTER_DATA.migrar_BI_estado_envio
EXEC INTER_DATA.migrar_BI_tipo_paquete
EXEC INTER_DATA.migrar_BI_local
EXEC INTER_DATA.migrar_BI_tiempo
EXEC INTER_DATA.migrar_BI_dia_de_la_semana
EXEC INTER_DATA.migrar_BI_rango_horario
EXEC INTER_DATA.migrar_BI_ubicacion
EXEC INTER_DATA.migrar_BI_estado_pedido
EXEC INTER_DATA.migrar_BI_tipo_medio_de_pago
EXEC INTER_DATA.migrar_BI_tipo_envio
EXEC INTER_DATA.migrar_BI_pedidos
EXEC INTER_DATA.migrar_BI_reclamos
EXEC INTER_DATA.migrar_BI_envios

--------------- VISTAS -------------------------------
--D�a de la semana y franja horaria con mayor cantidad de pedidos seg�n la
--localidad y categor�a del local, para cada mes de cada a�o.
GO
CREATE VIEW INTER_DATA.V_MayorCantidadPedidos AS
	SELECT
		T.tiempo_anio,
		T.tiempo_mes,
		DS.dia_de_la_semana,
		RH.rango_horario,
		L.local_localidad,
		L.local_categoria,
		SUM(P.pedidos_cantidad) AS 'Cantidad de Pedidos'
	FROM INTER_DATA.BI_pedidos P
	JOIN INTER_DATA.BI_local L
	ON L.local_id = P.local_id
	JOIN INTER_DATA.BI_dia_de_la_semana DS
	ON DS.dia_de_la_semana_id = P.dia_de_la_semana_id
	JOIN INTER_DATA.BI_rango_horario RH
	ON RH.rango_horario_id = P.rango_horario_id
	JOIN INTER_DATA.BI_tiempo T
	ON T.tiempo_id = P.tiempo_id
	GROUP BY
		T.tiempo_anio,
		T.tiempo_mes,
		DS.dia_de_la_semana,
		RH.rango_horario,
		L.local_localidad,
		L.local_categoria
GO

/* Monto total no cobrado por cada local en funci�n de los pedidos cancelados seg�n el d�a de la semana y la franja horaria 
                (cuentan como pedidos cancelados tanto los que cancela el usuario como el local). */
GO
CREATE VIEW INTER_DATA.V_MontoTotalNoCobrado AS
SELECT
	L.local_id,
    D.dia_de_la_semana,
    RH.rango_horario,
    SUM(P.pedidos_total) AS MontoNoCobrado      --asumimos que el envio esta a cargo del local
FROM INTER_DATA.BI_pedidos P
		JOIN INTER_DATA.BI_local L
		ON L.local_id = P.local_id
		JOIN INTER_DATA.BI_rango_horario RH 
		ON RH.rango_horario_id = P.rango_horario_id
		JOIN INTER_DATA.BI_dia_de_la_semana D 
		ON D.dia_de_la_semana_id = P.dia_de_la_semana_id
		JOIN INTER_DATA.BI_estado_pedido EP
		ON EP.estado_pedido_id = P.estado_pedido_id
WHERE EP.estado_pedido = 'Estado Mensajeria Cancelado'
GROUP BY L.local_id, D.dia_de_la_semana, RH.rango_horario
GO

/* Valor promedio mensual que tienen los env�os de pedidos en cada localidad. */

CREATE VIEW INTER_DATA.V_PromedioEnviosPorLocalidad AS
SELECT
    U.localidad,
	T.tiempo_mes,
	T.tiempo_anio,
    AVG(EP.envios_total) AS PromedioEnvios      
FROM INTER_DATA.BI_envios EP
		JOIN INTER_DATA.BI_ubicacion U
		ON U.ubicacion_id = EP.ubicacion_id
		JOIN INTER_DATA.BI_tiempo T
		ON T.tiempo_id = EP.tiempo_id
		JOIN INTER_DATA.BI_tipo_envio TE
		ON TE.tipo_envio_id = EP.tipo_envio_id
WHERE TE.tipo_envio = 'Pedido'
GROUP BY U.localidad, T.tiempo_mes, T.tiempo_anio
GO
/* Desv�o promedio en tiempo de entrega seg�n el tipo de movilidad, el d�a de la semana y la franja horaria. El desv�o debe calcularse en minutos 
 y representa la diferencia entre la fecha/hora en que se realiz� el pedido y la fecha/hora que se entreg� en comparaci�n con los minutos de 
   tiempo estimados. Este indicador debe tener en cuenta todos los env�os, es decir, sumar tanto los env�os de pedidos como los de mensajer�a. */

CREATE VIEW INTER_DATA.V_DesvioPromedioTiempoEntrega AS
	
	SELECT 
		TM.tipo_movilidad AS 'Tipo Movilidad',
		DS.dia_de_la_semana AS 'Dia de la semana',
		RH.rango_horario AS 'Rango horario',
		AVG(E.envios_desvio_promedio) AS 'Desvio Promedio Tiempo de entrega'
	FROM INTER_DATA.BI_envios E
	JOIN INTER_DATA.BI_tipo_movilidad TM
	ON TM.tipo_movilidad_id = E.tipo_movilidad_id
	JOIN INTER_DATA.BI_dia_de_la_semana DS
	ON DS.dia_de_la_semana_id = E.dia_de_la_semana_id
	JOIN INTER_DATA.BI_rango_horario RH
	ON RH.rango_horario_id = E.rango_horario_id
	GROUP BY
		TM.tipo_movilidad,
		DS.dia_de_la_semana,
		RH.rango_horario
GO

/* Monto total de los cupones utilizados por mes en funci�n del rango etario de los usuarios. */
CREATE VIEW INTER_DATA.V_MontoTotalCuponesPorRangoEtario 
AS
SELECT
	T.tiempo_anio,
	T.tiempo_mes,
	RE.rango_etario,
	SUM(P.pedidos_total_cupones) AS 'Monto Total Cupones'
    
FROM INTER_DATA.BI_pedidos P
JOIN INTER_DATA.BI_rango_etario RE
ON RE.rango_etario_id = P.rango_etario_id
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = P.tiempo_id
GROUP BY T.tiempo_anio,
	T.tiempo_mes,
	RE.rango_etario
GO

/* Promedio de calificaci�n mensual por local. */
CREATE VIEW INTER_DATA.V_PromedioCalificacionMensualPorLocal
AS
SELECT
    T.tiempo_anio,
	T.tiempo_mes,
	L.local_id,
	AVG(P.pedidos_promedio_calificacion) AS 'Promedio Calificacion'
FROM INTER_DATA.BI_pedidos P
JOIN INTER_DATA.BI_local L
ON L.local_id = P.local_id
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = P.tiempo_id
GROUP BY T.tiempo_anio, T.tiempo_mes, L.local_id


/* Porcentaje de pedidos y mensajer�a entregados mensualmente seg�n el rango etario de los repartidores y la localidad. Este indicador se debe tener 
 en cuenta y sumar tanto los env�os de pedidos como los de mensajer�a. El porcentaje se calcula en funci�n del total general de pedidos y env�os 
														mensuales entregados. */
GO
CREATE VIEW INTER_DATA.V_PorcentajeEntregasMensuales 
AS
SELECT
    U.localidad,
    RE.rango_etario,
    T.tiempo_anio,
    T.tiempo_mes,
    COUNT(CASE WHEN EE.estado_envio = 'Estado Mensajeria Entregado' THEN E.envios_cantidad END) AS 'Envios Entregados', -- para contar solo los entregados y no los cancelados
	SUM(E.envios_cantidad) AS 'Total Envios',     
    COUNT(CASE WHEN EE.estado_envio = 'Estado Mensajeria Entregado' THEN E.envios_cantidad END) * 100.0 / SUM(E.envios_cantidad) AS '% Entregado'  -- para sacar el % de envios entregados por rango etario
	
FROM INTER_DATA.BI_envios E     -- tanto pedido como mensajeria
		JOIN INTER_DATA.BI_Tiempo T ON T.tiempo_id = E.tiempo_id
		JOIN INTER_DATA.BI_rango_etario RE ON RE.rango_etario_id = E.rango_etario_id
		JOIN INTER_DATA.BI_ubicacion U ON U.ubicacion_id = E.ubicacion_id
		JOIN INTER_DATA.BI_estado_envio EE ON EE.estado_envio_id = E.estado_envio_id
GROUP BY 
U.localidad,
    RE.rango_etario,
    T.tiempo_anio,
    T.tiempo_mes
GO


/* Promedio mensual del valor asegurado (valor declarado por el usuario) de los paquetes enviados a trav�s del servicio de mensajer�a 
                                                     en funci�n del tipo de paquete. */

CREATE VIEW INTER_DATA.V_PromedioValorAsegurado
AS
SELECT
	T.tiempo_anio,
	T.tiempo_mes,
	TP.tipo_paquete,
	AVG(E.envios_total_asegurado) AS 'Promedio Mensual'
FROM INTER_DATA.BI_envios E
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = E.tiempo_id
JOIN INTER_DATA.BI_tipo_paquete TP
ON TP.tipo_paquete_id = E.tipo_paquete_id
JOIN INTER_DATA.BI_tipo_envio TE
ON TE.tipo_envio_id = E.tipo_envio_id
WHERE TE.tipo_envio = 'Mensajeria'
GROUP BY 
	T.tiempo_anio,
	T.tiempo_mes,
	TP.tipo_paquete
GO

/* Cantidad de reclamos mensuales recibidos por cada local en funci�n del d�a de la semana y rango horario. */

CREATE VIEW INTER_DATA.V_CantidadReclamosMensualesPorLocal
AS
SELECT
	T.tiempo_anio,
    T.tiempo_mes,
	L.local_id,
    DS.dia_de_la_semana,
    RH.rango_horario,
    COUNT(*) AS 'Cantidad de reclamos'
FROM INTER_DATA.BI_reclamos R
JOIN INTER_DATA.BI_local L
ON L.local_id = R.local_id
JOIN INTER_DATA.BI_dia_de_la_semana DS
ON DS.dia_de_la_semana_id = R.dia_de_la_semana_id
JOIN INTER_DATA.BI_rango_horario RH
ON RH.rango_horario_id = R.rango_horario_id
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = R.tiempo_id
GROUP BY 
	T.tiempo_anio,
    T.tiempo_mes,
	L.local_id,
    DS.dia_de_la_semana,
    RH.rango_horario


/* Tiempo promedio de resoluci�n de reclamos mensual seg�n cada tipo de reclamo y rango etario de los operadores. El tiempo de resoluci�n debe 
	calcularse en minutos y representa la diferencia entre la fecha/hora en que se realiz� el reclamo y la fecha/hora que se resolvi�. */

GO
CREATE VIEW INTER_DATA.V_TiempoPromedioResolucionReclamos
AS
SELECT
    T.tiempo_anio,
	T.tiempo_mes,
	TR.tipo_reclamo,
	RE.rango_etario AS 'Rango etario operador',
    AVG(R.reclamos_tiempo_promedio_resolucion) AS 'Tiempo promedio de resolucion'
FROM INTER_DATA.BI_reclamos R
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = R.tiempo_id
JOIN INTER_DATA.BI_rango_etario RE
ON RE.rango_etario_id = R.rango_etario_id
JOIN INTER_DATA.BI_tipo_reclamo TR
ON TR.tipo_reclamo_id = R.tipo_reclamo_id
GROUP BY 
	T.tiempo_anio,
	T.tiempo_mes,
	TR.tipo_reclamo,
	RE.rango_etario
GO


/* Monto mensual generado en cupones a partir de reclamos. */

CREATE VIEW INTER_DATA.V_MontoMensualCuponesReclamos
AS
SELECT
    T.tiempo_anio,
	T.tiempo_mes,
    SUM(R.reclamo_total_cupones) AS 'Total Cupones Mensual Reclamos'
FROM INTER_DATA.BI_reclamos R
JOIN INTER_DATA.BI_tiempo T
ON T.tiempo_id = R.tiempo_id
GROUP BY
	T.tiempo_anio,
	T.tiempo_mes
GO