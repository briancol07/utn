# GDD

## Indice 

* [Resumenes](./00-Resumenes/README.md)
* [O1-Estructura-de-datos](./01-Estructuras-de-Datos/README.md)
* [02-Manipulacion-de-datos](./02-Manipulacion-De-Datos/README.md)
* [03-Disenio-de-Datos](./03-Disenio-De-Datos/README.md)
* [04-Bases-de-datos](./04-Bases-de-Datos/README.md)

## Links 

* [Pagina-Catedra](https://sites.google.com/site/gestiondedatosutn/p%C3%A1gina-principal)
* [Extra-SQLBOLT](https://sqlbolt.com/lesson/introduction)
* [My-SQL](https://www.mysqltutorial.org/mysql-basics/)
* [Practice](https://learnsql.com/blog/mysql-practice/)
* [Dev-MySQL](https://dev.mysql.com/doc/refman/8.0/en/create-table.html)


## Otras cursadas

* [2022](https://github.com/MarcoPiatti/GDD-1C2022)
* [Las-4-Cifras](https://github.com/biandopa/LAS_CUATRO_CIFRAS/tree/develop/data)
* [Inmoviliarias](https://github.com/ailindenoya/tp-FRBA-inmobiliaria-gestion-de-datos?tab=readme-ov-file)


## Programa de la materia ( Sintetico ) 

* UNIDAD TEMATICA 1: ESTRUCTURAS DE DATOS
  * Estructuras de Datos.
  * Representación Computacional.
  * Aplicaciones con Estructuras de Datos
* UNIDAD TEMATICA 2: MANIPULACION DE DATOS
  * Clasificación.
  * Ordenamiento.
  * Compactación. 
* UNIDAD TEMATICA 3: DISEÑO DE DATOS
  * Modelo de Datos.
  * Redundancia y consistencia.
  * Normalización.
  * Modelo de Objetos.
* UNIDAD TEMATICA 4: BASES DE DATOS
  * Base de Datos Concepto y Tipos.
  * Modelo Relacional y Modelo orientado a objetos.
  * Concepto de SQL.
  * Recuperación y Concurrencia,
  * Aplicaciones con SQL
