# Modelo Relacional (Persistencia)

## Indice 

* [## Relacion ](#relacion)
* [## Tupla ](#tupla)
* [## Integridad Referencia ](#integridad)
* [## Normalizacion ](#normalizacion)
* [## Lenguaje SQL ](#lenguaje)
  * [### DDL ](#ddl)
* [## PK ](#pk)
* [## CHECK ](#check)

> Casos de uso es una herramienta 

Modelo Jerarquico 

```
Padres --> Hijos 
Red --> Con padres 
```

## Relacion <a name="relacion"></a>

Entre entidaddes ( OBJ -> Fisico ) : Agrupamiento de elementos (Del mismo tipo), unificacion y pertenencia

## Tupla <a name="tupla"></a>

Es generico, un conjunto de atributos no vacios con caracteristicas de elementos del dominio 

----

Reducir al minimo ocurrencias de cosas, mas chico el dominio mas facil manejarlo

Valor NULL es un valor desconocido cuando se asigna un valor por defecto 

* PK (Primary key) identifica univocamente a esa tupla , atributos o conjunto de atributos 
* FK (Foreign key) Atributo dentro de tupla que otro tiene como pk 

## Integridad Referencia <a name="integridad"></a>

No puedo borrar tupla a la que hace referencia. Para que no haya referencias cruzadas o rotas 

* Independiente de la implementacion -> Puesta en practica 
* No Hay orden de prioridad 

## Normalizacion <a name="normalizacion"></a>

> concepto , lo comun

Estandarizar : esquema standard . Poner la tabla de verdad con el null

* Iterativo 
  * 1FN :
    * PK 
    * No tiene atributos calculables
    * Atributos repetitivos ni compuestos
  * 2FN : 
    * Atributos no clave depende de la PK
  * 3FN : 
    * Todos los atributos no clave no dependen de algun atributo no clave 
* Ventaja 
  * Quitar redundancias 
  * Estandarizado
  * Estable 
* Deventaja 
  * Velocidad 
  * Performance 

## Lenguaje SQL <a name="lenguaje"></a>

* DDL 
  * Definir BBDD , CRUD
  * Create 
  * Alter
  * Drop
* DML 
  * Manipular informacion
* DCL 
  * Asignar Permisos
* Tipo datos 
  * Tipados 

### DDL <a name="ddl"></a>

```
Create Table nombre (
);
```
* Encapsulamiento 
  * Tablas = entidades
  * Atributos = Columnas

```
nombre tipo parametrizadores 
              |--> NULL
              \--> IS NOT NULL
```

Se pueden crear tablas sin PK pero para que sea mas performante se hace 

Alter modifica columna o dato 

## PK <a name="pk"></a>

Cuando se crea la tabla , se usa constrain para restringir el dominio y es recomendable ponerlo alterando la tabla 

FK es la referencia a otra tabla 

## CHECK <a name="check"></a>

Funcio que permite el ingreso  o no ( true or false ) sino entra una violacion ( constrain )

* `unique` para que sea un campo unico 
* `default` ante un null poner un valor 
* Autoincrement 
  * Serial 
  * Camina solo -> identity (inicio,incremento)
  * Nunca vuelve para atras 

Tablas maestras necesitan cargarse 
transaccion por ejecucion del sistema 
