# Clase 2 - Compiladores y Lenguaje C


## Interpretes y Compiladores 


 Interpretes   | Compiladores     
:-------------:|:--------------:
Linea a linea  | Analisis -> CodigoFuente -> Exe
No definir variables | Definir Variables 


Tambien hay lenguajes hibridos que analisan y lo llevan a un lenguaje intermedio.


## Proceso de compilacion 

```
Fuente --> Analisis  ---> Lexico                           -| 
                     |--> Sintactico                       -|-> Tabla de  
                     |--> Semantinco --> Codigo Intermedio -|   Simbolos 
```

## Compiladores 

* Dev c++
* Xcode
* Eclipse 
* Code Blocks 
* MinGw

## Variables de Entorno 

Lo que el SO va a ir a buscar cuando ejecuta algo para hacer uso de esa herramienta. 

## Ejecutar en linea de comando 

gcc puede ser utilizado con diversos flags (parametros) que nos permitiran hacer ciertas cosas 

``` bash 
# Cambia el nombre al archivo excutable tras compilarlo 
gcc -0 miejecutable hola.c
# Precompila
gcc -E hola.c -o hola.i
# genera el ensamblador (.s)
gcc -S hola.c
# Genera el objeto 
gcc -o hola.o hola.s
gcc -c -o hola.o hola.c 
```

## Uso de la memoria 

Memoria global : Todo lo declarado globalmente 
Pila : Esta area se forma cuando va hacia otra funcion se guarda en la pila
Memoria libre : se regula entre el heap y la pila 
Heap : 





