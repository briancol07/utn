#include<stdio.h>
#include<stdlib.h>

typedef struct nodo{
  int info;
  struct nodo *sgte;
}nodo;

typedef struct nodo* ptrNodo;  // Esto lo hacemos para no hacer un doble puntero en la invocacion

int pop(ptrNodo* );
void push(ptrNodo*,int);
void mostrarPila(ptrNodo*);

// 3 + 4 * 4 - 3 
// If there is a * we do a pop with the next item to * 
//
// 3 + 4 --> found * --> continue --> flag to 1 and do pop and push the pop item * new item 
//
// Una vez que tengamos la ecuacion guardada con sumas/restas  hacemos un pop sucesivo sumando o restando 
//

int main(){
  ptrNodo *pila =  malloc(sizeof(nodo));
  
  int val,i=0;

  while(i < 10){
    push(pila,i);
    i++;
  }
  while( val != 0){
    val = pop(pila);
    printf("%d\n",val);
  }


  return 0;
}


int pop(ptrNodo *pila){
  int x;
  ptrNodo *p = pila;
  x = (*pila)->info;
  *pila = (*pila)->sgte;
  free(p);
  return x;
}

void push(ptrNodo *pila, int x){
  ptrNodo p =  malloc(sizeof(nodo));
  p->info = x;
  p->sgte = *pila;
  *pila = p;

}
