#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX 255

typedef struct nodo{
  char dato;
  struct nodo* sgte;
}Nodo;

typedef struct pila{
  Nodo* top;
}Pila;

Nodo* crearNodo(char);
void destruir(Nodo* nodo);
void push(Pila *pila, char);
char pop(Pila *pila);
void desapilar(Pila *pila);
Pila *CrearPila();
void destruirPila(Pila *pila);

// Funciones listas 
int charInt(char);
char *remove_white_spaces(char *str);

// ecuacion 4 + 4 * 4 + 5 

int main(){

  char ecuation[MAX];
  
  int resultado = 0,flag = 0;
  scanf("%s",&ecuation);
  char aux[sizeof(ecuation)] ;
  strcpy(aux,remove_white_spaces(ecuation));   
  remove_white_spaces(ecuation);
  resultado = ecuation[0];

  for(int i =0; ecuation[i] != '\0';i++){
    switch (ecuation[i]){
      case ' ':
        continue;
        break;
      case '*':
        // falta pushear a la lista 
        resultado += charInt(ecuation[i-1]) * charInt(ecuation[i+1]);
        flag = 1; 
        break;
      case '+': 
        flag = 1; 
        resultado += charInt(ecuation[i+1]);
        break;
      case '-':
        flag = 1; 
        resultado -= charInt(ecuation[i+1]);
        break;
      default:
        flag = 0;
        break;
    }; // swtich ;
  } // for 

  printf("El resultado es %d\n",resultado);

  return 0;
}

int charInt(char c){
      return c - '0';
}
 

Nodo* crearNodo(char a){
  Nodo* nodo = (Nodo*) malloc(sizeof(Nodo));
  nodo->dato;
  nodo->sgte = NULL;
  return nodo;
}

void destruir(Nodo* nodo){
  nodo->sgte=NULL;
  free(nodo);
}

void push(Pila *pila, char dato){
  Nodo *nodo = crearNodo(dato);
  nodo->sgte = pila->top;
  pila->top = nodo;
}

void  desapilar(Pila *pila){
  if(pila->top != NULL){
    Nodo* eliminar = pila->top;
    pila->top = pila->top->sgte;
    destruir(eliminar);
  }
}


Pila * CrearPila(){
  Pila* pila = (Pila *) malloc(sizeof(Pila));
  pila->top = NULL;
  return pila;
}

void destruirPila(Pila *pila){
   while (pila->top != NULL){
     desapilar(pila);
      
   }
   free(pila);
}


char * remove_white_spaces (char *str){
  int i = 0, j = 0;
  while(str[i]){
    if (str[i] != ' '){
      str[j++] = str[i];
      i++;
    }
  str[j] = '\0';
  return str;
  }
}

