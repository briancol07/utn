# Trabajo Practico Automatas 

## Integrantes 

* Brian Colman
* Juan Cruz Rodriguez
* Sofia Sosa
* Roxana Steinman


## Ejercicios 

1. Dado una cadena que contenga varios numeros que pueden ser decimales, octales o hexadecimales, con o sin signo para el caso de los decimales, separados por el caracter '&', reconocer los tres grupos de constantes enteras, indicando si hubo un error lexico, en caso de ser correcto contar la cantidad de cada grupo.
La cadena debe ingresar por linea de comando o por achivo
2. Debe realizar una funcion que reciba un caracter numerico y retorne un numero entero 
3. Ingresar una cadena que represente una operacion simple con enteros decimales y obtener su resultado, se debe operar con \+,\- y \*. ejemplo 3+4\*7+4-5 = 29. Debe de poder operar con cualquier numero de operandos y operadores y operadores respetando la precedencia de los operadores aritmeticos. <br> La cadena ingresada debe ser validada previamente preferentemente reutilizando las funciones del ejercicio1.<br> para poder realizar la operacion de los caracteres deben convertirse a numeros utilizando la funcion 2. <br> la cadena debe ingresar por linea de comando o por archivo.

## Explicaciones 

* [punto1.md](./punto1.md)
* [punto2.md](./punto2.md)
* [punto3.md](./punto3.md)

## Conclusiones / Mejoras 

* Mejoras pendientes 
  * Realizar en forma modular (incluyendo headers y haciendo un MAKEFILE)
  * Mejorar el punto 3 ( con punteros y/o haciendo una calculadora polaca inversa) 
  * Mejorar en la utilizacion de punteros y memoria dinamica 
  * Lograr una mejor distribucion de tiempos y tareas 
  * En el punto 1 lo que se podria mejorar pero no lo tomamos como error es que el '\n' de fin de linea no nos permita contar los caracateres
    * Osea que cada numero este determinado por apertura y cierre de & 
  * En el punto 2 que no imprima el ultimo caso y que no me muestre el valor si es una letra 
    * Poner mas condicionales de como limitar la funcion (el dominio) 



