# Ejercicio 2 

## Codigo Explicacion 

Al principio pensamos en utilizar la funcion atoi o atof que nos convierte una cadena numerica en un entero pero luego pensamos la siguiente version mas simple 

``` 
void ejercicio2(){
  char num = '0';
  // Aca se puede hacer una comprobacion aver si se ingresaron mas de un caracter
  while((num != 'f') && ( num != 'F')){
    printf("Ingrese un numero un unico caracter numerico");
    printf("Si quiere terminar ingrese F o f\n");
    getchar();
    scanf("%c",&num);
    printf("El numero en entero es: %d\n",charInt(num));
  }
}
```

Creamos un ciclo que se corta cuando se ingresa el caracter f o F, el getchar lo que hace es no tomarme el '\n' de la linea anterior (se podria hacer un fflush pero es la version mas simple un getchar. 
Recibimos el caracater numerico y lo pasamos por la funcion principal que nos lo convertiria en entero. 


## Funcion Principal 
```
int charInt(char c){

  return c - '0';

}
```

## Ejemplo de ejecucion

```
Ingrese el numero de ejercicio que quiera ver
 1 ------------> Ejercicio 1
 2 ------------> Ejercicio 2
 3 ------------> Ejercicio 3
 4 o otro numero para terminar
2
Ingrese un numero un unico caracter numericoSi quiere terminar ingrese F o f
3
El numero en entero es: 3
Ingrese un numero un unico caracter numericoSi quiere terminar ingrese F o f
4
El numero en entero es: 4
Ingrese un numero un unico caracter numericoSi quiere terminar ingrese F o f
5
El numero en entero es: 5
Ingrese un numero un unico caracter numericoSi quiere terminar ingrese F o f
f
```
