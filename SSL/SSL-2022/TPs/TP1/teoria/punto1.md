# Ejercicio 1 

## Breve explicacion 

Elegimos leer de archivo, por que cuando leimos que se lea de consola tuvimos duda si era cuando se invocaba el comando de ejecucion pero no cambiaria mucho poder realizar la opcion que lo lea con argc y argv. 
Pero en este caso al realizar por archivo decidimos leer por lineas y tratarlo como si fuera un csv o tsv 

Utilizamos una funcion llamada strtok que forma parte de la libreria estandard string que lo que realizaria esta funcion es hacer un split de python que seria separar en base a un delimitador . 

## Ejemplo de strtok 

```
  char *token = strtok(str,delimiter);

  while(token != NULL){

    if(perteneceDecimal(token)) cantDecimal++;

    else if (perteneceOctal(token)) cantOctal++;

    else if (perteneceHexadecimal(token)) cantHexadecimal++;
    else printf("ERROR LEXICO %s\n",token);
    token = strtok(NULL,delimiter);

}
```
``` 

┌──────────────────┐
│1234&123&41234&878│
└──────────────────┘
1      2    3      4 
┌────┐┌───┐┌─────┐┌───┐
│1234││123││41234││878│
└────┘└───┘└─────┘└───┘

```

* La primer vez que pasa por strtok obtenemos 1234
* La segunda : 123 
* Tercera: 41234
* cuarta: 878 

Luego lo que realizamos con ese nuevo puntero (string que nos da) es analizarlo y ver si corresponde a decimal octal o hexadecimal.Basandonos en la tablas de transicion.


## Tablas de Transicion 

### Octal 

\-|0|1|2|3|4|5|6|7 
--|-|-|-|-|-|-|-|-
0-|1|4|4|4|4|4|4|4
1 |2|3|3|3|3|3|3|3
2+|4|4|4|4|4|4|4|4
3+|3|3|3|3|3|3|3|3
4 |4|4|4|4|4|4|4|4

``` mermaid 
flowchart LR
  A[0-] -- 0 --> B[1]
  B -- 0 --> D[2+]
  B -- 1-7 --> C[3+]
  C -- 1-7 -->C
```
  

### Decimal 

\-|0|1|2|3|4|5|6|7|8|9|- 
--|-|-|-|-|-|-|-|-|-|-|-
0-|2|3|3|3|3|3|3|3|3|3|1
1 |4|3|3|3|3|3|3|3|3|3|4
2+|4|4|4|4|4|4|4|4|4|4|4
3+|3|3|3|3|3|3|3|3|3|3|4
4 |4|4|4|4|4|4|4|4|4|4|4

``` mermaid

flowchart LR
  A[0-] -- 0 --> B[1+]
  A -- " - " --> C[2]
  C -- 1-9 --> D[3+]
  D -- 0-9 --> D
  A -- 1-9 --> D

```

### Hexadecimal 

\-|0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f|X
--|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-
0-|1|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5
1 |5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|2
2 |3|4|4|4|4|4|4|4|4|4|4|4|4|4|4|4|5
3+|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5
4+|4|4|4|4|4|4|4|4|4|4|4|4|4|4|4|4|5
5 |5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5|5

``` mermaid
flowchart LR
  A[0-] -- 0 --> B[1]
  B -- X--> C[2]
  C -- 0 --> D[3+]
  C -- 1-9A-F --> E[4+]
  E -- 0-9A-F --> E
```

## Ejemplo de ejecucion



```
Ingrese el numero de ejercicio que quiera ver 
 1 ------------> Ejercicio 1 
 2 ------------> Ejercicio 2 
 3 ------------> Ejercicio 3 
 4 o otro numero para terminar 
1
Ingrese el nombre del archivo
test.txt
Ingrese el delimitador a utilizar
&
ERROR LEXICO 878
ERROR LEXICO 0987878
ERROR LEXICO 878
ERROR LEXICO 878
ERROR LEXICO 878
ERROR LEXICO 878
ERROR LEXICO 1234
ERROR LEXICO 1234
ERROR LEXICO aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
ERROR LEXICO bbbbbb
ERROR LEXICO aaaaaa aaaaaaaa 
ERROR LEXICO  aaaaaa 
ERROR LEXICO bbbbbbbbb
ERROR LEXICO bbbb 
ERROR LEXICO 

DECIMALES: 14
OCTALES: 0
HEXADECIMAL: 0
```

