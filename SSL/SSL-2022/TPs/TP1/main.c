#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void printValues(char*, char*);
void printFileLines(char *);
int checkNumber(char*);
void printFileLinesWithFscanf(char*);

int main(){

  char string[] = {"123412&1234&097789&09098&9990909"};
  char delimiter[2] = "&";
  printf("The string %s\n",string);
  printf("The delimiter  %s\n",delimiter);
  printValues(delimiter,string);

  char *fileName = "test.txt";
  printf("----------------------------------\n");
  printf("This with getline (not standard) \n");
  printFileLines(fileName);
  printf("----------------------------------\n");
  printf("----------------------------------\n");
  
  char buff[255];
  printf("This with fscanf \n");
  printFileLinesWithFscanf(fileName);

  printf("----------------------------------\n");
  printf("----------------------------------\n");
  return EXIT_SUCCESS;
}

// This function print all the values of a string split by one delimiter
void printValues(char *delimiter, char *str){
  char * token = strtok(str,delimiter);
  while(token != NULL){
    // Para probar tire check Number 
    printf("%s,%s\n",token,checkNumber(token)?"si es un numero":"no es un numero entero");
    token = strtok(NULL,delimiter);
  }
}

// Imprime todas las lineas y los substrings separados por &

void printFileLines(char *name){
  FILE *fp = fopen(name,"r");
  char *line_buf = NULL;
  size_t line_buf_size = 0;
  // signed size_t para devolver valores ( utilizado con errores) 8 bytes 
  ssize_t  line_size;

  if (!fp){
    // si el archivo no se pudo leer 
     fprintf(stderr, "Error opening file '%s'\n", name);
     EXIT_FAILURE;
  }
  line_size = getline(&line_buf, &line_buf_size, fp);
  while(line_size >=0){
    printf("string : %s\n",line_buf);
    printValues("&",line_buf);
    
    line_size = getline(&line_buf, &line_buf_size, fp);
  }
}

int checkNumber(char *number){
  return atoi(number);
}

void printFileLinesWithFscanf(char * file){
  FILE *fp;
  fp = fopen(file,"r");
  char buff[255]; 
  int i =0;
  // It's better to use fgets because read more 
//  fscanf(fp,"%s",buff);
//  printf("sizeof buffer %ld\n",sizeof(buff));

  while(fscanf(fp,"%s",buff) != EOF){
//fgets(buff,255,fp) != NULL
    printf("line: %d  ",i++);
    printf("%s\n",buff);
   // fgets(buff,255,fp);
  }
    printf("%s\n",buff);
  

  fclose(fp);
}
