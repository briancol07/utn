#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void menu(); // imprime menu
void seleccion(int); // Se usa para ver que ejercicio se ejecuta


// Funciones de los ejercicios 
void ejercicio1(char *,char*); // El primer parametro es el archivo, el segundo es el delimitador 
void ejercicio2();
void ejercicio3();

void verificarTipos(char *, char *);
int charInt(char);

// Funciones para identificar si son decimales, octales o hexadecimal

int perteneceDecimal(const char*);
int perteneceOctal(const char*);
int perteneceHexadecimal(const char*);


int columnaDecimal(int);
int columnaOctal(int);
int columnaHexadecimal(int);

// Funciones para el 3 

bool esOperador(char);

// Variables contadoras

int cantDecimal = 0;
int cantOctal = 0;
int cantHexadecimal = 0; 

int main(){
  int var; 
  while(1){
    menu();
    scanf("%d",&var);
    seleccion(var);
  }
  return 0;
}

void menu(){
  printf("Ingrese el numero de ejercicio que quiera ver \n");
  printf(" 1 ------------> Ejercicio 1 \n");
  printf(" 2 ------------> Ejercicio 2 \n");
  printf(" 3 ------------> Ejercicio 3 \n");
  printf(" 4 o otro numero para terminar \n");
}

void seleccion(int var){
  char fileName[200]; // Para saber el nombre del archivo 
  char delimiter[2]; // Que tipo de delimitador utilizaremos 
                     // Puede ser que el archivo incluya la ruta 
  switch (var){
    case 1:
      printf("Ingrese el nombre del archivo\n");
      scanf("%s",&fileName);
      printf("Ingrese el delimitador a utilizar\n");
      scanf("%s",&delimiter);
      ejercicio1(fileName,delimiter);
      break;
    case 2: 
      ejercicio2();
      break;
    case 3: 
      ejercicio3();
      break;
    default:
      exit(0);
 }
}


void ejercicio1(char* fileName, char* delimiter){
  char cadena[255];
  FILE *fp;
  fp = fopen(fileName,"r");
  while(fgets(cadena,sizeof(cadena),fp) != NULL){
    verificarTipos(delimiter,cadena);
   }
  fclose(fp);

  printf("DECIMALES: %d\n",cantDecimal);
  printf("OCTALES: %d\n",cantOctal);
  printf("HEXADECIMAL: %d\n",cantHexadecimal);
}

void ejercicio2(){
  char num = '0';
  // Aca se puede hacer una comprobacion aver si se ingresaron mas de un caracter 
  while((num != 'f') && ( num != 'F')){
    printf("Ingrese un numero un unico caracter numerico");
    printf("Si quiere terminar ingrese F o f\n");
    getchar();
    scanf("%c",&num); 
    printf("El numero en entero es: %d\n",charInt(num));
  }
}
void ejercicio3(){

    char inf [401];
    char post [401];
    char pila [200];
    int pila2 [200];
    int ptr=0;
    int j=0;

    scanf("%s",&inf);

    fgets(inf,sizeof(inf),fp)

  return 0;
}

void verificarTipos(char * delimiter, char *str){
  char *token = strtok(str,delimiter);
  while(token != NULL){
    if(perteneceDecimal(token)) cantDecimal++;
    else if (perteneceOctal(token)) cantOctal++;
    else if (perteneceHexadecimal(token)) cantHexadecimal++;
    else printf("ERROR LEXICO %s\n",token);
    token = strtok(NULL,delimiter);
  }
}

int charInt(char c){
  return c - '0';
}

int perteneceDecimal( const char* cadena){
  static int tt[5][3] = { {2,3,1},
                          {4,3,4},
                          {4,4,4},
                          {3,3,4},
                          {4,4,4}};
  int e=0,j =0;

  // Ver si los caracteres perteneces al alfabeto 
  for( int i=0; cadena[i]!= '\0';i++){
    int numero = charInt(cadena[i]);
    if(!((numero >= 0 && numero < 10) || cadena[i] == '-')){
      return 0;
    }
  }
  int c = cadena[j];
  while(c != '\0' && e != 4){
    e = tt[e][columnaDecimal(c)];
    j++;
    c = cadena[j];
  }
  if(e == 2 || e == 3) return 1;
  return 0;
}

int columnaDecimal(int c){
  if (c == '0') return 0;
  else if (c == '-') return 2;
  else return 1;
}

int perteneceOctal(const char* cadena){
  static int tt[5][2] = { {1,4},
                          {2,3},
                          {4,4},
                          {3,3},
                          {4,4}};
  int e = 0, j = 0;
  for (int i = 0; cadena[i] != '\0'; i++){
    int numero = charInt(cadena[i]);
    if(!(numero >= 0 && numero < 8)){
      return 0;
    }
  }
  int c = cadena[j];

  while(c != '\0' && e != 4){
    e = tt[e][columnaOctal(c)];
    j++;
    c = cadena[j];
  }
  if(e == 2 || e == 3) return 1;
  else return 0;

  }

int columnaOctal(int c){
  if ( c == '0') return 0; 
  // no se podria reemplazar por return c == '0' ? charInt(c) : 1;
  return 1;
} 

int perteneceHexadecimal( const char * cadena){

  static int tt[6][3] = { {1,5,5},
                          {5,5,2},
                          {3,4,5},
                          {5,5,5},
                          {4,4,5},
                          {5,5,5}};
  int e = 0, j = 0;
  for (int i = 0; cadena[i] != '\0'; i++){
    int numero = charInt(cadena[i]);
    if(!((numero >= 0 && numero < 10)||(numero >= 49 && numero < 55)||( cadena[i] =='x'))){
      return 0;
    }
  }
  int c = cadena[j];

  while(c != '\0' && e != 5){
    e = tt[e][columnaHexadecimal(c)];
    j++;
    c = cadena[j];
  }
  if(e == 3 || e == 4) return 1;
  else return 0;

}

int columnaHexadecimal(int c){
  if (c == '0') return 0;
  else if ( c == 'x') return 2;
  return 1;
}


// Funciones 3 

bool esOperador(char c){
  if(c == '+' || c == '-') return true;
  return false ; 
}
