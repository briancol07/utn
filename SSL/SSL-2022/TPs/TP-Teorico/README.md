# Trabajo Practico 

> Lenguaje: GOLANG 

## Team 



## Overview 

* Breve historia del lenguaje 
* Binding 
* Scope 
* Buscar una funcion de ordenamiento que sea parte del lenguaje e investigar que metodo utiliza ( bubble sort, quick sort, etc)

## Archivos Requeridos 

* Presentacion (ppt o similar) 
* Codigo / Ejemplos practicos 
* Set de preguntas para sus companieros 
* BNF 

## Bibliography 

[kuree](https://kuree.gitbooks.io/the-go-programming-language-report/content/index.html)
