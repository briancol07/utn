# Hello world 

## Example 

```go 

package main 
import "fmt"

func main (){
  fmt.Printf("Hello, world\n");
}
```

## Compile & Run

> The name of the file should be hello.go

```bash
go run hello.go
```


