# The Paradigms of GO

## Object Oriented 

> Yes and no

* Allows types and methods, and an object oriented programing style. 
* There is no Class hierarchy but interfaces
* Method can be designed for all types including the primitive types such as int. 
* Object is called struct -> light-weighted like c/c++/java

## Imperative 

* Imperative language 
  * Loops 
  * Statements 
  * Selections 

## Concurrent programming 

* Native support for concurrent operations.
  * Multi-threading
  * Multi-processing 
  * Asynchrony 


