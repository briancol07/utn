# What are the NO's in Go design ?


1. No ternary testing operation '?:'
2. No generic types 
3. No exceptions
4. No assertations 
5. No "implements" for interface
6. No algebraic types 
7. No pointer arithmetic 
8. No semicolons and no opening brace on the next line 
9. No implicit numeric conversions 
10. No untagged uions, as in C
11. No overloading of methods and operators 
12. No type inheritance 
