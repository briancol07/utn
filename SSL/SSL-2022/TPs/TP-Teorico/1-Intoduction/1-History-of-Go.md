# History of Go 

The go programming language was released in late 2007 by Google. It is envisioned as an answer to some of the problems in Software infrastructure development at Google. 

## Based on 

* C++
* Java 
* Python 

## Main purpose 

Go was designed and developedto make working in this environment more productive. Besides its better-known aspects such as built-in Concurrency and garbage collection. 

### Considerations 

* Rigorous dependency management 
* The adaptability of Software architecture as systems grow
* Robustness across the boundaries between components

## Characteristics 

* Compiled
* Concurrent 
* Garbage-collected
* Statically typed language 
* Open source 
  * Google imports the public repository rather than the other way around 

