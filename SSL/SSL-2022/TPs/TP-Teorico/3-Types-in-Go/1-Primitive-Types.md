# Primitive Types in Go 

* String 
  * Is the set of all strings of 8-bytes (UTF-8)
  * Strign may be empty but not nil. 
  * Immutable 
* Numeric Type 
  * Unsigned 
    * uint8 -> unsigned 8-bit integer  
    * uint16 -> unsigned 16-bit integer  
    * uint32 -> unsigned 32-bit integer  
    * uint64 -> unsigned 64-bit integer  
  * Signed 
    * int8 -> signed 8-bit integer  
    * int16 -> signed 16-bit integer  
    * int32 -> signed 32-bit integer  
    * int64 -> signed 64-bit integer  
  * float 
    * float32 -> set of all IEEE-754  32-bit floating-point number   
    * float64 -> set of all IEEE -754 64-bit floating-point number  
  * Complex 
    * complex64 -> The set of all complex number with float32 real and imaginary parts
    * complex128 -> The set of all complex number with float 64 real and imaginary parts 
  * Byte 
    * byte alias for uint8 run alias for int32 
* Bool 
  * true and false 
* Error 
  * Interface type 
  * Wrappers around the string type 


