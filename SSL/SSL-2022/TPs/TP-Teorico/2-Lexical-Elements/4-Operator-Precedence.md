# Operator precedence in GO

> 5 the strongest 

Precedence  | Operator 
------------|----------
 5 | * / % << >> & &^
 4 | + - | ^
 3 | == != < <= > >= 
 2 | &&
 1 | ||

 Binary operators of the same precedence associate from left to right 

 ```
x / y * z
(x /y) * z 
 ``` 


