# Tokens in Go 


1. Identifiers 
2. Keywords 
3. Operators and Delimiters 
4. Literals 
5. This are ignored Except as it separates tokens 
  * Otherwise combine into a single token 
  * White space (U+0020)
  * Horizontal tabs (U+0009)
  * Carriage Returns (U+000D)
  * Newlines (U+000A)
  * New line and end of file -> Insertion of semicolon

while breaking the input into tokens, the next token is the longes sequence of characters that form a valid token


## Comments Style 

``` go 

// this is a comment 
/* also this (this is a multiline) */
