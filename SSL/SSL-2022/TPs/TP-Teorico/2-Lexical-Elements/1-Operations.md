# Operations on Numbers

Symbol | Description | Apply to 
-------|-------------|----------
\+ | sum | Integer, floats, complex values, strings
\- | Difference | Integer, floats, complex values 
\* | Product | Integer, floats, complex values
/ | Quotient | Integer, floats, complex values
% | Reminder | Integer
& | bitwise AND | Integer 
\| | bitwise OR | Integer 
^ | bitwise XOR | Integer 
&^ | bit clear (AND NOT) | Integer 
\<< | Left shift | Integer << unsigned integer
\>> | Right shift | Integer >> unsigned integer 

## Directly access/ manipulate the bits of an integer 

Go is very similar to C , so we have the bits operators 

see [bit.go](./examples/bit.go)
