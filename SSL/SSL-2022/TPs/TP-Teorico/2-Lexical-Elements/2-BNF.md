# BNF 

[Specification](htpps://go.dev/ref/spec#Notation)

## Constant declaration 

```
constDec1 = "const" ( ConstSpec | "(" { ConstSpec ";" } ")" ).
ConstSpec = IdentifierList [ [ Type ] "=" ExpressionList] . 

IdentifierList = identifier { "," identifier }.
ExpressionList = Expression { "," Expression }.
``` 

## Type declarations 

``` 
TypeDec1 = "type" ( TypeSpec | "(" { TypeSpec ";" } ")" ).
TypeSpec = identifier Type .
```

## For Loops 

```
ForStmt = "for" [ condition | ForClause | RangeCaluse] Block. 
Condition = Expression . 
``` 

## Select statements 

```
SelectStmt = "select" "{" { CommClause} "}" . 
CommClause = CommCase ":" StatementList . 
CommCase = "case ( SendStmt | RecvStmt ) | "default" .
RecvStmt = [ ExpressionList "=" | IdentifierList ":=" ] RecvExpr.
RecvExpr = Expression.`
```
## Return Statements 

```
ReturnStmt = "return" [ ExpressionList ] . 
```
