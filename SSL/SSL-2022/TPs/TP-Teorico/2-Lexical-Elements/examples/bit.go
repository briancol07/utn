package main 

import "fmt"

func main(){
  var i1 int = 0xFF;
  var i2 int = 0x04;
  var i3 int = 0xF0;
  var i4 int = 0x0F;
  

  fmt.Printf("0x%02x bitwise AND 0x%02X is 0x%02X \n",i1,i2,i1 & i2);
  fmt.Printf("0x%02x bitwise OR 0x%02X is 0x%02X \n",i3,i4,i3 | i4);
  fmt.Printf("0x%02X >> 1 is 0x%02X\n",i3,i3 >> 1);
  fmt.Printf("0x%02x bitwise XOR 0x%02X is 0x%02X \n",i3,i4,i3 ^ i4);
  fmt.Printf("0x%02x bit clear 0x%02X is 0x%02X \n",i1,i3,i1 &^ i3);
  
}
