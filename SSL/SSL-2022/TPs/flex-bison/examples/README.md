# Flex y Bison 

## flex & Bison [O'Reilly]

flex = archivo.l
bison = archivo.y

## Notes

Primero corremos flex 

```bash
flex archivo.l
```

que nos traduce el scanner a un programa en c llamdo lex.yy.c despues compilamos ese programa 

```bash 

gcc lex.yy.c -lfl
```
> (-lfl) the flex library nos da un pequenio main que llama solo escanner 


cada vez que el programa necesita un token llama a yylex() que lee la entrada de datos y devuelve el token, actua como co-rutina.

## Tokens and Values 

token | token's Value
------|-------------
Number | 258
ADD | 259
SUB | 260
MUL | 261 
DIV | 262
ABS | 263
EOL | 264 (end of line)

## Grammar and Parsing 

El trabajo del parser es saber cual es la relacion tentre los tokens de entrada , la forma mas comun es con un parse tree 

> 1 * 2 + 3 * 4 + 5

```mermaid 

graph TD;
  A---B[+]
  B---D[*]
  B---E[*]
  D---F[1]
  D---G[2]
  E---H[3]
  E---I[4]
  A[+]---C[5]
```
```
<exp> ::= <factor>
      | <exp> + <factor>
<factor> ::= NUMBER
      | <factor> * NUMBER
```


## Bison structure 

1. Declarations 
  * %{ C Code %}
2. Token Declarations 
  * %token token
3. Simplified BNF 
  * ::=  
  * ; mark end of rule 
  * Symbols 
    * rule have value
    * $$ (target symbol)
    * $1,$2... (values)
