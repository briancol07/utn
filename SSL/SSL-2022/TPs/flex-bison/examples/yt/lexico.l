%{
  #include<stdio.h>
  #include<string.h>
  #include "sintactico.tab.h"

  void showError();

%}

numbers ([0-9])+
alpha   ([a-zA-Z])+
newline ([\n])+


%%

"chmod"     { yylval.reservada = yytext; return (CHMOD);}
{numbers}   { yylval.number = atoi(yytext); return (ENTERO);}

";"         { return (PUNTOCOMA);}
{alpha}     { sscanf(yytext, "%s",yylval.cad); return(CADENA);}

[ \t\r]    {}
{newline}   { return(NEWLINE); }

.           { showError(); return(OTHER); }

%%


void showError(char* other){
  printf("  << Error Lexico: %s \n >>",other);
}


