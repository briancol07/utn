%{

  #include<stdio.h>

  int yylex();
  int yyerror(char *s);


%}

%token CADENA ENTERO OTHER PUNTOCOMA CHMOD
%token NEWLINE

%type <cad> CADENA
%type <number> ENTERO
%type <reservada> CHMOD



// union propio de bison 
// definicion de atributos 

%union{
  char cad[20];
  int number;
  char *reservada;
}

%%
//gramatica 
prog:
    INSTRUCCIONES
    
;

INSTRUCCIONES: 
             | INSTRUCCION PUNTOCOMA NEWLINE INSTRUCCIONES
              
INSTRUCCION:
            | CADENA  { printf("(cadena): %s\n",$1);}
            | ENTERO  { printf("(num): %d\n",$1);}
            | OTHER   
            | CHMOD   { printf("(chmod): %s\n",$1);}
%%

int yyerror(char *s){
  printf(" -> Error Sintactico %s\n",s);
}

int main (int argc, char **argv){
  yyparse();
  return 0;
}
