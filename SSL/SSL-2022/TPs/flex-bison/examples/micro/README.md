# Micro 

## Gramatica Lexica 

```

<token> -> uno de <identificador> <constante><PR>
                  <operadorAditivo><Asignacion><caracaterPuntuacion>
<identificador> -> <letra> {<letra o digito>}
<constante> -> <digito> {<digito>}
<letra o digito> -> uno de <letra> <digito>
<letra> -> una de a-zA-Z
<digito> -> uno de 0-9
<PR> -> una de inicio fin leer escribir
<operadoraditivo> -> uno de + -
<asignacion> -> :=
<caracterPuntuacion> -> uno de () , ; 

```

## Gramatica Sintactica 

```
<programa> -> 
              inicio <listaSentencias> fin 

<listaSentencias> ->
                   <sentencia> {<sentencia>}

<sentencia> ->
               <identificador> := <expresion> ; |
               leer ( <listaIdentificadores> ) ; |
               escribir ( <listaExpresiones> ) ;

<listaIdentificadores> -> 
                          <identificador> , {<identificador>}

<listaExpresiones> ->
                      <expresion> { , <expresion> }

<expresion> -> 
              <primaria> {<operadorAditivo> <primaria>}

<primaria> ->
              <identificador> | <constante> | ( <expresion> )

```

## Parser para micro 

```

<objetivo> ->
              <programa> FDT

<programa> ->
              INICIO <listaSentencias> FIN

<listaSentencias> -> 
                    <sentencia> {<sentencia>}

<sentencia> ->
              ID ASIGNACION <expresion> PUNTOYCOMA |
              LEER PARENIZQ <listaIdentificadores> PARENDER PUNTOYCOMA|
              ESCRIBIR PARENIZQ <listaExpresiones> PARENDER PUNTOYCOMA

<listaIdentificadores> -> 
                          ID {COMA ID}

<listaExpresiones> ->
                      <expresion> {COMA <expresion>}

<expresion> -> 
              <primaria> {<operadorAditivo> <primaria>}

<primaria> ->
              ID | CONSTANTE | PARENIZQ <expresion> PARENDER

<operadorAditivo> -> uno de SUMA RESTA

```




              
