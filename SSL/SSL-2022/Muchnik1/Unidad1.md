# Unidad 1 

## Definiciones Basica E Introduction a lenguajes Formales 

La sintaxis de un lenguaje de programacion describe las combinaciones de simbolos que forman un progrma sitacticamente correcto. Como esta Sintaxis esta basada en los lenguajes Formales debemos comenzar con este concepto 

Los **Lenguajes formales** esta formados por palabras , las palabras son cadenas y las cadenas estan constituidas por caracteres de un alfabeto. Esta frase involucra cinco terminos ( Lenguaje formal , palabra cadena, caracter y alfabeto) que son fundamentales en este campo. 

## Caracteres y Alfabetos 

> Un **caracter** (tambien llamado simbolo es un elemento constructivo basico, fundamental, indivisible. 

> Un **Alfabeto** es un conjunto finito de caracteres.($`\Sigma`$) y con los caracteres se construyen las cadenas de caracteres (lenguaje Formal) 

## Cadenas 

Una **Cadena** (cadenas de caracteres)  : de un alfabeto dado

* Finita 
* Concatenacion de caracteres 
* String 
* Ej 
  * 101010 --> A = {0,1}
  * abac --> B = {a,b,c}

## Longitud de una cadena

> Se representa con |S| (S siendo la cadena), la cantidad de caracteres que la componen. 

* Ej 
  * |abab| = 4
  * |b| = 1 

## Cadena Vacia 

> La cadena vacia que se simbiliza habitualmente con la letra griega $`\varepsilon`$ (epsilon), es la cadena que no tiene caracteres. Longitud = 0 => |$`\varepsilon`$| = 0. 

No forma parte de ningun alfabeto. 

## Simplificacion : Potenciacion de un simbolo

$`\Sigma = \{ a,b \} `$ , la cadena aaaabbbbbbb. 

> La Potenciacion de un caracter simplifica la escritura de cadenas como la del ejemplo .

En Este caso seria $` a^4b^7 `$

## Concatenacion de dos cadenas 

La operacion de concatenacion ($` S_1 \bullet S_2 or S_1S_2 `$) produce una nueva cadena formada por los caracteres de la primera seguidos por lo de la segunda.

* Ej
  * $` S_1 = aab \land S_2 = ba \Rightarrow S_1S_2 = aabba `$ 

La concatenacion normalmente se escribe sin el simbolo . Tambien se puede extender facilmente a mas de dos cadenas. 


