# Clase3 2021-04-22

* Regex / Extended Regex 


Sube los resueltos de las guias y tengo que empezar a hacer 

va a haber un tp individual , esta elaborando 

va a ser sobre regex utilizando bash, va a estar aclarado en el enunciado del tp 

1. Github 
  * Planilla donde cargar 
  * Sea con la cuenta institucional 
  * 
2.    

bash es poderoso para archivos de texto y para regex 

# Recapitulando 

Gramaticas -> generan lenguajes
 
* Gramaticas Regulares
* Gramaticas Independiente de contexto 

son cuatruplas (lenguaje terminales ,lenguaje no terminales,producciones , axioma )

ver diapositivas 

# Expresiones regulares 

Es una representacion de un lenguaje regular , describen un conjunto de cadenas.

* Operadores finitos
* Operadores infinitos 
  * clausura de Kleen
  * clausura positiva 
* Potencia
  * Extendidas 

Para los mails no se puede usar extendidas 

### Operadores finitos 

* * o nada 
* Union (+ o |)
* Potencia (no oficial)


$` \Sigma = {a,b,c}`$
$` L = {a ,abc} `$

Er : (a+b)c ==> L = {ac,bc} uno o el otro 

Er : a^3(b+c)^2

* Palabras posibles 
  * aaabb
  * aaacc
  * aaabc
  * aaacb 

Er para L = {aaabb,aaacc}

Er = a^3(b^2 + c^2) = aaa . (bb+cc) <- esta es pura 

### Operadores infinitos

* *
* + como super indice 

clausura de klene perteneces expresiones regulares 

### Klene 

de 0 a infinito 

* a\* => lr = {sigma,a,aa,aaa,...,aaaaaa}

(a+b) \*  
aa \* = a^+
a\*a\* = a\* 

Se pueden factorizar 
a + b\* 
se aplica primero la clausura potencia y luego la union 

## Ej 

```
S -> aS
s -> a

Er = aa\*
Er Extendida = a^+
por compresion L = {a^n / n>0} 

```
independiente de contexto son infinitos 
Es regular y es infinito 

L = {a^n b^n / n>3}

es finito => es regular 



