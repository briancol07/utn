# Introduccion

## Definiciones 

**Sintaxis** : Disciplina linguistica que estudia el orden y la relacion de las palabras en la oracion, asi como las funciones que cumplen. Modo de combinarse y ordenarse las palabras y las expresiones dentro del discurso.

<br>
**Semantica**: Parte de la linguistica que estudia el significado de las expresiones linguisticas.

<br>
**Lexico**: Conjunto de las palabras de una lengua.

## Lenguajes 

Un lenguaje es un sistema de comunicacion estructurado para el que existe un contexto de uso y ciertos principios combinatorios formales.

### Naturales 

* Caract Lenguajes Naturales:
  * Evolucionan con el paso del tiempo incorporando terminos y reglas 
  * Sus reglas gramaticales surgen luego del desarrollo del lenguaje para poder explicar su sintaxis 
  * En los lenguajes naturales el significado de cada palabra (semantica) y de cada oracion en general es mas importante que su composicion sintactica 
  * Existe la ambiguedad, ua que el receptor no recibe a veces lo que el emisor quiere transmitir 

### Formales 

* Es un conjunto de cadenas formados por caracteres de un alfabeto dado
* Las cadenas que constituyen un lenguaje formal se denominan palabras del lenguaje. No tienen una semantica asociada 
* A diferencia de los lenguajes naturale, los lenguajes formales no son ambiguos y no evolucionan naturalmente

> Los lenguajes formales son conjuntos de palabras 

* Una cadena es la concatenacion de 0 (cadena nula) o mas simbolos del alfabeto
* Una cadena es palabra de un lenguaje cuando es un elemento del conjunto de palabras que define el lenguaje
* Simbolo 
  * elemento constructivo basico; entidad fundamental e indivisible 

### ej 

Un lenguaje formado de tres palabras 

``` 
L = { negro , blanco , azul}
```
### ej 2 

``` c

int main(int argc , char * argv[]){
  if (x)
    resultado = 3 * x;
  return 0;
}
```
* if , main son palabras que pertenecen al lenguaje de las palabras reservadas de C . Es un lenguaje finito 
* X, resultado son palabras que pertenecen al lenguaje de los identificadores , es un lenguaje infinito 


## Alfabeto

* Los alfabetos se suelen representar con la letra griega $`\Sigma`$
* Los caracteres del alfabeto, en forma generica se suelen representar con letras minusculas en particular con las primeras de nuestro alfabeto a,b,c

### Ejemplos 


* $`\Sigma_{1} = {x,y,z}`$
* $`\Sigma_{2} = {0,1}`$

## Cadenas 

* Secuencia finita de caracteres de un cierto alfabeto que se arma por simple concatenacion 
* cadena vacia $`\varepsilon`$ o $`\lambda`$

### Longitud

> Se representa |s| la cantidad de caracteres, s siendo una cadena 

### Potenciacion

> El operador superindice utilizado para potenciar un simbolo indica el numero de veces que aparece el simbolo potenciado 

* $` a^{0} = \varepsilon`$
* $` a^{1} = a `$
* $` a^{2} = aa `$

### Concatenacion

> Al aplicarla a dos cadenas produce una nueva cadena formada por los carracteres de la primera seguidos inmediatamente por los caracteres de la segunda cadena

* No es conmutativa
* Es conmutativa si:
  * S1 = S2
  * Alguna es nula 
  * $` S_1 = a^{n} y S_2 = a^{m} `$

### Prefijo , sufijo , subcadena

Un prefijo de una cadena es una secuencia de cero o mas caracteres iniciales de esa cadena 
<br>
Un sufijo de una cadena es una secuencia de cero o mas caracteres finales de esa cadena 
<br>
Una Subcadena de una cadena es una secuencia de caracteres que se obtiene eliminando cero o mas caracteres iniciales y cero o mas caracteres finales de esa cadena. 
<br>

## Lenguaje universal 

Lenguaje infinito que contiene todas las palabras que se pueden formar con los caracteres del alfabeto mas la palabra vacia.
