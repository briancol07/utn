# Gramaticas 

La concatenacion de dos palabras produce una cadena que no siempre es palabra del lenguaje.
<br> 
Cardinalidad de un lenguaje formal, es la cantidad de palabras que lo componen.

## Sublenguajes 

Es un subconjunto de un lenguaje dado.

### Ej 

``` 
L1 = {a,ab,aab}
L2 = {ab,aab} 
L3 = { } 
```
L3 es el sublenguaje vacio 

> Sublenguaje vacio tiene cardinalida 0 meintras que el lenguaje que contine la palabra vacia que tiene cardinalida 1

## Lenguaje universal Sobre un Alfabeto 

El lenguaje universal sobre este alfabeto es un lenguaje infinito que contienen todas las palabras que se pueden formar con los caracteres del alfabeto.
Es cerrado bajo la concatenacion 
Existen estrucutras para generar las palabras que forman un LF. Se denominan:

## Gramatica Formal

* Conjunto de producciones
* L = {a} S->a (S produce a a) 

Toda produccion esta formada por tres partes, Lado izquierdo , lado derecho y las flechas que modifican que el lado izq de la produccion produce y la flecha, que indica que el lado izquierdo de la produccion "produce"

Toda gramatica es una 4-upla (Vn,vt,P,S)

* Vn: Vocabulario de no terminales 
* Vt: Vocabulario de terminales ocaracteres del alfabeto 
* P : Conjunto finito de producciones 
* S es un no terminal especial , que es el simbolo inicial o axioma desde el cuale se generan las otras 

### Chomsky 

* Gramaticas Regulares o Gramaticas tipo3 
* Gramaticas independeientes del contexto o Gramatica tipo 2 
* Gramaticas Sensibles al contexto o gramticas tipo 1 
* Gramaticas Irrestrictivas o Gramaticas tipo 0

## Gramaticas Regulares 

* Lado izq debe tener un solo no terminal 
* Lado derecho formado por un solo terminal o un terminal seguido por un no terminal

```
G = ({S,X},{a,b},{S->aX,X->b})
```

Se dice que una gramatica regular es lineal a derecha si todas las producciones con terminales y no terminales son de la forma A -> aB, Alguno lo llaman gramaticas estrictamente regulares a izquierda o derecha 

## Gramatica Irrestricta (GI)

Son las GFs mas amplias. Sus producciones tienen la forma general a -> b , donde a y b pueden ser secuencias de no terminales y/o terminales

## Gramaticas de Contexto (GSC)


Si L|F es infinito entonces simepre es un lenguaje regular , En cambio si uyn lf es infinito entonces  puede ser un lenguaje regualr o un lenguaje no regular 

Un lenguaje es regular si existe una gramaticca regular que lo genere. 
Un lenguaje regular si se puede representar mediante una Expresion Regular 
Un lenguaje es Regular si puede ser reconocicido por un automata finito 

## Destacar 

1. Las gramaticas generan lenguajes
2. Las expresiones regulares Representan lenguajes regulares 
3. Los automatas reconocen elementos de un lenguaje



