# Ejecucion y Compilacion 

En caso de compilar y ejecutar usando devC++ o codeblocks hacerlo apretando el boton 

En caso de usar gcc 

## Compilar 

``` bash 
gcc nombre.c
gcc nombre.c -o nombre
```

El primer comando lo que hara es compilar sin ningun flag el programa llamado nombre y generara un archivo a.out

En cambio el flag -o lo que hace es que en vez a.out tendremos un archivo llamado nombre el cual podremos tambien ejecutar 

## Ejecutar 

``` c
./a.out
./nombre 
```

En la terminal el primero ejecutaria el que compilamos primero en el ejemplo anterior y el otro ejecutaria el segundo.

