#include<stdio.h>
#include<stdlib.h>


int main(){

  int a = 7;

  while(a > 5) {
    printf("%d\n",a);
    a--;
  }

  do {
   a--; 
   printf("%d",a);
  }while(a > 5);

  for(int i = 0; i < 10 ; i++){
    printf("%d",i);
    if (i == 4){
      continue;
    }
    if (i == 6) break;
  }
  printf("%d",i);

  return 0;
}
