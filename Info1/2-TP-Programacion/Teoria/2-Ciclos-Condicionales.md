# Condicionales 

## IF 

```c 
if (condicion) instruccion;
```

* Si la condicion es verdadera la instruccion se ejecuta
* Si es falso no realiza la instruccion

## IF ELSE

``` c
if (condicion) instruccion;
else instruccion2;
```

> Si es verdadero hace la instruccion y si es falso va por el else y realiza la instruccion2 

## ? Operador ternaio

``` c 
condicion ? a : b;
```
> Si la condicion es verdadera realiza a , pero si es falsa realiza b.

## Formas de comparar 

* igualdad ( == ) 
* Distinto ( != )
* Mayor ( > )
  * Mayor igual ( >= )
* Menor ( < )
  * Menor igual ( <= ) 

## Switch 

> Es como un if pero con mayor lista de casos , se compara la expresion con los valores de los casos y si coincide entra en ese sino va al default 

``` c 
switch(expresion){
  case valor_1:
    instrucciones;
    break;
  case valor_2:
    instrucciones;
    break;
  default:
    instrucciones;
    break;
}
```

ver [ejemplo2](./ejemplos/ejemplo2.c)
