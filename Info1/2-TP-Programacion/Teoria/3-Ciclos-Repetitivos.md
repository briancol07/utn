# Ciclos Repetitivos 

## While 

> Mientras la condicion sea verdadera se cumple las instrucciones 

``` c
while (condicion) Instrucciones;
```

## Do-While

> Hacer mientras la condicion sea verdadera, se ejecuta almenos una vez a diferencia del while si empieza con condicion falsa

``` c
do { body}
while (condicion);
```
## For 

``` c
for (Inicializacion; Condicion; incremento){Instrucciones}
for (int i = 0 ; i < 5 ; i++) printf("%d",i);

for ( int i=0, j=5; i < j ; i++, j--)
```

## Break

> Termina el bucle 

## Continue 

> Salta al final del bucle osea emite el bucle una vez 

see [ejemplo3](./ejemplos/ejemplo3.c)
