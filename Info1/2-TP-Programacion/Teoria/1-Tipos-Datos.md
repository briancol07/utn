# Tipos de datos 

## Tipos 

* Numeros 
  * Enteros
  * Coma Flotante
* Caracteres 
* Array 
  * Strings 
* Complex Types

## Que es un tipo de dato ? 

Es un conjunto de propiedades para un objeto

* Tamanio de memoria y aliniamiento
* Un set de valores/rango valido
* Un set de operaciones permitidas 

## Enteros 

> Integers 

### Los clasicos 

* short int 
* int 
* long int 
* long long int 
* Combinaciones de estos con unsigned 

### stdint 

> Esta libreria agrega mas tipos de datos enteros , le numero luego de la palabra indica la cantidas de bits

* int8\_t
* int16\_t
* int32\_t
* int64\_t
* uint8\_t
* uint16\_t
* uint32\_t
* int64\_t

``` c 
int variable;  // Creacion de la variable 
unsigned var = 10; // Creacion y asignacion 

``` 

## Coma Flotatante 

> Son todos los numero con coma 

* float 
* double

``` c 
float pi = 3.14
double pi2 = 3.14;
```

## Caracteres 

``` c 
char c = 'a';
```

* 1 byte (8 bits)
* Range -128 -> +127
* ASCII 

### Secuencias de escape 

> A la hora de imprimir por consola se usaran 

* \\b -> Backspace  
* \\n -> Newline  
* \\r -> Retorno de carro  
* \\t -> tab Horizontal  
* \\v -> tab Vertical  
* \\' -> Comilla simple
* \\" -> Comilla Doble
* \\\ -> Barra invertida

Arrays como strings y otros se veran mas tarde 


ver ejemplo [ejemplo1](./ejemplos/ejemplo1.c)
