# TP N2 Programacion 

## Temas 

* Tipos de Datos 
* Variables 
* Ciclos Condicionales
* Ciclos Repetitivos 

## Archivos 

* [Teoria](./Teoria)
  * [0-Ejecucion-Compilacion.md](./Teoria/0-Ejecucion-Compilacion.md)
  * [1-Tipos-Datos.md](./Teoria/1-Tipos-Datos.md)
  * [2-Ciclos-Condicionales.md](./Teoria/2-Ciclos-Condicionales.md)
  * [3-Ciclos-Repetitivos.md](./Teoria/3-Ciclos-Repetitivos.md)
* [ejemplos](./ejemplos)
* [Ejercicios](./Ejercicios)
