# Typedef 

> Para darle a un tipo un nuevo nombre 

``` c

typedef unsigned char BYTE;

BYTE b1,b2;

```
Tambien se puede hacer con las structuras 

``` c
typedef struct Sample {

    double temperature;

      time_t timestamp;

}
``` 

Pero la mejor manera es hacer 

``` c 
typedef struct Guy{

    char *name;
    bool active;
    int dimensions;
    double pos[MAX_DIMENSIONS];
    struct Guy *friend;

}Guy;
```

ver [ejemplo2.c](./ejemplos/ejemplo2.c)
