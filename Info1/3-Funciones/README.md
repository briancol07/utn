# Funciones 

> Divide y venceras 


## Que son las funciones ? 

* Es un bloque de codigo que realiza una accion
* Se puede utilizar para 
  * Realizar solo una accion
  * Para cambiar algun valor 
  * O ambos 
* Encapsula pedazos de codigo 

## Por que usar funciones? 

> Mantenerlo simple y concentrarse en lo que importa 

* Dividir el problema en porciones mas chicas 
* Simplificar el problema ( haciendo caja negra) 
* Mejora la lectura del codigo 
* Aislar bloques de codigo 
* Desarollo y testeo independiente 
* Reutilizar codigo 

## Overview 

* [1-Funciones.md](./1-Funciones.md)
* [2-Llamado-Funcion.md](./2-Llamado-Funcion.md)
* [3-Scope.md](./3-Scope.md)
* [4-Typedef.md](./4-Typedef.md)
* [5-Enum.md](./5-Enum.md)
* [ejemplos](./ejemplos)
  * [ejemplo1.c](./ejemplos./ejemplo1.c)
  * [ejemplo2.c](./ejemplosejemplo2.c)
  * [ejemplo3.c](./ejemplosejemplo3.c)
