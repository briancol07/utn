# Funciones 

## Creando Funciones 

Cuando el bootloader termina llama a la funcion main.

``` c
int main() {... return 0;}
```

## Definicion 

``` c

tipo nombre_Funcion ( tipo dato1, tipo dato2....){
  /* Cuerpo de la funcion */
  return expresion;
}
```

* Tipo 
  * si no devuelve
    * void 
  * Si devuelve
    * int 
    * char 
    * etc  
* nombre\_Funcion
  * empieza con minuscula 
  * No puede empezar con numero
* tipo dato1...
  * Son argumento / parametros que recibe la funcion(desde main u otros) 
  * Son opcionales 
* return expresion
  * En caso de tener que devolver ahi iria el valor que retornaria a la funcion en la cual se llamo

## Que significa devolver?

Si la funcion solo realiza una accion sin modificar nada no tendria por que regresar a la funcion (puede ser main u otra) el valor de algo modificado, entonces iria void ( que seria vacio) encambio si modifica valores y le tendria que "devolver" el valor modificado a main ahi si iria un tipo.


