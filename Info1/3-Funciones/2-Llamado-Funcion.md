# Llamado a la funcion 

> Invocacion de la misma 

## Function call 

``` c 
nombreFuncion ( arg1, arg2,....);
```

* la funcion debe tener el nombre con el que se le creo y el tipo 
* Mismo orden de parametros 
* El valor de retorno puede ser ignorado 

ver [ejemplo1.c](./examples/ejemplo1.c)
