# Scope 

> El scope es el alcanze hasta donde llegan las variables, donde pueden ser modificadas.

* File scope 
  * Todo el archivo 
* Block scope
  * Solo visible en ese bloque 
* Tipos
  * Local 
  * Global 
  * Parametros 

## Variables locales 

* Solo visible en ese bloque 
* Visibilidad limitado a esa funcion
  * Cuando sale de esa funcion se destruye 

## Parametros 

* Block scope
* Limitado a esa funcion 
* Declarado en la definicion de la funcion

## Variables globales 

* File Scope
* Visible en todo el archivo 
* Visible para todas las funciones 


