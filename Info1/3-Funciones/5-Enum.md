# Enums 

> Un tipo particular que el usuario usa para definir

``` c 
enum  myEnum{
  CONST_1,
  CONST_2,
  CONST_3
};
```

CONST_1 se le asigna el valor 0 a menos que le pongamos un valor se puede dar rango de valores. 

Es como que cada variable al ser "enumerada" en vez de escribir el numero escribimos el nombre y el codigo cada vez que ve eso lo entiende como su numero correspondiente. 

ver [ejemplo3.c](./ejemplos/ejemplo3.c)
