# Introduccion a Punteros 

> Un puntero es una variable que guarda la dirreccion de memoria de otra variable 

``` c 

p -> Direccion memoria 
*P -> Valor en esa direccion 

``` 

## Ejemplo 

``` c 
int a = 5;
int * p = &a; // Se guarda la direccion de memoria en p 

printf("%d\n",p); // imprime la direccion de memoria 
printf("%d\n",&p); // Es la direccion de donde esta ubicado el puntero 
printf("%d\n",*p); // Valor en la direccion de memoria en este caso el valor de a 

```

## Punteros Fuertemente tipado 

``` c
// Guardan el tipo del valor que tienen
int * -> int 
char * -> char 

// Puntero generico 

void pointer -> solo imprime la direccion de memoria 
```

``` 
  Byte 3    Byte 2    Byte 1    Byte 0
┌────────┐┌────────┐┌────────┐┌────────┐
│00000000││00000000││00000100││00000001│
└────────┘└────────┘└────────┘└────────┘
    203       202       201       200     Direccion de ejemplo 
```

``` c

int a = 1025;
int *P = &a;

printf("%d\n",p);  // byte 0 dir 200
printf("%d\n",&p); // imprime 1025

```
> Ver los ejemplos 

## Punteros a Punteros 

``` 

┌────────────────────────────────────────┐
││││││││││││││││││││││││││││││││││││││││││ Memoria 
└────────────────────────────────────────┘
 |--| -> |--|  -> |--| -> |--|            Vars y punteros ( y donde apuntan) 
  S       Q        P       x 

```

``` c 
int x =5;
int *p = &x;
int **q = &p;
int ***s = &q;

```

Vos podes continuar al infinito y mas alla pero que significa cada uno de esos asteriscos.

Primero empecemos por "p" es un puntero que apunta a la direccion de memoria de x, por ende se puede acceder al valor y cambiar como tambien saber la direccion de memoria donde esta ubicado. Ahora "q" es un puntero que apunta a otro puntero por eso seria un doble puntero y tiene dos asteriscos ("\*") y como esta asignado la direccion de memoria del puntero p se puede acceder como veniamos haciendo con punteros a la direccion de memoria como tambien modificar el valor lo cual modificara el valor de "a". 
El ultimo es un triple puntero llamado "s" que es igual a los otros solo que este apunta a un puntero que esta apuntando a otro por eso los 3 asteriscos, teniendo todo lo mismo que los anteriores. 

## Llamado por referencia 

``` c 
void incremento( int *p){
  *p = (*p) +1;
}

incremento(&a);
``` 

> Ver ejemplos 

## Punteros y arrays 

``` c

int a[] = { 2,3,4,5,6};

int *p = &a[0] ; // Se puede escribir *p = a;

printf("%d\n",a); // Nos imprime la direccion de memoria de a 
printf("%d\n",*a); // Nos imprime el valor en el primer elemento de a 
printf("%d\n",a+1); // Nos imprime la siguiente direccion de memoria 
printf("%d\n",*(a+1)); // Muesta el valor en el siguiente elemento del array

```

Elemento en el indice i

* Direccion de memoria -> &a[i] o (a+i)
* Valor -> a[i] o \*(a+i)


