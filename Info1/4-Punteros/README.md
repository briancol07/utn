# Punteros 

(╯°□°）╯︵ ┻━┻ 

## Que es un puntero? 

> Es una variable que guarda el valor de la direccion de memoria de otra variable 

[youtube-Pointers](https://www.youtube.com/watch?v=zuegQmMdy8M)

## Overview

* [Apunte-Punteros.pdf](./Apunte-Punteros.pdf)
* [1-Pointers.md](./1-Pointers.md)
* [ejemplos](./ejemplos)
  * [allocating-memory.c](./ejemplos/allocating-memory.c)
  * [array-As-Function-Arguments.c](./ejemplos/array-As-Function-Arguments.c)
  * [call-by-reference.c](./ejemplos/call-by-reference.c)
  * [chars-Arrays-Pointers.c](./ejemplos/chars-Arrays-Pointers.c)
  * [chars-Arrays-Pointers2.c](./ejemplos/chars-Arrays-Pointers2.c)
  * [dynamic-Memory.c](./ejemplos/dynamic-Memory.c)
  * [ejemplo1.c](./ejemplos/ejemplo1.c)
  * [ejemplo2.c](./ejemplos/ejemplo2.c)
  * [ejemplo3.c](./ejemplos/ejemplo3.c)
  * [Function-pointer-Callbacks.c](./ejemplos/Function-pointer-Callbacks.c)
  * [function-Pointers.c](./ejemplos/function-Pointers.c)
  * [pointer-Arithmetic.c](./ejemplos/pointer-Arithmetic.c)
  * [pointer-Arrays.c](./ejemplos/pointer-Arrays.c)
  * [pointers-as-function-returns.c](./ejemplos/pointers-as-function-returns.c)
  * [pointers-multi-dim-arrays.c](./ejemplos/pointers-multi-dim-arrays.c)
  * [pointer-to-pointer.c](./ejemplos/pointer-to-pointer.c)


