#include<stdio.h>

int main(){

  int a = 10 ;
  int *p;
// se puede escribir tambien asi int *p = &a;
  p = &a;
  printf("a = %d\n",a);
  *p = 12;   // Dereferencia  
  printf("a = %d\n",a);

  int b = 20;
  *p = b; // Esto no cambiara a donde apunta sino que pondra el valor de b en a 

  return 0;
}
