#include<stdio.h>

int main(){

  int a = 1025;

  int *p = &a;

  printf("Size of integer is %d bytes \n",sizeof(int));
  printf("Address = %d, valor = %d \n",p,*p);
  printf("Address = %d, valor = %d \n",p+1,*(p+1);
  char *p0;
  p0 = (char*)p ; // Se hace un casteo/disfraz por que sino tiraria error por los tipos 

  
  printf("Size of char is %d bytes \n",sizeof(char));
  printf("Address = %d, valor = %d \n",p0,*p0);
  printf("Address = %d, valor = %d \n",p0+1,*(p0+1);

  // 1025 = 00000000 00000000 00000100 00000001

  void *p1;
  p1 = p;
  printf("Address = %d\n",p1);


  return 0;
}
