#include<stdio.h>

void print(char *c){ // Puede ser char c[]
  while(*c != '\0'){ // Puede ser tambien while(c[i] != '\0') o *(c+i)
    printf("%c",*c);
    c++;
  }
  printf("\n");
}

int main(){

  char c[20] = "hello";
  print(c);
}
