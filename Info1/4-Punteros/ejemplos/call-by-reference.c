#include<stdio.h>

void increment(int *p){
  *p = (*p) +1; 
}

int main (){
  int a;
  a = 10;
  printf("Antes del incremento a = %d\n",a);

  increment(&a); // Call by reference 

  printf("Despues del incremento a = %d\n",a);

  return 0;
}
