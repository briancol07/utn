#include<stdio.h>

void print(char *c){ // Puede ser char c[]
  while(*c != '\0'){ // Puede ser tambien while(c[i] != '\0') o *(c+i)
    printf("%c",*c);
    c++;
  }
  printf("\n");
}

int main(){
  // lo mismo que c[20] aca queda guardado en el espacio del array 

  char *c = "hello"; // queda guardado como una variable de compilacion 
  // En este caso no se puede modificar c 

  print(c);
}
