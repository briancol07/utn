// function pointers and callbacks 


#include<stdio.h>

void a(){
  printf("Hello");
}

void b(void (*ptr)()){ // una funcion puntero como argumento
  ptr(); // call-back al cual el puntero apunta 
}

int main(){

  void (*p)() = a; // a es una funcion callback 
  b(p);
}
