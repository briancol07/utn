#include<stdio.h>
#include<stdlib.h>

int add(int a , int b){
  printf("Direccion en add = %d\n",&a);
  int c = a+b;
  return c;
}

int add_ptr(int* a, int* b ){
  int c (*a) + (*b);

  printf("Direccion en add_ptr = %d\n",&a);
  printf("Direccion de a en add_ptr = %d\n",a);
  printf("valor en la direccion de a en add_ptr = %d\n",*a);
  return c;
  
}

// Funcion puntero 

int * ptr_add(int *a, int *b){
  // la forma de solucionarlo es 
  // int *c = (int*c)malloc(sizeof(int));
  // *c = (*a) + (*b);
  int c = (*a) + (*b);
  return &c;

}

void printHelloWorld(){
  printf("Hello world\n");
}


int main(){
  int x =2 , y =4;
  printf("Direccion en main= %d\n",&x);
  int z = add(x,y);   // el valor en x es copiado a a en la funcion
                      // el valor en y es copiado a b en la funcion 
  printf("Suma en add = %d \n",z); 

  z = add(&a,&b);
  // call by reference 
  printf("Suma en add_ptr = %d \n",z); 

  int *ptr = ptr_add(&a,&b); 
  // se pone hello world antes y se prueba
  printHelloWorld(); // rompe todo  cuando termina ptr_add se va las variables de la funcion y se pueden utilizar esas mismas direcciones deonde *ptr esta apuntando por eso imprimiria basura 
  printf("Suma en ptr_add = %d \n",*ptr); 


  return 0;
}

