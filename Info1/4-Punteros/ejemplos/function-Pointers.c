#include<stdio.h>

int add(int a, int b){
  return a+b;
}

int *func(int,int); // Esto seria una funcion que devuelve un puntero a un entero  

void printHello(){
  printf("Hello\n");
}

int main(){

  // puntero a la funcion toma (int,int) como argumento/ parametros y devuelve entero 
  int c;
  int (*p)(int,int); // si no ponemos los parentesis estamos devolviendo un puntero a un entero 
  p = &add; // no es necesrio que utilicemos el ampersand 
  c = (*p)(2,3); // de-referenciando y ejecutando la funcion 

  void (*ptr)();
   
  ptr =printHello;
  ptr();

  printf("%d",c);
  return 0;
}
