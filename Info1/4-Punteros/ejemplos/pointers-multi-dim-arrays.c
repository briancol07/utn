#include<stdio.h>

void func(int *a); // array de 1 dimension 
void func2(int a[][3]);
//void func3(int (*a)[2][2]); para usar con c

int main(){

  int c[3][2][2] = {{ {2,5}, {7,9}},
                    { {3,4}, {6,1}},
                    { {0,8}, {11,13}}};
  printf("%d %d %d %d",c,*c,c[0],&c[0][0]);
  printf("%d ",*(c[0][1] +1 ));

  int a [2] = {1,2};
  int b[2][3] = {{2,4,6},{5,7,8}};
  int x[2][4];

  func(a); // Devuelve int * 
  func2(x) // NO por que tiene que ser array de 3
  return 0;
}
