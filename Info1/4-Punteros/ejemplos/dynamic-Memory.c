#include<stdio.h>
#include<stdlib.h>

int main(){

  int a; // Va al stack 

  int *p;
  p = (int *)malloc(sizeof(int)); // allocation of memory 
  // Para guardar un espacio del heap (usarlo como espacio en memoria)
  *p = 10;
  free(p);
  p = (int*) malloc(sizeof(int)); // se agrega una nueva direccion 
  // apunta a esta nueva direccion y le pone un 20 ahi 
  *p = 20;
  free(p);
  p = (int*)malloc(20*sizeof(int)); // Reserva 20 enteros para p 
  // Se puede usar como p[0] ... p[19]

  // Los otros 10 quedan en memoria, consume memoria por eso luego de usarla debemos usar la funcion free para que no ocupe memoria 


  // Si malloc no encuentra espacio libre devuleve un null
  return 0;
}
