#include<stdio.h>

int main(){

  int x = 5; 
  int *p = &x;
  int **q = &p;
  int ***r = &q;

  printf("%d\n",*p); // 5 
  printf("%d\n",*q); // direccion de p 
  printf("%d\n",*(*q)); // 5 Nos da el valor 
  printf("%d\n",*(*r)); // direccion de q  
  printf("%d\n",*(*(*r))); // 5 

  ***r = 10; // cambia el valor de x 
  printf("x = %d\n",x);
  
  **q = *p +2 ; // Como *p es el valor de x entonces le sumaria 2 al valor de x 

  printf("x = %d\n",x);

  return 0;
}
