#Informatica 1 

## Temas 

- [ ] Diagramas de Flujo
- [ ] Tipos de datos 
- [ ] Variables 
- [ ] Ciclos Condicionales
- [ ] Ciclos Repetitivos 
- [ ] Estructura 
- [ ] Funciones
- [ ] Vectores 
- [ ] Punteros 
- [ ] Argumento linea de comando

## Archivos 

* [2-TP-Programacion](./2-TP-Programacion)
  * [README.md](./2-TP-Programacion/README.md)
* [3-Funciones](./3-Funciones) 
  * [README.md](./3-Funciones/README.md)
* [4-Punteros](./4-Punteros)
  * [README.md](./4-Punteros/README.md)
* [GuiaTP2010.pdf](./GuiaTP2010.pdf)



