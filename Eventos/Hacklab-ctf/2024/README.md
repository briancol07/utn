# Hack lab 2024

> Auspiciado por GISSIC

## Links 

* [Reglamento](https://gissic.frc.utn.edu.ar/downloads/hacklab-2024.pdf)
* [Software Seguro](https://www.softwareseguro.com.ar/) 

### Extra

* [CTF-cheatsheet](https://github.com/uppusaikiran/awesome-ctf-cheatsheet#awesome-ctf-cheatsheet-)

> No se puede hacer bruteforce, sino hay bloqueo
