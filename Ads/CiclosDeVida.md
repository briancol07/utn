Faltan agregar imagenes bn Z
# Ciclos de vida 

> Estados intermedios que pasa desde que nace hasta que muere 

* Software: 
  * Documentacion
  * Minutas 
  * Contratos 

* Estados 
  * Se define por las actividades que realiza 
  * Completar todas las actividades y pasar a la siguiente 

* Mapa Actividades:
  * Visualizacion 
    * Estados y actividades (Relacion) 

## Caracteristicas 

* Velocidad de desarollo
* Calidad del producto 
* Visibilidad interna (VI)
* Visibilidad Externa (VE) 
* Manejo Riesgos 
* Respuestas Cambios
* Costos 
* Experiencia del equipo 

## Code & Fix 

![image](img/CodeFix.png)

* Depende de  la gente 
* No sabes cuando termina (VI)
* Ve avances periodicos (VE) 
* ++ Respuesta cambios 

## Cascada Puro 

* Lento 
* No tiene manejo de riesgo 
* No contempla cambios 
* VI Alta 
* VE Baja ( Solo al final se ve ) 

### Iterativos 

> Superponer 

* 1 o mas estados Repetidos por un mismo equipo o ejecutadas en paralelo
* Definida al principio 
* Cascada iterativo 
  * hasta Disenio = Cascada puro 
  * + experiencia -VI

### Incrementales 

* Caso particular iterativo 
* Define diversas Entregas (DLC) que agregan valor 
* Desde la primer entrega es funcional 
* Cascada iterativo incremental
  * -Tiempo + VE 
  * + Rta Cambio 

### Eentrega calanderizada 

> Con un deadline, se entrega hasta donde se llego 

### Evolutivos 

> Al comienzo no se sabe cuantas iteraciones hay 

* A medida que avanza se definen nuevas iteraciones 
* Dan mas valor 

## Prototipado 

* Modelo 
* Funcionalidad atravez de lo visual 
* Evolutivo 
  * Arrastra error 
  * De la base para arriba (-calidad)
* Desechable
  * + calidad 
  * Boceto 
  * Desechable 
* Caracteristica 
  * +VE ---> luego baja
  * No Exp
  * + Manejo Cambio
  * Mal riesgo 

## Espiral 

* Orientado a Riesgos
  * Se evalua cada ciclo 
* Evolutivo 
* Cada ciclo una "cascada"
  * No sabes cuanto vas a ciclar 
* Caracteristicas 
  * -Velocidad
  * +VE 
  * Altisima Calidad 

----
## Riesgo  

* Probabilidad que ocurra algo malo 
* Elementos 
  * Probabilidad 
  * Impacto monetario
  * Exposicion 
    * Probabilidad x Impacto 
* Disminuir 
  * Plan 
    * Mitigacion : Disminuir Probabilidad
    * Contingencia : Reducir Impacto 

## Proceso Unificado (UP)

> Seguir metodologia  

* Herramientas : Como organizar 
* Modelo de proceso 
* Casos de uso 

----

## Metodologias Agil 

* Individuos e interaccion
* Software funcionando 
* Colaboracion Cliente 
* Respuesta al cambio 
* Cliente = Feedback 
* Autogestionado < 10 personas 
* Uso Prototipos 
* SW con valor y funcional 
  * Maximizar comunicacion 
  * Maximizar visibilidad
  * Mejorar Funcionamiento equipo 

### Scrum 

> Framework 

* Roles 
  * Product Owner 
    * Maximizar valor producto 
  * Scrum Master
    * Conocer y comunicar 
  * Scrum Team 
* Tareas 
  * Reuniones diarias 
  * Desarrollo basado en Test ( TDD )
    * Tambien en aceptados por usuario (ATDD) 
  * Integracion continua 
  * Peer Programing 
  * Revisiones codigo y disenio 
* Desventajas 
  * Pobre estimacion Largo plazo 
  * Dificil equipos grandes 
  * Necesita Exp 
  * El cliente debe saber lo que quiere / necesita 
  * Muchas Reuniones 

### Proceso Scrum 

1. Planificacion (Release)
    * llega primera vez pedido 
2. Backlog (Producto) 
    * Establecen requisitos  
3. Sprint (Planificacion) 
    * Como / que haran  
4. Sprint Backlog 
    * Seleccionar Principales 
5. Sprint 
    * Se hacen daily stand up
      * Que hizo 
      * Que se hara 
      * Impedimentos?
6. Revision Sprint 
    * Antes de dar por terminado el  prototipo 
7. Producto inestable
8. Retrospectiva Sprint 
    * Ver proceso y aprender 

### Resumen 

* Agiles 
  * Equipo Motivado y calificado 
  * Proyecto nuevo 
  * Cliente Involucrado 
  * Colaboracion Todos 
* No agiles
  * Mucha documentacion 
  * Equipo poca gente con experiencia 
  * Cliente limitada participacion 
