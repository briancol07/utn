# Planificacion 

> Preveer el futuro <br>
No se puede gestionar lo que no se puede medir 
## Objetivo 

* Cuantitativo 
* Cualitativo 
* No debe variar 

## A donde quiero llegar ? 

* Especifico 
  * Definir con *Claridad* 
    * Que 
    * Como 
    * Cuando 
    * Donde 
* Medible 
  * Cuantitativo 
  * Determinar resultados o beneficios
* Realizable 
  * Posible  
    * Tiempo 
    * Recursos 
* Realista 
  * Cultura
  * Politicas 
  * Etica
  * Legales 
* Limitado en Tiempo 
  * Duracion Optima 

## Recursos a invertir 

* Materiales 
* Humanos 
* Dependencia de tareas 
  * Division 

## Control 

> Se analiza situacion actual y se compara con teoria , detectar desvios y cumplimiento de subobjetivos 

## Desvio 

> Situacion que no cumple con lo planeado 

* Tipos 
  * Bueno 
  * Malo 
* Determinar Causa 
  * Evitar que se repita y saber como solucionarlo
* Documentar 
* Corregir 
* Minimizar impacto y replanificar

## Gantt

> Planificacion y control 

* Documentar 
* Grafico barras 
* Calendario 
  * Unidades de tiempo (D/M/Y)   

Graficamente se ve como ocurren las tareas y por el porcentaje de cada una 
tiempo muerto registrarlo 
Tablero magnetico 

## Crithical Path Method (CPM) 

* Tecnica de planificacion 
  * Deterministica 
  * A traves de informacion historica 

Si o si un camino critico 

## Program Evaluation and Review Technique (PERT)

> Muy parecido a cpm , pero se basa en el calculo de las tareas y no basado en un historico 

* Tecnica de planificacion 
  * Probabilistica 
  * Plazos cortos (small scale) 
  * Nivel Gerencial (bajo/operativo) 

$` Duracion = (DuracionOptimista + 4 . DuracionNormal + DuracionPesimista) / 6 `$

## Diagramas 

![img](Pert&Cpm.png)

hay tareas ficticias se representan con una linea punteada , no consumen tiempo ni costo 

## Calculos 

* Para las Fechas Tempranas 
  * Sigo las flechas 
  * Tomo el mayor valor 
  * Empiezo del menor nodo 
* Para las Fechas Tardias   
  * Anti sentido flechas 
  * Tomo menor valor 
  * Empiezo del ultimo nodo 

## Margen Total (MT) 

> Si es 0 es una tarea critica , si se atrasa , todo el proyecto se atrasa 

$` MT = FechaFinTardia - Duracion - FechaInicioTemprana`$ 

## Intervalo de flotamiento (IF) 

> Cuanto se podia atrasar , pero es de un nodo 

$` IF = FechaTardia - Fecha temprana `$ 

##  Pert y Cpm 

* Ambos tienen :
  * Unico nodo inicial 
  * Unico nodo Final
  * Al menos un camino critico 
  * Facilidad para ver dependencias 
  * Facil deteccion de camino criticos por ende actividades criticas 
  * Facil de determinar tiempo muerto 


