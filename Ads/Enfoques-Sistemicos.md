# Enfoques Sistemicos 

* General a Particular 
  * Bajando por niveles (Progresivos)
  * Division en Subsistemas 
    * Como se relacionan entre ellos 

## Metodo Prueba y Error 

* Correctiva 
  * Proceso 
  * Entrada 

### Retroalimentacion 

![img](img/prueba.png)

## Aspectos a Evaluar 

> Eficiencia != Eficacia 

* *Eficiencia* 
  * Manera mas optima 
  * Hw y SW 
* *Productividad* 
  * Mas codigo en menor tiempo 
* *Portabilidad* 
  * Crossplatform 
  * Plataforma Tecnologica 
* *Mantenibilidad* 
  * Uso Documentacion 
    * Errores 
    * Sustentabilidad 
* *Documentacion* 
    * Calidad Sistema 
      * Satisfaccion (user) 
      * Costo Mantenimiento 
      * Grado Documentacion 

## Metodologia 

> Marco de referencia ( ninguna etapa se cambia de orden ) 

* Lenguaje 
  * Tabla decision 
  * Cursograma 
* Estandar 
  * Para que todos Entiendan 

### Tipos de Enfoque 

* Bottom-Up 
  * Particular a General 
* Top-Down  
  * General a Particular 

## Comparacion ( metodologias ) 

### Tradicional 

* *Bottom up*
* Cascada 
* Reciclar ---> Mas Tiempos 
* Impaciencia usuario 
* Limitaciones operativas 
* Inconvenientes Documentar 

![img](img/metod.png)

* Aspectos a mejorar 
  * Mayor interaccion con el usuario 
  * Mejor captura Requerimientos (Refinarlos) 
  * Mejora Calidad SW 
    * Relacion a tiempos 
    * Optimizar mantenimiento 

### Estructurado 

> Logico (que) ---- Fisico (como) 

* Modelizar 
  * Abstraccion 
    * Proceso mental 
    * Simplificar realidad
    * No limitaciones tecnologicas 
* Componentes : (Encapsulamiento)
  * Datos 
  * Procesos
* Enfasis 
  * Procesos
  * Refinar rq 
* Caracteristicas 
  * Favorece Documentacion
  * Top Down
* Herramientas 
  * DFD 
  * DD
  * DER  

## Requerimiento 

> Necesidades : lo que el sistema tiene que hacer 

## Requisitos 

> Como lo tiene que hacer , entorno para que el requerimiento funcione

## ABMC ( CRUD)

* Alta
* Baja 
* Modificacion 
* Consulta 

## Usuario 

* Mantener politica de usuario 
* Mejora calidad SW 
* Tipos:
  * Sabe lo que quiere 
  * No sabe hasta que lo ve 
    * Por estos prototipos 

## Analisis y Disenio orientado a objetos ( AOO )

* Metodo
  * Uml : unified modeling language 
  * Union entre modelo fisico con logico 
* Componentes 
  * Datos + Procesos
* Enfasis 
  * Datos ---> Encapsulamiento 
  * Refinar Rq
* Caracteristicas 
  * Robustece Documentacion
  * Bottom up 
