# Tabla de Decision 

## Decision 

> Evaluar alternativas y seleccionar una de ellas por medio del analisis 

* Pueden ser:
  * Programadas 
  * No Programadas 

## Tabla 

* Tecnica de Modelado (Analisis Estructurado)
  * Representacion 
  * Documentacion
  * No relevamiento  
* Decisiones
  * **Programadas** 
* Clara y precisa
  * Reducir ambiguedad 
  * Automatizar la toma de decisiones
  * Comunicar Decisiones complejas 
  * Identificar Aspectos no relevados 

### Creacion 

* Forma:
  * Intuitivo 
  * Masivo 

1. Identificar Condiciones 
    * Situaciones que pueden ocurrir 
2. Identificar Acciones  
    * Comportamientos esperados, en funcion de condiciones
3. Determinar Reglas 
    * Combinacion de condiciones que toman valores y generan acciones 
4. Asignacion de las acciones a las reglas 
5. Depuracion Tablas 
    * Eliminar Casos imposibles 
    * Eliminar inconsistencias 
      * Combinaciones que no se pueden dar en la realidad
    * Eliminar Redundancias 
      * Fusionar reglas, con mismas acciones y mismos valores de condiciones menos 1 

* Tener en Cuenta:
  * Controlar que no se omitio ninguna 
  * Solucionar ambiguedades 
  * Tabla completa sin depurar 
  * Tabla compacta todas depuradas 

### Tipos 

* Binaria 
  * Valores de condiciones : si o no 
  * Cantidad de reglas 
    * $` 2^N `$ 
    * N cantidad de valores 
* Extendida 
  * Condiciones con diferentes valores 
  * Cantidad de Reglas : 
    * $` N_{1} . N_{2} ... N_{m}`$
    * $`N_{x}`$ cantidad de valores posibles para condicion 
* Concatenadas 
  * Jerarquia
    * Abierta 
    * Cerrada 
  * Suma de tablas 
    * Cosas no tan relacionada entre ellas 

### Otras Tecnicas 

* Arbol decision
  * Binario 
  * Extendido  
