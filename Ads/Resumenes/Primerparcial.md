# Resumen Primer parcial 

## Informacion 

> Dato procesado que guarda relacion 

* Clara 
* Precisa
* Completa 
* Oportuna
* Confiable 

![img](img/info1.png)

## Etapa de la metodologia de sistemas 

> Conjunto de pasos en un orden que permiten el logro de un objetivo (se pueden omitir)

Metodologia ---> lelva a cabo un ciclo de vida 

* Pueden 
  * Saltearse
  * Solaparse 
  * Pero no modificarse orden 

## Ciclo de vida 

> El sistema debe pasar por esas etapas 

* Tiene que ser:
  * Logico 
  * Invariable
  * Inevitable 

## Fracaso de sistemas 

* Por 
  * Usuario 
    * Estudio Preeliminar 
  * Contexto 
    * Estudio de Factibilidad 
  * Objetivos 

## Estudio Preeliminar 

> Es el primer acercamiento , primeros contactos con la empresa <br>
> Consiste en la primera aproximacion a la organizacion, al problema y al sistema de informacion actual . Conocimiento inicial identificar problemas (Area donde se origino) 

* Elementos a Considerar 
  * Balances
  * Organigrama --> De estatutos 
  * Manual de procedimientos 
* Acercamiento Usuario 
  * Incorporarlo 
  * Politica de usuario 
  * Aporta recursos 
    * agilisa Proyecoto 
* Tecnicas 
  * No invasivas 
    * No tienen interaccion con el usuario 
    * Observacion personal 
    * Mediciones de tiempos 
  * Invasivas 
    * Entrevistas 
    * Cuestionarios 
      * Niveles bajos 
    
### Entrevistas 

* Nivel Gerencial / Directivo 
* Tipo
  * Estructuradas 
  * No estructuradas 
* Definir 
  * Duracion
  * Lugar 
  * Horario 
* Modalidad 
  * Cerradas y luego abiertas $`\triangle`$
  * Abiertas y luego cerradas $`\nabla`$
  * Ambas 
* Elementos 
  * Grabacion 
  * Filmacion 
  * Escritura    

## Relevamiento 

> Adquirir conocimiento detallado del area-problema + informacion (Realizar un modelo realidad)

### Tecnicas 

* Entrevistas 
* Factores criticos de exito 
* Disenio de aplicacion conjunta 
* Tecnicas de analisis y disenio Estructurado 
* Cuestionario 

## Estudio de Factibilidad 

> Analisis de condiciones adecuadas para poner en marcha 

### Tecnico 
 
Variables no controlables pero se deben considerar debido a que impactan de forma directa (influir o afectar) 

### Economico 

Se mide tasa de retorno de inversion 

$` TRI = \frac{InversionEstimada}{UtilidadEstimada}`$

* Utilidad 
  * Cuanto recupero por anio 

### Operativo 

Variables internas ( Si es posible llevarlo a caso) 
* HW 
* BBDD 
* SO 
* RRHH

## Analisis 

> Una vez elegida la alternativa se comienza a conceptualizar *Modelo Solucion* 

## Disenio 

> Formalizar , *Hacer Tangible* 

## Implementacion 

* Ponerlo en marcha 
  * Directa 
  * Paralelo 
  * Fases 

## Mantenimiento 

* Correctivo 
  * Reparacion bugs
* Perfectivo 
  * Nuevos Requerimientos
* Adaptativos 
  * Externas 
  * Ej: afip 

## Retiro o Sustitucion 

> Salida , baja o cancelacion , busqueda reempla  

## Planificacion 

> Tecnica para prever acciones futuras para lograr un obj.

* Saber Objetivo 
* Recursos 
  * Calidad 
  * Cantidad 
* Tiempo 
* Presupuesto 
* Tareas 
  * Duracion 
  * Orden 
  * Programar 
* Controles 

### Objetivos 

* Especifico 
  * Bien definido 
* Medible 
  * Determinar forma cuantificada los beneficios y resultados 
* Realizable 
  * Lograr obj en tiempo y forma 
  * Teniendo en cuenta Recursos y capacidades 
* Realista 
  * Factores 
    * Culturales 
    * Legales 
    * Eticos 
* Limitado en tiempo
  * Duracion optima del proyecto 

## Tecnicas de Planificacion

### Gantt 

* Tareas + Fechas ==> Calendario 
* Seguir viendo plan (Avances tareas),Porcentaje de cumplimiento.
* Gerencial Alto 

### CPM 

* Deterministica 
  * Duracion tarea atraves informacion historica
* Ver Dependencia de tareas 
* Gerencial Bajo

### PERT 
* Probabilistica
* $` Duracion = \frac{duracionoptima + 4.DuracionNormal + DuracionPesimista}{6}
* Gerencial Bajo

### Definiciones (CPM / PERT) 

1. Margen total (MT)  
  * Cuanto se puede retrasar una tarea 
2. Intervalo de Flotamiento (IF) 
  * Indica si un nodo es critico o no 
  * $`IF = 0`$
3. Tarea
  * Arco con sentido , une dos nodos 
  * Ficticia 
    * Duracion 0 
    * Indicar dependencia (Ni tiempo , Ni Costos)
    * Se hace con una linea punteada 
  * Critica 
    * MT = 0 
    * Tarea que si se atrasa , atrasa todo el proyecto 
4. Camino Critico 
  * Camino de tareas Criticas , Nodo inicial y Termina nodo Final.
  * Define la duracion total 
  * Almenos 1 camino critico 
  * Si todos son criticos es un proyecto rigido 
5. Fechas
  * Temprana 
  * Tardia 
   
## Control 

* Momento de supervision
* Comparar Estado Real con Planificado 
* Identificar Desvios a corregir
  * Conocer causas 
  * Replanificar 
* Momentos Necesarios 
  * Costos 
  * Persona qe planifica distinta a la que controla 

## Tabla de Decision 

> Tecnica De modelado para representacion y documentacion 

Evaluar Distintas alternativas o curso de accion , Seleccionar 1 por medio analisis 

* Tipos de Decision  
  * Programadas 
  * No Programadas 
* Tipos de Tablas 
  * Completa 
  * Compacta 
* Uso 
  * Reducir ambiguedades 
  * Validar fuentes Relevamiento 
  * Identificar aspecto no relevados 
  * Automatizar toma de decision 
  * Comunicar decision Complejas 
* Determinar Reglas 
  * $` 2^{n}`$
  * N condiciones (situaciones) 

### Depurar  

* Eliminar Casos imposibles 
* Inconsistencias 
  * Combinacion de condiciones que no se pueden dar
* Redundancias 
  * Fusionar Reglas que cumplen Mismas Acciones y/o mismos valores menos 1 

## Clasificacion Usuario  YOURDON 

* Operacionales 
  * Contacto Directo con el sistema 
* Supervisores 
  * Conocen Trabajo Operacionales
* Ejecutivo 
  * No se involucran con el Sistema 

## Por nivel de Experiencia 

* Amateur
  * No sabe usar el sistema implementado
* Novato 
  * Considera que sabe al tener alguna Experiencia en el campo
* Experto 

## Determinacion

* Usuario que no sabe lo que quieren 
* Otros que no saben lo que quieren hasta que lo ven 
* El que si sabe lo que quiere 

## Metodologia de Resolucion de Problemas (MRP)

> Metodo que intenta entender el comportamiento causas y efectos para la resolucion de problemas 

1. Objetivo 
  * Cuantificable
  * No cuantificable 
2. Alcance 
  * Desde donde hasta donde comprende 
  * Variables controlables (Endogenas) 
3. Resultado 
  * Materializacion del proceso 
  * Intervienen aspectos estables 
4. Alimentacion (Retroalimentacion)
  * Elementos de caracter variable que intervienen
5. Procedimiento 
  * Resultado =  Aspectos estables + Alimentacion

## Cadenas causa efecto 

* Lineales 
  * 1 causa 1 efecto 
* Bifurcadas 
  * 1 causa y mas de 1  efecto 
* Redes 
  * 1 efecto mas de 1 causa

## Ciclos de vida 

> Estados intermedios que atraviesa un objeto desde que nace hasta que muere 

Cada estapa , tiene un producto que nos indica que se termino esa etapa 

Factores | Descripcion
---------|------------
Velocidad de desarrollo |
Calidad Producto | 
Visibilidad Interna (VI) | Estado proyecto (VI) , que se conosca que sucede y que va a pasar 
Visibilidad Externa (VE) | Usuario/cliente pueda ver avance del proyecto 
Manejo de riesgo | Posibilidad que algo se desvie de la planificacion para llegar obj
Respuesta a cambios | Adaptacion 
Costos | 
Experiencia de proyecto 

### Prueba y error 

> Corto plazo + Prototipado , Entregas sucesivas y retroalimentacion

![img](img/PruebaError1.png)

Factor|Tipo 
------|----
Velocidad| Alta
Calidad | Mala
VI   | Nula
VE | Alta 
Riesgo | NO 
Exp.Lider| Nada
