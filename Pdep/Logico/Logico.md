# Paradigma Logico 

> La idea es hacer consultas de manera logica , aunque puede cambiar , todo esto en nuestra base de conocimiento . Esto se hace con las reglas de inferencia 
> 
> Programar deduciendo osea para deducir las posibilidades que puede haber 

* Echos : Afirmacion que representa un dato
* Reglas : Construcciones a=> q  
* Consultas: hacemos referencia a los predicados(como usamos nuestro programa) 
> Los predicados solo devuelven verdadero o falso,nunca numeros .

las variables empiezan con mayuscula , los echos hay que ponerlos todos juntos como las reglas , si los mezclamos tiran error 

> Siempre ver de donde saca la informacion 
``` prolog
% Esto es un echo  
estacion(argentina , otonio).
% Otro echo
quedaEn(laMatanza,Argentina).

% Reglas 

EstacionEnCuidad(Cuidad,Estacion):- 
	quedaEn(Cuidad,Pais),
	estacion(Pais,Estacion).

```
> la coma es como la "Y" logica 
> 
> Para hacer la "o" logica lo escribo en distintas reglas al predicado 

# inversibilidad 

> Cuando sirve tanto preguntar para verificar y para preguntar quien lo hace verificar 

> El flujo del paradigma que me lo hace verificar 

> Aveces tiene que tener un orden en especifico , tiene que estar correctamente instanciado 

> predicado **is** es como un igual , para resultados si es inversible pero para los operadores no 

* Totalmente Inversible 
	* Pasar variables sin instanciar en todos argumentos al mismo tiempo y generar soluciones que tengan sentido 
* Parcialmente inversible 
	* Podemos pasar variables sin instanciar aalguno de sus parametros y esperar solucionesque tengan sentido 
* No inversibles 
	* no pasar variables sin instanciar 

> Lo que te limita son las inversibilidad de los predicados 

``` prolog
precioPorKilo(Producto , Importe):-
	precio(Producto,Precio,Unidades),
	importe is Precio / Unidades.
```

``` prolog 
partido(islandia,5,coacia,3). 

hacenMuchosGoles(Continente):-
	continente(Pais,Continente),
	goles(Pais,Cantidad),
	cantidad > 2. 

goles(Pais,Goles):-
	partido(Pais,Goles,_,_).
goles(Pais,Goles):-
	partido(_,_,Pais,Goles).


fueGoleada(E1,E2):-
  partido(E1,G1,E2,G2),
  diferencia(G1,G2,Dif),
  Dif > 3.

diferencia(Nro1,Nro2,Dif):-
	Dif is Nro1 - Nro2.

diferencia(Nro1,Nro2,Dif):-
	Dif is Nro2 - Nro1. 
```

## not 
> Antes del not tiene que haber un predicado asi es inversible 
> Argumento del predicado es otro predicado 
``` prolog
% not ( predicado)

```
El not solo no es inversible 
El not solo no es inversible 

> Pattern matching : ligar lo que se puede ligar 


## Forall

> Para todo se tiene que cumplir la condicion !
> Todas las variables libres estan cuantificadas existencialmente , forall las cuantifica universalmente 
> 
> Todo lo que pasa en el primero tiene que pasar en el segundo 


> La primer consulta del for all tiene que tenes una variable libre , mayormente siempre es 1 . 
> 
> Las variables libres no tiene un valor anteriormente !! 
``` prolog
	forall(quiereIr(Persona,Destino), quedaEn(Destino,Zona))
```

$`\forall`$ Destino : quiereIr(Persona,Destino) $`\Rightarrow`$ quedaEn(Destino,Zona)

Aveces en hay que hacer una existencia antes del forall , 

``` prolog
% Con viveEn(_,Zona), nos aseguramos que tiene que existir que alguien viva ahi !! 
dificilDeEstacionar(Zona):-
	viveEn(_,Zona),
	forall(viveEn(Persona,Zona), tieneAuto(Persona)).

```

## Functores 

> Son como estructuras en el estructurado 

se pueden pasar functores como parametros  a los predicados ! .

``` Prolog
	personajeAdulto(Nomb):-
		personaje(Nomb,P),
		esAdulto(P).

	esAdulto(stark(E,_)):- E > 15 .

	esAdulto(lannister(E,_)):- E > 15 .
```
## Between 

``` prolog
% Nos dice si esta entre esos valores 
between(min,max,value).
```

## Findall  
> Es encontra todos los individuos que cumplan con tal consulta y guardamelos en un conjunto 

``` prolog
%findall(Individuo,Consulta,Conjunto).
	empleadosNecesitado(List):-
		findall(Nomb,Necesita(Nomb,_),List).

```
las consultas pueden ser de mas de uno y solo hay que ponerlo entre ().
aveces para que sea completamente inversible hay que darle existencia antes , osea nombrando el predicado 

## Is 

> Evitar == y is cuando no resolver cuentas , tambpoco utilizarlo para verificar 


* Relaciona numero con la cuenta ( Pepe is nafta * 3)
* algunos simbolos para las comparar cuentas ( < > => <=) 


