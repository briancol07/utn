# Paradigma Funcional 

> Este paradigma se basa en un conjunto de funciones (relaciones que cumplen las propiedades de unicidad y existencia), pueden ser evaluadas para obtener un resultado. El paradigma funcional eta basado en conceptos que vienen de la matematica. 

* Conceptos generales 
  * Concepto Funcion
  * Aplicacion
  * Pattern Matching en Haskell
  * Composicion
  * Aplicacion Parcial 
  * Orden Superior
  * Currificacion
  * Inmutabilidad 
  * Funciones por Partes 
  * Lazy Evaluation 
  * Recursividad Haskel 
* Tipos 
  * de Haskell
  * Typeclasses 
  * Inferencia de tipos 
  * Polimorfismo paramtrico y ad-hoc
  * Notacon point-Free
  * Data: definiendo nuestros tipos en Haskell
* Extras 
  * Definiciones Locales 
  * Expresiones Lambda 
  * Listas por comprension 
 
## Links 

* [Haskell for Dilettantes](https://www.youtube.com/watch?v=nlTJU8wLo7E)
  
  
  
  
  
  
