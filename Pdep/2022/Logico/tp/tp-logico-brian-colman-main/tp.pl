% TP 
% Base de conocimiento 

persona(bakunin).
persona(ravachol).
persona(emmaGoldman).
persona(judithButler).
persona(elisaBachofen).

% trabajaEn 
trabajaEn(bakunin,aviacionMilitar).
trabajaEn(ravachol,inteligenciaMilitar).
trabajaEn(emmaGoldman,profesoraJudo).
trabajaEn(emmaGoldman,cineasta).
trabajaEn(judithButler,profesoraJudo).
trabajaEn(judithButler,inteligenciaMilitar).
trabajaEn(elisaBachofen,ingenieriaMecanica).

% esBueno

esBueno(bakunin,conduciendoAutos).
esBueno(ravachol,tiroAlBlanco).
esBueno(rosaDubovsky,construirPuentes).
esBueno(rosaDubovsky,mirarPepaPig).
esBueno(emmaGoldman,Tarea):-
    esBueno(judithButler,Tarea).
esBueno(elisaBachofen,Tarea):-
    esBueno(judithButler,Tarea).

esBueno(judithButler,judo).
esBueno(elisaBachofen,armandoBombas).
esBueno(juanSuriano,armandoBombas).
esBueno(juanSuriano,judo).

% historial 

historial(bakunin,roboAeronabes).
historial(bakunin,fraude).
historial(bakunin,tenenciaCafeina).
historial(ravachol,fraude).
historial(ravachol,falsificacionVacunas).
historial(judithButler,falsificacionDeCheques).
historial(judithButler,fraude).
historial(juanSuriano,fraude).
historial(juanSuriano,falsificacionDeDinero).

% leGusta

leGusta(ravachol,juegosDeAzar).
leGusta(ravachol,ajedrez).
leGusta(ravachol,tiroAlBlanco).
leGusta(rosaDubovsky,construirPuentes).
leGusta(rosaDubovsky,mirarPepaPig).
leGusta(rosaDubovsky,fisicaCuantica).
leGusta(elisaBachofen,Gusto):-
    leGusta(judithButler,Gusto).

leGusta(judithButler,judo).
leGusta(judithButler,carreraAutomovilismo).

leGusta(elisaBachofen,fuego).
leGusta(elisaBachofen,destruccion).
leGusta(juanSuriano,judo).
leGusta(juanSuriano,armandoBombas).
leGusta(juanSuriano,ring-raje).

% Punto dos 

viveEn(laSeverino,bakunin).
viveEn(laSeverino,elisaBachofen).
viveEn(laSeverino,rosaDubovsky).

viveEn(comisaria,ravachol).

viveEn(laCasaDePapel,emmaGoldman).
viveEn(laCasaDePapel,juanSuriano).
viveEn(laCasaDePapel,judithButler).
% Punto 6
viveEn(casaDePatricia,sebastienFaure).

% Caracteristicas de la casa

laCasaTiene(laSeverino,[cuarto(4,8),tunel(8,false),tunel(5,false),tunel(1,true),pasadizo]).
laCasaTiene(laCasaDePapel,[pasadizo,pasadizo,cuarto(5,3),cuarto(4,7),tunel(9,false),tunel(2,false)]).
laCasaTiene(casaDelSolNaciente,[pasadizo,tunel(3,true)]).
laCasaTiene(comisaria,[]).
% Punto 6
laCasaTiene(casaDePatricia,[pasadizo,bunker(10,2)]).


% Punto 3 

viviendaConPotencial(Vivienda):-
    viveEn(Vivienda,Persona),
    desidiente(Persona),
    cumpleCondiciones(Vivienda,Metros),
    Metros >= 50.

cumpleCondiciones(Vivienda,MetrosTotales):-
    laCasaTiene(Vivienda,Cuartos),
    findall(Superficie,(member(Cuarto, Cuartos),superficie(Cuarto,Superficie)), MetrosCuadrados),
    sumlist(MetrosCuadrados, MetrosTotales).
    
superficie(cuarto(Largo,Ancho),Sup):- Sup is Largo * Ancho.
superficie(tunel(Metros,false),Sup):- Sup is 2 * Metros.
%superficie(tunel(M,true),0).
superficie(pasadizo,1).

% punto 6

superficie(bunker(Largo,Ancho),Sup):- Sup is Largo + Ancho.

% 4 NO vive nadie 
noViveNadie(Lugar):-
    not(viveEn(Lugar,_)).

tieneGustoEnComun(Vivienda):-
    viveEn(Vivienda,Persona),
    leGusta(Persona,Gusto),
    forall(viveEn(Vivienda,OtraPersona),(leGusta(OtraPersona,Gusto), Persona \= OtraPersona)).
    %forall((viveEn(Vivienda,OtraPersona),  Persona \= OtraPersona),leGusta(OtraPersona,Gusto)).

%desidientes 
%esBueno
esTerrorista(Persona):-
    esBueno(Persona,Accion),
    member(Accion, [armarBombas, mirarPeppaPig, tiroAlBlanco]).
    
noGustos(Persona):-
    not(leGusta(Persona,_)).

tieneHistorial(Persona):-
    persona(Persona),
    historial(Persona,Hist1),
    historial(Persona,Hist2),
    Hist1 \= Hist2.
    %historial(Persona,_).

viveConHistoria(Persona):-
    viveEn(Vivienda,Persona),
    viveEn(Vivienda,Otra),
    tieneHistorial(Otra),
    Persona \= Otra.

desidiente(Persona):-
    esTerrorista(Persona),
    gustosPosibleTerrorista(Persona),
    historiaPosibleTerrorista(Persona).

gustosPosibleTerrorista(Persona):-
    noGustos(Persona).
gustosPosibleTerrorista(Persona):-
    forall(esBueno(Persona,Algo), leGusta(Persona,Algo)).

historiaPosibleTerrorista(Persona):-
    viveConHistoria(Persona).

historiaPosibleTerrorista(Persona):-
    tieneHistorial(Persona).
