## Clase 2 

> 28-6-22

## Base de conocimiento 

```prolog 

programaEn(ana,haskell).
programaEn(ana,prolog).
programaEn(pedro,scala).
programaEn(pedro,haskell).

```

Hacer que todas las personas programen en c 

### No inversible 

```prolog

programaEn( _ , c).

```
No se puede saber quienes cumple, dira true ya que todos cumplirian. 

### Inversible 

```prolog 

programaEn(Persona,c):- 
  persona(Persona).

persona(ana).
persona(pedro).
```

Se agregan personas no necesariamente codean en otros lenguajes.

## Siguiente 

```prolog 

% No inversible 
siguiente(numero,siguiente):- 
  siguiente is numero + 1.

? siguiente(5,Sig).
sig = 6

? siguiente(9,10).
True 

? siguiente(Num,10).
% Error: Los argumentos no son suficientes (instanciados)

```

### Necesito Generadores 

numero(1) } Extension --> Definir cada uno de los numero 

``` prolog 
% Regla

numero(Numero):-
  between(1,10,Numero).

```
### Inversible 

``` prolog

siguiente (Numero, Siguiente):- 
  numero(Numero) , 
  Siguiente is Numero + 1 .

siguiente(anterior,10) % --> Primero entra al generador ve si cumple, si no no sigue 
```

## Mayor 

``` prolog 

esMayor(Menor,Mayor):- 
  Menor < Mayor. 

? esMayor(3,8). -> True 
? esMayor(9,5). -> False 
? esMayor(8,Mayor). --> Indeterminacion ( Error) 

% Inversible y mejorado
esMayor(Num1,Num2):-
  numero(Num1), 
  numero(Num2),
  Num1 < Num2.

? esMayor(8,Mayor).
Mayor = 9 
Mayor = 10 
```

## Bueno 

``` prolog 


bueno(juan).
bueno(ana).
bueno(pedro).
bueno(laura).

malo(Persona):-
  not(Bueno(Persona)). % NOT -> no inversible 

Persona(Persona) :-
  bueno(Persona)

persona(Emilio).
persona(Adrian).

% Inversible 

malo(Persona):-
  persona(Persona), 
  not(Bueno(Persona)).
```

## Quiere 

``` prolog 


quiere(ana,wifi).
quiere(ana,teatro).
quiere(ana,playa).
quiere(pedro,sierra).
quiere(pedro,playa).

tiene(marDelPlata, playa).
tiene(marDelPlata, teatro).
tiene(marDelPlata, casino).
tiene(marDelPlata, wifi).
tiene(tandil, sierra).
tiene(tandil, teatro).

puedeVeranear(Persona,Lugar):-
  forall( quiere(Persona, Algo), tiene(Lugar,Algo)).

% Con los generadores para hacerlo inversible  (Persona , Lugar)

puedeVeranear(Persona,Lugar):-
  persona(Persona),
  lugar(Lugar),
  forall( quiere(Persona, Algo), tiene(Lugar,Algo)).

persona(Persona):-
  quiere(Persona,_).
lugar(Lugar):-
  tiene(Lugar,_).
```

## SabeProgramar 

``` prolog 

sabeProgramarEn(Persona,Lenguaje):-
  persona(Persona),
  findall(UnLenguaje, ProgramaEn(Persona,Unlenguaje), Lenguajes).

persona(Persona):-
  programaEn(Persona,_).
```

Ver ejercitacion semana 2 





