creeEn(gabriel,campanita).
creeEn(gabriel,elMagoDeOz).
creeEn(gabriel,cavenaghi).
creeEn(juan, conejoDePascua).
creeEn(macarena,reyesMagos).
creeEn(macarena,campanita).
creeEn(macarena,elMagoCapria).

% quiere(persona,suenio). 
% Usamos esta forma para que el suenio sea el functor 

quiere(gabriel,ganarLoteria([5,9])).
quiere(gabriel, futbolista(arsenal)). 
quiere(juan,cantantate(100000)).
quiere(macarena,cantantate(10000)).

persona(Persona):-
    creeEn(Persona,_).

esAmbicioso(Persona):-
    sumarDificultades(Persona,Total),
    Total > 20. 

% La persona la puedo agregar antes de sumarDificultades asi queda para usarla despues

sumarDificultades(Persona,Cantidad):-
    persona(Persona),
    findall(Dificultad, dificultadSuenio(Persona,Dificultad), ListaDificultades),
    sum_list(ListaDificultades, Cantidad).

dificultadSuenio(Persona,Dificultad):-
    quiere(Persona, suenio),
    dificultad(suenio,Dificultad).

dificultad(ganarLoteria(Numero), Dificultad):-
    length(Numero,Cantidad),
    Dificultad is 10 * Cantidad.

dificultad(cantantate(CantDiscos), 6):-
    CantDiscos > 500000.
dificultad(cantantate(CantDiscos), 4):-
    CantDiscos =< 500000.

dificultad(futbolista(Equipo),3):-
    chico(Equipo).
dificultad(futbolista(Equipo),16):-
    not(chico(Equipo)).

chico(arsenal).
chico(aldosivil). 

% Punto 3 
 % mientras este unificado se puede poner 
    % Personaje \= campanita


tieneQuimica(Persona, Personaje):-
    creeEn(Persona,Personaje),
    cumpleCondiciones(Persona,Personaje).

cumpleCondiciones(Persona,campanita):-
    quiere(Persona,Suenio),
    dificultadSuenio(suenio,Dificultad),
    Dificultad < 5.

cumpleCondiciones(Persona,Personaje):- 
    Personje \= campanita,
    tieneTodoPuros(Persona),
    not(esAmbicioso(Persona)). 

tieneTodoPuros(Persona):-
    forall(quiere(Persona,Suenio),esPuro(Suenio)).

esPuro(futbolista(_)).
esPuro(cantante(Discos)):-
    Discos =< 200000.

% Punto 4 

puedeAlegrar(Persona, Personaje):-
    quiere(Persona,_),
    tieneQuimica(Persona,Personaje),
    cumpleCond(Personaje).

cumpleCond(Personaje):-
    not(enfermo(Personaje)),
cumpleCond(Personaje):-
    backup(personaje,Backup),
    cumpleCond(Backup).

enfermo(campanita).
enfermo(losReyesMagos).
enfermo(conejoDePascua).

backup(Personaje,Backup):-
    amigo(Personaje,Backup).

backup(Personaje, otroAmigo):-
    amigo(Personaje,Backup),
    backup(Backup,otroAmigo).

amigo(campanita,reyesMagos).
amigo(campanita,conejoDePascua).
amigo(conejoDePascua,cavenaghi).


