
% Base de conocimiento 

continente(americaDelSur).
continente(americaDelNorte).
continente(asia).
continente(oceania).

estaEn(americaDelSur, argentina).
estaEn(americaDelSur, brasil).
estaEn(americaDelSur, chile).
estaEn(americaDelSur, uruguay).
estaEn(americaDelNorte, alaska).
estaEn(americaDelNorte, yukon).
estaEn(americaDelNorte, canada).
estaEn(americaDelNorte, oregon).
estaEn(asia, kamtchatka).
estaEn(asia, china).
estaEn(asia, siberia).
estaEn(asia, japon).
estaEn(oceania, australia).
estaEn(oceania, sumatra).
estaEn(oceania, java).
estaEn(oceania, borneo).

jugador(amarillo).
jugador(magenta).
jugador(negro).
jugador(blanco).

aliado(X,Y):- alianza(X,Y).
aliado(X,Y):- alianza(Y,X).
aliado(X,Y):- alianza(amarillo,magenta).

ocupa(argentina, magenta, 5).
ocupa(chile, negro, 3).
ocupa(brasil, amarillo, 8).
ocupa(uruguay, magenta, 5).
ocupa(alaska, amarillo, 7).
ocupa(yukon, amarillo, 1).
ocupa(canada, amarillo, 10).
ocupa(oregon, amarillo, 5).
ocupa(kamtchatka, negro, 6).
ocupa(china, amarillo, 2).
ocupa(siberia, amarillo, 5).
ocupa(japon, amarillo, 7).
ocupa(australia, negro, 8).
ocupa(sumatra, negro, 3).
ocupa(java, negro, 4).
ocupa(borneo, negro, 1).

sonLimitrofes(X,Y) :- limitrofes(X,Y).
sonLimitrofes(X,Y) :- limitrofes(Y,X).

limitrofes(argentina,brasil).
limitrofes(argentina,chile).
limitrofes(argentina,uruguay).
limitrofes(uruguay,brasil).
limitrofes(alaska,kamtchatka).
limitrofes(alaska,yukon).
limitrofes(canada,yukon).
limitrofes(alaska,oregon).
limitrofes(canada,oregon).
limitrofes(siberia,kamtchatka).
limitrofes(siberia,china).
limitrofes(china,kamtchatka).
limitrofes(japon,china).
limitrofes(japon,kamtchatka).
limitrofes(australia,sumatra).
limitrofes(australia,java).
limitrofes(australia,borneo).
limitrofes(australia,chile).

% 1)loLiquidaron/1 que se cumple para un jugador si no ocupa ningun pais 

loLiquidaron(Color):-
  jugador(Color),
  not(ocupa(_,Color,_)).

% 2) OcupaContinente/2 que relaciona un jugador y un continente si el jugador ocupa todos los paises del mismo 

OcupaContinente(Jugador, Continente):- 
  jugador(Jugador),
  continente(Continente),
  forall(ocupa(Pais,Jugador,_), estaEn(Continente,Pais))
% Aca se puede invertir lo que esta dentro del for all 

% 3) seAtrinchero/1 que se cumple para los jugadores que ocupan paises en un unico continente 

seAtrinchero(Jugador):-
  estaEnContinente(Jugador,Continente),
  not((estaEnContinente(Jugador,OtroContinente), Continente \= OtroContinente)).

estaEnContinente(Jugador,Continente):-
  estaEn(Continente,Pais),
  ocupa(Pais,Jugador).

%4) elQueTieneMasEjercito/2 que relaciona un jugador y un pais si se cumple que es en ese pais que hay mas ejercitos que en los paises del resto del mundo y a su vez ese pais es ocupado por ese jugador 

elQueTieneMasEjercito(Jugador,Pais):-
  ocupa(Pais,Jugador,CantEjer),
  forall(ocupa(_,_,OtraCant), CantEjer >= OtraCant).

% otra forma 

%forall( ((ocupa(OtroPais,_,OtraCant) otroPais \= Pais)), CantEjer > OtraCant).


