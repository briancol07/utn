

% Lugar donde vive (persona, lugar)

vive(remy, gusteaus).
vive(emile,chezMilleBar).
vive(django, PizzeriaJeSuis).

% sabe cocinar / 3 ( nombre , expe , plato)

sabeCocinar(linguini,3,ratatouille).
sabeCocinar(linguini,5,sopa).
sabeCocinar(colette,9,salmonAsado).
sabeCocinar(horst,8,ensaladaRusa).

% trabaja en / 2 (persona, lugar)

trabajaEn(linguini,gusteaus).
trabajaEn(colette,gusteaus).
trabajaEn(horst,gusteaus).
trabajaEn(skinner,gusteaus).
trabajaEn(amelie,cafeDes2Moulins).

% 1) menu 

menu(Plato):- 
  persona(Persona),
  sabeCocinar(Persona,Plato).

estaEnMenu(Plato,Restaurante):-
  trabajaEn(Persona,Restaurante),
  sabeCocinar(Persona,_,Plato).

% 2) Cocina bien 

cocinaBien(Persona,Plato):-
  sabeCocinar(Persona,Exp,Plato),
  Exp > 7.
cocinaBien(Persona,Plato):-
  tutor(Persona, Tutor),
  cocinaBien(Tutor,Plato).
cocinaBien(remy,Plato):-
  plato(Plato).

% Plato 

plato(Plato):-
  sabeCocinar(_,_,Plato).

% tutor -> aux 

tutor(linguini,Rata):-
  trabajaEn(linguini,Lugar),
  vive(Rata,Lugar).

tutor(skinner,amelie).

% 3) esChef

esChef(Persona,Resto):-
  trabajaEn(Persona,Resto),
  cumpleCondiciones(Persona,Resto).

% Aca no generadores por que ya estan 
cumpleCondiciones(Persona,Resto):-
  forall(estaEnMenu(Plato,Resto),
  cocinaBien(Persona,Plato)).

cumpleCondiciones(Persona,_):-
  findall(Exp,sabeCocinar(Persona,Exp,_)),
  sumlist(ListaExp, Total),
  Total > 20.

% 4) esEncargado 

esEncargado(Persona,Plato,Resto):-
  trabajaEn(Persona,Resto),
  sabeCocinar(Persona,Exp,Plato),
  forall(sabeCocinar(OtraPersona,OtraExp,Plato), trabajaEn(OtraPersona,Restaurante), OtraPersona \= Persona), OtraExp < Exp).

% 5) esSaludable 

esSaludable(Plato):-
  plato(Plato,Tipo),
  calorias(Tipo,Plato,Calorias),
  Calorias < 75.

calorias(entrada(lista),TotalCal):-
  length(Lista,Cant),
  TotalCal is Cant * 15.

calorias(principal(Guarni, Min), TotalCal):-
  caloriasGuarnicion(Guarni, Cal),
  TotalCal is Min * 5 + Cal.

caloriasGuarnicion(papaFrita,50).
caloriasGuarnicion(pure ,20).
caloriasGuarnicion(ensalada,0).

calorias(postre(Cal), Cal).

% 6) Resenia positiva

reseniaPositiva(Restaurante,Critico):-
  restaurante(Restaurante),
  not(viveEn(_,Restaurante)),
  criticaPositiva(Critico, Restaurante).

restaurante(Resta):-
  trabajaEn(_,Resta).

criticaPositiva(antonEgo, Restaurante):-
  sonEspecialistas(Restaurante,ratatouille).

criticaPositiva(cormillot, Resto):-
  forall(estaEnMenu(Plato,Resto), esSaludable(Plato))

criticaPositiva(martiniano,Resto):-
  esChef(Persona,Resto),
  not(esChef(OtraPersona,Resto)),
  Persona \= OtraPersona.

sonEspecialistas(Resto,Plato):-
  forall(esChef(Persona,Resto),
  cocinaBien(Persona,Plato).

