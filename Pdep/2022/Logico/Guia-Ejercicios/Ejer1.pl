#!/usr/bin/env swipl


% Base de conocimeinto 

genero(titanic , drama).
genero(gilbertGrape , drama).
genero(atrapameSiPuedes , comedia).
genero(ironMan , accion).
genero(rapidoYFurioso , accion).
genero(elProfesional , drama).


gusta(belen,titanic).
gusta(belen,gilbertGrape).
gusta(belen,elProfesional).
gusta(juan,ironMan).
gusta(pedro,atrapameSiPuedes).
gusta(pedro,rapidoYFurioso).

% 1) solo le gusta Pelicual de genero / 2 relaciona una persona con su genero si todas las peliculas que les gusta pertenecen a dicho genero 

soloLeGustaPeliculaDeGenero(Persona, Genero):-
  persona(Persona),
  genero(Genero),
  forall(gusta(Persona,Peli), genero(Peli,Genero)).

persona(Persona):-
  gusta(Persona,_).
genero(Genero):-
  genero(_,Genero).

% 2) peliculas que le gusta por genero /3 relaciona una persona, con un genero y una lista de peliculas que le gusta dicho genero

peliculaQueLeGustaPorGenero(Persona,Genero,Lista):-
  persona(Persona),
  genero(Genero),
  findall(Pelicula, ((gusta(Persona,Pelicula),genero(Pelicula,Genero)), Peliculas).

% El segundo termino se puede hacer aparte 

gustaPersonaPelicula(Persona, Genero, Pelicula) :-
  gusta(Persona,Pelicula),
  genero(Pelicula,Genero). 


