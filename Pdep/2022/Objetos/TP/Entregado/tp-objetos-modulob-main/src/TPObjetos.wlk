class Regalo{
    var property marca 

    method esInteresante() = self.marcaPar() && self.condicionDeInteresante()
    method condicionDeInteresante(){}
    method marcaPar() = marca.size().even()
}

object scalextric inherits Regalo{
  var property precioBase = 27300
  var property adicionalAuto = 5250
  var property autosAdicionales = 0
  var property marca ="scalextric"

  

  method agregarAuto(cantidad){
    autosAdicionales += 1
  }
  
  method precio() = 
  	(precioBase + (autosAdicionales * adicionalAuto))
  
  method esValioso() = (self.precio() > 27500)

  override method condicionDeInteresante() = self.esValioso()

}

object yoyo inherits Regalo{
  var property color = "amarillo"
  var property precioBase = 5000
  var property adicionalColor = 1500
  var property coloresCaros = ["azul", "rojo"]
  var property colorNoInteresante = "rojo"
  var property marca = "rusell"

  method precio() = 
  (precioBase + (if (coloresCaros.contains(color)) adicionalColor else 0))

  method esValioso(){
    return color == "azul" or color == "amarillo"
  }
  
//   method esInteresante() = 
//   	(marca.size().even() && color != colorNoInteresante)

    override method condicionDeInteresante() = color != colorNoInteresante


}
  
  object balero inherits Regalo{
  	var property adornoMetalico = false
  	var property precioBase = 14100
  	var property adicionalAdornoMetalico = 1900
  	var property marca = "balerino"
  	const precioInteresante = 15000
  	
  	method esValioso(){
  		return adornoMetalico  
  	}
  	
  	method agregarAdorno(){
  		adornoMetalico = true
  	}
  	
  	method precio() = 
  		(precioBase + (if (adornoMetalico) adicionalAdornoMetalico else 0))
  
//   method esInteresante() = 
//   (marca.size().even() && self.precio() > precioInteresante)

  override method condicionDeInteresante() = self.precio() > precioInteresante
  
  
  }
  
  // punto 2 

class Persona{
    method leGustaRegalo(regalo) = regalo.esInteresante() && self.condicionParticular(regalo)

    method condicionParticular(regalo){}
    
}



object stefan inherits Persona {
  var property plataGastar = 5000

  // method leGustaRegalo(regalo){
  //  return  (regalo.esInteresante() && !(regalo.esValioso()) && plataGastar >= regalo.precio())
  // }

   override method condicionParticular(regalo) = !(regalo.esValioso()) && plataGastar >= regalo.precio())
  
  method inconformista(regalos) {
		return regalos.all({unRegalo => self.leGustaRegalo(unRegalo).negate()})
	}
}

object justina inherits Persona  {
  var property edad = 11

  // method leGustaRegalo(regalo) = regalo.esInteresante() &&
  // (if ( edad.odd() ){  regalo.esValioso() } else { regalo.precio().between(1000,9999)})
  
  override method condicionParticular(regalo)= (if ( edad.odd() ){  regalo.esValioso() } else { regalo.precio().between(1000,9999)})

  method inconformista(regalos) {
		return regalos.all({unRegalo => self.leGustaRegalo(unRegalo).negate()})
	}
}
 
object pedro inherits Persona {
	var property mejorAmigo = justina
	
  //  method leGustaRegalo(regalo) = regalo.esInteresante() && (mejorAmigo.leGustaRegalo(regalo))
	
  override method condicionParticular(regalo) = (mejorAmigo.leGustaRegalo(regalo))
	
	method inconformista(regalos) {
		return regalos.all({unRegalo => self.leGustaRegalo(unRegalo).negate()})
	}
}
// punto 3  

object giftDealer {
	var property conoceA = [pedro, justina, stefan]
	var property regalos = [scalextric, balero, yoyo]
	var property historicoRegalos = []
	
	method inconformistas() {
		return self.conoceA().filter( { unaPersona => unaPersona.inconformista(regalos) } )
	}

method leGustaRegaloBarato(persona) {
		if (persona.inconformista(regalos)){
			const nuevoRegalo = new Voucher (persona = persona)
			 historicoRegalos.add(nuevoRegalo) return nuevoRegalo
		}
		else  { 
			const regaloBarato = regalos.filter({unRegalo =>  persona.leGustaRegalo(unRegalo)}).min({x => x.precio()})
			const registroRegalo = new HistoricoRegalos (regalo = regaloBarato, fecha = new Date(), precio = regaloBarato.precio(), persona = persona)
		 historicoRegalos.add(registroRegalo) 
		 return regaloBarato
		}
	}	

	
method verHistorico() = historicoRegalos
method verHistoricoPersona(persona) = historicoRegalos.filter { registro => registro.persona() == persona }
method plataRecibidaEnRegalosPersona(persona) = (self.verHistoricoPersona(persona)).sum { registro => registro.precio() }
method quienRecibioMasPlataEnRegalos() = 
	conoceA.sortedBy({ persona1, persona2 => self.plataRecibidaEnRegalosPersona(persona1) > self.plataRecibidaEnRegalosPersona(persona2) }).first()

}
 // punto 4

object nazarena inherits Persona {
  var property numeroRandom = 0
  // method leGustaRegalo(regalo) = regalo.esInteresante() && if (numeroRandom > 0) { numeroRandom > 4 } else (return 1.randomUpTo(7) > 4 )
  	
  override method condicionParticular(regalo) = if (numeroRandom > 0) { numeroRandom > 4 } else (return 1.randomUpTo(7) > 4 )
}

class PersonaGenerica inherits Persona {
  var property nombre 
  var property nacimiento 

  // method leGustaRegalo (regalo) = regalo.esInteresante() &&
	// (if(nacimiento.year() < 1990) { return regalo.precio()>25000 } else return regalo.precio()<50000)

  override method condicionParticular(regalo) = if(nacimiento.year() < 1990) { return regalo.precio()>25000 } else return regalo.precio()<50000

	}

const personaGenerica = new PersonaGenerica ( nombre = "Persona Generica" , nacimiento = new Date(day = 12, month = 5, year = 1988))
class Voucher inherits Persona{
  // con esto puedo aumentar en 3 meses la fecha today.plusMonths(3)
  var property nombre = "Voucher"
  var property fechaVencimiento = new Date().plusMonths(3)
  var property precio = 5000
  var property persona 
  var property marca = "boxbig"
  
  method esValioso() = true 
  method precio() = precio
  method esValido(unaFecha) = fechaVencimiento > unaFecha
//   method esInteresante() = (marca.size().even() && self.esValido(new Date()))

  override method condicionDeInteresante() =  self.esValido(new Date())
  	
}

class HistoricoRegalos  {
  var property fecha
  var property persona
  var property precio 
  var property regalo
}
