object scalextric{
  var property precio = 27300
    var property cantAutos = 2

    method agregarAuto(cantidad){
      cantAutos += cantidad
        precio += cantidad * 5250
    }
  method esValioso(){
    return precio > 27500
  }

}

object yoyo{
  var property color = "amarillo"
    var property precio = 5000

    method pintar(nuevo){
      if (nuevo == "azul" or  nuevo == "rojo"){
        precio += 1500
      }
      else{
        precio = 5000 
      }
      color = nuevo
    }
  method esValioso(){
    return color == "azul" or color == "amarillo"
  }

}

object balero {
  var property adornoMetalico = false
    var property precio = 14100

    method esValioso(){
      return adornoMetalico
    }

  method agregarAdorno(){
    adornoMetalico = true
      precio += 1900
  }
}

// punto 2 

object stefan {
  var property plataGastar = 5000

    method comprarRegalo(regalo){
      return  (!(regalo.esValioso()) && plataGastar >= regalo.precio())
    }
}

object justina {
  var property edad = 11

    method comprarRegalo(regalo){
      if ( !((edad % 2) == 0)){
        return regalo.esValioso()
      }
      else {
  	return ((regalo.precio().toString().length()) == 4)


  }
  }

}

object pedro {
	var property mejorAmigo = justina

   method comprarRegalo(regalo){
	return	mejorAmigo.comprarRegalo(regalo)
	}
}

// punto 3  

object giftDealer{
  // para regalos [scalextric,yoyo,balero]
  // para personas [stefan,pedro,justina]
  const property personas = [] 
  const property regalos = []

  method inconformistas(){

  }

}


// punto 4

object nazarena{
  
  method gustaRegalo(regalo){
    return 1.randomUpTo(7) > 4 
    // if(1.randomUpTo(7)>4){ return true}
    // return false 
}
}

class PersonaGeneria{
  var property nombre 
  var property nacimiento 

  method gustaRegalo(regalo){
    if (nacimiento.year() < 1990) {
      if (regalo.precio() > 25000 && regalo.precio() < 50000){ return true }
  }
    return false 
  }
}

// punto 5 

// sera una clase o un objeto no se 

class RegaloBarato{
  var property regalo
  var property persona

 method leGustaRegalo(persona){
   if(! persona.gustaRegalo(regalo)){
     // genero voucher 
   }
 }
}

class Voucher{
  // con esto puedo aumentar en 3 meses la fecha today.plusMonths(3)

  const property fecha 
  
}



class Historico {
  var property persona 
  var property regalo 
  const property costoRegalo 
  const property fechaOperacion
}


// punto 6 
