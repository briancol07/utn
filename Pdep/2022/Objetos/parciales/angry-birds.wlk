class Pajaro{
  var property ira 
  var property estaEnojado = false 
  
  method fuerza(ira)
  method hacerEnojar()
}

class PajaroComun inherits Pajaro{
  override method fuerza(ira) = 2 * self.ira()
}

object red inherits Pajaro{
  var property cantEnojos = 0 
  override method fuerza(ira) = 2 * cantEnojos * self.ira()
}

object bomb inherits Pajaro{
  var property cantMaxFuerza = 9000
  override method fuerza(ira) = (2 * self.ira()).min(cantMaxFuerza)
}

object chuck inherits Pajaro{
  var property velocidad

  override method fuerza(_){
    return 150 + 5 * (velocidad - 80)
  }
  override method hacerEnojar() {
    velocidad *= 2 
  }

}

object Terence inherits Pajaro{
  var property multiplicador 
  var property cantEnojos = 0

  override method fuerza(ira) = multiplicador * cantEnojos
}

object matilda inherits Pajaro{
  var property huevos = []

  override method fuerza(ira) = 2 * self.ira +huevos.sum({huevo => huevo.peso()})

  override method hacerEnojar(){
    huevos.add(new Huevo(peso = 2))
    }

  
}

class Huevo{
  var property peso 
}

object isla {
  var property pajaros = [] 

  method pajarosFuertes(){
    return pajaros.filter({pajaro => pajaro.fuerza() > 50})
  }
  method fuerzaIsla(){
    self.pajarosFuertes().sum({pajaro => pajaro.fuerza()})
  }
  method manejoIra(){
    pajaros.forEach({pajaro => pajaro.ira() = pajaro.ira()-5 }) 
  }
  method InvacionCerditos(cerdos){
    pajaros.forEach({pajaro => pajaro.ira() = pajaro.ira() + (cerdos / 100).truncate(0)})
  }
  method eventosDesafortunados(){

  }
  }
