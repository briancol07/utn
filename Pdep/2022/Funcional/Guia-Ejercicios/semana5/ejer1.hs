#!/bin/ghci 

{-
 - Dada una lista de tuplas, sacar la cantidad de elementos utilizando foldl y foldr 
 -
 -}


 -- cantidadDeElementos [(8,6),(5,5),(5,6),(7,8)]
 -- 4
 

-- Foldl 
cantidadDeElementos :: [a] -> Int 
cantidadDeElementos = foldl (\sem _ -> sem + 1) 0 

-- Foldr
cantidadDeElementos':: [a]-> Int
cantidadDeElementos' = foldr (\_ sem -> sem +1) 0 
