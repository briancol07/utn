#!/bin/ghci 


{-
 -
 - Dada una lista de (empleado, gasto) conocer el gasto total usando foldl y foldr
 -
 -Main>monto [(“ana”,80),(“pepe”,40),(“juan”,300),(“maria”,120)]
 540
 -}


-- Foldl 

monto::[(String,Int)] -> Int 
monto lista = foldl (\sem (_,b) -> sem +b) 0 lista

-- Foldr
monto'::[(String,Int)] -> Int 
monto' lista = foldr (\(_,b) sem-> sem +b) 0 lista

