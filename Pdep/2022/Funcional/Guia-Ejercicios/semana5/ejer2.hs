#!/bin/ghci

{-
 - Dada una lista de pares (empleado gasto), conocer el empleado mas gastador usando foldl y foldr 
 -
 -}

 -- masGastador [(“ana”,80),(“pepe”,40),(“juan”,300),(“maria”,120)]
 -- (“juan”,300)
 
 -- Foldl
 
masGastador :: [(String,Int)] -> (String,Int)
masGastador (cab:lista) = foldl tieneMayorGasto cab lista 

tieneMayorGasto :: (String,Int)->(String,Int)->(String,Int)
tieneMatorGasto persona persona2 | snd persona2 > snd persona2 = persona 
                                 | otherwise = persona2

-- Foldr 

masGastador':: [(String,Int)] -> (String,Int)
masGastador' (cab:lista) = foldl tieneMayorGasto cab lista 


