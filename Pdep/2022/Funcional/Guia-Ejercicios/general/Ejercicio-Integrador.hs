data Alumno = Alumno {
  nombre :: String,
  nacimiento :: (Int,Int,Int),
  legajo :: Int,
  materias :: [string],
  criterio :: CriterioEstudio

} deriving (Show)

data Parcial = Parcial {
  materia :: String,
  cantPreguntas :: Int

}deriving (Show)

type CriterioEstudio = Parcial -> Bool
-- 

materia :: Parcial -> String 
materia (Parcial mat _ ) = mat 

cantidadPreguntas :: Parcial -> Int 
cantidadPreguntas (Parcial _ cant) = cant

estudioso :: CriterioEstudio
estudioso _ = True

hijosDelRigor :: Int -> CriterioEstudio
hijosDelRigor n (Parcial _ preguntas) = preguntas > n 

cabulero :: CriterioEstudio
cabulero (Parcial materia _) = (odd.length) materia 

-- Cambiar criterio 
-- Patter matching 
cambiarCriterioEstudio :: CriterioEstudio -> Alumno -> Alumno

cambiarCriterioEstudio nuevoCriterio (Alumno nombre fecha legajo materias _) = Alumno nombre fecha legajo materias nuevoCriterio)

-- Otra forma 
cambiarCriterioEstudio nuevoCriterio alumno = Alumno 
    (nombre alumno) (fechaNacimiento alumno)
    (legajo alumno) (materiasQueCursa alumno) nuevoCriterio


estudia :: Parcial -> Alumno -> Bool 
estudia parcial alumno = (criterioEstudio alumno) parcial
