# Practica de Tipos 

> Determinar para cada una de las funciones 

* Que hace?
* Dar el prototipo de las funciones 

## Ejercicios 


* [ejer1.hs](./ejer1.hs)
* [ejer2.hs](./ejer2.hs)
* [ejer3.hs](./ejer3.hs)
* [ejer4.hs](./ejer4.hs)
* [ejer5.hs](./ejer5.hs)
* [ejer6.hs](./ejer6.hs)
* [ejer7.hs](./ejer7.hs)
* [ejer8.hs](./ejer8.hs)
* [ejer9.hs](./ejer9.hs)
* [ejer10.hs](./ejer10.hs)
* [ejer11.hs](./ejer11.hs)
* [ejer12.hs](./ejer12.hs)
* [ejer13.hs](./ejer13.hs)

