#!/bin/ghci 

-- Funcion
f x y = (x.fst.head) y > (x.snd.head) y 

-- Tipo

f:: ( Ord b) => (a -> b) -> [(a,a)] -> Bool

-- Invocacion 

f (+1) [10,20]
