#!/bin/ghci 

-- Ejercicio 6

f2 x _ [] = x 
f2 x y (z:zs) | y z > 0 = z + f2 x y zs
              | otherwise = f2 x y zs 

-- Tipo 

f2:: Int  -> ( Int -> Int ) -> [Int] -> Int 

-- Invocacion 


