#!/bin/ghci 

-- Ejercicio 9

g2 f x l = (head . filter ((x ==).f) l 

-- Tipo 

g2 :: (Eq h) => (a -> h) -> h -> [ a ] -> a 

-- Invocacion 

g2 (+2) 7 [3,5,7]
