#!/bin/ghci 

-- Ejercicio 4 

p1 x y z = ((>z) . length . filter x . map y)

-- Tipo

p:: (a -> Bool) -> (c -> a) -> Integer -> [c] -> Bool

-- Invocacion
