#!/bin/ghci 

-- Ejercicio 1 

p n = (head . filter n)

-- Tipo 

P:: ( a -> Bool) -> [a] -> a 

-- Invocacion 

P (== 'a') ['a','y','n','a']
