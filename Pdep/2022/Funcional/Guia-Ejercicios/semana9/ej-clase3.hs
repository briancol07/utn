#!/bin/ghci 

-- 3a) auh 

type Medida = Cuidadano -> Cuidadano 

auh :: Medida
auh cuidadano = aplicarMedida (patrimonio cuidadano < 0) AumentosSueldoPorHijo cuidadano

aplicarMedida cond f cuidadano | cond = cuidadano { sueldo = sueldo + f cuidadano } 
                               | otherwise = cuidadano


-- 3b) impuestoGanancias 

impuestoGanancias :: Number -> Medida
impuestoGanancias minimo cuidadano = aplicarMedida (sueldo cuidadano > minimo) ( disminuirSueldodif minimo)

disminuirSueldodif :: Number -> Cuidadano -> Number 
disminuirSueldodif minimo cuidadano = (sueldo cuidadano - min) * (-0.3)

-- 3c) impuestoAltaGama 

impuestoAltaGama :: Medida 
impuestoAltaGama cuidadano = aplicarMedida (tieneAutoAltaGama cuidadano) disminuirSueldoSegunAuto cuidadano

disminuirSueldoSegunAuto :: Cuidadano -> Number 
disminuirSueldodif cuidadano = (-0.1) * valorAutoAltaGama cuidadano

valorAutoAltaGama :: Cuidadano -> Number 
valorAutoAltaGama cuidadano = (snd.head.filter.autoAltaGama.bienes) cuidadano 

-- 3d) negociarSueldoProfesion

negociarSueldoProfesion :: String -> Number -> Medida 
negociarSueldoProfesion profesion porcentaje cuidadano = aplicarMedida ( tieneProfesion profesion cuidadano) ( aumentarSueldoPorcentaje) cuidadano 

tieneProfesion unaProfesion cuidadano = (unaProfesion ==).profesion) cuidadano 

aumentarSueldoPorcentaje :: Number -> Cuidadano -> Number 
aumentarSueldoPorcentaje porcentaje cuidadano = (porcentaje * sueldo cuidadano) / 100 


