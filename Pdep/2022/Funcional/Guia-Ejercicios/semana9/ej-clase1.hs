#!/bin/ghci

diferenciaDePatrimonio:: [Cuidadanos] -> Number 
diferenciaDePatrimonio cuidad = maxPatrimonio cuidad - minPatrimonio cuidad

maxPatrimonio cuidad = (maximum.map patrimonio) cuidad 
minPatrimonio cuidad = (minimum.map patrimonio) cuidad 

patrimonio :: Cuidadano -> Number 
patrimonio cuidadano = foldl (\num (_,valor) -> sem + valor) (sueldo cuidadano) [bienes]


