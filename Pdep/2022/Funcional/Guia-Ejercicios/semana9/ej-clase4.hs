#!/bin/ghci 

--4a) Gobierno a 

gobiernoA :: Gobierno 

gobiernoA = unGobierno [1999..2003][impuesto ganancias 30000, negociar sueldoProfesion "profesor" 10, negociar sueldoProfesion "empresarios" 40, impuestoAltaGama auh]

--4b) muy parecido a gobiernoA

--4c) Gobernar un anio 

gobernarUnAnio :: Gobierno -> [Cuidadano] -> [Cuidadano]
gobernarUnAnio gobierno cuidad = (map aplicarMedidasGob (medidas gobierno)) cuidad 

aplicarMedidasGob :: [Medida] -> Cuidadano -> Cuidadano 
aplicarMedidasGob medidas Cuidadano = fold (flip ($)) Cuidadano medidas 

--4d) Gobernar periodo Completo 

gobernarPeriodoCompleto :: Gobierno -> [Cuidadano] -> Cuidadano 
gobernarPeriodoCompleto gob cuidad = foldr ($) cuidad ( replicate (length.periodo) gob) (gobernarUnAnio gob)

-- 4e) Distribuyo riqueza 

distribuyoRiqueza :: Gobierno -> cuidad -> Bool
distribuyoRiqueza gob cuidad = DiferenciaDePatrimonio cuidad > (DiferenciaDePatrimonio gobernarPeriodoCompleto gob) cuidad 


