# Semana 9 

> Ejercicios de practicas Integrales 

## Las medidas 

Se nos pide que hagamos un analisis ede las decisiones economicas de un gobierno haciendo un programa en haskell.
Vamos a modelar a un cuidadano con su profesion, su sueldo anual, la cantidad de hijos y una lista de bienes 

```haskell 

Type Bien = (String , Number)
Data Cuidadano = UnCuidadano { profesion :: String ,
                               sueldo :: Number,
                               cantidadDeHijos:: Number,
                               bienes :: [Bien]} deriving Show 
``` 

Como se ve la profesion es un String, el sueldo es un entero, la cantidad de hijos un entero y los bienes son tupla con descripcion y valor del bien. Algunos cuidadanos de ejemplo: 

```haskell
homero = UnCuidadano "SeguridadNuclear" 9000 3 [("casa", 50000),("deuda",-70000)]
frink = UnCuidadano "Profesor" 12000 1 []
Krabappel = UnCuidadano "Profesor" 12000 0 [("casa",35000)]
burns = UnCuidadano "Empresario" 300000 1 ("empresa",1000000),("empresa",500000),("auto",200000)]
```
Tenemos tambien una cuidad 

```haskell
springfield = [homero, burns, frink, krabappel]
```
Se piden las siguientes funciones. Solo puede usarse recursividad en los puntos 4 o 5, aunque no es necesaria 

## 1. **diferenciaDePatrimonio**:

Recibe una cuidad y dice cual es la diferencia entre el cuidadano que mas, patrimonio tiene y el que menos patrimonio tiene. El patrimonio de cada cuidadano se obtiene suamndo el valor de su sueldo y de sus bienes.

* [ej-clase1.hs](./ej-clase1.hs)

## 2. **tieneAutoAltaGama** 

Recibe un cuidadano y dice si tiene un auto de alta gama, o sea, si tiene entre sus bienes un auto que valga mas de 100000

* [ej-clase2.hs](./ej-clase2.hs)

## 3. **auh, impuestoGanancias, impuetoAltaGama, negociarSueldoProfesion**

Medidas : se aplican a un cuidadano y lo retorna modificado 

1. **auh** 
  * Hace que aumente el sueldo de la persona en 1000 por cada hijo, si el patrimonio de la persona es menor a 0 ( en otro caso el cuidadano no cambia)
2. **impuestoGanancias**
  * Si el sueldo supera el minimo edad, disminuye su sueldo el 30% de la diferencia. Si no supera el minimo queda igual. 
3. **impuestoAltaGama**
  * Si el cuidadano tiene algun auto de alta gama, disminuye su sueldo en un 10% del valor del auto, sino disminuye nada 
4. **negociarSueldoProfesion**
  * Esta medida recibe una profesion y un procentaje. Si el cuidadano tiene esa profesion, entonces aumenta su sueldo el porcentaje indicado. Si no es su profesion, entonces queda igual. 

```haskell
-- auh
Main> auh (UnCuidadano "Doctor" 2000 2 [("deuda", - 3000)]
> UnCuidadano "Doctor" 4000 2 [("deuda", -3000)]

-- impuestoGanancias

Main> impuestoGanancias 2000 (unCuidadano "Doctor" 20100 0 [])
> UnCuidadano "Doctor" 20070 0 []

-- impuestoAltaGama

Main> impuestoAltaGama (UnCuidadano "Profesor" 30000 0 [("auto", 120000)])
> UnCuidadano "Profesor" 18000 0 [("auto", 120000)]
```

* [ej-clase3.hs](./ej-clase3.hs)

## 4. Gobierno

Un Gobierno esta representado por las listas de anios que goberno y por la lista de medidas que aplico 

```haskell
data Gobierno = UnGobierno [Int] [Cuidadano -> cuidadano]
```

1. Escribir la funcion constante **gobiernoA**, que devuelve un gobierno que goberno entre 1999 y 2003, y aplico impuesto a las ganancias de 30.000, negocio sueldo de profesores por 10%, negocio suledo de empresarios por 40% y aplico impuesto a vehiculos de alta gama y tambien aplico la auh.
2. Escribir la funcion constante **gobiernoB**, que devuelve un gobierno que goberno desde 2004 hasta 2008 y aplico impuesto a las ganancias de 40.000, negocio sueldo de profesores por 30% y negocio sueldo de camionero por 40%.
3. Hacer la funcion **gobernarUnAnio**, que recibe un gobierno y una cuidad y aplica todas las medidas de gobierno a cadauno de los los cuidadanos de la cuidad. Retorna la cuidad cambiada
4. Hacer la funcion **gobernarPeriodoCompleto**, que recibe un gobierno y una cuidad y gobierna a esa cuidad todos los anios del periodo (Es decir, gobierna tantos anios a la cuidad como haya durado el mandato).
5. Hacer la funcion **distribuyoRiqueza** que dice si un gobierno hizo justamente eso en una cuidad. Esto es cuando luego de gobernar por todo su periodo, la diferencia de patrimonio es menor que al iniciar 

* [ej-clase4.hs](./ej-clase4.hs)

## 5. Cuidadano Especial

1. Crear al cuidadano **kane**, que no tiene hijo, es empresario, tiene 100000 de sueldo, e infinitos trineos todos llamados "Rosebud", y cada uno $5 mas caro que el anterior 
2. Decir que sucede con lo siguiente. Justificar Conceptualmente ambas respuestas 


```haskell 
--1 

Main> kane 
> UnCuidadano "Empresario" 100000 0 [("Rosebud",5),("Rosebud",10),("Rosebud",15)... 

--2 

Main> gobernarUnAnio gobiernoA [kane]
Main> gobernarUnAnio gobiernoB [kane]
```

## 6. Dar tipo mas generico

```haskell
f1 x y z = map (*x). filter (y z)
```
