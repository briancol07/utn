#!/bin/ghci 

{-
 - Resolver la funcion find' que se encuentra el primer elemento que cumple una condicion. No se puede resolver con recursividad. Si ningun elemento cumple la condicion dejar que falle.
 -
 - find' :: ( a -> Bool) -> [a] -> a 
 - 
 -}

find' :: ( a -> Bool) -> [a] -> a 
find' f list = (head.filter f) list
 
AR
data Politico = Politico {proyectosPresentados :: [String], sueldo :: Number, edad :: Int } deriving Show
 
 politicos = [ Politico ["ser libres", "libre estacionamiento coches politicos", "ley no fumar", "ley 19182"] 20000 81 ,
 Politico ["tratar de reconquistar luchas sociales"] 10000 63, Politico ["tolerancia 100 para delitos"] 15500 49 ]
 
{-
 - Queremos Encotrar 
 - a) un politico Joven menos de 50 anios 
 - b) alguno que haya presentado mas de 3 proyectos 
 - c) alguno que haya presnetado algun protecto que tengas mas de 3 palabras
 -
 - No hay que generar funciones, sino aprovechar find' y desde la consola resuelva los 3 requerimientos 
 -}

-- a) find'((<50).edad) politicos
-- b) find'((>3).length.proyectosPresentados) politicos 
-- c) 
