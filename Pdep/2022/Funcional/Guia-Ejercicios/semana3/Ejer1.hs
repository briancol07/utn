#!/bin/ghci 

{- Queremos calcular el sueldo de los empleados de nuestra empresa
 - Tenemos dos tipos de empleado:
 -  * Los comunes: nos interesa saber el sueldo basico y el nombre
 -  * Los jerarquicos: nos interesa conocer el sueldo basico, la cantidad de gente a cargo y el nombre 
 -
 -  El useldo que cobran los comunes se determina por el sueldo basico, en los empleados jerarquicos se calcula como sueldo basico + plus por la cantiadad de gente a cargo ( 500 por persona a cargo)
 -
 -  > sueldo (jefe 15000 3 "sonia") 
 -  16500
 -}


-- En este caso el nombre lo puse adelante 
--
data Empleado = Comun {nombre::[Char], sueldo:: Int}| 
                Jerarquico{nombre::[Char], sueldo:: Int, cantPersonal:: Int}deriving Show

calcSueldo :: Empleado -> Int 
calcSueldo (Comun _ sueldo) = sueldo 
calcSueldo (Jerarquico _ sueldo cantPersonal) = sueldo + cantPersonal * 500

