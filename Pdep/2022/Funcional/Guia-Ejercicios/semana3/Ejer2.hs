#!/bin/ghci 

{- Se conocen estas bebidas 
 -  data Bebida = Cafe {nombreBebida :: String} | 
 -                Gasesosa{ sabor::String, azucar::Number}
 -
 - Dado un producto determinar si es energizante 
 -  * Si es cafe es energizante, si es un capuchino 
 -  * Si e una gaseosa es energizante si es de sabor pomelo y tiene mas de 10g de azucar 
 -
 -}

data Bebida = Cafe {nombreBebida :: [Char]} | 
               Gaseosa{ sabor::[Char], azucar::Int}

esEnergizante :: Bebida -> Bool 
esEnergizante( Cafe _ ) = True 
esEnergizante( Gaseosa nombre azucar) = nombre == "pomelo" && azucar >= 10 

