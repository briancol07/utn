#!/bin/ghci

-- Definir la funcion esMayorA que verifique si el doble del siguiente de la suma entre 2 y un numero es mayor a 10 
--

doble :: Int -> Int 
doble = (2*)

sig :: Int -> Int 
sig = (+1)

masDos:: Int -> Int 
masDos = (+2)

esMayorA :: Int -> Bool
esMayorA = (>10).masDos.sig.doble 

