#!/bin/ghci

-- Definir las funciones booleanas estandar. Sin usar las funciones predefinidas 
-- Definir la funcion and 

and1 :: Bool -> Bool -> Bool
and1 bool1 bool2 | bool1  = bool2
                 | otherwise = False

-- Definir la funcion or 

or1 :: Bool -> Bool -> Bool
or1 bool1 bool2 | bool1 = True
                | bool2 = True
                | otherwise = False 


