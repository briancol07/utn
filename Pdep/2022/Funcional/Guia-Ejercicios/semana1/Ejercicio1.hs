#!/bin/ghci 

-- Definir la funcion calcular, que recibe una tupla de 2 elementos y devuelve nueva tupla segunn las sig reglas:
-- Si el primer elemento es par lo duplica: si no lo deja como esta
-- Si el segundo elemento es impar le suma1: si no deja como esta 

calcular :: (Int,Int) -> (Int,Int)

calcular (num1,num2) = (primerFunc num1,segunFuncion num2)

primerFunc :: Int -> Int 
primerFunc num | even num = num * 2 
               | otherwise = num 

segunFuncion :: Int -> Int 
segunFuncion num | odd num = num + 1 
                 | otherwise = num 
