#!/bin/ghci

-- Definir la funcion notaMaxima que dado un alumno devuelva la maxima nota del alumno 
--

type Nota = Int 
type Alumno = ([Char], Nota, Nota, Nota) 

notaMaxima :: Alumno -> Nota 
notaMaxima ( _, nota1, nota2, nota3) = max(max nota1 nota2)nota3

notaMax ( _, nota1, nota2, nota3) = (max nota1 . max nota2) nota3


