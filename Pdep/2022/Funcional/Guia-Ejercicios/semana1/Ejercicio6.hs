#!/bin/ghci 

-- Dar expresiones lambda que sean equivalentes a las siguientes expresiones 
--
-- 1) Triple 
-- 2) Siguiente
-- 3) Suma 
-- 4) SumarDos


triple :: Int -> Int 
triple = \x -> x * 3 

sigui :: Int -> Int 
sigui = \x -> x + 1 

suma :: Int -> Int -> Int 
suma = \x y -> x + y 

sumarDos :: Int -> Int 
sumarDos = \x  -> x + 2 
