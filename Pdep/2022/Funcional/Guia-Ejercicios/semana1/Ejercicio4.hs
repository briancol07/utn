#!/bin/ghci 

-- Definir la funcion cuadruple reutilizando la funcion doble

doble :: Int -> Int 
doble = (2*)

cuadruple :: Int -> Int 
cuadruple = doble.doble
