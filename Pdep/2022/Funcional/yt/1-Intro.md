# Haskell 

* pure (mathematical) functions 
* Immutable data --> result nothing else 
* No/less side-effects 
* Declarative 
* Easier to verify 

## Fucntions Definition 

```
name arg1 arg2 ... argn = <expr>
```

```haskell

in_range min max x = x >= min && x <= max
```
we don't have something like return statement 

## Types 

```haskell

name :: <type>
x :: Integer 
y :: Bool 
z :: Float 
```

### Types (Functions)

```haskell
in_range :: Integer -> Integer -> Integer -> Bool 
in_range min max x = x >= min && x <= max
```

## Function (let)

we bind something to a name and use it

```haskell
in_range :: Integer -> Integer -> Integer -> Bool 
in_range min max x =
    let in_lower_bound = min <= x
        in_upper_bound = max >= x
    in 
        in_lower_bound && in_upper_bound
```

## Function (where) 


```haskell
in_range :: Integer -> Integer -> Integer -> Bool 
in_range min max x = ilb && iub
    where 
        ilb = min <= x
        iub = max >= x
```

## Functions Infix

you write the function between the arguments 

