#!/bin/ghci

{-

Punto 1 : Datos de una Persona 

Queremos saber los puntos de scoring de una persona, que se da por la siguiente manera :
  * Si tiene una cantidad par de amigues, multiplica el nivel de stress por la edad 
  * Si en cambio tiene mas de 40 anios, multiplica la cantida de amigeus por la edad 
  * de lo contrario, es la cantidad de letras del nombre *2 

Resolverlo utilizando guardas, composicion, aplicacion parcial y funciones de orden superior 
-}

-- Tipo de dato persona 
 
data Persona = Persona { nombre::[Char],
                 edad::Int, 
                 nivelStress::Int, 
                 preferencias::[[Char]],
                 amigos::[[Char]]
}deriving Show 


-- Ejemplos de prueba para realizarlo mas simple a la hora de probar 
pedro  = Persona {nombre = "Pedro",edad = 24,nivelStress = 20,preferencias = ["Mar del Plata","Las Toninas"], amigos = ["juan","pipo"]}
pipo  = Persona {nombre = "Pipo",edad = 22,nivelStress = 33,preferencias = ["Mar del Plata","Las Toninas"], amigos = ["juan","pipo"]}
juan = Persona {nombre = "Juan",edad = 39,nivelStress = 20,preferencias = ["Mar del Plata"], amigos = ["juan","pipo"]}
debora  = Persona {nombre = "Debora",edad = 50,nivelStress = 90,preferencias = ["Las Toninas"], amigos = []}
test1 = Persona {nombre = "test1",edad = 25,nivelStress = 10,preferencias = ["Las Toninas"], amigos = ["amigo1","amigo2"]}
test2 = Persona {nombre = "test2",edad = 41,nivelStress = 10,preferencias = ["Las Toninas"], amigos = ["amigo1"]}
test3 = Persona {nombre = "Rigoberta",edad = 31,nivelStress = 10,preferencias = ["Las Toninas"], amigos = ["Rigoberta"]}

-- Una persona tiene nombre edad nivelStress _ amigos 
-- Cantidad de amigos 

cantidadAmigos:: Persona -> Int 
cantidadAmigos = (length.amigos)


-- puntosScoring
puntosScoring:: Persona -> Int 
puntosScoring persona | cantidadAmigos persona `mod` 2 == 0 = edad persona * nivelStress persona 
                      | edad persona > 40 = edad persona * cantidadAmigos persona
                      | otherwise = length(nombre persona) * 2 
                      
puntosScoring' :: Persona -> Int 
--nombre edad nivelStress _ amigos 
puntosScoring' (Persona nombre edad nivelStress _ amigos) | (even.length) amigos = edad * nivelStress  
                                                     | edad > 40 = edad * length amigos 
                                                     | otherwise =  2 * length nombre 
