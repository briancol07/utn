#!/bin/ghci 


data Persona = Persona { nombre::[Char],
                 edad::Int, 
                 nivelStress::Int, 
                 preferencias::[[Char]],
                 amigos::[[Char]]
}deriving Show 

pedro  = Persona {nombre = "Pedro",edad = 24,nivelStress = 20,preferencias = ["Mar del Plata","Las Toninas"], amigos = ["juan","pipo"]}
pipo  = Persona {nombre = "Pipo",edad = 22,nivelStress = 33,preferencias = ["Mar del Plata","Las Toninas"], amigos = ["juan","pipo"]}
juan = Persona {nombre = "Juan",edad = 39,nivelStress = 20,preferencias = ["Mar del Plata"], amigos = ["juan","pipo"]}
debora  = Persona {nombre = "Debora",edad = 50,nivelStress = 90,preferencias = ["Las Toninas"], amigos = []}
test1 = Persona {nombre = "test1",edad = 25,nivelStress = 10,preferencias = ["Las Toninas"], amigos = ["amigo1","amigo2"]}
test2 = Persona {nombre = "test2",edad = 41,nivelStress = 10,preferencias = ["Las Toninas"], amigos = ["amigo1"]}
test3 = Persona {nombre = "test3",edad = 31,nivelStress = 10,preferencias = ["Las Toninas"], amigos = ["Rigoberta"]}

-- Parte a 
--  Saber si una persona tinee un nombre firme que son aquellos que comienzan con la letra P

tieneNombreFirme :: Persona -> Bool
tieneNombreFirme persona = (== 'P').head $ nombre persona

-- Parte b
--  Saber si una persona es interesante , esto ocurre si tiene 2 o mas amigues 


esInteresante :: Persona -> Bool 
esInteresante persona = (>=2).length $ amigos persona

