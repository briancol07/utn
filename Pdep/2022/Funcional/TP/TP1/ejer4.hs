#!/bin/ghci 

{-
 - Punto 4 
 -}

type Preferencia = (Destino) -> Persona -> Bool

deschufarse :: Preferencia
deschufarse destino persona = (nivelStress.destino) persona < nivelStress persona

enchufarse :: Num -> Preferencia 
enchufarse nivelDeseado destino persona = (nivelStress.destino) persona == nivelDeseado

socializar :: Preferencia
socializar destino persona = (length.amigos.destino) persona > (length.amigos) persona

sinPretenciones :: Preferencia 
sinPretenciones destino persona = True 

