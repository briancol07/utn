module Library where
import PdePreludat

doble :: Number -> Number
doble numero = numero + numero

-- punto 1 

data Persona = Persona { nombre::String,
                         edad::Number,
                         nivelStress::Number,
                         preferencias::[Preferencia],
                         amigos::[Persona]
}deriving Show

-- Ejemplos de prueba

pedro  = Persona{ nombre = "Pedro" , edad = 18, nivelStress = 15, preferencias = [], amigos = [pipo]}
pedro2  = Persona{ nombre = "Pedro" , edad = 18, nivelStress = 15, preferencias = [], amigos = [pipo]}
pedro3  = Persona{ nombre = "Pedro" , edad = 45, nivelStress = 50, preferencias = [], amigos = [pipo]}
pipo  = Persona { nombre = "Pipo", edad = 18, nivelStress = 50, preferencias = [], amigos = []}
pipo2  = Persona { nombre = "Pipo", edad = 18, nivelStress = 75, preferencias = [], amigos = []}
pipo3  = Persona { nombre = "Pipo", edad = 18, nivelStress = 40, preferencias = [], amigos = []}
juan = Persona { nombre = "Juan", edad = 18, nivelStress = 20, preferencias = [], amigos = []}
juan2 = Persona { nombre = "Juan", edad = 37, nivelStress = 80, preferencias = [], amigos = []}
debora  = Persona{ nombre = "Debora", edad = 50, nivelStress = 90, preferencias = [], amigos = [debora,pedro,pedro]}
test1 = Persona { nombre = "test1", edad = 25, nivelStress = 40, preferencias = [], amigos = []}
test2 = Persona { nombre = "test2", edad = 41, nivelStress = 45, preferencias = [], amigos = [pedro]}
test3 = Persona{ nombre = "Rigoberta", edad = 31, nivelStress = 10, preferencias = [], amigos = [pipo]}


-- Cantidad de amigos 

cantidadAmigos:: Persona -> Number
cantidadAmigos = length.amigos

-- Punto 1 
-- puntosScoring

--puntosScoring:: Persona -> Number
--puntosScoring persona | even.cantidadAmigos $ persona = edad persona * nivelStress persona
 --                     | (>40).edad $ persona = edad persona * cantidadAmigos persona
  --                    | otherwise = (2*).length.nombre $ persona

-- Correccion 

puntosScoring :: Persona -> Number
--nombre edad nivelStress _ amigos 
puntosScoring (Persona nombre edad nivelStress _ amigos) | even.length$ amigos = edad * nivelStress
                                                     | edad > 40 = edad * length amigos 
                                                     | otherwise =  2 * length nombre
-- Punto 2 

tieneNombreFirme :: Persona -> Bool
tieneNombreFirme  = (=='P').head.nombre

-- Parte b
-- Saber si una persona es interesante , esto ocurre si tiene 2 o mas amigues 

esInteresante :: Persona -> Bool
esInteresante = (>=2).cantidadAmigos

-- Punto 3
--
--cambiarNivelStress persona variable = persona { nivelStress = variable (nivelStress persona)}

type Destino = Persona -> Persona

cambiarNivelStress :: (Number -> Number) -> Persona -> Persona
cambiarNivelStress  funcion persona = persona { nivelStress = funcion.nivelStress $ persona}

edadMaxima20 :: Persona -> Number
edadMaxima20 persona = min (edad persona) 20

restaEdad :: Persona -> Number -> Number
restaEdad persona = flip (-) (edadMaxima20 persona)

agregarAmigo :: Persona -> Persona -> Persona
agregarAmigo amigo persona = persona { amigos = amigo:amigos persona }

stressMax :: Number -> Number
stressMax  = max 0

-- Integrante 1 

marDelPlata :: Number -> Destino
marDelPlata mes persona | mes == 1 || mes == 2 = cambiarNivelStress (+10) persona
                        | otherwise = cambiarNivelStress (stressMax.restaEdad persona) persona

-- Integrante 2 
lasToninas :: Bool -> Destino
lasToninas plata persona    | plata = cambiarNivelStress (ceiling.(/2)) persona
                            | otherwise  = marDelPlata 7 persona

-- Integrante 2 

puertoMadryn :: Destino
puertoMadryn = agregarAmigo juan . cambiarNivelStress (*0)

-- Integrante 1 
laAdela :: Destino
laAdela = id

--Punto 4

type Preferencia = Destino -> Persona -> Bool

-- Integrante 2 
desenchufarse :: Preferencia
desenchufarse destino persona = (nivelStress.destino) persona < nivelStress persona

-- Integrante 2 
enchufarse :: Number -> Preferencia
enchufarse nivelDeseado destino persona = (nivelStress.destino) persona == nivelDeseado

-- Integrante 1 
socializar :: Preferencia
socializar destino persona = (cantidadAmigos.destino) persona > cantidadAmigos persona

-- Integrante 1 
sinPretenciones :: Preferencia
sinPretenciones _ _ = True

-- Ejemplos de contingente 

ariel :: Persona
ariel = Persona{ nombre = "Ariel" , edad = 21, nivelStress = 45, preferencias = [desenchufarse, socializar], amigos = [juan2]}
pedro4 :: Persona
pedro4 = Persona{ nombre = "Pedro" , edad = 38, nivelStress = 35, preferencias = [sinPretenciones], amigos = [juan2]}

---------------------------------------
-- Parte 2 
---------------------------------------
-- Punto 5 Contingentes 

carola =Persona{ nombre = "Carola" , edad = 21, nivelStress = 45, preferencias = [], amigos = []}

type Contingente = [Persona]

contingente1 :: Contingente
contingente1 = [ariel, pedro4]
contingente2 :: Contingente
contingente2 = [carola,pedro4]
contingente3 :: Contingente
contingente3 = [ariel, pedro4, juan2]

-- Destino apto (integrante 1)
cumplePrefe :: Destino -> Persona -> Bool
cumplePrefe destino persona = any (\unaPrefe -> unaPrefe destino persona) $ preferencias persona

destinoApto :: Destino -> Contingente -> Bool
destinoApto destino = all $ cumplePrefe destino

-- Destino piola (integrante 2)

destinoPiola :: Destino -> Contingente -> Bool
-- destinoPiola destino = (< 100) . sum . map (nivelStress . destino)
destinoPiola destino contingente = (<100)  (foldr ((+) . nivelStress . destino ) 0 contingente)

-- Punto 6 Paquete de destinos
gustavo :: Persona
gustavo  = Persona { nombre = "Gustavo" , edad = 25, nivelStress = 90, preferencias = [], amigos = []}

type Paquete = [Destino]

paquete1 :: Paquete
paquete1 = [marDelPlata 3, laAdela, lasToninas False]
paquete2 :: Paquete
paquete2 = []

-- Parte a (integrante 1): Stress de un paquete

stress :: Paquete -> Persona -> Number
stress [] persona = nivelStress persona
stress paquete persona = nivelStress (foldr ($) persona paquete)

-- Parte b (integrante 2): Alguna persona la pasa bien

laPasaBien ::[Persona] -> Bool
--laPasaBien [] = False
--laPasaBien (x:xs) | elem (nombre x) ((map nombre.concatMap amigos) xs) = True
 --                 | otherwise = laPasaBien xs
laPasaBien contingente = any (\persona -> elem (nombre persona) ((map nombre.concatMap amigos) contingente)) contingente

--listaAmigosContingente contingente = map nombre (concatMap amigos contingente)
  
-- Punto 7: Contingente TOC (ambos integrantes)}

fabiana = Persona { nombre = "Fabiana" , edad = 41 , nivelStress = 50, preferencias = [] , amigos = [juan] }
rigoberta = Persona{ nombre = "Rigoberta", edad = 31, nivelStress = 20, preferencias = [], amigos = [juan]}
veronica = Persona{ nombre = "Veronica", edad = 11, nivelStress = 51, preferencias = [], amigos = [juan]}

contingente4 :: Contingente
contingente4 = [fabiana, rigoberta, veronica, juan2]
contingente5 :: Contingente
contingente5 = [juan2,fabiana,rigoberta]

contingenteTOC :: Contingente -> Bool
contingenteTOC [] = True
contingenteTOC [_] = True
contingenteTOC (_:persona:personas) = (even . puntosScoring ) persona && contingenteTOC personas 



-- Punto 8: Contingente infinito
contingenteInfinito ::Contingente
contingenteInfinito = contingente3 ++ contingenteInfinito
