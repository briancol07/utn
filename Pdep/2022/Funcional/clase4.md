# Clase 4 

## Repaso 

```haskell

not.even.(+1) 
-- (2*). espera otra funcion 
map((2*).fst)[(5,9),(3,4),(2,1)]
filter (even).(+3).snd)[(3,1),(2,3),(5,1)]

data Persona = Deportista { nombre :: String, edad :: Number, fuerza :: Number} deriving Show 

-- julia persona ( definirlo asi para no escribirlo durante ejecucion ) 

julia = Deportista "julia" 25 50 

-- cuando aparece > es para decir en ejecucion 
-- edad es una funcion 

> edad julia 

cumplirAnios:: Persona -> Persona
cumplirAnios:: unaPersona = unaPersona { edad = edad unaPersona +1 }

cumplirAnios' :: Persona -> Persona 
cumplirAnios' ( Deportista nombre edad fuerza) = Deportista nombre (edad +1) fuerza
```

## Funciones recursivas 

``` haskell 

-- factorial f(n) = n f(n-1)

factorial :: Number -> Number 
factorial 0 = 1  -- Caso base 
facotrial num = num * factorial(num-1) -- Caso recursivo 

-- Contar elementos 

contarElem:: [a] -> Number 
contarElem [] = 0 
contarElem(cab:cola) = 1 + contarElem(cola)
-- cab puede ser anonima ya que no se utiliza 

```

## Funcion Any 

> Si almenos algun elemento cumple con la condicion, es una funcion recursiva  

``` haskell 

> any even [7,9,8,5] --> True 

any:: ( a -> Bool) -> [a] -> Bool
any f [ ] = False 
any f (cab:cola) = f cab || any f cola
```

## Funcion All 

> Si todos cumplen la condicion, Tambien es una funcion recursiva  

``` haskell

> all (odd fst) [(3,8),(1,2),(5,3)] --> True 

all :: (a -> Bool) -> [a] -> Bool
all _ [ ] = True 
all f (cab:cola) = f cab && all f cola -- recursiva 

```

## Funcion Maximo 

> De una lista nos dara el mayor numero de todos 

```haskell 

> maximo [ 3,8,9,3,7,1] --> 9

maximo :: [Number] -> Number 
maximo[max] = max 
maximo (cab:otraCab:cola) | cab > otraCab = maximo (cab:cola)
                          | otherwise = maximo (otraCab:cola)

maximo' [max] = max 
maximo' (cab:cola) = cab `max` maximo cola
```

## Funcion esCreciente 

> Saber si una lista va de menor a mayor 

```haskell 

> esCreciente [3,8,10,12] -- > True

esCreciente :: [Number] -> Bool 
esCreciente [_] = True 
esCreciente (cab:otra:cola) | cab < otra = esCreciente(otra:Cola)
                            | otherwise = False

esCreciente'(cab:otra:cola) = cab < otra && esCreciente(otro:cola)
```

## FOLDL 

> Reducir conjunto a un elemento usadno la funcion foldl, va de izq a der 

``` haskell 
-- Recibe una operacion/funcion y una semilla mas la lista(conjunto) 
> foldl (+) 0 [3,2,1] --> 6 

-- Recursivo 
foldl _ z [ ] = z 
foldl f sem (cab:cola) = foldl f (f sem cab) cola   

foldl :: ( a -> b -> a) -> a -> [b] -> a 

{- 

foldl (+) 0 [3,2,1]
foldl (+) (+3) [2,1]
foldl (+) (+3 2) [1]
foldl (+) (+5 1) [ ] 
foldl (+) (6) 
BASE = 6 
-} 

sum xs = foldl (+) 0 xs 
productoria xs = foldl (*) 1 xs

foldl ( \sem (_,elem) -> sem + elem ) 0 [("juan",30),("pedro",100),("ana",50)]

```
