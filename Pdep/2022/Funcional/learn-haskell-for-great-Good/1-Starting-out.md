# Starting out 

* Precedence of the operators 
  * or use patentheses to explicitly specify order 

## Boolean Algebra 

Type | symbols 
-----|---------
AND  | &&
OR   | \|\|
NOT  | not 
TRUE | True 
FALSE| False 
Equality | == 
inequality | /= 

## Calling Functions 

* Infix Functions 
  * \* , + , - , / 
* Prefix Functions 
  * Succ
  * max/min
  * Div 
  * with backstick can be infix 

## Baby's First Function

> Functions start with lowercase 

```haskell
-- FunctionName Parameter = Body 
doubleMe x = x + x 
```

To load in ghci 

```bash 
:l name.hs 
```

> Functions can also call others 

```haskell 
doubleSmallNumber x = 
                      if x > 100  then x 
                        else x * 2 
```

## An Intro to list 

> List -> Homogeneous (**same type**) data structures 

``` haskell 
let LostNumbers = [4,8,15,16,32,42]
```

### Concatenation 

``` haskell 
-- operator ++ (only with strings)
list = strings 
str ++ str 
```

### Adding something to the beginning 

``` haskell
-- use operator : 
'a' : "small cat" 
-- " a small cat"
5:[1,2,3] 
-- [5,1,2,3]
```

### Accessing list elements 

``` haskell 
-- operator !! 
[0,1,2,3,4] !! 2 
-- 2 
```

### List inside List 

``` haskell 
[[1,2,3],[1,2,3]]
```

### Comparing List 

> Lexicographical order 

If the items they contain can be compared ( < <= >= > ) 

``` haskell 
[3,4,2] > [3,4,4]
-- False 
``` 

### More list operations 

``` haskell 
-- head 
head [5,4,3] 
-- 5 

-- tail 
tail [5,4,3] 
-- [4,3]

-- last 
last [5,4,3] 
-- 3

-- init 
init [5,4,3] 
-- [5,4]

-- null 
null [5,4,3] 
-- False

-- reverse 
reverse [5,4,3] 
-- [3,4,5]

--take 2 
take 2 [5,4,3] 
-- [5,4]

-- drop 1
drop 1 [5,4,3] 
-- [4,3]

-- maximum
maximum [5,4,3] 
-- 5 

-- minimum 
minimum [5,4,3] 
-- 3

-- sum 
sum [5,4,3] 
-- 12 

-- product 
product [5,4,3] 
-- 60

-- elem tell you if the elment is on the list 
4 `elem` [3,4,5,6]
-- True 
```

### Ranges 

``` haskell 
[1..20]
-- 1,2,3,4,....,20

[2,4..20]
-- 2,4,6,8,10 ..20 

[20,19..1]
-- 20,19,18,17 ..1

['a'..'z']
-- a,b,c,d,e ..'z'

[13,26..24*13]
-- first 24 multiples of 13
```

### Produce infinite or long list 

``` haskell 
-- cycle (inifite list) 
take 10 (cycle[1,2,3])

-- repeat 
take 10 (repeat 5)

-- replicate 
replicate 3 10 
```
## List Comprehension

* filter 
* transform 
* combine 

``` haskell 
-- each element double | bind 1 to 10 to x 
[x*2 | x <- [1..10]]

[x*2 | x <- [1..10], x*2 >= 12]

-- many predicates as we want, all separated by commas 

[x | x <- [10..20], x /= 13, x /= 15, x /= 19]

-- Also we can have multiple predicates 

[x + y | x <-[1,2,3] , y <-[10,100,1000]]

-- [11,101,1001,12,102,1002,13,103,1003]
```

### Our Length

``` haskell

-- _ temporary variable (don't care about values)
length` xs = sum[1| _ <-xs]
```

### Example Remove nonUppercase

```haskell
nonUpercase st = [c | c <- st, c 'elem'['A'..'Z']
```

## Tuples 

> Stores heterogeneous elements as a single value 

* Store element /= types 
* Fixed size
* Sorround by () separated by commas 

``` haskell 

-- list of size two (pair)
(1,3)
(1,'a')

-- list of size three (triple)
(3,'b',"hello")
``` 

> You cant compare tuples of different sizes 

### Pairs 

> Only in pairs 

``` haskell 

fst(8,11)
-- 8

snd (8,11)
-- 11 
```

### zip 

> from list produce pairs 

``` haskell 

zip [1,2,3,4,5] [5,5,5,5,5]
-- [(1,5),(2,5),(3,5),(4,5),(5,5)] 

```
