# Learn you a Hasklee for Great Good !


[book](http://learnyouahaskell.com/)

## Intro 

> Haskell = Lazy 

* Think differently
* Purely Functional 
  * Functions 
    * Calculate
    * Return 
  * You tell what stuff is 
* No change of value 
* No side effects 
* Statically typed 
* Type inference 
* Elegant and consice 

> Referential Transparency : If a function is called twice with the same parameters it's guaranteed to return the same result both times 


## Overview 


