# Repaso 

> Listas --> Tienen el mismo tipo

```haskell
type Punto = (Number,Number)

distancia :: Punto -> Number
-- Pattern matching 
distancia (x,y) = sqrt(x ** 2 + y ** 2)
```

Ahora si necesitamos tener un punto para un plano o un espacio no nos servira la idea anterior por eso necesitamos un data .


```haskell
--             -> Constructor
--             |
data Punto = PuntoPlano { coordx::Number , coordy::Number}
           | PuntoEspacio {coordx::Number , coordy::Number, coordz::Number}

distancia(PuntoPlano coordx coordy) = sqrt(coordx**2 + coordy**2)
distancia(PuntoEspacio coordx coordy coordz) = sqrt(coordx**2 + coordy**2 + coordz** 2)
```
## Bool type 

```haskell
data Bool = True | False 
```

## Area 

``` haskell
data Figura = Circulo {radio::Number} | rectangulo {base::Number,altura::Number}

area:: Figura -> Number
area(Circulo radio) = Pi * radio ** 2
area(Rectangulo base altura) = base * altura 
```

##  Lista por Comprension

``` haskell
--                  -> Img       -> Generador     -> Filtro
--                  |            |                |
numerosPares xs = [unNumero | unNumero <- xs , even numero]
mayoresAN xs = [ unNumero | unNumero <- xs , unNumero > n]
divisiblesPorN xs = [ unNumero | unNumero <- xs ,((== 0) (`mod` n)) unNumero]
```

## Orden Superior

> Argumentos espera una funcion 

``` haskell
seleccionar(Number-> Bool) -> [Number] -> [Number]
seleccionar f xs = [ unNumero | unNumero <- xs , f unNumero]

-- > seleccionar even [ 3,4,8,7,10,3] --> [ 4,8,10]

```

## Funcion Filter 

```haskell
filter :: (a -> Bool) -> [a] -> [a]
-- f xs = [x | x <- xs, f x ]
```

## Funcion map 

``` haskell 
map :: (a->b) -> [a]-> [b]
-- f xs [ f x | x <-xs]

map (\(num1,num2) ->(num,num2+1))[(3,8),(7,5),(2,5)]
```

