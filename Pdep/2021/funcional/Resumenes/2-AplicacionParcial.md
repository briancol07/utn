# Aplicacion parcial / Composicion Funciones / Expresiones Lambda 

## Currificacion 

> Todas las funciones reciben un unico parametro y devuelven un resultado 

``` haskell

suma :: Integer -> Integer -> Integer 
suma x y = x + y
```

* La aplicacion de funciones asocia a izquierda 
* El tipo de funciones asocia a derecha 

``` haskell
suma :: Integer -> (Integer -> Integer)
(suma x) y = x + y
```

Teniendo en cuenta que la funcion suma espera un entero y devuelve una funcion que espera un entero y devuelve un entero 

Al estar currificada la funcion suma podemos aplicarla parcialmente. ej : suma 10. Es otra funcion que espera un entero y devuelve otro entero 

> Tenermos que hacer **import Text.Show.Functions** para que la pueda mostrar por pantalla 

## Composicion de Funciones 

``` haskell

esParSiguiente :: Integer -> Bool
esParSiguiente x = (even.siguiente) x 

esImparSigSumaSiete  :: Integer -> Bool
esImparSigSumaSiete nro = (odd.siguiente.suma 7) nro
```
> Los parentesis son importantes 

## Expresiones Lambda 

> Funciones Anonimas 

``` haskell
\x -> x +2
\param1 param2 ... -> body 

(\x ->\y -> x `mod` y == 0) 20 7
