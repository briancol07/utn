# Introduccion Paradigma Funcional 


## Que es una Funcion?

* Es una relacion entre un Dominio y un Codominio 
* Dom ----> CoDom 
* Transformacion de una entrada en una salida 

## Detalles 

* Siempre que apliquemos una funcion a un valor siempre vamos a obtener el mismo resultado.
* Transparencia Referencial. 
* Puedo reemplazar una Expresion cualquiera por el resultado y todo sigue funcionando.  


## Variables 

> Es una incognita, es decir un valor que no fue calculado 


## Ejemplo de implementacion 

``` haskell

siguiente nro = nro + 1 
```

## Como lo ejecutamos ?

``` bash 
ghci miPrograma.hs
./miPrograma.hs 

```
Tambien en VSC se puede usar :r to reload the file, osea volver a compilarlo si hacemos algun cambio 

## Definiciones de guardas 

> Si tenemos una funcion que para ciertos valores del dominio la imagen la calculamos de una manera y para otro valor del dominio la calculamos de otra forma 

* En matematica seria Funcion por partes 
* En Haskell seria con guardas 

``` haskell

calcular nro | even nro = siguiente nro 
             | otherwise = doble nro

doble nro = nro * 2 

```

## Evaluando Expresiones 

* Notacion infija Operadores 
* Notacion Prefija 

Infija | Prefija
-------|--------
2 + 5  | (+) 2 5
5 `mod`2 | mod 5 2  

## Tuplas 

> Permiten representar un tipo de dato compuesto pero con elementos que pueden ser de distinto tipo. Pero con un numero de elementos fijos 

* Tupla de 2 elementos 
  * (1,5)
* Tupla de 3 elementos 
  * (2,5,2020)



