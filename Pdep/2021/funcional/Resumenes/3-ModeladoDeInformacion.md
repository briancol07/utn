# Modelado de Informacion 


## Tipos de Datos Simples 

* 2 :: Integer 
* True :: Bool
* 2.6 :: Double 
* 'z' :: Char 

## Tipos de Datos Compuestos 

* " Hola Mundo" :: String 
* doble :: Integer -> Integer
* ['5','c','j','6'] :: [Char]
* [3,4,5,6] :: [Integer] 
* ('a',False) :: (Char,Bool)
* Estudiante "ana" [5,6,3]:: Persona (\*)

## Listas 

> Conjunto de elementos del** mismo tipo**. Si no hay elementos es una lista vacia 

``` haskell
[True,False] :: [Bool]
[] :: [a]
``` 

### Construccion de la lista 

> Las listas tienen una cabeza (head) y una cola (tail)

* [1,2,3]
* (1:[2,3])
* (1:2:[3])
* (1:2:3:[])

## Patter Matching 

> Mecanismo interno que permite verificar si un patron coincide con un valor 

Pattern | Denota 
--------|--------
(x:xs)  | El constructor **:** separa la lista en cabeza y cola
(x:y:xs)| Lista de almenos 2 elementos 
[x]     | Lista de 1 solo elemento
[x,y]   | Lista de 2 elementos
[]      | Lista vacia, no se peude separa en cabeza y cola 

### Funciones para manejo de listas 

* Head ---> Cabeza de la lista
* tail ---> Cola de la lista
* length ---> Cantidad de elementos de la lista
* ++ ---> Concatena dos listas 
* take ---> Primeros n elementos de la lista

## Tuplas 

> Tambien son un tipo de dato compuesto , pero cada elemento puede ser de un tipo distinto. La cantidad de elementos es fija 

Pattern | Denota 
--------|--------
(x,y)   | Tupla de 2 elementos de cualquier tipo 
(x,y,z) | Tuplas de 3 elementos de cualquier tipo 

### Funciones que se aplican a tuplas 

* fst 
* snd 

listas | Tuplas 
tipo de dato recursivo | NO
Cantidad de elementos variable | Elementos fijos 
Todos los elementos tienen que ser homogeneos | Puede tener elementos heterogeneos 
