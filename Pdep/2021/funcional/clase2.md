# Clase 2 

## Introduccion a funcional 

Una funcion f es una relacion entre un conjunto dado dom y codom (co dominio) 

dentro del programa hay abstracciones 

Es muy parecido al de mateamtica pero no lo es por que al hacer en programacion puede ser que no siempre del dominio caiga en el mismo valor de imagen 

* Propiedades 
  * Unicidad 
  * Existencia 

Transparencia Referencial ===> Paradigma declarativo
Podemos reemplazar una expresion por el resultado y todo sigue funcionando 

funcional es un paradigma dentro del declarativo 

No tienen efecto colateral 

## Variable 

imperativo ---> Espacio en memoria 
Funcional ----> Variable matematica (es una incognita) aun no fue calculado 


## Haskell

puro de funcional, descriptas usando ecuaciones 

lado izquierdo tengo la ecuacion a definir
lado derecho definicion de la funcion 



```haskell
siguiente nro = nro + 1 

```

para llamar interprete ghci nombreArchivo.hs 
invoco a la funcion siguiente 5 
y nos devolvera 6 

pero aca no restrigimos el dominio 
haskell tiene inferencia de tipo 

## Como limitar el Dominio de la funcion 


```haskell
siguiente :: Integer -> Integer
siguiente nro = nro +1 
```

## Guardas 

para ciertos valores lo calculamos de una manera y para otra se calculan de otra manera

```haskell

calcular nro | even nro = siguiente nro
             | otherwise = nro * 2
```

even nos dice si es par 
otherwise (sino) se multiplica por 2 

la suma es una funcion, tiene como notacion infija , se puede usar como prefija
(+) 6 8 

6 `mod` 4  para saber el resto de la division
mod 6 4 


## Modelando valores 

```haskell
aprobo :: Integer -> Bool
aprobo nota = nota >= 6 
```

## Tupla

Definir un par o x cantidad de elementos pero siempre es fija 

* funciones
  * Para 2 elementos
  * fst
  * snd

```haskell

segundo :: (Integer,Integer,Integer)-> Integer
segundo (_,x,_) = x
```
Variales anonimas con guion bajo 


