# Clase 3 


## Currificacion

Nos referimos que toda funcion recibe un unico parametro y devuelve un resultado 

```haskell

suma :: Integer -> Integer -> Integer
suma x y = x + y

```

la aplicacion de funciones se asocia a izq 
el tipo de las funciones se asocia a derecha 

suma espera los argumentos de a uno por vez 

la podemos aplicarla parcialmente(solo si esta currificada)  a un solo argumento 

osea aplicacion parcial poder aplicar una funcion con menos parametros 

```haskell

import Text.Show.Functions

```

## Composicion de funciones 

la imagen de la funcion g este definida dentro del dominio de f 

f.g ==> f(g(x))==> FOG

```haskell
esParSiguiente :: Integer -> Bool
esParSiguiente x = (even.siguiente) x
esParSiguiente x = even.siguiente $ x 
```

Los parentesis son importantes
En vez de parentesis se puede usar el apply ($) 


Se pueden componer las funcines parciales 

```haskell

(even.suma 5)
```

va esperar un numero para devolverme un booleano 


## Funciones Lambda Funciones anonimas 

```haskell

(\x -> x + 2 )7

```
Mas que nada para usar sobre consola funciones que se crean en linea 
haskell infiere el tipo mas generico

## Tipos de datos 

Integer-Bool-Double-Char-String

## Listas 

Los elementos deben ser homogeneos (mismo tipo) 

[] listas vacias 

```haskell
[True,False]::[Bool]
[1,2,3] o (1:[2,3]) o (1:2:3:[]) o (1:2:[3])
```

(cabeza:[cola]) 

```haskell

cabeza (x:_) = x 
```
Extra 
doble.siguiente.suma 2 $ x > 10 
esMayorA num = ((>10).doble.siguiente.suma 2) num 

