# Clase 5 

## Repaso 

funciones de orden superior 

any esperaban una funcion y luego una lista y nos dice si hay alguno que cumple con la condicion que le paso por parametro 
elemento -> bool 

* usamos laboratorio de funciones 


all parecido al any pero verifica si se cumple para todos los elementos del conjunto 

elemento -> Bool 

filter me devuelve los que cumplen con la condicion 


map produce una transformacion de conjunto 



hay otras funciones sobre listas 

* all 
* any 
* filter 
* map 


## Recursividad

Viene de la mano de induccion 

* Mecanismo 
  * Definir conjuntos infinitos
  * Definir funciones recursivas sobre ellos con garantia de terminacion 
  * Probar porpiedades sobre sus elementos 

factorial es un ejemplo

``` haskell 

-- Factorial
factorial 0 = 1 
factorial n = n * factorial (n -1) 

-- Recursividad con listas 
length [] = 0 
length (x:xs) = 1 + length xs 

--last

last [x] = x 
last (x:xs) = last xs 

-- elem 

elem [] = []
elem e (x:xs)

```  



