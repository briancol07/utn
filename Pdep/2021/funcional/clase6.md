# Clase 6 

## Funcion Foldl

> Es una funcion de orden superior que procesa una estructura de datos en un cierto orden y construye un valor de retorno.

Es una funcion recursiva , que la aplica con la lista 

```haskell

foldl :: (a->b->a) -> a -> [b] -> a
foldl _ valorNeutro [] = valorNeutro
fold funcion valorNeutro (cab:cola) = foldl funcion (funcion valorNeutro cab) cola
```
cuando llega a la lsita vacia devuelve el valorNeutro 

El foldl significa que es de izquierda a derecha aplicando de la semilla a la lista , encambio el foldr aplica toda la lista y luego la aplica con la semilla  

Existe tambien foldl1 pero lo que hace es que toma el primer valor de la lista como semilla 

## Ej 

``` haskell

--cantidadDeElementos :: [(Integer,Integer)] -> Integer
-- Con el tipo conmentado la funcion de abajo sirve para cualquier lista 
cantidadDeElementos lista = foldl (\sem _ -> sem +1) 0 lista 

cantidadDeElementos' lsita = foldr (\_ sem -> sem +1) 0 lista 
``` 


## Flip 

> Cambia de lugar los parametros 

``` haskell

cantidadDeElementos'' lista = foldr contar 0 lista 
contar _ sem = sem +1 

cantidadDeElemento''' lista = foldl (flip contar) 0 lista 
```


