# Clase 4 

## Modelado de informacion 

Tipos de datos 
* Nocion de tipo de datos 
* 

un tipo de dato se compone de un conjunto de elementos con algunas carracteristicas en comun 
un conjunto de operaciones para manipular dichos elementos 

## Como definimos un tipo de dato 

* Que forma va a tener 
* un mecanismo unicopara acceder a cada elemento 

* Algebraico 
  * Tuplas 
  * Tipos propios
    * Utilizando constructores


## Constructores

Comienza en mayuscula 
no tienen asociada una regla de reduccion 
puede o no tener argumento

clausula data 

* Introudce un nuevo tipo algebraico 
* Introduce los nombres de los constructores

## ej 

Bool 

data ColoresPrimarios = Rojo | Amarillo | Azul 

con argumento 
circulo = Circulo 7
rectangulo = Rectangulo 4 5 

Circulo o Rectangulo son los constructores 

## Patern matching 

Mecanismo de acceso 

* Pattern 
  * Expresion especial 
  * Solo con constructore y variables sin repetir 
  * Argumento en el lado izquierdo de una ecuacion 
* Matching 
  * Inspecciona el valor de una expresion
  * Puede fallar o tener exito 
  * Si tiene exito liga las variables del patron 

```ghci 
data Figura = Circulo {radio :: Double} | Rectangulo { base :: Double altura::Double} deriving Show 

area :: Figura -> Double 
area (Circulo radio) = pi * radio^2
area (Rectangulo base altura) = base * altura

``` 
deriving Show es para que se puedan mostrar en pantalla 

invocacion 


```ghci

Main > area rectangulo
20
```

## Listas por Comprension 

* Una notacion especial para escribir listas 

numerosPares numero = [num | num <- numeros , even num]


[x^2 | x <- [1..10].even x ] 

1. Generador 
2. Filtro 


