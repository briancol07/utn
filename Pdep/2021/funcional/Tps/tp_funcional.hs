import Text.Show.Functions()      

--type Habilidades = [String] 
type Suenios = (Persona -> Persona)
--type Suenios = [String]
data Persona = Persona{
        edad :: Int,
        suenios :: [Suenios],
        nombre :: String,
        habilidades :: [String],
        felicidonios :: Int
} deriving Show



sueniosPersona :: Persona -> Int
sueniosPersona persona = length (suenios persona)

--unaPersona1 = Persona 19 ["Recibirse","Viajar"] "Pepe" ["arquitectura", "Programar??"] 101
unaPersona2 = Persona 19 [viajar ["Roma","Paris"], enamorarse unaPersona3] "Pepe" ["arquitectura", "Programar??"] 101
unaPersona3 = Persona 19 [] "Pepe" ["arquitectura", "Programar??"] 101
--unaPersona1 = Persona 19 [viajar ["Roma","Paris"]] "Pepe" ["arquitectura", "Programar??"] 101
--unaPersona2 = Persona 26 ["Recibirse","Viajar"] "Pepe" ["arquitectura", "Programar??"] 100
--unaPersona3 = Persona 26 ["Recibirse"] "Pepe" ["arquitectura", "Programar??"] 50
--unaPersona4 = Persona 26 ["Recibirse"] "Pepe" ["arquitectura", "Programar??"] 14
--unaPersona5 = Persona 26 ["Recibirse"] "Pepe" ["arquitectura", "Programar??"] 12
--unaPersona6 = Persona 26 ["Recibirse"] "Maximiliano" ["arquitectura", "Programar??"] 12
--unaPersona7 = Persona 26 ["Recibirse"] "Melina" ["Cocinar"] 12

-- Punto1 a (Integrante 1)
coeficienteDeSatisfaccion :: Persona -> Int
coeficienteDeSatisfaccion persona| felicidonios persona > 100 = felicidonios persona * edad persona
                                 | felicidonios persona > 50  = sueniosPersona persona * felicidonios persona
                                 | otherwise = div (felicidonios persona) 2

-- Punto 1 b (integrante 2)
gradoDeAmbicion :: Persona -> Int
gradoDeAmbicion persona | felicidonios persona > 100 = (felicidonios persona) * sueniosPersona persona
                                            | felicidonios persona > 50 = edad persona * sueniosPersona persona
                                            | otherwise = 2 * sueniosPersona persona



-- Punto 2 a (Integrante 1)

nombreLargo :: Persona -> Bool
nombreLargo persona =  (>10).length$ nombre persona

-- Punto 2 b (integrante 2)


esSuertuda :: Persona ->Bool
esSuertuda  = even . (*3) . coeficienteDeSatisfaccion 

-- Punto 2 c (integrante 3)

tieneNombreLindo :: Persona -> Bool 
tieneNombreLindo persona = (=='a').last $ nombre persona

-- Punto 3 Integrante 1  

recibirse :: String -> Persona -> Persona
recibirse nombCarrera unaPersona = unaPersona{
        habilidades = habilidades unaPersona ++ [nombCarrera],
        felicidonios = felicidonios unaPersona + length(nombCarrera) * 1000
}  

queTodoSigaIgual :: Persona -> Persona
queTodoSigaIgual unaPersona1 = unaPersona1

-- Punto 3 Integrante 2

viajar :: [String] -> Suenios
viajar ciudades unaPersona  = unaPersona {
        edad = edad unaPersona + 1,
        felicidonios = felicidonios unaPersona + 100 * length ciudades
}

-- Punto 3 Integrante 3

enamorarse :: Persona -> Persona -> Persona
enamorarse unaPersona otraPersona = unaPersona {
        felicidonios = felicidonios unaPersona + felicidonios otraPersona
}

-- Combo perfecto 
bonusExtra:: Persona -> Persona
bonusExtra unaPersona = unaPersona { felicidonios = felicidonios unaPersona + 100}

-- Combo perfecto 
comboPerfecto :: Persona -> Persona
comboPerfecto unaPersona = (bonusExtra . viajar ["Berazategui","París"] . recibirse "Medicina") unaPersona



-- Punto 4 

-- Fuente minimalista Integrante 1
fuenteMinimalista :: Persona -> Persona 
fuenteMinimalista otraPersona = otraPersona{suenios = (eliminarPrimer(head.suenios )) otraPersona}

-- Funcion Auxiliar 
eliminarPrimer :: Persona -> [Suenios]
eliminarPrimer unaPersona = tail.suenios $ unaPersona




-- Fuente Copada Integrante 2
--fuenteCopada :: Persona -> Persona
--fuenteCopada unaPersona = foldl ..... ..... suenios

-- Fuente a Pedido Integrante 3



-- Funete Sorda 

fuenteSorda :: Persona -> Persona
fuenteSorda unaPersona = unaPersona 

-- Punto 5 

-- fuenteGanadora fuentes unaPersona       |
--                                         |
--                                         | 


-- Punto 6 

-- Punto 7 

                                  
