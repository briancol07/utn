#!/bin/ghci

import Text.Show.Functions

-- los felicidonios,un numero que cuantifica el nivel de felicidad que siente ( debe ser positivo) 
-- Edad 
-- suenios 

coefSatisfaccion edad suenios feli  | feli > 100 = feli * edad 
                                    | feli <= 100 && feli > 50 = feli * suenios
                                    | otherwise = feli / 2 

