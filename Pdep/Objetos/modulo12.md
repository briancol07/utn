# Modulo 12 

## Colecciones revisited 

* #{} 
  * Definia un conjunto en el cual no habia orden ni tambpoco elementos repetidos
* []
  * Definia lista , pueden ser repetidos 

``` wollok
const unConjunto = new Set()
unConjunto.add(1)
unConjunto.add(3)
unConjunto.size()
```

## Repaso de filter 

```wollok
method misionesDificiles() =
    misiones.filter({ mision => mision.esDificil() })

// { mision => mision.esDificil() }.apply(new LiberarDoncella(cantidadTrolls = 4))
// { mision => mision.esDificil() }.apply(new LiberarDoncella(cantidadTrolls = 7))
```

Delega el algoritmo de filtrado a la coleccion 

## Repaso de la interfaz de las colecciones 

> Map permite transformar lo elementos de una coleccion generando una nueva coleccion 

``` wollok 
method solicitantesDeMisMisiones() =
  misiones.map({ mision => mision.solicitante() })

method totalPuntosDeRecompensa() = 
  misiones.sum({mision => mision.puntosRecompensa()}) 

```

## Ordenar una coleccion 

``` wollok 
method misionesOrdenadasPorPuntos() = 
  misiones.sortedBy({mision1, mision2 =>
    mision1.puntosRecompenza() < 
    mision2.putosRecompenza()})
```

* Si la condicion se cumple el primer elemento se ordena antes 
* Caso contrario el segundo queda antes 

## For-Each 

Aplica una operacion sobre todos los elementos de una coleccion.

``` wollok

method cambiarSolicitanteDeMisiones(nuevoSolicitante) {
      misiones.forEach(
      { mision => mision.solicitante(nuevoSolicitante) })
}


```

## Dictionary 


