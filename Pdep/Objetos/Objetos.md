# Orientado a Objetos 




# Declaratividad Vs Imperatividad 

funcional trae las reglas de matematica 
logica : deducciones  y tratar de inferir 

Nuevo desafio Imperatividad 

## Que es un programa en objetos 

conjunto de objetos que se envian mensaje para alcanzar un determinado objetivo 

Es una relacion entre valores y operaciones 

## Wollok

Es orientado a objetos con un ide 

Tiene warnings y errores para detectar y corregir de manera temprana
Tiene inferencia de tipos 

Generacion automatica de diagramas 
Interfaz grafica (wollok game) 

## Primeros pasos 

Pepita es una golondrina 
Va a ser un modelo de la realidad 


``` wollok
/* efecto*/
  pepita.volar(2)
  pepita.comer(8)
/* consulta*/ 
  pepita.energia()


  object pepita{
        method volar(kilometros){
     // implementacion  
       }
}
``` 

## Que es una variable en objetos?

* Es una referencia a un objeto 
* Asignar => reapuntar una referencia 
* Pueden existir distintas referencias que apuntan al mismo obj
* Para mandarle un mensaje a un objeto necesitamos conocerlo mediante una referencia 


## Estado Interno 

Cada objeto puede tener sus propias referencias (atributos), al conjunto de estos se lo conoce como estado interno .
Los atributos solo pueden ser vistos y modificados por el objeto al que pertenecen.

se interacctua con el objeto mediante mensajes . Es encapsulado 

## Encapsulamiento 

> como que lo metodos y los datos de los objetos estan en cada uno. Ellos se encargan de si mismos 

## Ejemplo

``` wollok

    object pepita{
        var energia=100
        method volar(km){
            energia=energia-40-5*km 
           }
        method comer(gr){
            energia=energia+2*gr
        }
        method energia(){
            return energia 
            }
        }
```

> Nunca mezclar efectos con consultas , tratar de mantenerlos separados .

## Delegacion 

* Auto-Delegacion 
    self ( es una que tiene ella misma ) 

Se puede negar con el not o con ! 

``` wollok 
 object entrenador{
    method entrenar(pajaro){
        pajaro.comer(10)
        pajaro.volar(20)
        if(pajaro.energia()<20)
            pajaro.comer(10)
        else
            pajaro.comer(2)
            }

        }
```
En vez de hardcodear un pajaro en concreto podemos poner pajaro asi puede entrenar a cualquier tipo de pajaro. 
Por que es polimorfica , no importa cual sea lo hara 
osea por que entienden los valor que ponen . 

----

## Listas 

``` wollok 
const listas = [1,2,3]
lista.filter(criterio) 

const vacas = [new Vaca(), new Vaca()]
vaca.add(new vaca()) 
vacas.size()
vacas.filter(..)

object criterioEstaContenta{
  method apply(vaca) = vaca.Estacontenta()
  }
vacas.filter({vaca => vaca.Estacontenta()})
```
Representar muchas vacas con el mismo elemento

* Igualdad
* Identidad 
* Mutabilidad 

``` wollok
conts vacas = []
method leche dispo () = vacas 
   . filter { vaca => vaca.Estacontenta()}
   . sum { vaca => vaca.litrosDeLeche()}
  }
// Cambiar vacas por bichos 

method TodasContenta() = vacas 
  .all{vaca=>vaca.estacontenta()}
method ordeniar(){
  vacas.foreach{
    vaca => if(vaca.Estacontenta())
              vaca.odeniar()}}
```


----------

## Herencia 

Comunicar clases , proveer metodos entre gerarquia 
``` wollok 
class Tanke{
  const armas = [] 
  const tripulantes = 2 
  var salud = 1000
  var property prendidoFuego = false
  
  method emiteCalor() = prendidoFuego || tripulante>3
  method sufrirDanio(danio){
    salud -= danio
  }

  method atacar (obj) {
    armas.anyOne().dispararA(obj)
  }
}

object lanzallama{
  method dispararA(obj){
    objetivo.prendidoFuego(tru)
    }
}
// no tengo que codificar 10 misiles distintos de esta manera 

class Misil{
  const potencia

  method dispararA(obj){
    obj.sufrirDanio(potencia)
    }
}
// usar las mismas cosas para representar cosas 
// para no hacer las metrallas de diferentes tamanios 

class Metralla {
  const property calibre
  method dispararA(obj){
    if (calibre > 50 )
      obj.sufrirDanio(calibre/4)
      }
}
```
> Armar relacion entre clases que se llama herencia , Puede ser herencia simple o multiple 

* Usamos herencia simple 
* Superclase (Supertipo)
  * Idea que engloba sub clases 
* Subclase
  * Solo puede tener una super clase
* object es la raiz 
 
El metodo look up

## abstracta \<abstrac>
> Si no esta instanciada es abstracta 

## extra
no superclases para ponerle el nombre , interfaz que entiende tales mensajes 
no tiene nada que ver super clase ni herencia (herramienta para evitar repeticion codigo) 

arbol bien modelado , logica un lugar y disponibles en mucho 

-------
``` wollok

class Recargable{
  var cargador = 100
  method recargar 
  method agotada() = cargador <= 0 
}
class Lanzallamas inherits Recargable {
  method dispararA(obj){
    cargador -=20 
    obj.prendidoFuego(true)
}

class Metralla inherits Recargable{
  const calibre
  method disparar(obj){
    cargador -= 10
    if (calibre > 50)
      objetivo.sufrirDanio(calibre/4)
      }
}
```

## Notas ultima clase

``` wollok

throw new de("mensaje")

Throw new tuExep()
  try {}
  catch error : tuEx{}
// tipo patern matching 

// initialize (mantenimiento)

class x { 
  var property p 
  method y (){
      return self.p()
    }
}
// Desde afuera no se puede acceder al atributo 
// si o si tiene que haber otro metodo
```

las listas si son const no podes reasignar para apuntar a otro lcaso lista mutable

