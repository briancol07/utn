# Modulo 10 

## Errores y excepciones 

* Excepcion es un evento que altera el flujo normal de mensaje entre objetos .


## Como generar una excepcion 

``` wollok
// Enviando un mensaje 
self.error("Paso tal cosa")
// Instanciando una excepcion 
throw new UserException(message = "Paso tal cosa")

class UserExcepton inherits Exception {}

method inscribirse(alumno, materia) {
  try {
    materia.validarCupo()
    alumno.validarCorrelativas(materias) 
    ...
  } catch e : UserException{
    ???? que hacer?????
    }
}
``` 
