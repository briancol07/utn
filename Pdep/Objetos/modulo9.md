# Modulo 9 

## Objetos que no se instancian Explicitamente

``` 
const alumno = object{
  method estudia() = true
  } 
```
 
## Instanciacion sin parametros 

 ``` wollok

 class Ave {
   var energia = 0 
   method volar (kms) { energia = energia - (kms * 5)}
}

/// const cormoran = new Ave() 

```
## Instanciacion con Parametros 

``` wollok
class Presentacion {
 var property lugar
 const property musicos
 var property valorEntrada
 var property entradasVendidas
 ...
}

const seruEnLuna = new Presentacion(
  lugar = lunaPark, 
  musicos = [seruGiran], 
  valorEntrada = 2000, 
  entradasVendidas = 180)

```

