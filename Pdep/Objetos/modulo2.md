# Modulo 2 

## Estado de un objeto 

> Cada objeto tiene estado interno 

Al modelar una solucion con objetos , buscamos en general que el estado de un objeto sea manipulado solamente por el objeto .
Cada referencia define un atributo que tiene un nombre y un valor que esta apuntando 

## Objetos en wollok 

```wollok 

object pepita{
  var energia = 100 

  method volar () {
    energia = energia - 10 

    }
  method comer (cantidad) {
    energia = energia + 2 * cantidad
  }

}
```

## Accessors 

> el fin es publicar la referencia (getter) o modificar dicha referencia (setter) 

``` wollok 

object pepita { 
  // getter 
  method energia () { return energia}

  // setter 
  method energia (_energia) { energia = _energia}
}

```

para interactuar : 
  * pepita.energia()
  * pepita.volar() 
  * objeto.mensaje()

Si tiene parametros van dentro del parentesis 

## Referencias wollok 

* Variables
  * var 
  * Referencia que puede cambiar el objeto al que apunto
* Valores 
  * const 
  * Referencia fija a un objeto que no puede cambiar, una vez ininicializada no es valida una nueva operacion de asignacion 

## Relaciones bidireccionales 

``` wollok

object juan {
  var mascota 
  method mascota(_mascota){mascota = _mascota}
}
object firulais{
  var duenio
  method duenio(_duenio){duenio = _duenio}
}
```
juan.mascota(firulais)
firulais.duenio(juan) 

* Cualquiera de los dos puede enviar mensajes al otro 
* Requiere mantener ambas referencias sincronizadas

``` wollok 
object juan {
  var mascota 

  method mascota(_mascota){
    mascota = _mascota
    mascota.duenio(self)
    }
}
obj firulais{
  var duenio 
  method duenio(_duenio){duenio = _duenio}
}
```
self ---> Es un referencia al propio objeto donde estamos ubicados 

## Referencias globales 

* Objetos autodefinidos 
  * well known objects (wko)

``` wollok
object juan {
      method jugarConMascota(){
        firulais.jugar()
            }
```
Esto no es buena practica por que juan solo puede jugar con firulais

## Identidad 

> En el ambiente cada objeto tiene su propia identidad , y cadauno sabe quie n es , dos referencias son identicas si apuntan al mismo objeto  
