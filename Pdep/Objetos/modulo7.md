# Modulo 7 

## Repaso de Accessors 

``` wollok 

object pepita {
       var energia = 0
       method energia() = energia       // GETTER
       method energia(_energia) {    // SETTER
         energia = _energi}
       method volar() {
       energia = energia + 10
       }
}
import pepita.*
 
describe "tests para pepita" {
  test "energia inicial para pepita" {
  assert.equals(100, pepita.energia())
  }
  test "energia para pepita luego de volar" {
    pepita.volar()
    assert.equals(90, pepita.energia())
    }
 }
 
```
## Propiedades (property)

* Si una variable(var) , se genera su getter y setter 
* Si la referencia es constante(const) , se genera su getter y setter

``` wollok

object dodge1500 {
  var property color
  var kilometraje
  var property conAire = false
  const property patente = "RVM 363"
  const property anioPatentamiento = 1979
  method kilometraje() = kilometraje
  method realizarViaje(kilometros) {
    kilometraje = kilometraje + kilometros
  }
}
``` 
> La Propiedad constante solo define getter 
