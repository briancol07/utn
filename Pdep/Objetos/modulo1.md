## Modulo 1 

## Objeto 

> Representan distintos tipos de entidades 

## Que es un objeto? 

> Es algo que puedo representar atravez de una idea , pero abstrayendose, lo que importa es nuestro modelo mental 

## Objetos conceptuales 

> Un sistema es un conjunto de objetos que se envian mensajes para alcanzar un determinado objetivo 

## Composicion de un objeto 

> Un concepto, un ente, importa su interfaz 

Felix|
-----|
maullar()|
comer()|
dormir()| 

## Mensaje 

> Los objetos interactuan atravez de mensajes <br>
> Es lo que el objeto emisor le envia como orden al receptor . El emisor no sabe como se resuelve el mensaje, solo pide que se devuelva. Receptor recibe mensaje y se ejecuta un metodo(codigo)

## Ambiente 

> Lugar donde viven los objetos 

* Imagen ==> Smaltalk 
* Virtual machine ==> java 
* Engine ===> Javascript

## Referencias 

> Variable, apunta o referencia a un objeto.Necesita nombre , que es la forma que tiene el observador de dirigirse al objeto observado 

## Mensaje y metodo 

### Method lookup 

Las estrategias que utilizan los lenguajes para resolver donde esta el codigo de un metodo al enviar el mensaje . 

### Acciones y preguntas 

Algunos mensajes responden a preguntas, mientras que otros mensajes simplemente disparan acciones que afecta al objeto en cuestion . 

> En preguntas solo nos importa resultado , en los otros los efectos colaterales 

