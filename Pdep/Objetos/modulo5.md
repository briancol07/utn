# Modulo 5

## Definicion de test 

Los test se definen en el mismo proyecto en que esta la solucion

* .wtest 
* No tener mismo nombre que el wollok 
* Objeto clave assert (obj autodefinido )

``` wollok
import pepita * 

describe "Tests de Pepita" {
  test "energia inicial de pepita" {
    assert.equals(100,pepita.energia())
  }
  test "pepita comienza siendo fuerte"{
    assert.that(pepita.esFuerte())
  }
  test "pepita vuela y baja su energia"{
    pepita.volar(5)
    assert.equals(85,pepita.energia())
  }
  test "pepita come y sube su energia"{
    pepita.comer(120)
  }
}

```
## Describe 

* Para indentificar un conjunto de test 
* se utilizan llaves para delimitarlo 
* los test van adentro 

## Test 

terminar de leerlo 
