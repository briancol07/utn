# Modulo 8 

## Como objetos 

``` wollok


object elPrincipito {
  var property precio = 100
  method esCaro() = precio > 150
  method aumentar(aumento) { precio = precio + aumento }
}

object martinFierro {
  var property precio = 100
  method esCaro() = precio > 150
  method aumentar(aumento) { precio = precio + aumento }
}

object elReinoDelReves {
  var property precio = 100
  method esCaro() = precio > 150
  method aumentar(aumento) { precio = precio + aumento }
}
```

lo que vemos es que todos los objetos tienen el mismo comportamiento  

## Definiendolo como clase

``` wollok

class libro{
  var property precio = 100
  method esCaro() = precio > 150 
  method aumentar(aumento){ precio = precio + aumento}
}
```
De esta manera toda la logica que teniamos es comun para todos 

Se representa con diagrama de uml 

## Instanciando 

Instanciar es crear una instancia (objeto) apartir de la clase 

``` wollok
const elPrincipito = new Libro()
```

## Estructura de un objeto 

Al instaciar , el objeto tiene todas las referencias definidas en su clase (estructura)

## Method lookup con clase

Como bien lo dice , si el elemento no lo encuentra en la referencia va a ir hacia la clase , y asi sucesivamente hasta que encuentre lo que necesita . 

## Polimorfismo con Clases 

``` wollok
class DVD {
 var property precioMinuto = 10
 var property cantidadMinutos = 90
 method precio() = precioMinuto * cantidadMinutos
 method esCaro() = cantidadMinutos > 100
}

class Lamina {
 var property ancho = 29.7
 var property alto = 42
 var property material = new Material()
 method precio() = alto * ancho * self.precioBase()
 // self.precioBase() se refiere al objeto receptor del mensaje
 // una instancia de la clase lamina 
 method esCaro() = self.precioBase() > 50
 method precioBase() = material.precio()
}
class Materia{
  var property precio = 5 
  }

``` 

objetos autodefinidos o wko: son objetos conocidos, porque están representando un concepto de negocio dentro de la aplicación. Esto ocurre cuando un objeto tiene un comportamiento específico, y nos interesa modelarlo en forma separada de otros objetos. 

por último, las clases son importantes cuando se que existen múltiples objetos que comparten comportamiento y no tiene sentido que los nombre por separado: el viaje que hice ayer en colectivo, se parece mucho al viaje de la semana pasada. Si solo difieren en la información que guardan las referencias, el comportamiento se debe ubicar en un solo lugar para no repetir la misma idea una y otra vez
