# Modulo 4

## Numeros 

> Los objetos se representan como objetos inmutables 

* Numero no cambia su estado interno
* La suma de 1 + 2 resulta un nuevo numero que representa el 3 

``` wollok 
t a = 1
var b = a + 10  // suma
b = b - 1       // resta
b = b * 2       // multiplicación
b = b / 2       // división
b = b % 2       // resto
b = b ** 3      // elevado a (3 en este caso)
5.between(2, 7) // preguntamos si 5 está entre 2 y 7 ==> sí
3.min(6)        // el menor número entre 3 y 6 ==> 3
3.max(6)        // el mayor número entre 3 y 6 ==> 6
(3.1416).truncate(0)  // la parte entera de 3.1416 ==> 3 -- (3.1416).truncate(2)  // 3.14
(3.1416).roundUp(0)   // el primer entero mayor a 3.1416 ==> 4  -- (3.1416).roundUp(2)   // 3.15

```

## Booleanos 

> Dos obj representados por "true" y "false",al igual que los numeros son inmutables . 

* Palabras clave 
  * and: a && b 
  * or: a || b  
  * not: !a

## Strings 

> Las cadenas se limitan con una o dos comillas , tambien son inmutables

## Fechas  

> Tambien inmutables , que representa dia , mes anio , 

``` wollok 

>>> const hoy = new Date() 
// toma la fecha del día
>>> const unDiaCualquiera = new Date(day = 24, month = 11, year = 2017)
// se ingresa en formato día, mes y año
```
## Lambdas : objetos bloque 

``` wollok 
Lambda 
>>> const abs4 = { => 4.abs() }
>>> abs4
{ => 4.abs() }

>>> abs4.apply()
4

Bloques con parametro 

>>> const abs = { numero => numero.abs() }
>>> abs
{ numero => numero.abs() }
>>> abs.apply(-8)
8

``` 
## Contexto de los bloques 

Se puede ver que el closure accede a la variable "to" que esta definida fuera del contexto 

``` wollok 
var to = "world"
const helloWorld = { "hello " + to }
helloWorld.apply() == "hello world"
        
to = "someone else"
helloWorld.apply() == "hello someone else" 
```
## Colecciones 

> la coleccion nos permite representar un conjunto de objetos relacionados. Los jugadores de un equipo , un cardumen , lista de compras de supermercado .

* Conjuntos 
  * Modelan al conjunto matematico 
  * No hay orden 
  * No hay elementos repetidos
  * const numeros = #{1,2,3,4}
* Listas 
  * Tienen orden 
  * Puede haber elementos repetidos 
  * const fila = [francisco,mirta,hector]

``` wollok 
object shrek {
  ...
  method misionesDificiles() =
  misiones.filter({ mision => mision.esDificil() })
  method solicitantesDeMisMisiones() =
  misiones.map({ mision => mision.solicitante() })
} 

object liberarAFiona {
  var cantidadTrolls = 5
  method esDificil() = cantidadTrolls.between(4, 5)
  method solicitante() = "Lord Farquaad"
}

object buscarPiedraFilosofal { 
  var kilometrosDistancia = 40
  method esDificil() = kilometrosDistancia > 100
}

} 
```
El resultado de enviar el mensaje misionesDificiles() no altera las referencias de la coleccion original 

* Funciones
  * Filter
  * Map 
  * sum 
