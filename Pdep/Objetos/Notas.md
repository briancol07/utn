# Notas 

Aca iran las notas rapidas de algunas cosas que despues pasare a ortro lado 


```
class Bebedero{
  method esUtilParaAtender(animal)=
    animal.tieneSed()
}
class Comedero{
  const pesoMaximoSoportado
  method esUtilParaAtender(animal)=
    animal.tieneHambre() && animal.peso() < pesoMaximoSoportado
  method atender(animal){
    if(self.superaPesoMaximo(animal))
      self.error("El animal pesado")
      animal.comer(racion)
  }    
}
class Gallina{
  method tieneSed()= false 
  method tieneHambre() = true
  method peso () = 4 
}
class Vaca {
  var property peso
  var property tieneSed = false 
  method tieneHambre() = peso < 200 
}

class Cerdo { 
  var property peso
  var property tieneHambre = false
  var vecesQueComioSinBeber = 0 
  method tieneSed() = vecesQueComioSinBeber > 3 
}


```

## Conjunto como objeto 

* Colecciones
* Objetos con referencias a otros objetos
* Entienden mensajes
* Trabajar forma declarativa 

```
 animales.all({animal => animal.tieneHambre()})
 animales.filter({animal => animal.tieneSed()}) 
 animales.map({animal=>animal.peso()}).sum()
 animales.sum({animal => animal.peso()}) 

 animales.max({animal => animal.peso()})
 animales.find({animal => animal.peso() > 250})
 animales.forEach({animal => animal.comer(0.3)}) 

```

## list vs Set

* Listas 
  * Construccion: [] o new List() 
  * Admite repetidos 
  * Ordenadas 
  * Tenemos asList() o sortedBy(criterio)
* Sets 
  * Construccion: #{} o new Set()
  * Sin repetidos 
  * Sin orden 
  * Tenemos asSet()
* Son polimorficas ambas

## Herencia
> Los metodos y atributos definidos en la superclase los hereda la subclase

* Relacion entre 2 entidades 
  * Sublase -> Particular
  * Superclase -> Mas general 
* No impacta a la instanciacion ni al uso de objetos
* Si no la explicitamos una superclase, hereda de object 

``` 
class ComederoRecargable inherits Comedero{
  var stockDeRaciones = 0 
  method tieneRaciones() = 
    stockdeRaciones > 0 

  method recargar (cantidadDeComida){
    stockdeRaciones += self.cantidadDeRaciones(cantidadDeComida)
    }
  method cantidadDeRaciones(cantidadDeComida) = cantidadDeComida.div(racion) 

 // Piso comportamiento de la clase de la cual hereda  
  override method cosumoEnergetico() = stockDeRaciones * 2  
// super lo que hace es que vaya a buscar a la clase padre 
  override method esUtilParaAtender(animal) = 
    self.tieneRaciones() && super(animal)
  }
