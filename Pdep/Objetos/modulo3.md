# Modulo 3 

## Introduccion Polimorfismo 

> El observador puede cambiar la reerencia sin preocuparse por el envio del mensaje que permanece sin cambios . Entonces esos objetos referenciados son polimorficos con el observador 

Alcanza con que al menos 1 de los mensajes en comun 

``` wollok

object galvan {
      var sueldo = 15000
      method sueldo() = sueldo
      method sueldo(_sueldo) { sueldo = _sueldo }
}

object baigorria {
     var cantidadEmpanadasVendidas = 100
     var montoPorEmpanada = 15
     method venderEmpanada() {
     cantidadEmpanadasVendidas = cantidadEmpanadasVendidas + 1 
     }
                      
     method sueldo() = cantidadEmpanadasVendidas * montoPorEmpanada
}
object gimenez {
     var dinero = 300000
     method dinero() = dinero 
     method pagarA(empleado) { dinero = dinero - empleado.sueldo() }
}

```

