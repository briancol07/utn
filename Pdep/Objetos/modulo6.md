# Modulo 6 

## Repaso 

``` wollok 

object daniel {
  method llevaA(pasajero) = !pasajero.esJoven()


}
```
## Pruebas 

``` 
import choferes.* 

describe "Test de Daniel"{
  test "Daniel no lleva a un pasajero joven"{
      const pasajeroJoven = object{
        method esJoven() = true}
      assert.notThat(daniel.llevaA(pasajeroJoven))
    }
  test "Daniel lleva a un pasajero que no es joven"{
    const pasajeroViejo = object{
      method esJoven() = false}
    assert.that(daniel.llevaA(pasajeroViejo))
  }

  
}

```
