# Fisica 1 

> Resumen de la materia , cursada con el profesor Civetta , anio 2020
>
> En este archivo sera toda la teoria , en otro usare para practica.
> 
## Temas:

* Optica 
* Cinematica 
* Dinamica


## Optica 

Variable | Significado
---------|------------
x        | Distancia al objeto real  
$`x\prime`$| Distancia a la imagen
y        | Altura del objeto real
$`y\prime`$ | Altura de la imagen
 
### Reflexion   

1. Refraccion $`n_{1} .\sin i =  n_{2} .  \sin j `$
2. Formula de Descarte : $`\frac{1}{x} + \frac{1}{x'} = \frac{1}{F}`$ 
3. Aumento lateral:  $` A=\tfrac{y\prime}{y} = \tfrac{-x\prime}{x}`$ 
4. Formula de Gauss : $` \tfrac{1}{x} - \tfrac{1}{x\prime} = \tfrac{1}{F}`$
5. Aumento lateral:  $` A=\tfrac{y\prime}{y} = \tfrac{x\prime}{x}`$
6. Potencia: $`P=\frac{1}{F}`$ 

### Elementos Principales de espejos esfericos 
> Pueden ser espejos concavos ) o convexos (
> 
* Centro de curvatura "c"
* Vertice del espajo "v"
* Foco del espejo "F"

1. Distancia focal : $` f = \tfrac{r}{2}`$ 

### Rayos Principales
> inserte imagen 

* Imagen virtual: Se forma por una prolongacion de rayos
* Imagen real: Se forma por interseccion de rayos . En una pantalla

### Espejos convexos

> Inserte imagen 

* Imagen 
	* Virtual 
	* Menor 
	* Derecha 

### Espejos Concavos
> Inserte imagen 
$` x > C > F > V`$

* Imagen 
	* Real 
	* Menor 
	* Invertida 

> Inserte imagen 

$`x=C>F`$

* Imagen 
	* Real 
	* Igual 
	* Invertida

> Inserte imagen 

$`C>X>F`$

* Imagen 
	* Real 
	* Mayor 
	* Invertida

$`C>X=F>0`$

* No se forma imagen . El objeto se encuentra sobre el foco 
> Inserte imagen
$`C>F>X`$ 
* Imagen 
	* Virtual 
	* Mayor 
	* Derecho 


### Lentes  

* Convergentes 
> Inserte Imagen
* Divergentes 
> Inserte Imagen
1. $`X=2F`$ 
	* Imagen 
		* Igual 
		* Real 
		* Invertida  
2. $`F<X<2F `$
	* Imagen 
		* Mayor
		* Real 
		* Invertida 
3. $`X>2F`$ 
	* Imagen 
		* Menor
		* Real 
		* Invertida
4. $`X<F`$
	* Convergente 
		* Imagen 
			* Mayor 
			* Virtual
			* Derecha 
	* Divergente
		* Imagen 
			* Menor
			* Virtual 
			* Derecha   

## Cinematica 

### Movimiento Rectilineo Uniforme (MRU)

Ecuaciones
 $`\left\{ \begin{array}{c} 
	X=X_{0} + v . t \\
	V=cte  \\
	A=0
\end{array}
\right.
`$
### Movimiento Rectilineo uniformemente variado (MRUV) 

Ecuaciones $`\left\{\begin{array}{c}
X_{(t)} = X_{0} + V_{0} . t + \frac{1}{2} . a . t^2 \\
V=V_{0} + a .t \\
a=cte
\end{array} 
\right.
`$
### Moviento Relativo 

$`V_{m} - t = V_{m}-a+V_{a}-t `$ 

### Movimiento circular (MC) 

* $`\alpha`$ : Angulo en Radianes 
* $`\omega`$ : Velocidad Angular 
* $`\gamma`$ : Aceleracion Angular 

Velocidad Angular media:  $`\omega_{m} =\frac{\Delta\alpha}{\Delta t} `$
</br>
Aceleracion Angular media: $`\gamma_{m} = \frac{\Delta\omega}{\Delta t}`$
</br>
Velocidad instantanea: $`\omega_{i} = \frac{d\alpha}{d t} `$
</br>
Aceleracion Instantanea: $`\gamma_{i} = \frac{d\omega}{d t} `$ 

$`\boxed{ \omega = 2 .\pi . f 
\omega = 2 . \pi . \frac{1}{t}
\Tau = 2 . \frac{\pi}{\omega}
`$ 
### Relacion entre aceleracion y movimiento escalar 
$`V=\omega . r`$ 

### Relacion entre aceleracion angular y aceleracion escalar 

$`\gamma=\frac{a}{r}\iff\alpha=\gamma . r `$

### Velocidad angular 
$`\omega = \frac{v}{r}`$
$`\left\{ \begin{array}{c}
   a=a_{t} + a_{n}\\
   a_{t}=\gamma . r . \tau  \\
   a_{n}=\omega^2 . r . (n)
 \end{array}
 \right.
 `$
### Aceleracion Centripeta 

$`A_c = W^2 . r  \lor A_c = \frac{v^2}{R}`$

## Dinamica 

### Leyes de Newton 
* Si $`\vec{F} = 0 \Rightarrow a=0 \Rightarrow v = cte`$
* $`\vec{F} = m * a \Rightarrow \Sigma \vec{F} = m . \vec{a}`$
* $`\vec{F_{mia-sobrecuerpo}} = \vec{F_{Cuerpo-Sobremi}}`$ 

### Recordar
$`\boxed{P_x=P.\sin\alpha} `$
$`\boxed{P_y=P.\cos\alpha}`$

### Fuerza de Rozamiento Estatico 

$`F_{re}\leq F_{re_{max}} = \mu_e n `$ 

### Fuerza de Rozamiento Dinamico 

$`F_{d}\leq\mu_d n`$

### Fuerza Elastica 

$`F_e = K . \Delta_x `$

* Fuerza elastica en serie 
	* $`\frac{1}{keq} = \frac{1}{k1} + \frac{1}{k2} + \frac{1}{kn}`$
* Fuerza elastica en paralelo	
	* $`k_{eq} = k1 + k2 + kn `$ 

