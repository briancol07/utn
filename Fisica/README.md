# Fisica 1 

## Temario <a name="temario"></a>

* Optica 
* Cinematica 
* Dinamica 
* Moa
* Fluidos 

## Links <a name="links "></a>

* [Guias-mas](https://drive.google.com/drive/folders/1yvyHsV-Q-XoE7x6FadvHwIeKaYMENhhv)
* [Ejercicios-Resueltos](https://www.utnianos.com.ar/foro/tema-frba-%E2%96%BA-f%C3%ADsica-i-%E2%96%BA-ejercicios-resueltos-las-gu%C3%ADas-de-tps)

