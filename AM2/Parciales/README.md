# Parciales 

## Temas 1er Parcial 

* Conjunto y funciones escalares 
* Limite y continuidad
* Derivabilidad 
* Diferenciabilidad 
* Composicion e implicita 
* Diferenciabilidad de orden superior 
* Extremos locales 
* Extremos condicionados y absolutos 
* Funcioes vectoriales 
* Curvas 
* Superficies 
* Ecuaciones diferenciales 1
* Ecuaciones diferenciales 2


## Temas 2do Parcial

* Integrales Dobles 1
* Integrales Dobles 2
* Integrales Triples 1
* Integrales Triples 2
* Integrales de linea
* Trabajo - Teorema Green 
* Campos Conservativos 
* Integrales sobre superficie 
* Flujo - Teorema de Gauss
* Teorema de Stokes 
* Ecuaciones Diferenciales 3
* Ecuaciones diferenciales 4
