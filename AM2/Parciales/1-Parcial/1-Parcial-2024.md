# Practica 

1. Resolver la ecuacion $` y - 2y^{\prime} = 4 `$ tal que $`y(0) = 0`$ $`y^{\prime}(0) = 0`$
2. Hallar la derivada direccional maxima de $`z = f(x,y)`$ en le punto (2,1) siendo que f se encuentra definida implicitamente por $`y + xz + z + \ln(z - xy) = 10`$ Indicar la direccion correspondiente 
3. Calcular mediante aproximacion lineal $`h = fog`$ en el punto (0.99 , 1.02) cuando f(u,v) queda definida por $`z- u^2 + v^2 + \ln(v,z) = 0 `$ siendo $`\bar{g}(x,y) = ( x y^2 , y - x^2 ) `$
4. Hallar la recta tangente a la curva interseccion de las siguientes superficies $`x^2 + y^2 + z^2 = 9`$ y  $`2x+ y = 0`$ en el punto ( 1,-2,2). Hallar la interseccion de dicha recta con el plano xy 

# Teorico 

1. Dado el campo escalar definido por 

$$
x = \begin{cases}
   \frac{y^3}{x^6 + y ^3}  &\text{si } (x,y) \neq (0,0) \\
   0  &\text{si} (x,y) = (0,0) 
\end{cases}
$$

Indicar si las siguiente preposiciones son verdaderas o falsas, justificar 

a) El campo escalar es discontinuo en (0,0) <br>
b) El campo escalar es derivable en todas direccion en (0,0)

2. Definir maximo local de una funcion escalar de n variables 
3. Analizar la existencia de extremos locales de la funcion $`f(x,y) = 3 - 2x^3 - 4y^4`$ en su dominio

