# AM2

## Links 

* [Resumen](https://www.utnianos.com.ar/foro/tema-%C3%ADndice-am-ii-todo-para-aprobar)
* Clases 
  * [Sebastian Stefanini](https://www.youtube.com/channel/UCijLrJPTR8WS7iKGa3Vfuqw)
  * [Ayudante](https://www.youtube.com/@milagrosflorencio8665)

## Clases 

1. Conjuntos y funciones escalares 
2. Límite y continuidad 
  * [limite](https://www.youtube.com/watch?v=hyHzu4_mAr8)
  * [Continuidad](https://youtu.be/XrCkckHXTS0)
3. Derivabilidad
  * [Derivabilidad](https://youtu.be/MuhxZbpzC5A)
  * [Derivabilidad-Orden-Superior](https://youtu.be/jcffGpaf1cU)
4. Diferenciabilidad
  * [Diferenciabilidad](https://youtu.be/nyUKcK-f8yU)
  * [Aprox-lineal](https://www.youtube.com/watch?v=5zXsaKjqFCU)
5. Composión e implícita 
  * [Composicion](https://www.youtube.com/watch?v=xEoZEDwrt-w)
  * [Implicitas](https://youtu.be/izGENSPpZ78)
6. Diferenciabilidad de orden superior
  * [Diferenciabilidad-orden-sup](https://youtu.be/xXE3vmjXWN0)
7. Extremos locales
  * [Extremos locales](https://www.youtube.com/watch?v=A4YAQIr4OXc&feature=youtu.be)
8. Extremos condicionados y absolutos
  * [Extremos condicionados](https://www.youtube.com/watch?v=3mJbI7xYkBQ)
9. Funciones vectoriales
  * [Funciones-Vectoriales](https://youtu.be/4bMQzq52oO4)
10. Curvas
  * [Curvas](https://youtu.be/LCx9-YjxTgE)
  * [En-Plano](https://youtu.be/fnxbvJpuF-4)
11. Superficies
  * [Superficies](https://youtu.be/q4B01KAQL2k)
12. Ecuaciones diferenciales I
  * [Ecuacion Dif](https://youtu.be/ZXFsUIyYYvs)
13. Ecuaciones diferenciales II
14. Simulacro 
15. 1º Evaluación
16. Integrales dobles I 
  * [Integrales](https://youtu.be/Gz884lGtXrk)
17. Integrales dobles II
  * [Integrales2](https://youtu.be/H4IQhAS0y9I)
  * [Integrales3](https://www.youtube.com/watch?v=v6XSV4ohEtY)
18. Integrales triples I
  * [Triples](https://youtu.be/yraeybjD3jg)
19. Integrales triples II
  * [Triples2](https://youtu.be/GkH_TKIVu8M)
  * [Longitud-de-una-curva](https://youtu.be/xkmXvsaaR48)
20. Integrales de línea
21. Trabajo - Teorema de Green
22. Campos conservativos
23. Integrales sobre superficie 
24. Flujo - Teorema de Gauss
25. Teorema de Stokes
26. Ecuaciones diferenciales III 
27. Ecuaciones diferenciales IV
28. Simulacro
29. Simulacro
30. 2 eval 
31. 1 recu (1er parcial)

