# Unidad 1 

* Espacio Muestral
* Definicion Probabilidad 

> Def: Experimento: Es toda accion o procedimiento que origine resultado 

* Experimento $`\varepsilon`$:
  * Deterministico (Un resultado posible)
  * Aleatorios (Mas de un resultado)

> Def: Se llama espacio muestral al conjunto de resultados posibles de un explerimento $`\varepsilon`$. Denotaremos con **S** al espacio muestral asociado a $`\varepsilon`$.

### Ej1:

$`\varepsilon`$: Se arroja 1 dado y se observa que salio 

$` S = {1,2,3,4,5,6}`$

### Ej 2


$`\varepsilon`$: Se arroja 2 dados y se calcula la suma de los numeros.

$` S = {2,3,4,5,6,7,8,9,10,11,12} `$

<br>
> Def: Sea $`\varepsilon`$ un experimento y S su espacio muestral. Se llama suceso o evento a cualquier subconjunto del espacio muestra. Lo denotaremos en mayusculas.

### Ej 3 

2 Conjuntos 

* A = {2,4,6,8,10,12}
* B = {7,8,9,10,11,12}
* Si a y b son sucesos:
  * $` a \cap b = suceso => {8,10,12} `$
  * $` a \cup b = suceso => {2,4,6,7,8,9,10,11,12} `$
  * $` a^c = suceso => a^c = {3,5,7,9,11} `$ 

> Def : Sea $`\varepsilon`$ un experimento y S su espacio muestral. Se llama probabilidad en S a cualquier funcion $` P: \mathbb{A} -> \R `$

* P(S) = 1 ---> Seria como 100%
* P(a) >= 0 ---> $`\forall x \subset S`$
* $` A_{1} \cap A_{2} = \varnothing => P(A_{1} \cup A_{2}) = P(A_{1}) + P(A_{2}) `$

> $` P(A) = \frac{|a|}{|S|} `$

### Ej 9

Se eligen al azar 3 motores de un conjunto de 10 entre los cuales 4 son a Inyeccion hallar probabilidad que:

1. Ninguno sea a inyeccion
2. A lo sumo uno sea a inyeccion
3. Almenos 2 sean a inyeccion

Espacion Muestral: {I-I-I-I-M-M-M-M-M-M}

1. P(ninguno sea a inyeccion) = Cant de formas de ninguno a inyeccion / Cant.de formas a legir 3 motores 

 6 x 5 x 4  ---> Tomo uno quedan 6 , tomo otro 5 y luego 4 (porque elijo 3)
 ---------
 10 x 9 x 8 ---> Si tomo el primero son 10 tomo 1 queda 9 otro 8 

2. P(Alo sumo uno a inyeccion) = P (ninguno a inteccion o exactamente 1 a inyeccion) =

P(Ninguno a inyeccion) + P(Exactamente 1 a inyeccion) = 


$` \frac{6*5*4}{10*9*8)} + 3*\frac{6*5*4}{10*9*8)} `$

Hay 3 formas de que se elijan 1 a inyeccion

I M M => 4 x 6 x 5
M I M => 6 x 4 x 5
M M I => 6 x 5 x 4

----

<br>
## Probabilidad Condicional 

> Def: Sea a,b incluidos en S con el simbolo a|b leido "a dado b" denotamos el suceso a sabiendo que ocurrio el suceso b



