# Resumen PPT 

Necesidad de utilizar los datos luego de apagar la VM o otro, un medio durable.
Tambien forma de compartir informacion entre procesos independientes, transmitirlos atraves de un almacenamiento temporal

## Formas de persistir 

* Archivos 
  * Formatos diversos 
  * Informacion poco ordenada o inconsistente 
  * Acceso potencialmente lento 
* RDBMS 
  * Estructuras formalizadas (tablas) y consistentes 
  * Motor dedicado a escribir y consultar datos 
  * Acceso optimizable 
* No-Relacional ( no-SQL )
  * Otros Paradigmas 
  * Mejor manejo de volumenes de datos 
  * Consistencia relajada 
  * Ventajas en velocidad y redundancia de datos 

## Decisiones de persistencia 

* Que persistir?
* Cuando persistir?
* Como persistir ?
* Cuando desnormalizar? 

## Propiedades de una relacion 

* No existen en ella tuplas repetidas 
* Las tuplas no estan ordenadas 
* Los atributos no estan ordenados 
* Todos los valores de los atributos son atomicos 

## Tipos de relaciones 

* Reales , tablas fisicas 
* Resultado de consultas
* Vistas 
* Snapshots 
* Otros 

## PK ( Primary Key ) 

* Debe identificarse univocamente 
* Atributo o conjunto de tributos del registro
* Unicidad 
  * No deben repetirse 
* Minimalidad 
  * Combinacion menor posible 
* naturales 
  * Pertenece naturalmente al registro
* Subrogadas 
  * Agregado artificialmente 
* Nunca pueden ser nulas , deben ser indistinguibles e inidentificables 
* Integridad referencia 
  * Debe apuntar a elementos que exitna en la tabla a la cual referencias 

## FK ( Foreign Key ) 

* Asociar / relacionar entidades
* Es un valor en el registro que es igual al de la clave primaria del registro de otra tabla 
* Integridad referencial 
  * Los registros apuntados por FK no pueden ser borrados hasta tanto ya no sean apuntados por nadie 

## Normalizacion 

* Minimizar redundancias 
* Mejorar la cohernecia y abstraccion de los datos
* Para 
  * minimizar inconsistencias 
  * Facilitar mantenimiento
  * Optimizar espacio
  * Optimizar velocidad 

### Formas 

* Canonica 
  * Hacernos ruido almacenar valores calculados 
* Primera forma ( FN1 )
  * Esta forma nos indica que no debemos tener atributos multivaluados 
* FN2
  * Nos indica que los atributos no clave deben depender de toda la clave 
* FN3 
  * Esta forma nos indica que los atributos no clave deben depender directamente de la clave y no transitivamente atraves de otros atributos 

## Denormalizacion 

* Aveces hay que ir encontra de la normalizacion por cuestiones de performance 
* Esto va a implicar que deberemos generar mecanismos para controlar la consistencia 
