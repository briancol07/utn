# Introduccion a arquitectura web 

Entre los estilos arquitectonicos, los dos primeros que nos serviran para entender la arquitecturas web son el centralizado ( o monolitico ) y descentralizado ( o distribuido ). 

* Monolitico 
  * codigo se ejecuta en mismo ambiente o vm 
* Descentralizado
  * Habra partes que se ejecuten en otros ambientes, quizas incluso otras computadoras 

## Arquitectura web 

Distribuidas en dos tipos de nodos, clientes y servidores , llamada cliente-servidor, los cliente se comunican con el servidor siguiendo un protocolo de pedido-respuesta 

La comunicacion se realiza atraves de internet utilizando el protocolo http 

## Como es? 

```
(Pedido)->Respuesta 
```
### Respuesta 

```
(CodigoDeRespuesta, Cabeceras, cuerpo)
```

## Pedido 

* Atributos 
  * Parametros 
  * Una URI/URL ( Identificacion/localizacion del contenido)
    * URI : Uniform resource identifier ( Identificador generico )
      * URL (uniform Resource locator ) Subconjunto URI : identifica un recurso por su ubicacion 
        * Se debe especificar donde se encuentra un recurso y el mecanismo para acceder a el 
  * Metodo que da semantica 

### Metodos 

* Get ( no tiene efecto ) : consultar 
* PUT , PATCH , DELETE operaciones con efecto ( Actualizar totalmente , Actualizar parcialmente, Eliminar)
* POST : efecto generica , crear informacion nueva en el servidor 

Con la url lo localizamos de forma univoca 

## Forma de diseniar URL 

## Orientado a procedimientos 

Cada ruta modela una accion, eventualmente parametrizada, donde la semantica de la operacion es propia de cada modelo de dominio 
`/comprar?idProducto=456&idZona=48`

## Orientado a recursos 

Cada rut apunta a un recurso bien definido, siguiendo algunas convenciones bien conocidas. `/productos/45` Con un get se obtiene el producto pero con delete lo elimina 

