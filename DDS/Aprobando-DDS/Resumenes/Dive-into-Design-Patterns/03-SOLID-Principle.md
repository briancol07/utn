# SOLID

## Single responsibility principle

> A class should ave just one reason to change 

* Reduce complexity 

## Open / Closed principle 

> Classes should be open for extension but closed for modifications 

* Open -> Extend <- New things in sub-classes 
* Closed -> Modifications <- Change code 

## Liskov Substitution principle 

> Extending a class, should be able to pass objects of hte subclass in place of objects of the parent class without breaking the client code 

Subcass should remain compatible with the behavior of the superclass 

* Parameters type subclass match superclass
* Returning type should match 
* No new error types
* No strengthen pre-conditions
* No weaken pre-conditions 
* No change values of private fields of the superclass 

## Interface segregation principle

> Client should not be forced to depend on method they do not use 

## Dependency inversion principle 

> High level classes shouldn't depend on low-level classes. both should depned on abstractions. Details should depend on abstractions 

* Low level class 
  * Basic operations 
    * Working with disk 
* High level class 
  * Complex business logic  
