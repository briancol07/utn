# Introduction to Design Patterns 

## What's a Design patter?

Are typical solutions to commonly ocurring problems in software design , Blueprints that you can customize to solve a recurring design problem. 

## What does the pattern consist of? 

* Intent ;      Briefly describes the problem and solution 
* Motivation:   Futher explanation 
* Structure:    Classes ashow each part of the pattern and how they are related 
* Code Example: In one of the popular programming languages makes it easier to grasp the idea behind the pattern  


## Classification 

* Basic and low-level are called `idioms`, apply to a single programming language 
* Architectural pattern, apply for any language
* Intent or purpose 
  * Creational Patterns: Provide object creation mechanisms that increase flexibility and reuse existing code 
  * Structural Pattern: Explain how to assemble objects and classes into larger structures ( keeping structure flexible and efficient ) 
  * Behavioral patterns: Effective communication and the assignment of respoinsibilities between objects 

29
