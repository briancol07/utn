# Basics of OOP

## Index 

* [## Introduction ](#intro)
* [## Class hierarchies ](#class)
* [## Pillars of OOP ](#pillars)
  * [### Abstraction ](#abstraction)
  * [### Encapsulation ](#encapsulation)
  * [### Inheritance ](#inheritance)
  * [### Polymorphism ](#polymorphism)
* [## Relations between objects ](#relations)
  * [###  Association ](#association)
  * [### Dependency ](#dependency)
  * [### Composition ](#composition)
  * [### Agregation ](#agregation)

## Introduction <a name="intro"></a>

Is a paradigm based on te concept of wrapping pieces of data, and behavior related to that data, into special bundles called objects. Which are constructed from a set of "blueprints", defined by a programmer called classes.

![Class-diagram](./img/Example-class.png)

Data Stored inside the object's fields is often referenced as state, and all the objects methods define its behavior 

So a class is like a blueprint thatdefines the structure for objects, which are concrete instances of the class 

## Class hierarchies <a name="class"></a>

A parent class is called a superclass. Its children are subclasses this inherit state and behavior from their parent, defining only attributes or behaviors that differ.

![Inherit](./img/inherit.png)

Subclasses can override the behavior of methods that they inherit from parent classes.

## Pillars of OOP <a name="pillars"></a>

* Abstraction
* Encapsulation 
* Polymorphism 
* Inheritance 

### Abstraction <a name="abstraction"></a>

You shape objects of the program based on real-world objects. The objects only model attributes and behaviors of real objects in a specific context, ignoring the rest 

> Abstraction is a model of a real-world object or phenomenon limited to a specific context, which represents all details relevant to this context with high accuracy and omits all the rest. 

### Encapsulation <a name="encapsulation"></a>

> Ability of an object to hide parts of its state and behaviors from other objects, exposing only a limited interface to the rest of the program 

* Private : Accessible only from within of the method of its own class 
* Protected: Makes a memeber of a class available to subclasses as well 

![Interface](./img/Interface.png)

### Inheritance <a name="inheritance"></a>

> Ability to build new classes on top of existing ones. 

* Advantage:
  * Code reuse 

If you want to create a class that's slightly different fro man existing one, extend the existing class and put the extra functionality into a resulting subclass

![inheritance](./img/inheritance.png)

If a superclass implements an interface, all of its subclasses must also implement it 

### Polymorphism <a name="polymorphism"></a>

We can create an abstract superclass, so the subclasses have to implement the method of their father. 

![Polymorphism](./img/Polymorphism.png)

> Ability of a program to detect the real class of an object and call its implementation even when its real type unknown in the current context.

## Relations between objects <a name="relations"></a>

###  Association <a name="association"></a>

One object uses or interacts with another, Simple arrow drawn from an object and pointing to the object it uses. 
It can be bi-directional asssociation.

![Association](./img/Association-arrow.png)

### Dependency <a name="dependency"></a>

Is a weaker variant of association that usually implies that ther's no permanent link.Implies that an object accepts another object as a method parameter, instantiates or uses another object 

![Dependency](./img/dependendency.png)

### Composition <a name="composition"></a>

One is composed by one or more instances of the other. The distinction between this relation and others is that the component can only exist as a part of hte container.

![Composition](./img/composition.png)

### Agregation <a name="agregation"></a>

Is a less strict variant of composition, Where one object merely contains a reference to another. The container doesn't control the life cycle of hte component. Can exist without the container and can be linked to several containers at the same time. 

![Aggregation](./img/aggregation.png)
