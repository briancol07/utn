# Resumen Mapeo Objeto/Relacional 

* Impedance mismatch 
  * Y como podemos conciliarlas 
* Mapeo a traves de un framework ORM ( JPA / Hibernate ) 

## Impedance mismatch 

Tanto las interfaz como las estrategias no tienen referencias, no guardan estado y al ser stateless lo unico importante para el modelo relacional es saber cual es el criterio

La herencia nos puede servir a la hora de hacer relaciones entre tablas 

Las variables de clase ( static o companion ) no se pueden asociar a un regirstro por que son globales a todos los objetos 

* Para este tema tendremos que solucionar algunas cuestiones 
  * La identidad 
  * Los tipos de datos 
  * Las relaciones ( Cardinalidad ) 
  * Los tipos de colecicon ( manejo de duplicados y orden ) 
  * La direccion ( forma de navegar ) 
  * La herencia 

## Manejo de identidad 

* Creacion de claves,
  * Natural 
  * Subrogada 

### Conversiones de tipo

* String sin limitaciones pero Varchar si tiene longitud
* Rango para los decimales 
* Fechas tienen tipos no siempre compatibles 
* Tipos booleanos 
* Tipos enumerados 

## Hidratacion 

La hidratacion del grafo de objetos es el proceso de cargar datos desde una base de datos a una fuente remota y convertirlos en objectos completos en memoria incluyendo sus relaciones 


* Carga Selectiva 
* Carga diferida ( lazy loadingg ) 
* Carga anticipada ( eager loading ) 
* Carga parcial

### Lazy association 

Voy a traer la informacion cuando la necesite 

## implementaciones 

### Data Mapper 

Tener un objeto que modela el acceso a los datos: el DAO ( data access object ) 
* Interfaz 
  * Insert
  * Update 
  * Delete 
  * GetClientes 
  * GetCliente 

### Active record 

Otro aproach que se popularizo ( frameworks ruby on rails o grails ) , es decorar a los objetos de dominio agregandoles las responsabilidades de save() , update(), delete(), y metodos de clase que resuelvan las busquedas 

## Mapeo manual

* Ventajas 
  * Control sobre el algoritmo 
  * En queries complejas tengo ventaja por que puedo trabajar a mismo nivel de sql 
  * Puedo modificar la estrategia de hidratacion de objetos 
* Desventaja 
  * Es un trabajo tedioso y repetitivo 
  * Devbes definir el nivel de profundidad de lo que vamos a traer para cada uno de los casso 
  * Dificulta reutilizacion 
  * Trabajo de bajo nivel 

## Mapeo con framework 

* Permiten subir el grado de declaratividad 
* Elimina parte algoritmica de la conversion 
* Se define un lenguaje de alto nivel para efectuar consultas 
* El mapeo se resuelve con 
  * Archivos de configuracion
  * Annotations en los mismos objetos de dominio ( springboot / jpa ) 
  * Definiendo un domain specific language o DSL

## Que viene primero ( huevo o la gallina ) 

1. Generaremos modelo relacional y luego adaptamos el modelo de objetos en base a las tablas generadas 
2. Generamos el modelo de objetos y en base a este crean las tablas 

### Opcion 1 

Supone que es mas importante la forma en que guardo los datos que las reglas de negocio que modifican esos datos. 

Para sistemas enlatados o OO es uno de los multiples sistemas 

### Opcion 2 

Objeto prevalece ante el esquema que se persiste. 

## Conclusiones 

* Manejo de la identidad 
* Relaciones entre entidades 
  * Cardinalidad 
  * Direccion
* Relacionar los dominio de la base vs los tipos que definen los objetos 
* Como manejar las collecciones ( orden y duplicados ) 
* Que estrategia adopatar para los casos de generalizacion ( herencia ) 
* cual es la recion entre los ciclos de vida de cada entidad ( Cascada ) 
* Que indices podemos agregar para que la informacion  sea ccesibble de forma mas rapida 
* Ademas de las decisiones comunes a cualquier esquema de persistencia hidratacion de los objetos persistencia del grafo en el medio 
