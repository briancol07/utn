# SOLID

* **S**ingle Responsibility Principle 
* **O**pen/closed principle 
* **L**iskov substitution Principle
* **I**nterface segregation Principle
* **D**ependency inversion principle

## Libros 

Agile Software development, Priciples, Patterns and Pratices ( Robert Martin )


## Single Responsibility Principle 

> Try to make every class responsible for a single part of the functionality provided by the software and make that responsibility entirely encapsulated by the class 

If a class does too many things, you have to change it every time one of thse things changes.While doing that, you're risking breaking other parts of the class which you didn't even intend to change.

> TODO:insert diagram  
## Open CLosed Principle 

> Classes should be open for extension but closed for modification.

The main idea of this principle is to keep existing code from breaking when you implement new features. 
This principle isn't meant to be applied for all changes to a class. If you know that there's a bug in the class just go on and fix it. DOn't create a subclass for it. A child class shouldn't be responsible for the parent's issues. 

> TODO:insert diagram

