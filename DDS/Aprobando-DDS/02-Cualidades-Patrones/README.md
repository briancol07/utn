# Clase 2 

## Index

* [## Material](### Material)
* [## Tarea](### Tarea)

> Cualidades de disenio, patrones de disenio y manejor de errores, Patron : strategy y template method

## Material <a name="Material"></a>

- [ ] Presentacion
- [ ] Patrones: Template y Strategy 
- [ ] Cualidades de disenio
  - [ ] Guia para comunica un disenio
  - [ ] Las entradas del disenio
- [ ] Disenio del manejo de errores 
  - [ ] Excepciones java
- [ ] Macowins 

## Tarea <a name="tarea"></a>

- [ ] Introduccion a los patrones de Disenio
- [ ] Ejercicio QMPG
