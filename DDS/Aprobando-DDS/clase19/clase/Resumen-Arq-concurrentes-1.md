# Resumen arquitecturas concurrentes ep 1 


* [link](https://medium.com/arquitecturas-concurrentes/arquitecturas-concurrentes-episodio-1-el-diablo-est%C3%A1-en-los-detalles-692766ac669b)

> El diablo esta en los detalles 

## Sobre la arquitectura 

* Disenio logico de alto nivel
  * Modulos , nodos , red 
* Disenio de Aspectos dificiles de cambias 
  * Tecnologias de base, lenguaje, etc 
* Disenio Fisico:
  * Seleccion de HW y despliege SW 

## Haciendo arquitectura 

* Iterativo 
* Constructivo 
* Verificable 
  * Si no se puede validar de forma rapida entonces el proceso esta fallando 
* Holistico 
  * Economicas 
  * Politicas 
  * Interpersonales 
* Potenciado por la tecnologia 
  * Pensar soluciones no comprar lo primero 
* Enriquecido por hsotia 

## Cualidades arquitectonicas 

> simple 

* Permiten desarrollar buen SW 
  * Libre duplicaciones
  * Minimice redundancia 
  * Facil de probar 
  * Facil de extender 
  * Facil de mantener 

