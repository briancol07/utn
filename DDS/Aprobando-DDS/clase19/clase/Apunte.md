# Clase 19 Arquitectura 

* Diagramas 
  * UML 
    * Diagrama componentes 
    * Diagrama Paquetes 
    * Diagrama Despliegue 
  * Diagrama Stack 
    * Tecnologias 
    * Lenguajes 

## UML Despliegue 

### 1. Nodo 

![Nodo](../img/nodo.png)

### 2. BBDD

![bbdd](../img/bbdd.png)

### 3. Red 

![Red](../img/Red.png)

### 4. Internet 

![Internet](../img/Internet.png)

### 5. Load Balancer 

![lb](../img/lb.png)

 
### 6. Cola mensajes 
![cm](../img/cm.png)

## Arquitectua Cliente servidor 

![Cliente-Servidor](../img/Cliente-Servidor.png)

## Arquitectua WEB

![web](../img/web.png)

## Arquitectua Microservicios 

![ms1](../img/ms1.png)
![ms2](../img/ms2.png)


## Plataformas de despliegue 

* Bare metal = On premise 
* Nube 
  * IaaS
  * PaaS
  * SaaS



