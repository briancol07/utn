# Resumen PPT 

## Herencia en ORM 

No se puede usar el super tipo ni subtipo

Alternativas usando FK's
* Single table 
* Joined 
* Table per Class

## Single table 

Una sola tabla , campos nullables, Discriminador 

## Joined 

* Una tabla para la clase padre 
* Una tabla para cada clase hija 
* Pks de clases hijas son FK a clase padre 

## Table per class 

* Una tabla para cada clase concreta 
* Campos de la super clase duplicados 
* Al apuntar a 2 tablas la Fk desde otras entidades se pierde 
* No se puede indexar 

## Herencia 

* Como Compararlas 
  * Distribucion de atributos 
  * Nullabilidad de campos 
  * Consultas polimorficas 
  * Integridad referencial / indices 

### Consultas polimorficas 

Son las que acceden a un objeto tipado como la clase padre, sin saber el tipo concreto de la instancia finalmente devuelta 

--| Single Table | joined | Table per class 
--|--------------|--------|----------------
Distribucion de atributos | Todos juntos, incluso si no se usan | Comunes arriba y especificos abajo | Atributos comunes duplicados 
Campos Not NULL | No admite en campos no comunes | Admite | admite 
Integridad Referencial/indices | Permite FK/indices | Permite | No Permite 
Consultas Polimorficas | No requiere joins extra (costo) | Utiliza left joins por cada subclase | Utiliza unions por cada suclase 

### Mapped Superclass 

* Mapea los atributos de la clase padre a las tablas de las clases hijas 
* No permite consultas polimorficas 
* No se puede apuntar a una mapped superclass 
