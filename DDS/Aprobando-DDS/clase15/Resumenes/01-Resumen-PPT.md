# Resumen PPT 

MVC : Modelo -> Vista -> controlador 

* Estilos 
  * Cetralizado 
  * Distribuido 


## La web 

Hypertext : La forma mas habitual de hipertexto es la de hipervinculos o referencias cruzadas automaticas que van a otros documento 

> HTML : Hyper text markup language 

## Cliente Servidor 

```
Cliente -> Pedido -> Servidor 
        <- Respuesta <-
```

* Hay un servidor que centraliza la informacio 
* Flujo es restringido 
* El cliente pued hacer pedidos de recursos 
* El servidor solo puede responder pedidos 
* El servidor no puede enviar informacion no pedida 

## HTTP 

> Hypertext transfer protocol 

* Orientado a texto 
* Stateless 

* Pedido 
  * Ruta: Metodo + URI 
  * Cabecera: Meta data  json 
  * Cuerpo  Datos del Pedido en si 
* Respuesta 
  * Codigo: Tipo de respuesta 
  * Cabecera: Metadata Json 
  * Cuerpo  Datos de la respuesta en si 

### Metodos + URI 

* Metodo 
  * Get : Leer recurso 
  * Post: Crear un recurso 
  * PUT: Modificar un recurso 
  * Delete: Eliminar un recurso 
* URI 
  * Ubicacion + recurso 

```
URI= |          Ubicacion      | Recurso 
      http://www.quemepongo.com/usuario/1
```
* Protocolo HTTP 
* Direccion IP : Identifica univocamente un servidor 
* Dominio: Nombre facil para una ip 
  * DNS ( Domain name system ) 

## Rutas Rest 

* Usan los metodos http 
* Los recursos tiene una forma similar a directorios 
  * Dar de alta un usuario -> POST /Usuario
  * Obtener un usuario por su id -> GET /usuarios/{id}
  * Obtener los amigos de un usuario -> Get /usuarios/{id}/amigos 
  * Eliminar un usuario -> Delete /usuarios/{id}

## Codigos de repuesta 

* 20X -> Exito 
* 30X -> Redirecciones 
* 40X -> Errores de cliente 
* 50X -> Errores del servidor 
