# Aprobando DDS

## Index

* [## Paginas Utiles](### Paginas Utiles)
* [## Temario](### Temario)
  * [### Primer Parcial](#### Primer Parcial)
  * [### Segundo Parcial](#### Segundo Parcial)

Creare los temas necesarios para aprobar la materia 

En los proximos apuntos ordenera el material de las cosas que necesito saber para poder aprobar la materia.

## Paginas Utiles <a name="paginas"></a>

* [Refactoring-Guru](https://refactoring.guru/design-patterns)

## Temario <a name="temario"></a>

### Primer Parcial <a name="primer"></a>

1. [ ] [Clase 1](./clase01/README.md)
2. [ ] [Clase 2](./clase02/README.md)
3. [ ] [Clase 3](./clase03/README.md)
4. [ ] [Clase 4](./clase04/README.md)
5. [ ] [Clase 5](./clase05/README.md)
6. [ ] [Clase 6](./clase06/README.md)
7. [ ] [Clase 7](./clase07/README.md)
8. [ ] [Clase 8](./clase08/README.md)
9. [ ] [Clase 9](./clase09/README.md)
10. [ ] [Clase 10](./clase10/README.md)

### Segundo Parcial<a name="segundo"></a>

11. [ ] [Clase 11](./clase11/README.md)
12. [ ] [Clase 12](./clase12/README.md)
13. [ ] [Clase 13](./clase13/README.md)
14. [ ] [Clase 14](./clase14/README.md)
15. [ ] [Clase 15](./clase15/README.md)
16. [ ] [Clase 16](./clase16/README.md)
17. [ ] [Clase 17](./clase17/README.md)
18. [ ] [Clase 18](./clase18/README.md)
19. [ ] [Clase 19](./clase19/README.md)
20. [ ] [Clase 20](./clase20/README.md)
21. [ ] [Clase 21](./clase21/README.md)
