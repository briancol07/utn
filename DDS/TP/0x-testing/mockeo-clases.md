# Mockeo de clases 

Aca utilizaremos mockito para que sea mas simple la parte de testing 

* Dependencias 
  * Junit 
  * Mockito core 

## Dependencias 

Necesitamos de ambas.

### Mocito-core 

```java 
<!-- https://mvnrepository.com/artifact/org.mockito/mockito-core -->
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>5.4.0</version>
    <scope>test</scope>
</dependency>
```

### junit 

```java
<!-- https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api -->
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-api</artifactId>
    <version>5.10.0</version>
    <scope>test</scope>
</dependency>
```

## Importar static method 

> Importaremos todos los metodos estaticos 

```java
import static org.mochito.Mochito.*;
```

## Creacion de la clase impostora 

nombre-actual sera la interfaz como clase que cumplira lo que nosostros querramos, la que estamos simulando por que nos faltan detalles

```java
nombreInterfaz nombre-actual = mock(nombreIntefaz.class);
when(valore);
```


