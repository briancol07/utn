# Refactoring 

## Que no es refactor ?

* Refactorizar no es incorporar funcionalidades nuevas 
* Refactorizar no es optimizar 

## Refactoring 

Es cambiar la estructura del codigo para hacerlo mas simple y extansible.

* Cambiar nombre variables 
* Reemplazar condicionales 
* Separar las clases 

## Busca 

* Metodos mas corto y con nombres bien definidos que revelen exactamente el proposito para el que fueron creados 
* Metodos y clases con responsabilidades claras y bien definidas (mayor cohesion)
* Evaluar costo de tener variables calculables 
* No tener god class ni managers 
* Objetos chicos 
* Evitar cilos de dependencia 
  * Evitar relaciones bidireccionales 


