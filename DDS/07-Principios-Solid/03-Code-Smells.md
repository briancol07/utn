# Code Smells 

Son seniales de que algo se puede mejorar 

## Codigo Duplicado

* Once and only once 
* Don't repeat yourself (DRY)
* Para evitar esto:
  * Composicion 
  * Herencia 

## Metodos largos 

* Debemos 
  * Identificar el objetivo que cumple 
  * Implementar esa abstraccion
* Los metodos generados pueden ser utilizado en otro contexto 
* Delegar en sub metodos 

## God class 

Son clases que poseen demaiadas responsabilidades, que estan fueremente acopladas a muchos otros objetos 

* Soluciones:
  * Generar nuevas clases 
    * alta cohesion 
  * Generar subclases 
  * Generar interfaces 

Si la God Class es resposable de una interfaz grafica, se debe comenzar a pensar en el patron MVC e ir desvomponiendo las responsagbilidades entre las diferets capas involucradas 

## Parametros largos 

Puede darse cuando varios algoritmos se combinan en un solo metodo . 

