# Principio Solid

Son principios basicos de programacion orientada a objetos, que nos ayuda a obtener mejores disenios implementando una serie de reglas o principios 

> Nos ayudan a evitar el codigo sucio

* Single responsibility Principle
* Open Closed Principle
* Liskov Substitution Principle
* Interface Segregation Principle
* Dependency Inversion Principle 


## Single responsibility Principle

Cada clase debe tener responsabilidad sobre una sola parte de la funcionalidad del SW (Clases cohesivas)

Esta responsabilidad debe estar encapsulada por la clase y todos sus servicios deben estar estrechamente alineados con esa responsabilidad 

> Evitar la clase god 

## Open Closed Principle

Las entidades deben estar abiertas para la expansion, pero cerradas para su modificacion

Se basa en la implementacion de herencias y el uso de interfaces para resolver el problema 

> Se sugiere evitar la utilizacion excesiva de los switch y propiciar el ppolimorfismo entre objetos 

## Liskov Substitution Principle


Cada clase que hereda de otra puede usarse como su superclase sin necesidad de conocer las diferencias entre las clases derivadad (tambien se aplica a interfaces)


## Interface Segregation Principle


Los clientes de un omponente solo deberian conocerr de este aquellos metodos que realmente usan y no aquellos que no necesitan usar 

Mucas interfaces cliente espercificas son mejor que una intefaz de proposito general 

> Se deberia propiciar un disneio orientado a interfaces, para mantener el acoplamiento entre clases al minimo posible y tambien evitar generar inrterfaces extensas 


## Dependency Inversion Principle 

Los modulos de alto nivel no deben depender de modulos de bajo nivel. Ambos deben depender de abstracciones. Es una forma de desacoplar modulos 

> Se sutiere utilizar inyectores de dependencias 
