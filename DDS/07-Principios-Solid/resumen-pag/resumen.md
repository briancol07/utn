# SOLID the first 5 principles of object oriented design 

[SOLID](https://www.digitalocean.com/community/conceptual-articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)

## Introduction 

SOLID is an acronym for the first five OOD principles by Robert C. Martin

These pinciples establish parctices that lend to develping sw with considerations for maintaining and extending as the project grows. 

Adopting these practices can avoid code smells, refactoring code and agile or adaptative sw development

## SOLID

* S - Single responsibility principle
* O - Open closed principle
* L - Liskov substitution principle 
* I - Interface segregation principle 
* D - Dependency inversion Principle 

## Single-Responsibility Principle (SRP)

> A class shoudl have one and only one reason to change meaning that a class should habe only one job 

# Open-closed principle (OCP)

> Objects or entities hsoudl be open for extension but closed for modificacion 

The classs should be extendable without modifying the class itself

## Liskov substitution principle 

> Let q(x) be a properly provable about object of x of type T. Thern q(p) should be provable for objects y of type s where s is subtype of T

This mean that every subclass or derived class should be substituatable for their base or parent class


## Interface segragation principle 

> A client should never be forced to implemtn an interface that it doesn't use, or clients shouldn't be forced to depend on mehtods they do not use 

## Dependency Inversion Principle 

> Entities must depend on abstracctions, not on concretions. It stated that the high-level module must not depend on the low-level module, but they should depend on abstractions.




