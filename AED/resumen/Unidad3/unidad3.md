# Unidad 3: Estructuras de Control 

* if 
* while 
* do while 
* for 
* switch 

[Control](./control.cpp)

## if 

> Es una estructura condicional que indica que camino recorrer si se cumple o no un estado o condicion.

usa condicional 

* menor igual <=
* menor < 
* mayor igual >=
* mayor > 
* igual == 
* distinto != 

```c++

// if (condicion){true}
// else {false}

```

Se puede usar sin el else, se pueden anidar unos dentro de otros, pero lo que quiere decir es que si la condicion se cumple hace lo que esta en las primeras llaves en caso de que no sea verdad hace lo de las segundas (luego del else) 

Todo numero mayor que cero se considera verdadero y si es menor igua se cuenta como falso 

## While 

> Es una estructura de iteracion (repeticion), realiza cierto bloque de codigo mienras ocurra una condicion, es decir mientras la condicion sea verdadera. 

``` c++

// while(condicion){hacer esto}

```
mientras que se cumpla la condicion repetira el "hacer esto"

## For 

> El for es parecido al while, para recorrer vectores o algo que sepamos. Tiene un inicio, una condicion y una post-condicion.

```c++
// for(int i=0;condicion;aumento/decremento);

```

## Do while 

> La diferencia entre do while y while es que este almenos una vez realiza la accion y luego compara, haciendo como el while (se ejecuta almenos 1 vez). 

```c++

// do { } while(condicion);
```

## switch case 

> Pregunta el valor de una variable y dependiendo el valor, hace distintas acciones (parecido a un menu de opciones)

```c++

switch(condicion){
  case 1:
    statements
    break;
  case 2: 
    statements 
    break;
  default:
    statements 
    break;

}
```


