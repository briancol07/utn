# Unidad 5: Structs

> Son un conjunto de datos o variables que se encuentran agrupados en estas variables. No es necesario que sean del mismo tipo.

## Declaracion

```c++

struct Nombre{
  int entero;
  char caracter;
  float decimal;
  double doble
};
``` 

## Utilizacion 

see [struct](./struct.cpp)

tambien se puede utilzar con arrays 

## Struct nodo 

> Utilizado en pilas colas y listas 

```c++

struct NodoEntero{
  int valorEntero;
  NodoEntero* sgte;
};

struct NodoCaracter{
  char valorChar;
  NodoCaracter* sgte;
}
```

