# Unidad 1 Bariables y tipos de datos 

## Que es un tipo de dato ? 

> Como el nombre lo dice indica de que tipo va a ser el dato que utilicemos. 

* Enteros (int)
* Caracteres (char)
* Floatantes (float)(coma flotante)
* Long (long) (float pero abarca doble de cantidad de numeros)

``` c++

#include<iostream>

using namespace std;

int main(){
  int entero = 5 ;
  char character = 'c';
  float decimal = 2.3;
  long enteroDoble = 2000000000;

  return 0; 
  }
```

## Imprimir en pantalla 

```c++

cout<<variable<<endl;
cout<<"Esto lo imprime";
```

## Variables inicializadas 

> Si no inicializamos, las variables con algun valor (asignarle) nos tirara un warning, pero no es nada mayor. Pero si decidimos dejarla sin inicializarla esta misma tendra basura en caso de que queramos utilizarla 

## Ingresar valores 

El valor ingresado por el usuario se guarda en la variable 

```c++
int var; 
cin>>var
```

## Resumen:

Existen varios tipos de datos que son utiles si vamos a realizar procesos. Estos tipos de datos estan asociados a variables, y estas variables contienen valores. Con los valores de estas variables se pueden hacer operaciones, validacion de datos etc. El uso de auxiliares es muy util porque nos permite la no perdida de datos importantes. 

