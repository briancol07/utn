# Unidad 6 : Archivos 

> Los archivos son un tipo de estructura de datos que persisten en memoria. Los archivos se almacenan fisicamente.

## Acceso 

```c++

FILE* f;
```
Esto lo que hace es enlazar una variable con un tipo de dato FILE, con esto ya tenemos asegurado que vamos a tener un archivo.

## Operaciones con archivos

* Crearlos 
* Borrarlos 
* Abrirlos 
* Cerrarlos 
* Escribirlos 
* Leerlos 

## Modo de apertura 

```c++

FILE* archivo;
archivo = fopen("nombreArchivo","ModoApertura");
```
![modos](./img/archivos1.png)

## fseek()

```c++
fseek(archivo,0,SEEK_END) // Posicional al final del archivo
fseek(archivo,0,SEEK_SET) // Posicional al inicio del archivo

```

## Archivos de texto 

![datos](./img/dato.png)
