#include<iostream>

using namespace std;

int multiplicar(int ,int);
void cambiarValor(int&);
void imprimir(int,int);

int multiplicar(int a , int b){
  return a * b; 
}

void cambiarValor(int &a){
  a = 20; 
}

void imprimir(int a , int b){
  cout<<"el valor de a ="<<a<<endl;
  cout<<"el valor de b ="<<b<<endl;
}



int main(){
  int num1 = 12;
  int num2 = 23;
  
  cout<<"la multiplicacion es: "<< multiplicar(num1,num2)<<endl;
  cambiarValor(num1);
  imprimir(num1,num2);
  return 0; 
}
