# Unidad 2: Acciones y funciones 

## Modularidad 

Es el concepto de tratar a un problema mayor, como una serie de problemas menores, obviamente mas faciles de resolver. Supongamos que tenemos que cambiarnos de ropa. El problema mayor seria "cambiarnos de ropa" y los subproblemas seria "ponerse la remera"
"ponerse el pantalos", "ponerse las zapatillas",etc. A eso es a lo que alude, dividir un problema que puede ser en problemas mas chicos.

## Funciones 

> Son modulos a los que se le pasan una serie de datos (variables) o no y devuelven una variable (devuelven un tipo de dato).

* Declaracion 
  * el tipo de dato de la funcion
  * el tipo de datos de los parametros que le pasamos 
  * los parametros 
  * Codeamos lo que va a hacer esa funcion 
* invocacion
  * llamado 
  * Dice que va a hacer uso de ella

### Declaracion 

```c++
// tipo nombreFuncion(parametros){acciones;}
int sumar(int numero1, int numero2){
  int numero3 = numero1 + numero2;
  return numero3;
}  
```
### Invocacion 

Si la funcion devuelve un valor, estos mismos tienen que ser accinados a otra variable o mostrar en pantalla directamente  

ver [Funciones](./funciones.cpp)

## Acciones 

> Parecidas a las funciones pero no devuelven valores, solamente hacen cosas. Se invocan de la misma manera 
```c++ 

void imprimir(int a){
  cout<<a<<endl;
  }
```

## Variables Globales y locales 

### Globales 

Se declaran fuera del main() o otra funcion  y pueden ser leidas y escritas por cualquier otra funcion.

### Locales 

Son variables de las correspondientes funciones y solamente son conocidas por esas funciones/acciones 

## Pasar valores por parametro o por valor 

### Por Valor 

solo le paso el valor de la variable y no es posible modificarloaunq lo intente.

### Por parametro 

Le paso la direccion de memoria donde esta alojado el valor .Esto nos permite modificar el valor 

```c++
void cambiarValor(int &numero){
  numero = 30;
}
```

## Ejercicio

Un programa que multiplique 2 valores cuales quiera, le cambie le valor a uno de estos a 20 y luego imprima todos los valores 

