#include<iostream>

using namespace std;

void ordenar(int[],int);
void agregar(int[],int &,int);
void eliminar(int[],int &,int);
void imprimir(int[],int);



int main(){
  int cant = 6;
  int vec[cant]={1,2,4,8,0,3};
  imprimir(vec,cant);
  ordenar(vec,cant);
  imprimir(vec,cant);
  agregar(vec,cant,3);
  imprimir(vec,cant);
  eliminar(vec,cant,4);
  imprimir(vec,cant);
  
  return 0;
}

void imprimir(int v[],int cant){
  for(int i=0;i<cant;i++){
    cout<<v[i]<<endl;
  }
}
void ordenar(int v[],int cant){
  for(int j=0; j<cant; j++){
    for(int i=0;i<cant;i++){
      if (v[i] > v[i+1]){
        int aux = v[i];
        v[i] = v[i+1];
        v[i+1] = aux;
      }
    }
  }
} 

void agregar(int v[],int &cant,int b){
  v[cant] = b;
  cant++;  
}

void eliminar(int v[],int &cant,int pos){
  if(pos < cant){
    while (pos < cant){
      v[pos] = v[pos+1];
      pos++;
    } 
    cant--;
  }
  else {
   cout<<"posicion no encontrada"<<endl; 
  }
}


