# Introduccion

## Algoritmo

> Es un conjunto finito y ordenado de acciones con las que podemos resolver un determinado problema. 

## Problema

> Es una situacion que se nos presenta y que, mediante la aplicacion de un algoritmo podemos resolver. 

## Analisis del enunciado 

Si vamos a diseñar un algoritmo para resolver un determinado problema, tenemos que tener estudiado y analizado el contexto de dicho problema.

* Implica
  * Comprender alcance
  * Identificar los datos de entrada
  * Identificar los datos de salida o resultados

## Recursos

* Disponibles
  * Memoria
  * Operaciones Aritmeticas
  * Operaciones Logicas 

## Teorema de la programacion estructurada 

* Estructuras basicas
  * Secuencial (accion simple)
  * Decision (accion condicional)
  * Iterativa (accion de repeticion) 

## Lenguajes de programacion 

Las computadoras entienden unos y ceros (binario) y nosotros lenguajes naturales. Los lenguajes de programacion son lenguajes formales que se componen de un conjunto de palabras,generalmente en ingles y reglas sintacticas y semanticas.
Podemos utilizar un lenguaje de programación para escribir o codifi car nuestro algoritmo y luego, con un programa especial llamado “compilador”, podremos generar los “unos y ceros” que representan sus acciones. De esta manera, la computadora será capaz de
comprender y convertir al algoritmo en un programa de computación.

## Programas de computacion 

Un programa es un algoritmo que ha sido codificado y compilado y que por lo tanto puede ser ejecutado en una computadora.

* Programa
  * Diseñar y desarrollar su algoritmo
  * Codificar el algoritmo utilizando un lenguaje de programacion 
  * Compilarlo para obtener el codigo de maquina (o ejecutable) 

## Pseudocodigo 

Surge de mezclar un lenguaje natural con ciertas convenciones sintacticas y semanticas propias de un lenguaje de programacion.
Tambien se pueden detallar en diagramas (Nassi-Shneiderman conocido como diagramas Chapin) 

## Diagramas 

1. Acciones 
![accion](./img/Accion.png)
2. Condicional 
![condicional](./img/condicional.png)
3. Repeticion
![repeticion](./img/Repeticion.png)
4. Comienzo y fin (c y f )
![comienzo](./img/comienzo.png)
5. Funcion
![funcion](./img/Funciones.png)

## Funciones 

Seria como un modulo que realiza acciones empieza con el primer item de funcion y termina con la R que seria el retorno a la funcion original, podria devolver un valor como no. 

## Codificacion Hola mundo 

Tiene que guardarse lo siguiente en un archivo .c 

```c
// Son directivas para el preprocesador y incluye las definiciones de entrada y salida de bliblioteca standardkkk
#include <stdio.h> 
// programa o funcion principal
int main(){
  printf("Hola mundo \n"); // Recibe un argumento y lo muestra por pantalla 
  // \n es un salto de linea 
  return 0;
}
```

## Compilacion 

La compilacion se realiza con el compilador (GCC o MinGW) , escribimos en consola(terminal) : gcc NombreArchivo.c -o NombreArchivo.exe 

## Variables 

Z 


