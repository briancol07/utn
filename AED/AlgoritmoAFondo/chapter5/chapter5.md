# Punteros a Caracter 

Como el leguaje de programacion c no provee un tipo de datos que permita definir cadenas o string. Las cadenas se implementan sobre arrays de caracteres.

## Conceptos iniciales 

las variables o elementos del array tienen direcciones de memoria consecutivas.

see [ej1](./ej1.c)

## Aritmetica de direcciones 

como el identificaodr de una cadena es equivalente a la direccion de memoria del priero de sus caracteres y las direcciones de los caracter subsiguientes son consecutivas entonces la direccion el i-esimo caracter se puede calcular sumando el valor entero i  a dicho identificador 

see [ej2](./ej2.c)


## Funciones utiles 

* strcmp
* strlen

## Funcion malloc 

> Permite direccionar n bytes consecutivos de memoria siendo n un entero que le pasamos como argumento.

La memoria que gestionamos con mallloc permanece asignada durante toda la ejecucion del programa y trasciende a la funcion que la invoco 

see [ej3](./ej3.c)
