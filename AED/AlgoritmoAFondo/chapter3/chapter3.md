# Funciones 

## Metodologia top-down 

> Poromne pensar la solucion de un problema en base a una secuencia de acciones a nivel general cuyos pasos iremos refinando sucesivamnte.De lo general a lo particular 

## Funciones 

Los modulos (acciones que ejecutamos en nuestro algoritmo) se implementan como funciones. Es un subprograma invocado desde el programa principal o desde otra funcion, ejecuta una tarea determinada y luego , retorna el control al programa o funcion que la invoco 

## Prototipo de una funcion

Se puede decir header(con muchas comillas) describe la lista de argumentos que la funcion espera recibir y el tipo de dato de su valor de retorno

```c

double valorAbsoluto(double);
```
1. Recibe un unico argumento double 
2. Retorna un double 

## Invocar a una funcion

```c 
#include <stdio.h>
// prototipo de la funcion
double valorAbsoluto(double);
int main()
{
  double v, a;
  printf("Ingrese un valor numerico: ");
  scanf("%lf",&v);
  // invoco a la funcion
  a = valorAbsoluto(v);
  printf("El valor absoluto de %lf es %lf\n",v,a);
  return 0;
}
``` 
