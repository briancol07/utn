# Sistemas Operativos 

> Este Forma es una nueva forma que estoy probando y talves es como deberia haber preparado la materia

## Temario 


### [00-Biografia y Resumenes](./00-Biografia-Resumenes/README.md)

----

### [01-Presentacion y conceptos Basico](./01-Presentacion-Conceptos-Basicos/README.md)
> Repaso de conceptos necesarios de arquitectura de computadoras .
### [02-Procesos](./02-Procesos/README.md)
> No son solo Programas en ejecucion
### [03-Planificacion](./03-Planificacion/README.md)
### [04-Hilos](./04-Hilos/README.md)
### [05-Arquitectura de kernel](./05-Arquitectura-Kernel)
### [06-Concurrencia ( Sincronizacion y mutua Exclusion](./06-Concurrencia/README.md)
### [07-Deadlock](./07-Deadlock/README.md)

-----

### [Primer Parcial](./Primer-Parcial/README.md)

----

### [08-Memoria](./08-Memoria/README.md)
### [09-Memoria virtual](./09-Memoria-Virtual/README.md)
### [10-Filesystem ](./10-FileSystem/README.md)
### [11-Entrada Salida](./11-Entrada-Salida/README.md)

----

### [Segundo Parcial](./Segundo-Parcial/README.md)

