# Repaso De arq de computadoras 

## Index 

* [## Elementos basicos de una computadora ](#elementos)
  * [### Procesador ](#procesador)
  * [### Memoria ](#memoria)
  * [### Entrada Salida (I/O)  ](#i-o)
  * [### BUS ](#bus)
* [## Registros del procesador](### Registros del procesador)
  * [### Registros visibles por el usuario ( Programador ) ](#registors)
  * [### Registros de Control y estado ](#controlestado)
* [## Instrucciones ](#instrucciones)
* [## Ciclo de instruccion ](#ciclo)
  * [### Fetch ](#fetch)
  * [### Decode ](#decode)
  * [### Execute ](#execute)
* [## Interrupciones ](#interrupciones)
* [## Ciclo de instruccion Con interrupciones ](#mix)
* [## Manejar Interrupciones ](#manejar)
  * [### Visto de CPU ](#vistoCPU)
* [### Visto de OS ](#vistoOS)
* [### Multiples interrupcciones ](#mul-interrupciones)
* [## Overhead ](#overhead)
* [## Jerarquia de memoria ](#jerarquia-memo)
* [## Tecnica de E/S ](#e-s)
* [## Programa ](#programa)
* [## Registros ](#registros)
* [## Multiprogramacion ](#multiprograma)
* [## Multiprocesamiento ](#multiproces)

## Elementos basicos de una computadora <a name="elementos"></a>

### Procesador <a name="procesador"></a>

"Procesa" los programas que hagamos. Genera un archivo ejecutable con las instrucciones.
* Elementos importantes: 
  * Registros ( CPU )
    * Visibles usuario
    * Control (PSW) y estado (PC)
  * ALU 

### Memoria <a name="memoria"></a>

* Donde van a estar los programas que se van a ejecutar y todos los datos que estos programas usen.
* Sistema operativo los administra

### Entrada Salida (I/O)  <a name="i-o"></a>

Es nuestra comunicacion con el exterior 
* Ejemplos 
  * Teclado 
  * Pantalla 
  * Placa RED
 
### BUS <a name="bus"></a>

Manera de mantener interconectados a los componentes

----

## Registros del procesador 

### Registros visibles por el usuario ( Programador ) <a name="registors"></a>

Pueden manipularse para hacer los programas. Con los lenguajes de alto nivel. 
* Ejemplos 
  * Ax
  * Bx 

### Registros de Control y estado <a name="controlestado"></a>

No son visibles para el usuario ( No se pueden manipular de manera directa ) Pero se pueden consultar ( PSW ( Program status word ), PC (Program counter) ) 

* PC: Contiene la direccion de la proxima instruccion a ejecutarse
* IR: Guarda la instruccion en si que se esta ejecutando en ese momento pero no la direccion
* MAR: Contiene la direccion del proximo operando a ser utilizado por la instruccion
* MBR: Guarda la instruccion que esta en la celda cuya direccion tiene mar 
* PSW: Indica el estado luego de realizar una operacion 

----

## Instrucciones <a name="instrucciones"></a>

Lo que el procesador sabe hacer ( mov,add,sub,...), y el conjunto de instrucciones son sentencias. 

* Tipos 
  * Privilegiadas ( Ejecutadas por el kernel ) 
    * No todos los programas pueden ejecutarlas 
  * No Privilegiada ( Ejecutadas por todo lo demas ) 

## Ciclo de instruccion <a name="ciclo"></a>

Cada instruccion tiene un flujo para poder ser ejecutada 

```
Inicio --> Fetch --> Decode --> Execute --> FIN
```
![Ciclo-de-Instruccion](./img/ciclo-Instruccion.png)

> Cada vez que se ejecuta una instruccion se actualiza el pc 

### Fetch <a name="fetch"></a>

En esta etapa va a buscar cual es la siguiente instruccion que debe leer , lee memoria y la guarda en registro ( IR = Instruction Register )

### Decode <a name="decode"></a>

La decodifica ( Saber cual intruccion es ) y si necesita algun operando ( en caso de que si los trae a memoria y decodifica) 

### Execute <a name="execute"></a>

Se ejecuta con los operandos necesarios 

---- 

## Interrupciones <a name="interrupciones"></a>

* Se utilizan para cortar la ejecucion de la secuencia actual de instrucciones 
* Permiten que el sistema operativo tome el control para que elija el proximo programa a ejecutar 
* Son notificaciones de eventos dirigidas al prodcesador 
* Clasificacion
  * Hardware ( Asincronas ) / Software ( Sincronas ) ( I/O )
  * Enmascarables / No enmascarables 
    * Enmascarables pueden no ser atendidas con urgencia 
  * E/S 
  * Fallas de HW ( no enmascarables ) 
  * CLOCK ( No enmascarable ) 
  * Internas o externas al procesador 

> Interrupt Handler ( Rutina de manejo de interrupciones ) es el vector que se encuentra en el PIC e indica que rutina debe ejecutar cada interrupcion para poder atenderla 

## Ciclo de instruccion Con interrupciones <a name="mix"></a>

Luego de ejecutar cada instruccion, se pregunta si estan habilitadas las interrupciones y luego si existe alguna para ser atendida ( `Ciclo de instruccion ininterrumpible` Se tiene que terminar la instruccion )

![Interrupcion-Diagrama](./img/interrupciones.png)

Se pueden deshabilitar solo las interrupciones privilegiadas.

## Manejar Interrupciones <a name="manejar"></a>

Cuando ocurre una interrupcion, se da paso al manejador de interrupciones y luego de atenderla vuelve al curso normal de ejecucion 

### Visto de CPU <a name="vistoCPU"></a>

1. Se genera Interrupcion  ( ** No corta la interrupcion de un proceso la mitad ** ) 
2. Finaliza Instruccion Actual 
3. Verifica si hay 
4. Si las hay guarda PC y PSW 
5. Se carga en PC el manejador

### Visto de OS <a name="vistoOS"></a>

1. Se guarda informacion del procesador 
2. Procesa Interrupcion
3. Restaura datos guardados 
4. Restaura PSW y PC 

### Multiples interrupcciones <a name="mul-interrupciones"></a>

Puede pasar que se produzca una interrupcion en el manejador. Se manejan por orden sequencial y se atienden de a 1. 

## Overhead <a name="overhead"></a>

El tiempo invertido en administrar las tareas del SO se llaman **OVERHEAD** ( tareas para que la computadora funcione )

## Jerarquia de memoria <a name="jerarquia-memo"></a>

![Jerarquia-memoria](./img/Jerarquia-memoria.png)

## Tecnica de E/S <a name="e-s"></a>

Son las maneras que la computadora se conecta con el exterior 

* E/S Programada 
  * Procesador actua de manera activa en la transferencia entre I/O y memoria. 
* E/S por interrupcion 
* Acceso directo a la memoria ( DMA )

----

## Programa <a name="programa"></a>

Conjunto de instrucciones y sentencias escritas para realizar una tarea especifica. 

## Registros <a name="registros"></a>

* Acumulador ( AC ) 
* IR
* PC / IP 
* MDR
* MAR
* PSW / ST

## Multiprogramacion <a name="multiprograma"></a>

Es un modo de operacion que permite que 2 o mas programas se ejecuten de forma recurrente .

## Multiprocesamiento <a name="multiproces"></a>
 
Cuando hay varios procesadores ejecutando varios programas al mimso tiempo 
