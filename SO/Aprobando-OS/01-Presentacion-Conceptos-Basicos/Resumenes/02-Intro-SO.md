# Introduccion a Sistemas Operativos 

## Index 

* [## Intro ](#intro)
* [## Capas de la computadora ](#capas)
* [## Modos de ejecucion ](#modos)
* [## Llamadas al sistema ( Syscalls ) ](#syscall)
* [## Cambios de modo ](#cambio-modo)
* [## Cambio de contexto ](#cambio-contexto)

## Intro <a name="intro"></a>

* Funcion OS 
  * Es el primer programa que ejecuta la CPU ( Se administra a si mismo )
  * Administrar los recursos de la computadora.
    * HW 
    * SW 
    * Interfaz usuario
  * Interfaz comun entre HW y programador 
* Evolucion 
  * Monoprogramados 
    * Procesamiento en serie 
    * Sistemas en lotes Sencillos 
  * Multiprogramados ( Manera concurrente ) 
    * Sistemas den lotes multiprogramados 
    * Sistemas de tiempo compartido 


## Capas de la computadora <a name="capas"></a>

* Aplicacion ( Usuario final )
* Utilidades ( Programador ) 
* Sistema operativo ( Programador ) 
* Hardware ( Diseniador de SO )

## Modos de ejecucion <a name="modos"></a>

> Medidas de proteccion 

![Modo-ejecucion](./img/Modo-ejecucion.png)

* Modo Kernel ( nivel 0 ) 
   * Pueden ejecutar todas las instrucciones 
* Modo Usuario ( Nivel 3 )
  * Solo las no privilegiadas 

![Syscall](./img/Syscall.png)

## Llamadas al sistema ( Syscalls ) <a name="syscall"></a>

Instrucciones que se pide al SO por que el programa no las puede realizar. Permite que se ejecuten instrucciones privilegiadas ( Modo kernel ) que los procesos no pueden porque ejecutan en modo Usuario 

* Suelen usarse a traves de una API por medio de Wrappers ( Mejores ,faciles y portables )
  * Wrapper -> Syscall 
  * Printf  -> Write 
* Funciones incluidas en el kernel del SO
* Utilizada para solicitar un servicio al SO 
* Ejemplos Syscalls
  * Operaciones sobre archivos: open(), read(), write()
  * Operaciones sobre procesos: fork(), exit(), kill()
  * Manipulación de dispositivos: release(), eject().
  * Otras: time(), sem\_wait()

![Syscall2](./img/Syscall2.png) 

## Cambios de modo <a name="cambio-modo"></a>

Las syscall se ejecutan en el SO y una vez terminadas pasan a modo Usuario. lo mismo con las interrupciones .

> Siempre que se atienda una syscall implica un cambio de contexto y modo.  Pero puede atenderse una interrupcion si esto ( puede venir en modo kernel ).

## Cambio de contexto <a name="cambio-contexto"></a>

El cambio de contexto implica guardar el estado de un proceso y cargar el estado de otro proceso para que el sistema operativo pueda cambiar de un proceso a otro 

Caracteristica | Cambio de modo | Cambio de contexto 
---------------|----------------|---------------------
Proposito | Transicion entre modo usuario y kernel | Cambiar la ejecucion entre procesos 
Nivel de operacion | CPU | SO 
Cuando Ocurre | Syscalls, interrupciones, excepciones | Planificacion de CPU, Interrupciones,syscalls
Duracion | Breve | Prolongadas 


Intrerrupcion usa syscall pero la syscall no usa interrupcion 
