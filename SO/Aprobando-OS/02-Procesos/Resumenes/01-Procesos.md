# Procesos 

Es una secuencia de instrucciones ( Programa ) que esta siendo ejecutada. 

Programa = Estatico ( entidad pasiva ) 
proceso = Dinamico 

Se crean atravez de syscalls 

## Estructuras ( Proceso ) 

1. Codigo 
  * Espacio asignado para almacenar la secuencia de instrucciones 
2. Datos 
  * Espacio asignado ( variables globales ) 
3. Pila ( STACK )
  * Espacio usado para llamadas a funciones, parametros y variables locales 
4. Heap 
  * Espacio de memoria dinamica 

* **Grado multiprogramacion** : Cantidad de procesos en memoria 
* **Grado Multiprocesamiento** : ( Running ) Procesos corriendo al mismo tiempo 

## Contexto de ejecucion 

Cuando se realiza una ejecucion concurrente , Si se llama al SO por syscall o interrupcion, se toma la decision de ejecutar otro proceso, para que esto ocurra, en algun lado se almacenan los datos de estado ( PC,Contexto de ejeccucion y PSW, los registros ) 

## PCB Process control block (imagen)

Siempre esta en la RAM, contiene toda la informacion relacionada al proceso 
El resto de laimagen puede pasarse a SWAP.

* Imagen
  * Codigo 
  * Datos 
  * Heap 
  * Pila 
  * PCB
    * Atributos de procesos 
      * PID => Process id 
      * PPID => Parent process id 
      * UID => user id 
      * Informacion de gestion de memoria, planificacion y otros .

![Imagen](./img/imagen-proceso.png)

## E/S Bloquante 
Las syscall bloqueantes o no ( Depende como esten configuradas )

## Creacion de un proceso 

* Creados por os 
* Creados por otro programa 

1. Asignacion PID
2. Reservar espacio para las estructuras ( Codigo ,Datos, pilas , heap) 
3. Inicializar PCB
4. Ubicar PCB en listas planificaciomn

## Finalizacion de un proceso en linux 

* Fin Normal 
* Fin por otro proceso ( KILL)
* FIn por falla o condicion de error 

## Ciclo de vida de un proceso 

> Desde la creacion hasta su finalizacion 

Hay mas diagramas pero veremos de 5 y 7 estados 

### Diagrama de 5 estados 

![Diagrama](./img/Diagrama-5-estados.png) 

* New procesos que se estan creando 
* Exit lo procesos que ya terminaron 
* Ready : un proceso esta esperando para entrar a running 
* Running: proceso en ejecucion 

### Diagrama de 7 estados 

![Diagrama](./img/Diagrama-7-Estados.png) 

Tenemos el estado de blocked que se puede pasar a disco, pero lo que pasara es que el pcb siempre estara en memoria. 

( Ready / suspended) y ( suspended/blocked)  se encuentra en disco 

## Cambio de un proceso / Contexto 

> Ocurre cuando luego de un cambio de modo de un proceso al S.O., este último elige otro proceso para ejecutar (por algún motivo), entones en el 2° mode switch cambia de proceso

Cambio de contexto es cambio el proceso en estado running.
El contexto es todo lo que necesita el sistema operativo para ejecutar un proceso 
Luego de un cambio de modo 

* Si hubo un cambio de proceso entonces dos cambios de contexto ( Guardr ctx de proceso y se restaura )
* Si hubo dos cambios de contextos no necesariamente hubo cambio de proceso, puede elegir ejecutar el mismo proceso 
* Si hubo cambio de modo hubo un cambio de contexto 
* Si hubo un cambio de contexto no necesariamente hubo cambio de modo  ( interrupcion anidadas ) 

