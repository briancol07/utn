# Test 

## Autoevaluacion 

1. Cuantos procesos puede ejecutar en un determinado moemnto en una CPU ? 
    1. 1
    2. Mas de 1, solo si es multiprogramado
    3. 2
    4. Ninguna de las anteriores 
2. En la pila se guarda 
    1. Variables locales, variables globales y parametros 
    2. Variables globales, parametros
    3. Memoria Dinamica, parametros
    4. Ninguna de las anteriores
3. Un Proceso puede modificar los siguientes elementos de la imagen  
    1. Codigo, datos y pila
    2. Datos, pila , heap y pcb
    3. Datos pila y heap 
    4. Ninguna de las anteriores 
4. La funcion free() permite liberar espacio de 
    1. Datos 
    1. Pila
    1. Heap
    1. Ninguna de las anteriores 
5. Desde el estado Ejecutando se puede llegar al estado 
    1. Suspendido, listo y finalizado
    1. Bloqueado, Finalizado y listo 
    1. Ninguna de las anteriores
6. Estado Finalizado es utilizado para un proceso que 
    1. Su padre finalizo 
    1. No existe mas en elsistema 
    1. Ninguna de las anteriores 
7. El estado Suspendido sirve para 
    1. un proceso que esta siendo depurado
    1. Un proceso esperando por un evento 
    1. Ninguna de las anteriores 
8. La imagen de un proceso puede estar ubicada en memoria cuando su estado es 
    1. Bloqueado
    1. listo/suspendido 
    1. Bloqueado/suspendido
    1. Ninguna de las anteriores 
9. Un proceso switch implica 
    1. Un cambio de modo
    1. Dos cambios de modo
    1. Que se realizo una llamada al sistema
    1. Ninguna de las anteriores
10. En el PCB se guarda el estado de los registros del procesador mientras 
    1. El proceso esta en estado ejecutando 
    1. El proceso no esta en estado ejecutando
    1. Ninguna de las anteriores 
11. Un cambio de proceso puede ocurrir cuando 
    1. Lueog de realizar una lalmada al sistema ejecute el mismo proceso 
    1. Luego de realizar una llamada al sistema haya un cambio de modo
    1. Luego de atender una interrupcion hay un cambio de modo 
    1. Ninguna de las anteriores 

### Respuestas 

1. A
2. D
3. C
4. C
5. B
6. C
7. A
8. A
9. B
10. B
11. D
