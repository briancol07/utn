# Arquitectura de kernel 

Se crearon diferentes arquitecturas 

* Monolitico 
* Arquitectura por capas 
* Microkernel 

![Arquitecturas](../img/Arquitecturas.png)

### Monolitico 

Un gran bloque de codigo, solo sus bibliotecas estan separadas. 

* Inseguro 
* Cualquier error puede afectar a todos 
* Mucho Acoplamiento 
* Comunicacion fluida 

### Por Capas 

Todo funciona en modo kernel pero cambia la comunicacion , para que dos capas se comuniquen tienen que pasar por las que estan en medio. 

* Mas lento 
* Se puede tratar el problema en la capa 
* Mas abajo HW y la de mas arriba Usuario 

### Microkernel 

Hay funcionalidades que se ejecutan en modo kernel y otras en modo usuario. Kernel mas chioc pero distribuido . Si un error ocurre en modo usuario solo se cae un modulo. 
Los modulos entre si no se pueden comunicar tienen que pasar por el kernel. 

* Ventajas 
  * Estabilidad 
  * Escalabilidad 
  * Mantenimiento 
* Desventaja 
  * Paso de mensajes mas lento 
  * No muy vendido 


