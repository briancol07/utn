# Test 

1. Un Kernel Monolitico 
  * Es aquel que se divide en capas, todas corriendo en modo kernel
  * Es aquel que consiste en un unico modulo corriendo en modo kernel
  * Ninguna de las anterioeres 
2. Un microkernel 
  * Es mas robusto y confiable que un kernel monolitico 
  * Es menos robusto y confiable que un kernel en capas 
  * Ninguna de las anteriores 
3. Un kernel monolitico 
  * Utiliza mensaje para la comunicacion entre modulos 
  * Sus modulos estan fuertemente acoplados 
  * Ninguna de las anteriores
4. Un microkernel 
  * Corre solo las funcionalidades esecnciales en modo kernel
  * Por su disenio es normalmente mas eficiente que el kernel monolito 
  * Ninguna de las anteriores 

## Respuestas 

1. B
2. A
3. B
4. A
