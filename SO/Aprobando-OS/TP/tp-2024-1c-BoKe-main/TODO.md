# Deudas con la sociedad 😢
- Jara JR: Mensajes CPU_KERNEL
- Juano: PR con sus cambios + **testing**
- Colo:  PR con sus cambios + **testing**
- Jara SR: refactor estructuras de mensajes.


# Check 2 Fecha: 25/05/2024

## Objetivos:

-   **Módulo Kernel:**
	-   Es capaz de crear un PCB y planificarlo por FIFO y RR.
	-   Es capaz de enviar un proceso a la CPU para que sea procesado.
-   **Módulo CPU:**
	-   Se conecta a Kernel y recibe un PCB.
	-   Es capaz de conectarse a la memoria y solicitar las instrucciones.
	-   Es capaz de ejecutar un ciclo básico de instrucción.
	-   Es capaz de resolver las operaciones: SET, SUM, SUB, JNZ e IO_GEN_SLEEP.
    
-   **Módulo Memoria:**
	-   Se encuentra creado y acepta las conexiones.
	-   Es capaz de abrir los archivos de pseudocódigo y envía las instrucciones al CPU.
	-   Módulo Interfaz I/O:
	-   Se encuentra desarrollada la Interfaz Genérica.
	- 
## Tareas

### Kernel:
#### Es capaz de recibir instrucciones por consola (Jara JR 27/04)
- La conosla debe poder leer las instrucciones según el tp
- La consola debe llamar a las funciones correspondientes (ver el diagrama de flujo de excalidraw)

#### Tiene implementada la comunicación con CPU (ver excali) (Jara SR 27/04) 
- Guarda como corresponda los sockets de interrupt y dispatch
- Tiene una función send_cpu que crea un hilo pero no es bloqueante ni queda en loop. Puede enviar por interrupt o dispatch
- El socket de interrupt nunca queda en loop escuchando. Solo se usa para enviar
- El socket de dispatch primero tiene que enviar un send
- Si hubo un send por dispatch, dispatch queda en loop esperando la respuesta
- Cuando recibe un mensaje por dispatch tiene que revisar lo que recibe y delegar la tarea al "sub-modulo" que corresopnda (hilo finalización, planificador de corto pa bloquear o para interrumpir, etc.)

#### Es capaz de crear un PCB (Colo 29/04):
- Debe existir el TAD del PCB 
- Debemos tener sus operaciones asociadas
	- Create
	- Destroy
	- Get and Set (si es que hacen falta)

#### Debe poder enviarle a memoria el mensaje para crear y destruir el proceso (Brian 29/04)
- La función send, (la serialización ya estaría lista)

#### Tiene creada las colas de estado de los procesos: (Colo 06/05)
- Cola de new
- Cola de ready
- Cola de bloqued
- Cola de exec
- Todas las colas deberían tener funciones:
	- get algún proceso
	- set algún proceos
	- ⚠️ son funciones primitivas que solo se encargan de manejar la cola, y tienen los mutex para proteger las colas pq son sección critica. No son funciónes que crean hilos ni nada, complicado. Deberían ser funciones lo más simples y básicas posibles con pocas responsabilidades

#### Hilo genesis (ver diagrama en excali)
- Es una función que crea un hilo (no se queda en loop ni es bloqueante para quen llame a esta función)
- Instancia el PCB (delegando esta responsabilidad)
- Encolar en NEW el PCB (delegando esta responsabilidad)
- Mensaje a Memoria para crear el proceso (delegando la responsabilidad)
- Llama al hilo de admisión

#### Manejo de multiprogramación (ver diagrama en excali)
- Neceistamos una variable global para saber cuantos procesos se están manejando. (o una función que sume los procesos en las listas de ready, bloqued y exec)
- Esta variable define si podemos aceptar que más procesos pasen de new a ready
- Necesitamos funciones de get y set para modificar esta variable y esta variable se encuentra en sección critica
- Puede ser un semaforo o una variable
	- En el diagrama aparece como semáforo
	- Juano sugirió que nos manejemos por eventos y esto lo podemos manejar con una variable. 
		- Eventos:
			- Cambio de multiprogramación
			- Fin de un proceso
			- Creación de un nuevo proceso
		- Una de las ventajas de esto es que el hilo no queda en wait. Muere porque tenemos otros eventos que lo van a llamar cuando sea necesario

#### Hilo de admisión (ver diagrama en excali)
- Es una función que crea un hilo (no se queda en loop ni es bloqueante para quen llame a esta función)
- Quita un proceso de new (delega esta función)
- Encola el proceso en ready (delega esto)
- Revisa si puede hacer lo anterior dependiendo del grado de multiprogramación (delega esto)

#### Planificador de Corto plazo READY → EXEC
- ver diagrama excali
- Quitar un proceso de ready (delega esto a las func de la cola)
- "Encola" en exec (delega esto al planificador fifo o rr o vr)
- Le envía al CPU el proceso (esto lo delega al send_cpu de comunicación)

#### Planificador de Corto plazo  EXEC → READY
- ver diagrama excali
- Recibe la llamada de comunic con cpu 
- Revisa el motivo de desalojo (esto sería un interrupt)
- Quita el proceso de exec
- Lo encola en ready
- Se vuelve a llamar a sí mismo para hacer READY → EXEC

#### Planificador de Corto plazo  EXEC → BLOCKED
- ver diagrama excali
- Recibe la llamada de comunic con cpu
- Debe revisar el motivo de desalojo:
	- Usar una IO
	- Wait o Signal
- Llama al gestor de recursos para que haga lo que corresponda dado el motivo de desalojo
- Quita el proceso de exec
- Lo encola en bloqued

#### Planificador de Corto plazo BLOCKED → READY
- ver diagrama excali
- Recibe la llamada del gestor de recursos
- Esta llamada no debería ser bloqueante con las otras transiciones
- Debe quitar el proceso indicado de la lista de blocked
- Lo encola en Ready
- Esto lo necesitamos ahora solo para IO_GEN_SLEEP

#### Gestor de recursos
- ver diagrama excali
- Debe llevar la cuenta de los recursos y de las io
	- Pudendo agregar o quitar nuevas IO en runtime
	- Debe saber cuáles están disponibles y cuáles no
- Debe tener una lista con las peticiones de los procesos para usar los recursos
	- Debe tener los datos de la petición
	- Debe saber a qué proceso pertenece la petición
	- Debe saber que recurso es requerido por la petición
	- Cuando se libera un recurso debe:
		- usarlo usar el recurso con la siguiente petición que lo necesita
		- Avisar al planificador de corto que el proceso que usó el recurso debe pasar de BLOCKED → READY (y actualizar el pcb ?)
 
#### Planificar procesos por FIFO
- Crear el algoritmo _en un archivo separado_ que use las funciones de las colas para planificar los procesos (osea que los mueva de new a ready y de ready a exec )

#### Planificar procesos por RR
- Crear el algoritmo _en un archivo separado_ que use las funciones de las colas para planificar los procesos (osea que los mueva de new a ready y de ready a exec )
- Crear el timmer que use la función send por interrupt para desalojar el proceso por fin de Q

### CPU:
#### Inicializa ✅
- levanta los sockets de interrupt, dispatch y conexión con memoria
- Cada socket tiene funciones para usarlos (con sus mutex)
- crea un hilo para cada uno
- Existe el TAD del PCB y del Contexto de ejec.
	- Junto con sus funciones asociadas

#### variable de interrupt
- es una sección crítica
- debe tener funciones para guardar y eliminar interrupts

#### una función para hacerle send a memoria (Gusty 29/04)
- es una función que NO crea un hilo y ES bloqueante
- queda bloqueada porque espera la respuesta
- la función retorna la respuesta de memoria

#### TLB
- Una tabla que guarda páginas y marcos
- Puede ser FIFO o LRU (quizá debamos hacerlas por separado)
- Tiene funciónes para hacer get y set de entradas
- Hay un TAD para el conjunto pagina-marco-pid
	- y sus operaciones asociadas (create, destroy, etc)

#### Función para traducir direcciones de memoria
- es una función que NO crea un hilo y ES bloqueante
- Hace el calculo para pasar de dir. lógica a física
- le pide a la TLB la traducción de páigna a marco
- Si es TLB MISS, le pide a memoria con la función send
	- Y actualiza la TLB
- Retorna la dir física

#### Hilo de interrupt
- Escucha en loop
- Al recibir una interrupt la guarda (usando las funciones de la variable interrupt)

#### Hilo dispatch (ver excali)
- Arranca en escucha activa
- Puede recibir en un mensaje un PCB
- Fetch: pide a mem la sig. instrucción (usando send memoria)
- Decode: Delega la instrucción a la función correspondiente para esa instrucción
	- Por ahora solo SET, SUM, SUB, JNZ e IO_GEN_SLEEP
- Execute: la función actualiza el PCB
- Check interrupt: revisa la variable de interrupt
- Sigue con la prox instrucción o envía a KERNEL por dispatch el proceso

### Memoria

#### Se inicializa correctamente ✅
- Acepta conexiones nuevas

#### Handshake (Juanetete )
- la conexión con el kernel es de permanente escucha en loop
	- función kernel_slave() para crear y finalizar procesos
- la conexión con otros modulos espera una instrucción y retorna una respuesta

#### Necesita una tabla de instrucciones
- Con funciones asociadas para obtener instrucciones de un archivo o eliminar instrucciones cuando se elimina un proceso
- [ PID | [ PC | "instrucción" ] ]
- instrucción podemos guardarla como string y que el procesador decodifique
- Esta tabla es sección crítica

#### Neceista una tabla de páginas
- Esta tabla es sección crítica
- con sus funciones asociadas
	- Set de una página nuevo a un marco para un PID
	- Get cuando pidan por una página específica

### IO
#### Hacer la IO Genérica (Juanetetet 29/04)
- Se puede conectar y desconectar del kernel
- Queda luego esperando por instrucciones
- Retorna la respuesta requerida por el kernel
