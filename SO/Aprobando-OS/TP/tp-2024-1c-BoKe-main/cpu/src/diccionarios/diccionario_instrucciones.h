#ifndef DICCIONARIO_INSTRUCCIONES_CPU_H_
#define DICCIONARIO_INSTRUCCIONES_CPU_H_

#include <commons/collections/dictionary.h>
#include <instrucciones/instrucciones.h>

t_dictionary *get_diccionario_instrucciones();
void diccionario_instrucciones_destroy();

#endif