# Memoria virtual

> Def: Espacio de memoria secundaria ( Disco ) que puede ser direccionado como si fuera la memoria real. 

## Caracteristicas 

* Traduccion de direcion en tiempo de ejecucion 
* Proceso dividido en partes ( paginas o segmentos ) Partes en memoria y disco 

## Motivaciones 

* Durante la ejecucion de un proceso no se usan todas sus partes.
* Hay espacio reservado sin unsar, no es necesario ocupar la memoria con partes que no se usan 

## Ventajas 

* Permite cargar todo el proceso en la memoria virtual.
* Podre ejecutar procesos mas grandes que la memoria real 
* Mas procesos cargados en memoria, permite aumentar el grado de multiprogramacion 
* Menos restricciones para el programador, no tendra que preocuparse tanto por la cantidad de memoria disponible 

## Mecanismo de busqueda de paginas 

1. Se realiza una referencia a memoria se verifica la presencia
  * Si no esta bit en 0 genera un pagefault y la MMU lanzara una interrupcion de software que indivara que la pagina buscada no esta en memoria 
2. MMU dispara la interrupcion de page fault y se bloquea el proceso hasta que este disponible la pagina en memoria 
3. El SO solicita la pagina y determina en que frame ubicar la pagina 
4. Una vez que IO termina, se recupera la pagina ( Memoria virtual ) y se carga en la memoria real. Se produce Interrupcion para que el SO tome control 
5. El So atiende la interrupcion, actualiza la tabla de paginas y desbloquea el proceso
6. Se ejecuta nuevamente la instruccion que provoco el fallo de la pagina , ahora encontrando la pagina

## Eficiencia de la memoria virtual 

* Requiere mas accesos a memoria y a disco 
* No mejora el rendimiento para la ejecucion del proceso 
* Mayor grado de mutliprogramacion y psoibilidad de tener procesos mas grandes en memoria 

## Principio de localidad o proximidad temporal

> Localidad : Durante un intervalo de tiempo, solo se usan unas paginas de forma activa 

**Localidad temporal**: se refiere a la tendencia de un programa a acceder a las mismas ubicaciones de memoria repetidamente en un corto periodo de tiempo. Esto significa que si un dato fue accedido recientemente, es probable que se sea accedido de nuevo en un futuro proximo. 

**Localidad Espacial**: Se refiere a la tendencia de un programa a acceder a ubicaciones de memoria cercanas entre si. Esto significa que si un dato ha sido accedido, es probable que los datos cercanos en memoria tambien sean accededidos en un futuro cercano 

## Paginacion Jerarquica o por niveles 

No dejo toda la tabla en Memoria Real, voy tomando lo que voy necesitando y los demas queda en Memoria virtual.
Tiene uan tabla de punteros a las tablas de paginas ( Divido en tablas mas pequenias). De manera que ahora solo voy a tener la tabla de punteros a tablas de paginas en Memoria. Pero voy a necesitar 3 accesos a memoria/disco para ver una direcicon y ademas podria tener pagefault en la tabla de paginas y/o Memoria real ( Mucho overhead ) 

![Paginacion-Gerarquica](../img/Paginacion-Jerarquica.png)

## Tabla de paginas invertidas 

Hay una tabla de paginas par todos los procesos ( Todo OS). La cantidad de entradas de la tabla es igual a la cantidad de marcos en memoria. Pero en las tablas de paginas que vimos antes, se indexaban por el numero de pagina y ahora por el numero de marco 

Tengo que comparar 1 por 1 las n paginas para encontrar la que va. 

![Tabla de paginas](../img/Tabla-de-Paginas-invertidas.png)

* Tengo una tabla de paginas pen el sistema en vez de una por cada proceso 
* Tiene muchisimo overhead ( busqueda )

para esto se usan las funciones de hash 


## Buffer de traduccion anticipada ( Translation look aside buffer) ( **TLB** )

> Cache de tabla de paginas 

* Sopoerte de HW 
* Mejora el rendimiento de la traduccion de direcciones 
* Puede reducir la cantidad de accesos a memoria 

![TLB](../img/TLB.png)

La TLB solo tiene informacion de un solo proceos en particular ( el que se esta ejecutando) 

## Politicas para administrar la Memoria virtual 

1. Politicas de recuperacion 
  * **Paginacion bajo demanda**: Traemos a memoria real las pgians que neceistamos de MV unicaemtne 
  * **Paginacion adelantada**: Idem anterior pero traigo unas extras basandome en el principio de localidad 
2. Politicas de segmentacion  
  * Solo tiene sentid ocon la segmentacion pura: algoritmos de primer ajuste, mejro ajuste 
3. Politicas de reemplazo y sustitucion 
  * Hay bit de bloqueo 
  * Bloqueo de marcos : usa el bit de bloqueo
  * Algoritmos de sustitucion 
4. Gestion de conjunto residentes 

TIPO| Reemplazo local | Reemplazo global 
----|----------------|-------------------
Asignacion fija | Numero de marcos asignados a un proceso no varia, paginas a cambiar son del mismo proceso | No es posible 
Asignacion Variable | Numero de marcos asignados a un proceso puede cambiar, Paginas a cambiar son del mismo proceos | Las paginas a reemplazar se eligen entre todos los marcos, Cada reemplazo puede afectar a otro proceso 
5. Politica de limpieza 
  * Limpieza bajo demanda 
  * Limpieaa adelantada  

## Algoritmos de sustitucion 

1. Optimo 
2. FIFO 
3. LRU
4. CLOCK 
5. CLOCK mejorado 

### Optimo 

La pagina a reemplazar se realizara una referencia en el futuro mas lejano, No produce pagefault cercano. No se puede implementar, solo de comparacion 

![Optimo](../img/Optimo.png)

### FIFO 

Se elige a la pagina que hace mas tiempo esta en memoria. 

![FIFO](../img/FIFO.png)

### LRU

Se elige la pagina que hace mas tiempo que no es referenciada. Necesito guardar en que instantes use las paginas o el ultimo uso. Mucho overhead y tamanio

![LRU](../img/LRU.png)

### CLOCK 

Se apunta al proximo marco a reemplazar pero 
* Bit de uso == 0 se reemplaza marco 
* Bit de uso == 1 Bit de uso pasa a 0 y apunta al siguietne marco y vuelve a preguntar 

![CLOCK](../img/CLOCK.png)
![CLOCK](../img/CLOCKS.png)

### CLOCK mejorado 

Requiere bit de modificado y de uso, no necesito actualizarla en disco lo cual es una ventaja. A la hora de seleccionar victima prefiero elegir una que no haya sido modificda .

La de mas arriba es mas seguro que salga la de mas abajo seguro se queda 

* u = 0 y m = 0 --> No accedido recientenmente, no modificado 
* u = 1 y m = 0 --> Accedido recientenmente, no modificado 
* u = 0 y m = 0 --> No accedido recientenmente, modificado 
* u = 0 y m = 0 --> Accedido recientenmente, modificado 

![Clock Mejorado](../img/C-Mejorado.png)

## Trashing 

Puede ocurrir que en algun momento determinado tengo muchos mas procesos en memoria y menos marcos por proceso. Entonces aumentara mi grado de multiprogramacion, que en parte es algo deseable, pero en consecuencia, el conjutno residente de cada proceso es mas chico.

Mas pagefault y llevara a aumentar IO y bajar tasa de uso de CPU. Esto lelva a que el SO traiga paginas por traer , apesar que no se use ny vuelvan a disco generando overhead.

## Consideraciones  (Tamanio Paginas)

Tipo |chica |grande 
---------|------|-----
Tabla de paginas | muchas entradas | Pocas entradas 
Fallos de pagina | MAS | Menos 
TLB | Mas fallos | Mas aciertos 
Fragmentacion | Menos | Mas 
Localidad | Mas preciso | menos Preciso 
Transferencias IO | Menos | Mas 
