# Test 

1. El Resident Set ( O conjunto residente ) 
  * Es el conjunto de paginas totales 
  * Es el conjunto de paginas del proceso que esten en memoria principal
2. Ante una referencia a una pagina que no existe en memoria principal 
  * El SO se da cuenta y dispara una interrupcion
  * El proceso se da cuenta y avisa al SO 
  * Ninguna de las Anteriores 
3. Una Ventaja del esquema de memoria virtual es 
  * El proceso puede ser mas grande que la memoria principal
  * El proceso puede ser mas grande que la memoria secundaria 
  * Ninguna de las anteriores 
4. El principio de localidad implica
 * Que el proceso realiza las mismas operaciones en un periodo de tiempo
 * Que el proceso usa las mismas referencias en un periodo de tiempo 
 * Que el proceso referencia las mismas paginas durante un periodo de tiempo 
 * Ninguna de las anteriores 
5. Con memoria virtual, la entrada en la tabla de paginas deberia tener como minimo
 * Un bit de presencia y otros de permisos ( R,W,X )
 * Un bit de presencia y otro de uso 
 * Ninguna de las anteriores 
6. Una tabla de paginas de dos niveles 
 * Sirve para ahorrar espacio en la memoria principal 
 * Sirve para ahorrar espacio en la memoria secundaria
 * Ninugna de las anteriores 
7. La tabla de paginas invertidas 
 * Tiene tantas entradas como lo permita la memoria principal 
 * Tiene tantas entradas como lo permita la memoria secundaria
 * Ninguna de las anteriores 
8. El algoritmo de reemplo de paginas CLOCK 
 * Es mas preciso y menos eficiente que el LRU
 * Es menos preciso y mas eficiente que el LRU
 * Ningunas de las anteriores 

## Respuestas 

1. B
2. C
3. A
4. C
5. C
6. A
7. A
8. B 

