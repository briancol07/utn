# Test auto evaluacion 

1. En un sistema operativo que admite procesos multihilo, el proceso contiene 
  * Un heap por cada hilo
  * una pila por cada hilo 
  * un PCB por cada hilo 
  * Ninguna de las anteriores 
2. Los hilos de ejecucio de un mismo proces compparte 
  * Variables locales y variables globales 
  * Variables globales y memoria dinamica 
  * Variables locales y memoria dinamia 
  * Ninunga de las anteriores 
3. Dos hilos de diferente proceso 
  * Comparten las variables globlaes 
  * Pueden ejecutar en secciones de codigo identicas entre si 
  * Ninunga de las antreioeres 
4. Los proceso con multiples hilos 
  * Requieren un PCB y un TCB
  * Requieren un PCB y TCB por cada hilo 
  * Solo requieren un TCB por cada hilo
5. Un hilo puede tener mas de un estado de ejecucion
  * Verdadero 
  * Falso
6. Un sistema que solo admite el uso de procesos monohilo
  * Pueden utilizar hilos a nivel de usuario ULT
  * Pueden utilizar hilos a nivel kernel KLT
  * Ninguna de las anteriores 
7. Los hilos ULT
  *  Son administrados por una biblioteca de usuaio con colaboracion del SO 
  * Son administrados por el sistema operativo 
  * Son administrados por una biblioteca de usuario 
8. Los hilos KTL de un mismo proceso pueden ejecutar en diferentes procesadores
  * Verdadero 
  * Falso 
9. El cambio de hilo, entre hilos de un mismo proceso 
  * Es mas rapido usando hilos KLT, respecto del os ULT
  * Tiene menos overhead usando hilos ULT respecto a los KLT 
  * Niguna de las anteriores 
10. En hilos ULT, realizar una E/S bloqueante produce
  * Que se bloquee solo el hilo que solicito la E/S
  * que se bloqueen todos hilos, solo si se utiliza la tecnica de jacketing 
  * El estado de cada hilo cambia a bloqueado 
  * Ninunga de las anterioeres

## Respuesta 

1. B
2. B
3. B
4. B
5. B ( Falso ) 
6. A
7. C
8. A ( Verdadero )
9. B 
10. D



