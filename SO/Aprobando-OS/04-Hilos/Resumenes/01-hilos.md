# Hilos 

Los hilos son una linea de ejecucionde un proceso , unidad de trabajo del SO. 

Hay un PCB por cada proceso ( esto ocurre cunado hay un solo hilo ), aca tenemos un solo solo contexto de ejecucion 

Pero en los sistemas multi hilo tendremos varios contextos de ejcucion, varios program counter y podemos ejecutar varios lugares del mismo codigo e un proceso. Esto puede pasar por que los recursos de un proceso pueden independizarse. 

## Estructura

TCB (thread control board ) , cada proceso tiene un pcb pero para los hilos uso el TCB. Algunas estructuras son compartidas otras forman parte del hilo. 

* Datos del contexto de ejecucion 
  * PC 
  * Flags
* Prioridad
* Estado 
* Puntero a la pila 
* La pila no se comparte 
* TID ( Thread ID) 

## Ventajas 

* Capacidad de respuesta 
* Procesamiento asincrono 
* Economia 
* Comparticion de recursos 
* Comunicacion eficiente 
* Permite multiprocesamiento

## Estado 

Los hilos no pueden ejecutarse al mismo tiempo en una misma cpu y si se bloquea el hilo puede ser que se bloquee el proceso 

## Implementacion 

* KLT ( Kernel )
* ULT ( Usuario )

> Se pueden implementar tanto separados como en conjunto 

![Implementacion](../img/Implementacion-hilos.png)
![Hilos-Kernel](../img/Hilos-Kernel.png)

### ULT ( Hilos de usuario )

La biblioteca crea todas las estructuras necesarioas ( TCB, pila.. ) pero el SO solo procesa el proceso ( no los reconoces ) 

* Ventajas 
  * Bajo Overhead
  * Planificacion Personalizada
    * Elegir algoritmo de planificacion 
  * Portabilidad 
* Desventajas 
  * No permite multiprocesamiento 
  * Syscall bloqueante bloquea todo el proceso 
  * Algoritmos de planificacion pueden limitar ejecucion de un proceso

### KLT ( Hilos de kernel ) 
 
El SO conoce todo el proceso y lo que tiene . Los kLT empiezan a competir por el procesador como los procesos 

> p\_thread es KLT 

* Ventajas 
  * Syscall bloqueante bloquea solo al hilo que la produjo 
  * Permite multiprocesamiento de hilos del mismo proceso
* Desventajas 
  * Mayor overhead respecto a los ULT
    * Cambio de hilo lo realiza SO 
  * Planificacion del SO 

### Combinados 

![KLT-ULT](../img/KLT-ULT.png)

## Jacketing 

Caracteristica que puede tener la bibliteca ULT, Los KLT no pueden usarlo 

1. Previene que un ULT se bloquee en csaso de que el dispositivo lo haga 
2. Convierte una E/S bloqueante en una no bloqueante  ( permite ejecutar otro hilo ) 


