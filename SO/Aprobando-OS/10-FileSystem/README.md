# File System 

## Clase 

* [Parte1](https://www.youtube.com/watch?v=nfTKxMMoFXs&list=PL6oA23OrxDZDs0NotvQKRzGLX-iG3wJa1&index=12)
* [Parte2](https://www.youtube.com/watch?v=XSiMjSOJPjU&list=PL6oA23OrxDZDs0NotvQKRzGLX-iG3wJa1&index=13)

## Indice 

1. [FileSystem](./Resumenes/README.md)
2. [Notas](./Resumenes/notas.md)

## Ejercicios 

* [Ejercicios](./Ejercicios/README.md)

## Test 

[Test](./Test-Autoevaluacion/README.md)
