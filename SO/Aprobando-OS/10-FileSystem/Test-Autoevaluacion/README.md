# Test 

1. Cual de las siguientes operaciones sobre archivos no es una operacion Basica
  * Copiar
  * Leer 
  * Borrar 
  * Truncar 
2. Cual de los siguiente items no deberia formar parte de la tabla o lista global de archvios abiertos 
  * Tipo 
  * Contador de aperturas
  * Ninguna de las anteriores 
  * Fecha de modificacion 
  * Puntero de archivo 
3. La estrategia de proteccion **Propietario, Grupo , resto** tipica de sistemas unix
  * Ninguna de las anteriores 
  * No permite especificar permisos grupales
  * No permite especificar permisos particulares e individuales a usuarios distintos al propietaro
4. Los bloques logicos utilizados para archivos:
  * Ninguna de las anteriores 
  * Son de tamanios variables y se utiliza un bloque entero para todo un archivo 
  * Son de tamanio fijo y por eso generan fragmaentacion externa
  * Cada bloque puede formar parte de un solo archivo 
5. El FCB ( File control Block )
  * Esta asociado a uno o mas archivos 
  * Esta asociado a cero o un archivo 
  * Ninguna de las anteriores
6. La tabla global de archivos abiertos 
  * Existe en memoria principal pero se persiste en disco tambien 
  * Ninguna de las anteriores 
  * Solo exite en memoria principal 
7. La asignacion de bloques sobre archivos de tipo indexada 
  * Tiene Fragmentacion interna y externa 
  * Solo tiene fragmentacion interna 
  * Ninguna de las anteriores 
8. La estrategia de estructura de registro ( o Journaling ) 
  * Sirve para tener backups de los datos 
  * Sirve para que las operaciones se completen luego de que el sistema se restablezca por una falla 
  * Sirve para garantizar la consistencia de los datos 
  * Ninguna de las anteriores

## Respuestas 

1. A 
2. E
3. C
4. D
5. B
6. C
7. B
8. C 

