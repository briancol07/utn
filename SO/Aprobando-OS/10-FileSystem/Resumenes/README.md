# File System 

> Maneja la persistencia de datos 

## Objetivos 

* Almacenar datos  y operar con ellos 
* Sopoerte para varios usuarios 
* Minimizar la posibilidad de perdida de datos 
* Maximizar desempenio del sistema 
  * SO : administra espacio en disco y aprovecharlo 
  * Usuario : tiempo de respuesta 
* Soporte distintos dispositivos
* Garantizar la integridad o coherencia de los datos 

## Atributos 

* Nombre 
* Tamanio
* Identificador 
* Permisos 
* Tipo 
* Fechas 
* Ubicacion 

## Operaciones con archivos

### Basicas 

* Crear 
* Borrar
* Abrir 
* Cerrar 
* Leer 
* Escribir 
* Reposicionar
* Truncar 

### Combinadas 

* Renombar 

## Apertura archivo 

Si queremos operar con el debe estar en memoria. Estructuras y mecanismos para administrar los archivos y garantizar y coherencia de los datos 

* Modo de apertura 
* Tabla global de archivos abiertos 
* Tabla de archivos abiertos por procesos 

## Bloqueos / Locks 

> Se pueden dar sobre el archivo completo o sobre bloques 

* Modo de acceso
  * Compartido 
  * Exclusivo 
  * Sugerido 
  * Obligatorio 

## Tipos de archivos 

* Archivos del sistema operativo 
* Archivos Regulares 
* Archivos Ejecutables 

## Metodos de acceso 

* Acceso secuencial 
  * Si queremos acceder a cierta parte d informacion, tenemos que pasar por donde esta almacenada 
* Acceso Directo 
  * Le paso la ubicacion del archivo y con eso es suficiente. 
* Acceso indexado 
  * Recorro los indices 

## Rutas 

* Ruta Absoluta : Desde root 
* Ruta relativa  : Desde la posicion actual 

## Directorio 

Es un archivo diferente a los regulares, tiene un listado de los archivos que contiene 

### Operaciones 

* Busqueda 
* Borar 
* Crear 
* Listar 
* Renombrar 
* Recorrer 

## Tipos de montajes 

Para filesystem externos, se necesita posicionar en alguna parte de mi sistema .

Un punto de montaje es el directorio o el archivo en el que se hacen accesibles un nuevo sistema de archivos, un directorio o un archivo. Para montar un sistema de archivos o directorios 

* Tipos 
  * SO ( BIOS )
  * Externo 

## Proteccion SO 

* Acceso Total 
* Acceso Restringido 
* Acceso Controlado ( Permisos de acceso ) 

### Tipos de permisos de Acceso 

* Tipo unix ( 777 ) ( Duenio - Grupo - Otros ) 
* Matriz de acceso 
  * Asigna a los usuarios por recursos
  * Por cada achivo tengo que tener un campo 
* Lista de control de acceso ( ACL )
  * Declaro los usuarios permitidos en recursos especificos 
* Contrasenias 

## Disco logico 

> No se opera de manera directa con los dispositivos sino que lo hace mediante dispositivos logicos 

Se trabaja con bloques de tamanio fijo llamados bloques logicos, se tiene fragmentacion interna 

## Paricion 

Una particion es el nombre que se le da a cada division presente en una sola unidad fisica de almacenamieto. 

## Estructura de FS 

* Bloque de arranque o booteo 
* Bloque de control del archivo ( FCB ) 
  * Se guardan los atributos 
  * Info de omo operarlos 
* Bloque de control de volumen 
  * Info de datos existentes 
  * Cantidad de bloques usados 
  * FCB libres 
  * Checkeo ante fallas 
* Estructuras de Directorios 
  * Entrada de dirctorio ( Nombre + atributos o punteros al FCB )

## Estructura de un FS en memoria 

* Tabla de montaje 
* Estructura de directorios 
* Tabla o lista global de archivos abiertos 
* Tabla o lista de archivos abiertos por proceso 

## Implementacion de directorios 

* Lista lineal 
* Lista ordena 
* Arbol 
* Tabla de hash

## Metodos de asignacion de bloques 

Tiene fragmentacion interna 

### Tipos 


#### Asignacion contigua 

* Se basa en poner los bloques de esa forma 
* El acceso rapido por que se donde esta el proximo bloque a leer 
* Acceso directo 
* Fragmentacion externa 

#### Asignacion enlazada o encadenada 

* Se basa en incluir un puntero al proximo bloque 
* Se pierde espacio por cada bloque 
* Si se pierde bloque intermedio pierdo todo lo que seguia apartir de ese 

### Asignacion Indexada 

* Usa bloques logicos ( metadata) 
* Tendra punteros a los diferentes bloques 
* Acceso directo y acceso secuencia 
* No hay fragmentacion externa 

## Gestion de espacios libres 

* Lista de bloques libres 
* lista de porciones libres 
* Bitmap 
* indexada 
* Agrupamiento en bloques 

##  Escritura de un archivo nuevo 

1. Se debe crear un archivo nuevo 
2. Asignarle bloques requeridos 
3. Escribir en el archivo 
4. Cerrar el archivo 

## Recuperacion 

* Comprobacion de coherencia 
* Backups 
* Journaling 
  * Toda operacion se trata como una transaccion, si no llego a hacer no se guarda 

## Area de swapping 


