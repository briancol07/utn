# Extended ( UFS )

![EXT](../img/EXT.png)

1. Super bloque 
  * anlogo al de la fat
2. Descriptor de grupos 
  * Dice donde empieza y termina un bloque
  * Bloques libres 
  * Infor sobre grupos 
3. Bitmap de bloques 
  * Bloques de grupo estan libres o ocupados 
4. Bitmap de inodos 
  * Un inodo es lo que llamamos FCB 
5. Tabla de inodos 
  * Inodos disponibles 
6. Bloque de datos 

> 1 inodo = 1 archivo 

## Inodos

Puntero a los bloques que contienen los archivos 

* FCB 
* Atributos de archivos 
* Puntero que permiten acceder a los datos 

## Tipos de punteros en el inodo

* Punteros Directos : Apunta a bloques de datos 
* Punteros indirectos : apuntan a un bloque de punteros ( Simples, dobles, Triples ) 

> Cantidad de punteros = Tam Bloque / Tam Puntero 

Los puteros dobles apuntan a un bloque que conteine un puntero simple  

## Tamanio maximos de un archivo 

ej Tam Bloque = 1KB 
Tam Puntero = 4 bytes ( PD ) 

( PD + IS + ID + IT ) * Bloque = ( 4 * 256 + 256 ^ 2 + 256 ^ 3) * 1KiB = 16GB 

## Accesos directos 

###  Soft link ( symbolic link ) 

Un archivo de soft link va a apuntear a una ruta en particular donde esta el archivo, Para esto creara otro archivo con tipo symbolic link con otro inodo y el primer puntero del archivo sera el puntero al archivo 

![img](../img/Soft-link.png)

Si yo borro el archivo real, a donde apunta el acceso directo no existe, se rompe.

### Hard link 

Agrego e lmismo valor del inodo para el archiov de acceso directo. Me evita tener que hacer una lectura intermedia y tener que leer un archivo e inodo extrar 

Perdemos cual entrada es la original y si en algun momento quiero borar uno de esas entradas debo modificar el campo de ocntador de HL 

![img](../img/Hard.png)


