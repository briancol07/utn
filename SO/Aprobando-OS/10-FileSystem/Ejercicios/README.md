# Ejercicios

> Para FAT : $`TamFS = TamMaxArchivo`$

## Ecuaciones 

### Tamanio maximo de filesystem

Es la cantidad de punteros que quiero direccionar por el bloque 

$`TamFS = 2^{cantPunteros} * Bloques`$

### Cantidad de punteros por bloque 

$`cant = \frac{TamBloques}{TamPunteros}`$

### Tamanio archivos 

Siendo i la cantidad de inodos 

$`\displaystyle\sum_{i=1}^n * TamBloque`$

> La cantidad de entradas son la cantidad de paginas que tiene el cual es el tamanio de disco dividido el tamanio del cluster 


## Ejercicio 2 

 ```
Dado un filesystem FAT12 con clusters 8 KiB:
a) ¿Cual es el espacio máximo direccionable teórico?
b) Si quisiera direccionar un espacio de 128 Mib, ¿Cuales dos tipos de cambio se le podrían hacer al filesystem?
c) Cual de los dos cambios sería más eficiente en términos de:
1. Un máximo aprovechamiento del espacio del disco
2. Un alto tiempo de respuesta a la hora de contar los clusters libres en el disco
```
* Datos 
  * FAT 12
  * Cluster $`8kib= 2^{13}`$ 
A)
$`MaxTeorico =2^{12} * 2^{13} = 2^{25}`$ 
B)
$`2^{27} =2^{12} * 2^{y}`$ 
$`2^{27} =2^{x} * 2^{13}`$ 

## Ejercicio 3 

 ```
Si un disco rígido de 8 GiB se formatea con FAT32, usando clusters de 4 KiB, y descartando el espacio ocupado por la
información administrativa del filesystem, se pide:
a) ¿Cuántas entradas tendría la FAT?
b) ¿Qué porcentaje del disco estaría ocupado por la FAT?
c) ¿Cuantos bits de cada entrada se desperdiciarían?
 ```  
* Datos 
  * Disc $`8Gib = 2^{33}`$
  * Fat 32 --> 28  --> 32 / 8 = 4 --> 4bytes
  * Cluster $`4Kib = 2^{12}`$

$` \frac{2^{33}}{2^12} = 2^{21}`$

> Cantidad de entradas x el fat me da cuanto estaria ocupado, luego con regla de 3 

$` Tam = 2^{21}* 2^2 = 2^{23}`$


## Ejercicio 4

```
Si se tiene un disco rígido de 4 GiB, y se desea formatear con FAT16:
a) ¿Cuál sería el tamaño mínimo de cluster para poder direccionar el disco? (descartando el espacio ocupado por la
información administrativa del filesystem)
b) Si en este esquema almaceno tres archivos: de 1 KiB, 20 KiB y 1 MiB respectivamente, ¿Qué espacio en disco
ocuparía cada uno?
c) ¿Qué principal desventaja presenta este esquema de formateo?
```

* Datos 
  * 4Gib  --> $`2^{32}`$
  * Fat 16 --> $`2^{4}`$

$` 2^{32} = 2^{16} * 2^y`$ 

$`y = 16`$ 

B) 1 kib  Como es menor que una pag ocupara 16 -> $`2^{16} = 64Mib`$

20kib = 2^16

## Ejercicio 7 
```
Se tiene un sistema con ext2 como filesystem. Los bloques de disco son de 1 KiB y los punteros son de 4 bytes. 
Indique el tamaño máximo teórico de un archivo para las siguientes conformaciones de punteros:
a) Solamente 12 punteros directos
b) 12 punteros directos y 1 indirecto
c) 12 directos, 1 indirecto, 1 doblemente indirecto y 1 triplemente indirecto
```
* Datos 
  * Ext2
  * Bloques = 1 Kib --> $`2^{10}`$
  * Punteros = 4 bytes --> 32 bits

$`TamFS = 2^{cantPunteros} * Bloques`$

$`TamFS = 2^{32} * 2^{10} = 2^{42} = 4Tib`$

$`cant(Punteros por bloque )  = \frac{TamBloques}{TamPunteros}`$

$`cant = \frac{2^{10}}{2^2}`$

### A ) 

$` TamArchivo = 12D * 1kb = 12kb `$

### B ) 

$` 12D + IS `$
$`TamArchivo =  (12D + IS) * 1Kb `$
$`TamArchivo =  (12 + 256) * 1Kb `$

### C ) 

$` 12D + IS + ID + IT `$
$` TamArchivo = (12 + 256 + 256^2 + 256^3) * 1kb `$


## 
