# Biografia 

* [Programa de la materia](./Programa-Sistemas-Operativos.pdf)
* Libros
  * Fundamentos de Sistemas Operativos - Silberschatz - Galvin - Gagne. 7ma Edición en adelante
  * Sistemas Operativos - Stallings. 5ta Edición en adelante (preferentemente en Inglés).

> El  que mas se utiliza es el de Silberchatz que es el de los dinosaurios

## Resumenes 

* [SO-for-Dummies](./SO-For-Dummies.pdf)
* [Resumen-Yami](./Resumen-SO-Yami.pdf)




