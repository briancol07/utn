# Concurrencia 

* Stallings 5ta ed Cap: 5
* Silberschatz 7ma ed Cap: 6 

## Topic

* [Concurrencia](./Resumenes/01-Concurrencia.md)

## Ejercicios 

* [Ejercicios](./Ejercicios/README.md)

## Test Autoevaluacion 

* [Test](./Test-Autoevaluacion/README.md)
