# Concurrencia, sincronizacion y mutua exclusion 

## Condicion de carrera 

Dependiendo quien llega primero a tomar un recurso, actua sobre el y si no tenemos cuidado y lo trabajamos de manera erronea, podemos generar inxonsistencias en nuestro resultado

* Competir por recursos 
* Compartir recursos 
* Condicion de carrera 


## Seccion critica 

Espacio en el codigo e el cual se hace uso de un recurso que esta compartido por difererentes procesos 

## Interaccion entre procesos 

* Comunicacion entre procesos 
* Competencia por los recursos
* Cooperacoin de los proceso 

## Seccion Critica 

1. Exclusion Mutua
  * Adentro de la seccion critica puede haber un unico proceso / hilo trabajando sobre el recurso
2. Progreso 
  * Todos los procesos esperan para entrar a la seccion critica y deben poder ejecutar . 
3. Espera Limitada 
  * Que no haya inanicion
4. Velocidad Relativa 
  * No haga lento el procesamiento 

* Consideraciones 
  * Lo mas chica posible 
  * Un Proceso que no esta en la seccion critica no interfiere de ninguna manera con otros 
  * Permanencia en seccion critica debe ser tiempo finito y reducido 
  * Un proceso puede tener muchas secciones criticas 

### Condiciones de bernstein 

> Si se cumple las siguientes condiciones entonces no existe la posibilidad de condicion de carrera y no estamos en presencia de una seccion critica 

* R(A) interseccion W(B) = Vacio 
  * Las variables que van a leer el proceso A no deben ser la que van a escribir en el B
* R(B) interseccion W(A) = Vacio 
  * Igual que la anterior 
* W(A) interseccion W(B) = Vacio 
  * Ambos conjunto de escritura deben ser distintos 



## Soluciones 

* Hardware 
* Software 
* Semaforos ( SO )
* Monitores ( Programacion ) 

### Software 

1. Espera activa 
  * Exclusion mutua
  * Problemas 
    1. Espera activa 
    2. No tenemos progreso
2. Estados 
  * Progreso 
  * Problemas 
    1. Espera Activa 
    2. No hay exclusion mutua
3. Estados ( Intervanido ) 
  * Exclusion mutua
  * Cambia su estado y luego ve si otro esta en la seccion critica 
  * Problema 
    * Puede haber mas de un proceso en las seccion critica 
4. Estados + Sleep 
   * Con el sleep ponemos el proceso a blocked 
   * Exclusion mutua 
   * Problema 
    * Pueden ceder sin que ninguno avance 
    * Livelock 
5. Cumple con requisitos 
  1. Algoritmo de dekker 
  2. Algoritmo de Petterson 
    * Exclusion mutua
    * Progreso ( livelock ) 
    * Espera activa 
    
### Solucion de hardware 

* Deshabilitar Interrupciones 
  * Evitar que procesador quite la ejecutcion a los programas, lo que hace que no queden sentencias al a mitad 
  * No se pueden deshabilitar las interrupciones de todas las cpus 
* Instruccion especiales del procesador 
   * Nos permite hacer operaciones atomicas que no pueden ser interrumpidas 
   * Test and set 
   * Exchange 

### Provista por SO ( Semaforos )

1. Son utilizados mediante wait(s) y signal(s), son funciones/herramientas provistas por el sistema operativo que se ejecutan atomicamete 
2. Permite exclusion mutua entre varios procesos 
3. Permite Sincronizar (u Ordenar ) varios procesos 
4. Permite controlar acceso a recursos ( Administrar ) 
5. Mas simple de utilizar 

* Funciones semaforos 
  * Iniciar/finalizar (Asignar recursos)
  * Wair(Sem) decrementa en uno el valor del semaforo
  * Signal(sem) Incrementa en uno el valor del semaforo 
* Estructura 
  * Lista de procesos bloqueados ( Variable global ) 
  * Un valor entero, contador de recursos que tiene 
* Caracteristicas 
  1. Exclusion mutua 
  2. Sincronizacion 
  3. Acceso a recursos en n-instancias 
* Tipos 
  * General o contador 
    * Iniciarse con valor 0 o positivos y el valor ira cambiando 
  * Binario 
     * Fuerte ( FIFO )
     * Debiles ( Otra planificacion ) 
  * Mutex 
    * No bloquea 
    * Tiene espera activa 
    * Solo para multiprocesadores 

> wait(Sem) < 0 ->> Se bloquea 

## Productor - Consumidor 

1 o mas entidades que son productores  y uno o mas consumidores de los recursos. Tiene que haber algo para que el consumidor consuma y otro espacio para que el productor deje lo producido ( no sobre algo ya existente ) 

1. Buffer espacio finito 
2. Buffer con espacio infinito 

![Productor-consumidor](../img/Productor-Consumidor.png)

### Solucion programacion Monitores 

Son provistos pr algunos lenguajede programacio. Solo un proceso o hilo puede utilizar el monitor en un determinado momento
* No son primitivas 
* Datos locales y variables de condicion 
* Detras hay soluciones de SW y semaforos 
* No resuelven tema de coordinacion 

