# Test 

1. Mutiprocesamiento implica 
  * Multiprogramacion
  * Procesamiento Distribuido
  * Ninguna de las anteriores 
  * NS/NC
2. Si los procesos interactuan compartiendo recurso del sistema entonces 
  * Es necesario sincronizar su uso, por que el SO no lo hace
  * No es necesario sincronizar su uso por que el SO los administra
  * Ninguna de las anteriores
  * NS/NC
3. En el ejemplo de los incrementos de una misma variable entre dos o mas hilos, visto en clase: 
  * Puede haber problemas debido al uso de variables del stack ( pilA )
  * El comportamiento es indefinido por que siempre se corromperan datos internos 
  * Ninguna de las anteriores
  * NS/NC
4. En la mutua exclusion 
  * Se pierde algo de la performance obtenida de la concurrencia 
  * Es obligatorio que dentro de la region critica el proceso este un tiempo reducido 
  * Ninguna de las anteriores
  * NS/NC
5. En las soluciones de software para garantizar mutua exclusion 
  * No existen algorimos que garantizen la mutua exclusion en un 100%
  * La performace es un factor clave que les juega encontra
  * Ninguna de las anteriores
  * NS/NC
6. En las soluciones de mutua exclusion por hardware 
  * La deshabilitacion de las interrupciones es un mecanismo ineficiente pero seguro 
  * Las instrucciones como **test and set** pueden ser igualmente interrumpidas 
  * Ninguna de las anteriores 
  * NS/NC
7. Al usar semaforos 
  * Al llamar a signal(s), se despierta a un proceso si el semaforo quedo positivo
  * Al llamar a signal(S) se despierta un proceso si el semaforo no quedo positivo
  * Ninguna de las anteriores
  * NS/NC
8. La **Atomicidad** de las funciones de manejo de semaforos se logra
  * Mediante el hecho de que dichas funciones son instrucciones de procesador
  * Usando otro semaforo 
  * Mediante solucion de software o Hardware
  * Ninguna de las anteriores 
  * NS/NC

## Respuestas 
1. A
2. A
3. D
4. A
5. B
6. A
7. B
8. C



