# Memoria Real 

## Clase grabada 

* [Clase](https://www.youtube.com/watch?v=ZoIYHGR1adA&list=PL6oA23OrxDZDs0NotvQKRzGLX-iG3wJa1&index=8)

## Indice 

* [Memoria](./Resumenes/README.md)

## Ejercicios 

* [Ejercicios](./Ejercicios/README.md)

## Test autoevaluacion 

* [Test](./Test-Autoevaluacion/README.md)
