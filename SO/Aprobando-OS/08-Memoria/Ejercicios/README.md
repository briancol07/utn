# Ejercicios 

## 1 

* Datos
 * 8 paginas  -> 2^3 para direccionar ( 000 ) 
 * 1024 bytes C/U  -> 2^10
 * Memoria fisica 32 -> 2^5 

```
-----------------------------
| Pagina | Direccionamiento |
-----------------------------
| 3 bits  | 10 bits          | 
----------------------------

```
A) Bits de direccion logica ( 3 + 10 )  = 13
B) Bits Direccion Fisica =  5 + 10 = 15  
C) Sin memoria virtual la direccion logica tiene que ser menor o igual que la fisica 

## 2 

* Datos 
   * 256 KiB
   * 20 bits direccionamiento logico
   * Paginas 4 KiB

A) Tamanio max = 256 KiB ( Todo el tamanio de memoria real ) 
B) Cantidad maxima de fragmentacion 4095


## 3 

Dir Fisica = Base + Desplazamiento 

![Ejer3](../img/Ejer-3.png) 
