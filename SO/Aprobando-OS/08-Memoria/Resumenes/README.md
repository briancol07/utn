# Memoria Real 

## Introduccion

* Gran array 
* CPU ( Lee o escribe)
* Kernel ( Administra ) 
* Para ejecutar se debe cargar en memoria el proceso 

## Requisitos 

1. Reubicacion 
2. Proteccion 
3. Comparticion
4. Organizacion fisica y logica 

## Proteccion 

Registros **BASE** y **LIMITE** son donde comienza y donde termina. 

## Asinacion de direcciones 

0. Tiempos de programa 
1. Tiempos de compilacion 
2. Tiempos de carga 
3. Tiempos de ejecucion 

## Direcciones logicas y  fisicas 

* Direccion Logica: Referencia a la ubicacion de memoria

## Memory management unit ( MMU )

* Componentes de hardware que hara la traduccion de las referencias a las posiciones de memoria 
* Es la encargada de traducir la direccion logica a una direcion fisica 
* Cpu conoce la direcion logica 
* Dentro del procesador 

![MMU](../img/MMU.png)


## Direccion Logica y fisica 

* Direccion logica : Ubicacion de memoria utilizada por los procesos, son independientes de la ubicacion real en memoria
  * Direccion Relativa: Expresada segun un punto conocido
* Direccion Fisica: Verdadera ubicacion en memoria 

## Carga, enlace y bibliotecas compartidas 

### Enlace estatico 

Las bibliotecas del sistema se aniaden al ejecutable en la etapa de enlace. No se puede actualizar la biblioteca

### Carga dinamica 

Tenemos cargadas las bibliotecas en el ejecutable.
Podemos cargarla en tiempos de ejecucion a la imagen del proceso

### Enlace dinamico 

No agranda la imagen del proceso en memoria sino que el SO permite a los procesos para usar la biblioteca, con actualizar la biblioteca estamos, no hay que compilar

## Asignacion de memoria 

* Cargar procesos en memoria 
* En memoria SO y procesos de usuario 

### 1. Particionamiento FIJO

* Se divide la memoria en particiones de tamanio fijos.
  * Todas las particionesson del mismo 
  * Las particiones son todas diferentes 
* Se debera conocer donde empieza y donde termina cada particion. 
* Multiprogramacion y procesos limitados 

![Particionamiento-Fijo](../img/Particionamiento-FIjo.png)

#### Tenicas para asignar 

* 1 sola cola 
  * SI el proceso no puede entrar bloquea la fila 
* Una cola por cada particion 
  * Carga procesos que entren , si muchos procesos de tamanios parecidos dejan peticiones libres y colas largas

### 2. Particionamiento variable 

A cada proceso se le asigna la memoria que necesita. No hay numero fijo de particiones. Lo que pasara es que la memoria se ira fragmentando, generando huevos mas chicos cada vez .
* Necesitamos dos estructuras 
  * Una para los procesos 
  * Otra para los huevos 
  * Cada una con su inicio y tamanio 

![Particionamiento-Variable](../img/Particionamiento-Variable.png)


#### Algoritmos de ubicacion de procesos 

1. ** Primer ajuste**: primer hueco disponible desde el comienzo de memoria ( Menor overhead ) 
2. ** Siguiente ajuste**: Primer huevo disponible desde la ultima asignacion
3. ** Mejor ajuste**: Busca el hueco mas chico donde pueda ubicarse 
4. ** Peor ajuste** : Busca el hueco mas grande, esto hace que no queden huecos muy pequenios

### 3. Buddy System

Se asigna a los procesos tamanios de memoria que son potencias de 2. Va a dividir la memoria en porciones con tamanio 2^n y le asignan al proceso el tamananio mas chico posible
* Tiene fragmentacion interna
* Se usa primer ajuste

![Buddy](../img/Buddy.png)

## Fragementacion Interna 

Si en un particionamiento fijo tenemos particiones de menor tamanio ocupadas y quiere meter un proceso con tamanio chico, se ira a una particion mas grance. El espacio que sobra es la fragmentacion interna.

![Interna](../img/Fragmentacion-Interna.png)

## Fragmentacion Externa

Se da cuando la memoria libre es mayor al tamanio del proceso que se quiere cargar pero la misma esta fragmentada en varios huevos de menor tamanio que el proceso . Haciendo que no sea posible cargarlo en memoria 

![Externa](../img/Fragmentacion-Externa.png)

## Compactacion 

Proceso en el cual se reubican los procesos al comienzo de la memoria. para poder tener todos los huecos contiguos. Genera mucho overhead y los procesos no se puede ejecutar durante este proceso 

## Segmentacion 

* Proceso no necesita estar contiguo en memoria 
* Tamanios variables 
* Cada segmento una parte del proceso 
* Segmentos se ubican en los huecos disponibles 
* Hay Fragmentacion externa 
* Necesito una estructura externa para segmentos 
* No hay fragmentacion interna 

### Direccion logica a fisica 

Veo la base del segmento y hago el desplazamiento ( lo sumo ), todo esto en la tabla de segmentos 

n segmento | Desplazamiento 
:----------:|:---------------:
3     | 50A

### Problemas con proteccion ( Segmentation Fault )

Es un tipo de violacion a la seguridad que ocurre cuando el desplazamiento es mayor al limite del segmento, Accediendo a un espacio de memoria que no le corresponde .

### Proteccion ( Permisos ) 

* Escritura ( W )
* Lectura ( R ) 
* ejecucion ( X ) 

## Paginacion 

* La memoria se divide en frames 
* El proceso no necesita estar contiguo en memoria 
* Guardar en desorden 
* Tenemos fragmentacion interna pero no externa 
* El tamanio de la paignas usualmente 4K
* Necesitamos una tabla de paginas del proceso 
* La MMU consutla la tabla y el S.O sera el encargado de separar el proceso en paginas y crear tabla 
* No hay segmentation fault 

![paginacion](../img/Paginacion.png)

### Direccion Logica a fisica 

* Si esta en decimal hago num\_pagina * tamanio\_pagina + desplazamiento 
* Cantidad de paginas por proceso 2^x 
* Tamanio de pagina es 2^y 

> En la paginacion tambien tendremos esta "tabla extra" que estara en memoria y necesitaremos 2 accesos a memoria 

### Problemas con la proteccion 

Como las paginas son divisiones con tamanios fijos y no con algun sentido logico, es mas dificil darles permisos, por que podria pasar que comparta permisos con la pila y le den a ambos permisos 
Por cada pagina necesiot un bit para saber si el frame es valido o no 

### Comparticion 

El SO permite que 2 o mas procesos tengan una pagina o mas en el mismo frame. 

### Tabla o vector de marcos libres 

Con 1 esta libre y con 0 esta ocupado , bitmap en frames 

## Segmentacion paginada 
 
* Combinacio nde segmentacion y paginacion . 
* Primero segmentamos y luego paginamos. 
* Combina las ventajas de ambas, al paginar segmentos evito la fragmentacoin externa pero segumos teniedo paginacion interna. 
* No tenemos segmentation fault 

### Direccion fisica a logica 

La direccion logica tiene el numero de segmento y de pagina y tengo que buscar en ambas paginas para saber el frame a la que apunta y luego hacer desplazamiento 

* La responsabilidad del SO es armar actualizar y eleiminar tablas , CPU navegarlas 

