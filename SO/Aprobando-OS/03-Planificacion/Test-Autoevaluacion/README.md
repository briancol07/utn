# Test de Auto evaluacion 

1. Los planificadores de largo y mediano plazo pueden afectar el orden de los procesos
  * En la lista de procesos listos para ejecutar
  * En la lista de procesos nuevos 
  * En la lista de procesos suspendidos 
2. El planificador de corto plazo es invocado cuando
  * Se realiza una llamada al sistema y una interrupcion 
  * Se realiza una llamada al sistema o una interrupcion
  * Ninguna las anteriores
3. El response tieme es un criterio de planificacion 
  * Orientado al usuario
  * Orientado al sistema
  * Ninguna de las anteriores 
4. Al utilizar el algoritmo round robin, el quantum 
  * Deberia ser lo mas grande posible 
  * Deberia ser lo mas chico posible 
  * Ninguna de las anteriores 
5. Linterrupcion de clock permite 
  * Que el procesador realice un cambio de prooceso 
  * Que un proceso pueda realizar una llamada al sistema
  * Ninguna de las anteriores 
6. Para algoritmos que usan la proxima rafaga de CPU, como SJF
  * Generalmente solo se puede implementar con un estimador de rafagas 
  * Generalemente se pueden implementar de una, por que los procesos proveen esa informacion
  * Ninguna de las anteriores 
7. Starvation es una problematica que 
   * Ocurrira siempre que se utilizen algoritmos SJF
   * Podria nunca ocurrir, independientemente del algoritmo utilizado 
   * Ninguna de las anteriores
8. En un algoritmo multinivel realimentado (Feedback multinivel ) 
  * La prioridad de los procesos es dinamica 
  * La prioridad de los procesos es estatica 
  * Ninguna de las anteriores 
9. En un SO con RR  y procesos CPU-BOUND , IO-BOUND se verian perjudicados 
   * Los procesos CPU bound 
   * Los procesos IO-bound 
   * ninguna de las anteriores 

## Respuestas 

1. A
2. B
3. A
4. C
5. C 
6. A
7. B
8. A
9. B
