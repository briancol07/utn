# Planificacion

Conjunto de politicas y mecanismos del SO que gobiernan el orden en que se ejecutan los procesos.

La ejecucion de un proceso va a alternar entre rafagas de CPU y E/S 

* Problemas de la multiprogramacion 
  * Disparidad de tiempo en el uso de la CPU 
  * Procesos que no llegan a ejecutarse nunca 
  * Degradacion del tiempo de respuesta del sistema
  * Memoria Ram llena y CPU sin utilizarse 
* Tipods de porcesos 
  * CPU bound : requieren mas tiempo de CPU 
  * I/O  bound : requieren mas tiempo de entrada saldia 


## Planificadores 

1. Largo plazo 
2. Mediano Plazo 
3. Corto plazo 

### Largo plazo ( Admision y salida ) 

* Son lo procesos que entran en new y exit. 
* Regula el grado de multiprogramacion 

### Mediano plazo ( Reactivacion y suspension ) 

* Activar y suspender procesos, para bajar grado de multiprogramacion. 
* Pasa a memoria de disco. 
* Swap in / swap out

### Corto Plazo ( Activacion y temporizacion ) 

* Entre los estados Ready, running y blocked (se colo).
* Decide cual sera el proximo proceso a ejecutar de la cola de listos. 

## Algoritmos de planificacion 

* Sin desalojo 
  * FIFO
  * Short job firrst ( SJF ) 
  * Highest ratio reponse next ( HRRN ) 
* Con Desalojo 
  * SJF ( SRT ) 
  * Round Robin  ( RR ) 
  * Virtual Round Robin ( VRR ) 

> Algunos pueden sufrir de inanicion ( Starvation ) : Situacion que se le niega la posibilidad de utilizar un recurso .( un proceso nunca entra a Running ) 

### FIFO 

* El primero que llega es el primero en ejecutar 

![Tabla FIFO](../img/Tabla-fifo.png)
![cuadro-fifo](../img/Cuadro-fifo.png)

### SJF

* Prioriza el proceso que tenga menor rafaga de cpu 
* Sufre de starvation

![Tabla SPN](../img/tabla-spn.png)
![cuadro-SPN](../img/Cuadro-spn.png)

### HRRN 

> No tiene desalojo 

* Tiene aging, cuanto mas espera la cola de listos mas aumenta su prioridad 
* Formula: `R = (W/S)+1` ( w = tiempo esperado en ready , S = lo que va usar de cpu)

![Tabla HRRN](../img/Tabla-HRRN.png)
![cuadro-HRRN](../img/Cuadro-hrr.png)

### SJF con desalojo 

Igual que el otro pero con desalojo , y tambien puede ocurrir inanicion

![Tabla SJF](../img/Tabla-SJF.png)
![cuadro-SFJ](../img/Cuadro-SJF.png)

### RR 

Cola de procesos en listos es fifo , pero utiliza un quantum de tiempo, que salta una interrupcion.
Si el quantum es muy grande seria como fifo, y sino mucho overhead 

![Tabla RR](../img/Tabla-RR.png)
![cuadro-RR](../img/Cuadro-RR.png)

### VRR
Tiene un quantum de tiempo pero ademas tiene 2 colas de procesos listos para ejecutar, La cola llistos auxiliar tendra mas prioridad, se le da mas prioridad a los io bound 

## Simultaneidad de eventos 

1. Interrupcion clock 
2. Interrupcion por finalizacion de evento 
3. Llamada al sistema 

![Simultaneidad eventos](../img/Simultaneidad-eventos.png) 

## Colas multinivel
 
Cada cola tiene su propio algoritmo de planificacion, mientras que hayan procesos en las colas prioritarias las de abajo no se ejecutan ( mucha inanicion 

Existen con retroalimentacion, Si hay desalojo por quantum bajoa su prioridad y se usan mecanismos de aging para evitar inanicion en las de mas abajo 
