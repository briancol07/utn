# Deadlock 

* Los recursos que tenemos son limitados 
* Procesos : 
  * Solicitan
  * Usan 
  * Liberan 
* Si recurso no esta disponible, un proceso puede bloquearse  

![Deadlockl](../img/Deadlock-autos.png)

> Deadlock: Bloqueo permanente de un conjunto de procesos donde cada uno de estos procesos estan esperando un evento que solo puede generar un proceso del conjunto 

> Livelock : es un deadlock pero implica un bloqueo de procesos que consumen procesador 

## Tipos de recursos 

* Reutilizables 
  * Semaforos 
  * Variables 
* Consumibles 
  * Interrupciones 
  * Mensajes por socket 

Las instancias existen solo si las puedo usar de manera indistinta por que para mi y el sistema son identicas entre si 

## Grafo de recursos 

Permite representar el estado del sistema con respecto a la asignacion de los recursos a cada proceso en un momento determinado. 

![Tipos](../img/Tipos.png)
![Grafo](../img/Grafo.png)

## Ciclos 

* Si no hay ciclos no hay deadlock 
* Si hay algun ciclo , puede significar existencia 
* Si hay un ciclo y todos los recursos tienen una instancia si o si hay deadlock 

## Condiciones para existencia de deadlock 

* CNyS
  * Mutua Exclusion 
  * Retencion y espera 
  * Sin desalojo de recursos 
  * Espera circular 

## Tratamiento de deadlock 

1. Prevencion del deadlock 
2. Evasion de deadlock 
3. Deteccion y Recuperacion 
4. No tratarlo 
5. Estrategia integrada 

### 1. Prevencion 

> Garantiza que nunca ocurra impidiendo que se produzca alguna de las 4 condiciones 
1. Evitar mutua exclusion 
  * Si hay recursos que no se pueden compartir no puede evitarse 
2. Evitar retencion y espera 
   * Solicitar todos los recursos juntos ( Si no puede todos no se le asignan )
   * Solicitar de a 1 o varios, usarlos y liberarlos 
3. Tener desalojo de recursos 
  * Si un recurso solicita uno y esta en uso debe desalojar los recursos que tiene 
  * Si A pide recursos que B tiene y este esta esperando otros, desaloja B para darselos a A
4. Romper espera circular 
   * Orden creciente 

### 2. Evasion deadlock 

> Garantiza que nunca ocurra 

1. Denergar el inicio de un proceso 
  * El proceso podra iniciar si y solo si las sumas maximas de necesidades declaradas por todos los procesos, mas la necesidades maximas declaradas por el proceso que quiere iniciar son menores que los recursos totales del sistema 
2. Denegar la asignacio nde un recurso 
  * Algoritmo del banquero 
    * Estado seguro ( No hay deadlock , se asigna ) 
    * Estado inseguro ( Puede existir deadlock no se le asigna el recurso del proceso ) 

![algoritmo](../img/Algoritmo-Seguridad.png) 

> El algoritmo del banquero se debe ejecutar cada vez que un proceso solicite un recurso

### 3. Deteccion y Recuperacion de deadlock 

> Puede ocurrir deadlock, no hay restricciones para asignar recursos disponibles. Periodicamente se ejecutar el algoritmo de deteccion apra determinar la existencia de Deadlock 

* Opciones de recuperacion 
  * Terminar procesos involucrados 
  * Retroceder un proceso al estado anterior 
  * Expropiar recursos hasta que no exista deadlock 
  * Termnar algun proceso involucrado 
* Criterios para Seleccion de que proceso terminar 
  * Menor tiempo de CPU Consumido 
  * Menor cantidad de salida producida 
  * Mayor tiempo restante estimado 
  * Menor numero total de recursos asignados 
  * Menor prioridad 

> La unica forma de determinar deadlock es con un algoritmo de deteccion 

Estado  | Descripcion
:------:|:-----------:
Algoritmo del banquero | Deteccion y recuperacion de deadlock 
No se puede pedir recursos si > Mi | Solicitamos lo que necesito ( antes )
Se hace cada vez que solicitan un recurso | Se ejecutan periodicamente ( Detecta bajo uso cpu / recursos ) 
Ve si hay uin estado seguro o estado inseguro | Ve si hay deadlock o no 
Nunca llega Deadlock | Puede llegar a ocurrir deadlock 

### 4. No tratarlo

1. En la prevencion tiene mucho costo disponer un orden para acceder a los recursos 
2. En la evasion/Prevencion se tiene muchisimo overhead con el algoritmo del banquero 
3. Y en la retencion y recuperacion igual 

### 5. Estategia integrada 

Agrupacion de recursos y a cad grupo se le aplica alguna de las tecnicas 
