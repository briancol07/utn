# Introduccion 

* Inicialmente se usaba la tecnica de overlaying 
* Las referencias a memeoria dentro de un proceso son DL que pueden ser traducidas a DF en tiempo de ejecucion
* Un proceso puede estar en MP o en Swap, y esto pued ser transparnete para el 
* No es necesario que todo el proceso este en MP 
  * + procesos en memoria
  * Procesos + grandes que la memoria 

## Principio de localidad
 
* La localidad es un set de paginas que se utilizan en conjunto. Los procesos utilizan diferentes localidades durante su ejecucion. Las localidades se pueden  solapar 
* Localidad Temporal : si en un momento una poscion de memoria particular es referenciada, entonces es muy probable que la misma ubicacion vuelva a ser referenciada en un futuro cercano
* Localidad Espacial: Si una localizacion de memoria es referenciada en un momento concreto, es probable que las localizaciones cercanas a ella sean tambien referenciadas pronto.

## Paginacion por demanda 

* Las Paginas se cargan en memoria fisica solo cuando se necesitan 
* El swapping es lazy se mueven solo las paginas que se necesitan 
