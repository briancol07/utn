#include<stdlib.h>
#include<stdio.h>

int main(void){
  int i = 1;
  int *p;
  printf("antes i vale %d \n",i);

  p = &i; // p apunta a i
  *p = 2; // Sele asigna a donde este apuntando p(i) el valor 2

  printf("ahora i vale %d y el contenido de p vale %p \n",i,*p);

  return 0;

}
