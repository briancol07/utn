# Manejo de Punteros y Memoria Dinamica en C 

## Introduccion

### Proceso 

Es un programa en ejercucion y se pude dividir en 4 segmentos:

* Codigo 
* Datos (globales)
* Heap 
* Stack 

### Memoria Estatica 

Memoria reservarda al declarar variables (int,float,char,etc), estructuras como asi los punterosa tipos de datos, se aloja en el stack del proceso. En este caso el programador no podra modificar el espacio en memoria que ocupan ni tampoco tendra que encargarse de liberarla. 
Al producirse una llamada a una funcion, se almacena en el stack del proceso la direccion al a que debe retornar la ejecucion tras finalizar la llamada, y otros datos adicionales.

### Memoria Dinamica 

Se reserva en tiempo de ejecucion y se aloja en el heap del proceso (los datos apuntados por los punteros). El programador es el que se encarga de reservar y controlar dicha memoria.
Esta se puede ir modificando durante la ejecucion del programa.

* Ventajas 
  * Se puede reservar tamanio no conocido hasta el momento de ejecucion 


### Tiempo de vida 

* Estaticas 
  * se crean una unica vez y se destruyen con la destruccion del mismo 
  * clasificador static 
* Automatica 
  * No declaradas con static 
  * Se crean al entrar al bloque en el que fueron declaradas y se destruyen al salir de este bloque. 
* Asignada 
  * Memoria reservada en forma dinamica (heap) 

## Puntero 

### Que es ? 

Es la direccion de algun dato en memoria. No es el dato en si mismo sino su posicion en la memoria (Referencia) 

### Como lo declaramos? 

```c
int *p;
```

Si bien esta declarado, no esta inicializado y apunta a basura

### Como incializamos un puntero? 

> Usaremos la biblioteca stdlib.h 

* Malloc 
  * Sirve para solicitar un bloeuq de memoria del tamanio indicado por parametro 

``` c 
void *malloc(unsigned numero_de_bytes);
```
> Malloc no nos garantiza que la zona de memoria no este utilizada, para eso debemos chequear que no sea  NULL, lo que devolveria si es que esta ocupada. 

### Saber tamanio 

> Con sizeof , recibe por parametro un tipo de dato y nos devuelve el tamanio en bytes

```
int *p = malloc(sizeof(int));
```

## Ejemplo de programa 

```c 
#include <stdlib.h>

int main(void){
  int *p = malloc(sizeof(int));
  *p = 2;
  return 0;
}
```
al hacer \*p estamos diciendo "al contenido de p" o "a lo apuntado por p". si hicieramos p = 2 estariamos modificando al punterop y no al valor apuntado por el puntero p 

* Para inicializar:
  * Reservar el espacio en memoria
  * Darle un valor inicial al dato contenido ese espacio en memoria

## Operaciones de punteros

### Direccion de una variable (&)

> Nos devuelve la direccion en memoria de su parametro 

* see [ampersand](./ampersand.c)
* see [desreferencia](./desreferencia.c)


