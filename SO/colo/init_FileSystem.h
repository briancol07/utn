#ifndef INIT_FILESYSTEM_H_
#define INIT_FILESYSTEM_H_

// se usa para inicializar conexion con kernel

#include <commons/log.h>
#include <commons/config.h>
#include "../../shared/include/Sockets.h"
#include "./t_config_FileSystem.h"
#include <stdbool.h>
#include <stdlib.h>


bool cargar_configuracion(t_log *logger, t_config_FileSystem* config );
bool generar_conexion(t_log *logger, int *fd_memoria, t_config_FileSystem* cfg_memoria); 
void cerrar_programa(t_log *logger, t_config_FileSystem *cfg);

#endif // INIT_FILESYSTEM_H_