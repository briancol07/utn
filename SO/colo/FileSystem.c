#include "../include/FileSystem.h"


t_config_FileSystem* initialize_cfg() {
  t_config_FileSystem* cfg = malloc(sizeof(t_config_FileSystem));
  cfg->IP_MEMORIA = NULL;
  cfg->PUERTO_MEMORIA = NULL;
  return cfg;
}

int main(int argc, char const *argv[]) {

  t_log *logger =
      log_create("logs/FileSystem.log", "FileSystem", true, LOG_LEVEL_TRACE);
  t_config_FileSystem *cfg_FileSystem = initialize_cfg();

  int* fd_memoria;

  if (!cargar_configuracion(logger, cfg_FileSystem) || !generar_conexion(logger,fd_memoria,cfg_FileSystem) ) {
    cerrar_programa(logger, cfg_FileSystem);
    return 1;
  }

  return 0;
}