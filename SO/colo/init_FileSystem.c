#include "../include/init_FileSystem.h"

bool cargar_configuracion(t_log *logger, t_config_FileSystem *config) {

  t_config *cfg = config_create("cfg/FileSystem.config");

  if (cfg == NULL) {
    log_error(logger, "No se encontro  el FileSystem.config\n");
    return false;
  }

  if (!config_has_property(cfg, "IP_MEMORIA") ||
      !config_has_property(cfg, "PUERTO_MEMORIA")) {
    log_error(logger, "Le faltan propiedades a FileSystem.config\n");
    return false;
  }

  config->IP_MEMORIA =     strdup(config_get_string_value(cfg, "IP_MEMORIA"));
  config->PUERTO_MEMORIA = strdup(config_get_string_value(cfg, "PUERTO_MEMORIA"));

  log_info(logger, "Se cargo FileSystem.config correctamente :)\n");

  config_destroy(cfg);

  return true;
}

bool generar_conexion(t_log *logger, int *fd_memoria, t_config_FileSystem *cfg_memoria) {

  *fd_memoria =
      crear_conexion(logger, "Server Memoria", cfg_memoria->IP_MEMORIA, cfg_memoria->PUERTO_MEMORIA);

  return *fd_memoria != 0;
}

void cerrar_programa(t_log *logger, t_config_FileSystem *cfg) {
  log_destroy(logger);
  free(cfg->IP_MEMORIA);
  free(cfg->PUERTO_MEMORIA);
  free(cfg);
}
