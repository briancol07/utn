#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/wait.h>
#include<errno.h>

int main(){

  int id1 = fork();
  int id2 = fork();

  if (id1 == 0){
    if (id2 == 0){
      pritnf("Process y \n");
    } else {
      printf("Process x \n");
    }

  }else {
    if (id2 == 0){
      printf("We are in process z \n");
    }else {
      printf("we are the parent process\n");
    }
  }

  while(wait(NULL) != -1) || errno != ECHILD );
  return 0;
}
