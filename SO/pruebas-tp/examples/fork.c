#include<stdio.h>
#include<string.h>
#include<stdlib.h>
// this is for fork linux lib 
#include<unistd.h>

int main(int argc , char *argv[]){
  // this will print 2 times hellow fork 
  int id = fork();
  printf("Hellow fork %d \n",id);

  if ( id == 0 )  printf("Hello from child process");
  else printf("hello from main");

  // to fork only from main  
  if ( id != 0 ){
    fork();
  }

 
  

  return 0;
}
