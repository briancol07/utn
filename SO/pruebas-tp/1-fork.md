# Fork()

Work with multiple proccess 

see [fork.c](./examples/fork.c)

When you use fork() a child process is created and started executing the following lines. 
The child process lunch and start executing at the same time that the main program 

Also gives us an id (number)

The part of the program will be executed the times of fork you have 

The child proccess is the id number 0, the other id is main 

```c 
fork();
fork();
```

This will give us 4 times one from the main and other for the child

if you want to get like three you need to call it from the main not from the child 
