# Segmentacion con Paginacion 

* La paginacion y la segmentacion se pueden combinar en la segmentacion con paginacion
* En este esquema de gestion de memoria los segmentos se paginan 
* Se apoya la vision de la memorio que tiene el usuario 
* Se resuelve el problema de la asignacion dinamica 
* Es necesario una tabla de segmentos y una tabla de paginas por cada segmento 
* La traduccion de direcciones es mas compleja y peude requerir un mayor numero de accesos a memoria en el peor cado 
