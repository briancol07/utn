# Memoria Fisica Real Principal (RAM)

> No sabe que cosas contiene, pueden ser variables o procesos,etc 

## Uso Eficiente 

1. Reubicacion 
  * Si va de disco a swapping tendria que poder seguir accediendose 
2. Proteccion 
  * Un proceso no deberia poder accceder al espacio de memoria de otro proceso 
3. Comparitcion 
  * Permite que varios procesos compartan espacios de memoria 
4. Organizacon fisica y logica 
  * La fisica = Array grande 

## Proteccion 

Registro Base (donde empieza) y limite (donde termina)

## Asignacion de Direcciones (Address Binding) 

* Creacion proceso (pasos)
  * Codigo Objeto
  * Ejecutable 
  * Carga 

0. Tiempors de programa 
1. Tiempos de compilacion 
2. Tiempos de carga 
3. Tiempo de Ejecucion 

## MMU Memory Management Unit 

> Componente de HW, hace la traduccion de las referencias a las posiciones de memoria 

Traducir Direccion logica ---> Direccion Fisica 

## Direcciones Fisicas y logicas 

* Direccion 
  * Logica 
    * Referencia a una ubicacio nen memoria ulitzada por los procesos. Son independientes de la ubicacion real en memoria 
  * Relativa 
    * Un tipo de direccion logica, en que la direccion se expresa segun a un punto conocido 
  * Absoluta/fisica 
    * Referencia a una verdadera ubicacion en memoria 

## Carga, Enlace y bibliotecas compartidas 

* Enlace Estatico 
  * Se aniaden bibliotecas al ejecutable en la etapa enlace 
* Carga Dinamica 
  * Teemos cargadas las blibliotecas en el ejecutable, cargamos otras en tiempo de ejecucion 
* Enlace Dinamico (En tiempo de ejecucion) 
  * Hy una referencia a la bilbioteca compartida y se accede unicamente cuando la necesitemos, no se agranda la imagen 

## Asignacion de memoria 

* Cargar procesos en memoria 
  * Optimiza para asegurar un grado alto de multiprogramacion 
* Memoria 
  * Reside el SO y procesos de usuario 

## Asginacion de memoria para procesos(estrategias) 
 
### Particionamiento fijo 

Se divide la memoria en particiones de tamanios fijos 

* Todas las particiones son del mismo tamanio
* Las particiones son de tamanios diferentes 

> Un proceso no puede ocupar mas de un espacio 

> Grado de multiprogramacion y tamanio de procesos limitados 

#### Asignacion procesos a particiones 

1. Cola de procesos de nuevos
  * A medida que se liberan cargo otros 
  * Una cola de espera por cada particion 

### Particionamiento Variable 
  * No hay numero fijo de particiones 
  * Memoria se ira fragmentando y generando huecos 

#### Algoritmos de ubicacion de procesos 

1. Primer Ajuste 
  * Primer hueco disponible desd el comienzo de la memoira (mejor rendimiento - overhead)
2. Siguiente Ajuste 
  * Primer hueco disponible desde la posicion de la ultima asignacion
3. Mejor Ajuste 
  * Busca el hueco mas chico donde peuda ubicarse 
4. Peor Ajuste 
  * Busca el hueco mas grande 
  * Se hace para que no queden huecos pequenios 
 
### Buddy System (Descomposicion Binaria) 

Compensa la desventajas del particionamiento fijo y dinamico

Divide la memoria en potencias de 2(2^n) redondeado a la siguiente, va a dividir toda la memoria en porciones con tamanio de 2^n y le asignan al proceso un espacio lo mas chico posible donde entre.

## Fragmentacion Interna y Externa 

### Interna

Si en un particionamiento ijo tnemos particions de menor tamanio ocupadas y se quiere meter un proces ocn tamanio chico deber ponerlo en una particion mas grande generando derroche de memoria, ya que no se puede usar hasta que se libere

### Externa 

Se da cuando la memoria libre es mayor al tamanio del proceso que se quiere argar per ola misma esta fragmentada en varios huevos de menor tamanio que el proceso haciendo que no sea posible cargarlo en memoria 

## Compactacion 

Proceso por el cual desplazamos los procesos al comienzo de la memoria por lo que hay memoria suficiente (libre). Me genera ese hueco necesairio 

## Segmentacion 

> Particionamiento dinamico pero con mejoras 

* Proceso no necesia esta contiguo en memoria 
  * Se divide en segmentos de tamanio variable 
* Cada Segmento representa una parte del proceso desde la vison del programador 
* Vamos a poder ubiscar esos segmentos en cualquier hueco que este disponible 
* Seguimos con Fragmentacon externa, sera menor por los sementos son mas chicos 
* No tendremos fragmentacion Interna

## Direccion logica a Fisica

Con la base y el desplazamiento 

* Cantidad maximas de segmentos por proceso 2^x 
* El tamanio maximo de un segmeno 2^y 
* Tamanio maximos de las idrecciones 2^(x+y)

## Segmentation Fault 

Violacion a la seguridad que ocurre cuando el desplazamiento es mayor al limite del segmento 

## Proteccion Permisos 

* Escritura 
* Lectura 
* Eejcucion 

## Paginacion 

> Neceista tabla extra y son dos accesos a mma 

* Se dividira en frames 
* No neceista estar contiguo en memoria 
* Los procesos y la memoria se dividen en partes de l mismo tamanio, asi cualquier pagina de cualquier proceso peude estar en cualquier frame 
* Vamos a tener fragmentacion interna
* No vamos a tener fragmentacion externa 

## Direccion logica a fisica 

* La cantidad maxima de paginas por proceso es 2^x 
* Tamanio de paginas es 2^y 

