# Introduccion

* Un programa de bargarse en memoria desde disco y colocarse detnro de un proceso para que se ejecute 
* La memoria principal y los registros son los unicos dispositivos de almacenamiento a los que peude acceder la CPU directamente
* El acceso a registro es muy rapido
* El acceso a memoria principal puede durar varios ciclos 
* Las memorias cache se colocan entre la memoria principal y la CPU para acelerar el acceso a la informacion

## Vinculacion de direcciones 

> La cinculacion de instrucciones y datos a direcciones de memoria pueden realizarse en 3 estapas 

1. Compilacion 
  * Si se conoce a priori la posicion que a a ocupar un proceso en la memoria peude generar codigo absoluto con refernecias absolutas a memoria, si cambia la posicion del proceso hay que recompilar el codigo 
2. Carga
  * Si no se conoce la posicion del proceso en memoria en tiempo de compilacion se debe generar codigo reubicable 
3. Ejecucion 
  * Si el proceso puede cambiar de posicion durante su ejecucion la vinculacion se retrasa hasta el momento de ejecucion. 
  * No necesita soporte hw para el mapeo de direcciones 

## Espacio Direcciones 

Espacio de direcciones logicas vinculadas a un espacio de direcciones fisicas separadao es crucial para buena gestion de memoria 

* Direccion logica
  * Es la direccion que genera el proceso (Direccion virtual)
* Direccion Fisica 
  * Direccion que percibe la unidad de memoria 

## Base y limite 

> Par de registros base y limite definen el espacio de direcciones logicas 

## Memory management Unit (MMU)

Es un dispositivo hardware que transforma las idrecciones virtuales en fisicas
El valor del regirstro de reubicacion (Registro base) es aniadido a cada direccion generada por un proceso de usuario en el momento en que es enviada a la memoria
El programa de usuario trabaja con direcciones logicas, nunca ve las direcciones fisicas reales 

## Asignacion de espacio contiguo 

La memoria principal se encuentra dividida en dos partes 
  * SO residente (kernel) Normalmnet en posiciones bajas de la memoria junto al vector de interrupciones
  * Zona para los procesos de usuario normalmente en posiciones mas altas 

La Zona para procesos de usurario se encuentra dividida a su vez en varias particiones que se asignaran a los procesos 
* Particionamiento estatico 
  * las particiones se establecen en el momento de arranque del SO y permanecen fijas durante todo el tiempo
  * Particionamiento Dinamico 
    * Las particiones cambian de acuerdo a los requisitos de los procesos 

## Asignacion estatica 

* First fit 
* Best fit 

> Fragmentacion interna -> Dentro de la particion de mi proceso tengo una parte que nose puede usar 

Los registros de reubicaion se usan para proteger los procesos de usuario unos de otros y del codigo y datos del SO

El registro base contiene la direccion fisica mas baja a la que se puede acceder el proceso 
El registro limite contiene el tamaniao de la zona de memoria accesible por el proceso, las direcciones logicas deben ser menores que el registro limite

La comparticion de memoria entre proces no es sencilla
* Los procesos no pueden compartir memoria directamente debido a la proteccion 
* Una solucion consiste en implicar al SO en la comparticion de memoria 

## Asignacion Dinamica 

Ahora el tamanio y ubicacion de las particiones no es fijo sino que cambia a lo largo del tiempo, Cuando llega un proceso se le asigna memoria de un hueco lo suficientemente grande para que quepa 

* Con el espacio sobrande del hueco se crea una nueva particion libre (hueco)
* La Comparticion se puede conseguir mediante solapamiento de las particiones 

> Fragmentacion externa : Cuando en la memoria hay espacio para que entre el proceso pero la memoria no es contigua, por ende no podria acceder el proceso 


* Primer Ajuste (First Fit)
  * Se asigna el primer huyeco lo suficientemente grande 
* Mejor Ajuste (Best Fit) 
  * Se asigna al hueco mas pequenio que es lo suficientemente grande, hay que buscar en la lista entera de huecos, salvo si esta ordenada por tamanio
  * Da lugar al hueco mas pequenio
* Peor Ajuiste (Worst Fit)
  * Asigna el hueco mas grande hay que buscar en la lista completa de huyevos, salvo si esta ordenada por tamanio 
  * Da luegar al hueco mas grande 
