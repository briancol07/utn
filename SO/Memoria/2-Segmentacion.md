# Segmentacion 

* Esquema de gestion de memoria que apoya la vision que el usuario tiene de la memoria
* Un programa es una coleccion de segmentos 
* Un segmento es una unidad logica tal como:
  * Programa principal 
  * Procedimeinto 
  * Funcion 
  * Metodo
  * Objeto 
  * Variables (locales,globales) 
  * Bloque comun
  * Pila 
  * Tabla simbolos
  * Array 

Tablas chicas de segmentos

## Esquema de segmentacion 

* Una direccion logica consiste en:
  * Numero de segmento 
  * Desplazamiento 
* Tabla de segmentos 
  * Informacion sobre la ubicacion de los segmentos en memoria 
  * Base : Contiene la direccion fisica en la que comienza el segmento 
  * LImite : Especifica la longitud del segmento 
* Registro base de la tabla Segmentos (STBR) 
  * Apunta a la localizacion en memoria de la tabla de segmentos 
* Registro de la longitud de la tabla de Segmentos (STLR) 
  * Indica el numero de segmentos usados por un programa 
* El numero de segmento s es legal si s < STLR

* Proteccion 
  * Cada Entrada de la tabla de segmentos hay 
    * Privilegios de lectura/escritura/ejecucion
  * Los bit de proteccion estan asociados con los semgnetos, la comparticion de codigo occure a nivel de segmento 
  * ya que los segmentos varian en longitud la asignacion de memoria es un problema de asignacion dinamica 

## Cambio de direccion 

De logica a fisica es con la base y el limite, la base seria donde empezaria y el limite es hasta donde va el segmento 

