# Paginacion 

* El espacio de direcciones logicas de un proceso puede ser no contiguo en memoria asi se puede asignar memoria al proceso siempre que hay alguna disponible 
* se divide la memoria fisica en bloques de tamanio fijo llamados marcos (potencia de 2) 
* Se divide el espacio de direcciones logicas de los procesos en bloques llamados paginas 
* Se mantiene una lista con los marcos libres 
* Para Ejecutar un programa de tamanio n paginas hace falta encontrar n marcos libres y cargar el programa 
* Se usa una tabla de paginas para transformar las direcciones locas en direcciones fisicas 
* Aparece Fracgmentacion interna


Fragmentacion en la ultima pagina 

## Traduccion de Direcciones

* Direcciones generada por un proceso es dividida en 
  * Pagina(p): usada como indice en la tabla de paginas que contiene la direccion base de cada pagina en memoria fisica 
  * Desplazamiento(d): Se combina con la direccion base para definir la direccion de memoria fisica que se envia al a unidad de memoria 


```
Numero de pagina | Desplazamiento 
-----------------------------------
        p        |        d
-----------------------------------
     m -  n      |        n 
``` 

Doble de lento que segmentacion 
Puede ocupar megas y la voy a tener que tener en memoria completa 

## Tabla de Paginas

* La tabla de paginas se mantiene en memoria principal 
* El registro base de la tabal de paginas (PTBR) apunta al incion de la tabal de paginas 
* El registro longitud de la tabal de paginas (PRLR) indica el tamanio de la tabla de paginas 
* E neste esquema cada acceso a dato o instruccion requiere dos accesos a memoria. Uno para la tabla de paginas y otro para obetner el dato o instruccion 
* Se puede agilizar el proceso usando una pequenia memoria asociativa o TLB(Translation look-aside buffer) 
  * Pequenia memoria cache 
  * Guarda traduccion (ahorrar acceso) 

## Memoria Asociativa 

* Memoria asociativa-buesqueda en parealelo 
* Traduccion de direcciones (p,d)
  * Si p esta en un registro asociativo se obetinee el numer ode marco 
  * Si no, se obtiene el numero de marco de la tabla de paginas que esta en memoria principal 

``` 
# Pagina | #marco 
-----------------
         |
-----------------
         |
-----------------
``` 

Al principio vacia pero amedidad que voy accediendo voy generando accesos.

## Tiempo de Acceso Efectivo 

* Busqueda Asociativa = e(epsilon) Unidades de tiempo 
* Acceso a memoria = m 
* Tasa de aciert - Probabilidad de encontrar una pagina en los registros asociativos entre valor dpeende de las peticiones de paginas y del numero de registros asociativos 
* Tasa de acierto = a 
* Tiempo de acceso efectivo ( Effective Access Time EAT) 
  * EAT = (m+e) alpha + (2m+e) (1- alpha) = 2 m - alpha m + e 
  * si lo encuentro es ( m+e) . alpha
  * si no lo encuentro (2m + e) (1 - alpha) 

## Proteccion de la memoria 

* La proteccion de la memoria se implementa asociando un bit de proteccion con cada pagina 
* Hay un bit de validez en cada entrada de la tabla de paginas 
  * Valido indica que la pagina asociada esta en el espacio de direcciones logico del proceso y por tanto es legal el acceso 
  * Invalido : indica que la pagina no esta en el espacio de direcciones logico del proceso 


## Tablas de paginas Multinivel 

* Divide el espacio de direcciones logicas en multiples tablas de paginas 
* Un ejemplo simple es una tablade pagina de dos niveles

Cada nivel que agrego, agrega un nivel a memoria.

## Tabla de paginas Invertidas 

* Una entrada por cada marco de memoria 
* Las entradas continene la idreccion virutal de la pagina almacenada en el marco con informacion sobre el proceso que la posee
* Disminuye la memoria necesaria para almacenar cada tabal de paginas pero aumenta el tiempo requerido para buscar en la tabal cuando ocurre una referencia a memoria
* Solucion: Usar una tabla hash para limitar la busqueda a una entrada (o unas pocas como mucho)

los hash tienen colisiones una solucion puede ser tomar el contiguo/siguiente
