# Memoria Virtaul

La traduccion de direcciones debe realizarse en tiempo de ejecucion por que en tiempo de ejecusion se permite la reubicacion de los procesos 
El proceso debe estar dividido en partes ( Paginas o segmentos) 

**Def**: Dispositivo de almacenamietno secundario que puede ser direccionado como si fuese memoria real. 

> No es la memoria RAM sino Disco 

* Ventajas: 
  * Me permite correr procesos mas grandes que la memoria real porque nunca lo carga completamente en memoria real 
  * Permite aumentar el grado de multiprogramacion 
  * Existen menos restricciones para el programador para ver si hay memoria insuficiente 

> Las direcciones logicas hacen referencia a la memoria virtual y las direcciones fisicas a la memoria real .

```
Logica --> MMU --> Real 
```

## Estructura de tabla de paginas y traduccion de direcciones 

Si el bit de prescencia esta en 1 indica que la pagina esta en memoria.
Si esta en cero el valor que tiene en la tabla de pagina no nos interesa vamos a tener que tirar un page fault o fallo de pagina 

**Def**: page Fault: es una excepcion que arroja la MMU cundo un programa quiere acceder a una direccio nque no se encuentra en la memoria principal por que ha sido puesta en disco. 

* Pasos
  1. Se realiza una referencia a memoria 
  2. Se dispara pagefaul
    * SO bloquea proceso 
    * Mientras otro proceso puede usar CPU 
  3. Mientras el proeso esta bloqueado hay que traer la pagina que solicito memoria 
    * Buscar la pagina solicitada a disco
    * Se ubica a la pagina en cualquier marco libre ( Real ) 
  4. Se produce una interrupcion para que el SO tome control 
    * SO actualiza tabla de paginas 
    * Desbloquea el proceso ( READY )
  5. Se ejecuta nuevamente la instruccion que provoco el page faul 
 
Si no hay espacio lo que se hace es desplazar un proceso de la memoria principal a disco 

* Ventajas 
  * COrrer procesos mas grandes en memoria real
  * Permite aumentar ampliamente el grado de multiprogramacion del sistema 
  * Existen menos restircciones para el programador 
* Desventajas 
  * No es eficiente 
  * Requiere mas accesos a memoria 
  * Requiere mas accesos a disco 
  * No mejora el rendimiento para la ejecucion
    * Mecanismo de pagina demora 
    * Puede producirse page fault y bloquear el proceso 

## Estructura de tablas de paginas 

Paginacion jerarquica o por niveles : 

Se separa la tabla de paginas del mismo tamanio. sea gregar uan nueva tabla que contiene punteros a las tablas .

Punteroa la tabla | Numero de patgina | Desplazamiento 
Cantidad de niveles + 1 en la cantidad de accesos 


puede haber de 1 a 2 pagefault 

### Tabla de paginas invertidas 

Tener una unica tabla de paginas para todos los procesos 

* Tendra tantas entradas como frames en la memoria 
* Se dice invertida por que esta indexada por frame no por numero de pagina 
* Ventajas  
  * Tengo una tabla de tamanio fimo 
* Desventajas 
  * Es muy lento , genera mucho overhead
  * Hay que incorporar nuevas estructuras para que compartan recursos 
  * Para saber que no esta en memoria hay que iterar por toda la tabla 

## Hash 

Es una funcion que devuelve un numero a partir del numero de pagina y el PID. Ese numero va ser directamente el numero de frame. Es decir que el uso de la funcion de hash se va a utilizar en reemplazo de la busqueda secuencial dado que es mucho mas rapido.

* Desventaja 
  * Coliciones 

Si la pagina devuelve un null no es la pagina que estamos buscando entonces produce un pagefault porque no podemos localizar la pagina requerida 

## Tranlation lookaside buffer ( TLB ) 

Es una memoria cache de acceso por contenido administrada por la MMU cuyo objetivo es mejorar el rendimiento de la traduccion de direcciones. **Reducir cantidad de accesos a memoria** 

* Guarda etiquetas con un dato asociado, Numero de pagina y numeor de frame. 
* Cada vez que se realiza una traduccion se guarda el resultado en la TLB, para no tener que pedirselo en la TLB.
* No esta indexada
* Cuando no se encuentra valor 
  1. La pagina esta presente en memoria y solamente necesita crear la entrada de la TLB
  2. La pagina no esta presente en memoria y se necesita general un Pagefault.
* Ventajas 
  * Es muy rapida 
  * Reduce cantidad de accesos 
* Desventajas 
  * Suele tener muy pocas entradas 


