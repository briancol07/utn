# Arquitectura kernel 

## Monolitico 

Hay un unico modulo que hace todo. Todas las funcionalidades del SO estan en un archivo ( o verios sin orden) y cada "modulo" puede tener acceso a los demas.

* Deventajas 
  * Es un sistema muy dificil de mantener 
  * El trackeo de errores es muy complejo 
  * Al estar todo tan acomplado un pequenio cambio puede afectar todo el sistemas 
* Ventajas 
  * Gran fluidez de la informacion, mayor eficiencia 
  * No hay overhead 

## Multicapas 

* Separan funcionalidades por capas ( cada modulo ) y se comunican con los otros por interfaces 
* Ventajas : 
  * Es mas simple de mantener 
  * Cambios no afectan todo el sistema
* Desventajas 
  * Menor fluidez en la informacion. 

## MicroKernel 

Separa las funcionalidades que se ejecutan en modo kernele de las que se ejecutan en modo usuario. La idea es tener un modulo que corra en modo kernele bien chico ( con funcionalidades minimas) y el resto que corran modo usuario

* Ventajas 
  * Provee mas flexibilidad a la hora de poner o sacar modulos
* Desventajas 
  * Esta estrategia tiene mas overhead por que para comunicarse lo hacen atravez de kernel
