# Procesos 

Progrma: Secuencia de instrucicones compiladas a codigo maquina. 
Ejecucion concurrente Sos o mas programas ejecutando en el mimso intevalo de tiempo, no es en el mimso instante 
Sistem monoprogramado : El procesaor puede jecutar solo un program en periodod de determinado 

Multiprogramacion : Modo de operacion que permite que dos o mas programas ejecuten de forma concurrente . 
* Permite una reduccion de timpoes 
* permite ejecutar a multiples usuarios de forma concurrente 

Multiprocesamiento: Hace referencia a mas de un programa ejecutando al mismo instante. 

Def: Es un programa que esta siendo ejecutado en un isntante determinado y ademas se le asigno memoria/recursos.
* Aecuencia de insturcciones que debe ejecutar el procesador 
* COnjunto de datos 
* Estado 
* Atributos que lo identifiquen 
* Tiene los recuross que le fueron asignados 

## Estructura 

* Stack 
  * Memoria que se reserva al declarar variables 
  * Variables locales 
  * Llamadas a funciones
  * Parametros
  * Retorno de las funciones 
* Heap 
  * Memoria dinamiaca 
  * En tiempo de ejecucion 
* Datos 
* Codigo 
* PCB 
  * Process control block 
  * Se guarda el contexto de ejecucion 
  * Compuesto por 
    * PSW 
    * Identificador 
      * PID
      * PPID
      * UID
    * Registors del procesador 
    * Informacion de planificaicon de CPU 
    * Informacion I/O 
    * Contable 

## Ciclo de vida de un proceso 

Tiempo que rtanscurre entre la creacion y finalizacion de un proceso. 

## Estados de un proceso

Running , not running 
Ready , blocked 

## Llamadas al sistema bloqueantes o no bloqueantes 

Bloqueante: Se ejecutan en el momento que la llaman.
No bloqueante : Cada vez que ejecutara la sysccall. no necesariamente pasa por bloquedo,. 

## Diagrama de estados 

### New 

Cuando se quiere ejecutar un programa, pasa un tiempo entre que cpu lo recibe, hasta que lo empieza a ejecutar. 

* Se preparan las estructuras 
* Se inicializa el PCB
* Se espera admision 

New --> Ready 

### Exit Finalizado 

De cualquier estado puede pasar a exit, Aca se libera la memoria recurso, semaforos etc 

* Valor de retorno: 
* Todas menos PCB se eliminan 

## En disco , no ram 

### SUSP / Ready 

La diferencia con ready es que estos se encuentran en el disco y deberan ser pasados a memoria para ejecutar 

* Cuando hay demasiados procesos en ready, pasa a susp/ready 
* Cuando el nivel de multiprogramacion disminuye y se decide traer un proceso que estaba en disco a RAM  o alrevez 

### SUSP / Blocked 

Se toman los procesos que estan bloqueados, y pasan estructura menos PCB a disco 

## Creacion de Procesos 

* Lo crea el SO para dar algun servicio 
* Lo crea otro proceso 
  * Proceso que solicitar crearlo seria el padre 
  * Pueden ejecutarse de forma concurrente o esperar a que los hijos terminen 

### Pasos 

1. Asignar PID
2. Reservar espacio para estructuras ( Codigo, datos, pila y heap) 
3. Inicializar PCB
4. Ubicar PCB en listas de planificacion 

### Tabla de procesos 

Cuando un proceso es creado es agregado a la table ade procesos del sistema. Es manejada por el SO y el grado de multiprogramacion es definido a partir de la cantidad de procesos activos en la TDP. 

## Finalizacion de Procesos 

* Formas 
  * Kill ( SO | Proceso ) 
  * Se finaliza el mismo 
    * Normal exit 
    * Abnormal exit 

## Cambio de procesos 

Se produce cuando viene ejecutando un proceso en el procesor, por algun motivo deja de ejecutar y comienza a ejecutar otro. 
El cambio de contexto puede tener como objectivo: ejecutar otro proceso, atender una interrupcion o ejecutar una syscall
* Se debe guardar el contexto de ejecucion del proceso 1 para luego poder reanudarlos desde que fue interrrumpido . 
  * Overhead  ( Se debe minimizar ) 
