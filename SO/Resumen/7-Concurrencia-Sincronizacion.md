# Concurrencia y Sincronizacion 

* Formas de concurrencia
  * Multiprogramacion 
  * Multiprocesamiento 
  * Procesamiento Distribuido 
  * Comparticion de recursos y competicion por recursos 
* Por que se usa? 
  * Mejora performance 
  * Aplicaciones estructuradas 

## Condicion de carrera 

Donde varios componentes modifican datos compartidos y obtienen diferentes resultados finales dependiendo del orden en el que se ejecuten. Por eso hay que sincronizarlos pero solo a los procesos cuando quieren acceder a los mismos datos ( Modo escritura ) en modo lectura no hace falta.

## Formas de interaccion entre los procesos 

* Comunicacion entre procesos 
* Competencia de los procesos por los recursos 
* Cooperacion de los procesos via comparticion 
* Cooperacion de los proceso via comunicacion 

## Requisitos dentro de la seccion critica 

* Mutua exclusion : 
  * Solo un proceso peude acceder en la seccion critica usando un recurso 
* Progreso 
  * Cuando un proceso termina de usar el recurso de la seccion ciritca debe dar aviso a los demas 
* Espera limitada 
  * Se relaciona con progreso 
* Velocidad relativa 

## Consideraciones 

* La seccion critica debe se lo mas pequenia posible. Por que puede bloquear recursos 
* Un proceso que no esta en una seccion critica no interfiere de ninguna manera con otros procesos que la quiera ejecutar
* La permanencia en la seccion critica debe ser finita y reducida 
* Un proceso puede tener muchas secciones criticas 

## Soluciones 

* De SW 
* De HW 
* Provista por SO : Semaforos 
* Provista por lenguajes de programacion: ( Monitores ) 

### De SW 

* Tiene espera activa 
* No hay progreso 
* Tienen orden para acceder a la seccion critica 
* SI funcionan
  * Algoritmo dekker 
  * Algoritmo Peterson 

### Livelock 

* Similar deadlock , pero aca va variando entre los procesos sin lograr un avanze util. 
* Forma de inanicion 
* Ejemplo de los simpsons que quiere pasar por la puerta al mismo tiempo 

## Provista por SO 

* Mutua exclusion
* Sincronizar / ordenar ejecucion 
* Limitar o controla la cantidad de accesso a un recurso 
* `Wait()` y `Signal`

### Wait()

Decrementa el valor del semaforo en 1. Depues pregunta si el valor del semaforo es menoir que 0. Si lo es este proceso que esta haciendo el wait se va a bloquear 

### Signal() 

Incrementa el valor del semaforo en 1. Despues pregunta si el valor del semaforo es menor o igual a 0. 
Si lo es desbloquea a alugno de los procesos bloqueados. 

## Valores 

* Valor iniciacion > 0 
* Si es mayor a cero: representa la cantidad de instancias disponibles de un recurso
* Si es menor a 0: Representa la cantidad de procesos/hilos bloqueados en espera a ser llamados 
* Si es 0 : no hay disponibilidad ni espera 

## Tipos Semaforos 

* Contador / General Permite controlar el acceso a una cantidad de recursos 
* Binario
  * Garantiza orden de ejecucion
  * Mutex 

## Pueden ser interrumpidos 

Son atomicos ( Signal y wait), se ejecutan por completo en un cilco de instruccion o no se ejecutan. no pueden ser interrumpidas 

## Soluciones por Lenguajes de programacion 

Hacen uso de encapsulamiento, Va a ser una clase monitor que ejecute un proceso activo en el monitor 

* Mutua exclusion 
* Permite sincronizar procesos 

> Productor Consumidor 




