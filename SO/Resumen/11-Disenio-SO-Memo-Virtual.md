# Disenio del SO para memoria virtual 

Metodos, estrategias y algoritmos que utiliza el SO para tratar de mejorar el rendimiento del uso de la memoria virtual.

* Politicas 
  * Recuperacion
  * Ubicacion 
  * Reemplazo o sustitucion 
  * Conjunto residente 
  * Limpieza 

## Recuperacion 

> Lo que acabo de usar es probable que lo vuelva a usar

* Paginacion bajo demanda 
  * Las traigo a medida que las neceisto 
  * Si la necesito y no esta espero a que se cargue 
* Paginacion adelantada (prepaging) 
  * Si me pedis la 0 te traigo varias contiguas por que ya estoy ahi.

## Ubicacion 

Tiene que ver solamente cuando usamossegmentacion pura/simple con memoria virtual.

* Algoritmos 
  * Primer ajuste 
  * Siguiente ajuste 
  * Mejor ajuste 
  * Peor ajuste 

Eso es con segmentacion por que no tiene sentido analizar esto dado que la memoria esta dividida en frames que son todos iguales. 

## Reemplazo y sustitucion 

* Bloqueo de marcos 
  * Se refiere a agregar un bit de bloqueo a uno de los marcos de la memoria. Ese bit indica que esa pagina no la puedo reemplazar. No tendria sentido que todas las paginas tuvieran este bit en 1 ya que no estariamos haciendo uso de la memoria virtual
* Algoritmos de reemplazo o susttucion 
  * Permite elegeir cual es la siguient pagina a reemplazar, Todos menos el FIFO tratan de reducir el page fault. El criterio para medir / comparar estos algoritmos es la cantidad de fallos de pagina que generan 
  * Algoritmos 
    * Optimo: se elige la pagina a la que se realizara una referencia en el futuro lejano . Solo existe a modo de comparacion para medir a los demas algoritmos 
    * FIFO : se elige la pagina que mas tiempo esta en memoria. 
    * LRU ( least recently used ): Se elige la pagina que hace mas tiempo no es referenciada. Es el de mas overhead 
      * Marco | Bit de presencia | Bit de modificado | instante de referencia 
    * Algoritmo de reloj ( CLOCK ) : es una combinacion entre FIFO y LRU pero se agrega un bit de uso . Me indica si la pagina se uso hace mucho tiempo o no. Cada vez que se carga una pagina en la memoria real o cada vez que se hace referencia se pone el bit en 1. 
    * Clock Modifivado : Trata de ver que paigna debe ser la victima , bit de uso y bit de modificado 

## Conjunto residente 

Tengo mi proceso por completo en la memoria virtual y en la memoria real tengo el espacio de direcciones que lefue asignado al proceso. Mi conjunto residente es aqeul conjunto de paginas que se encuentran en la memoria real.

## Limpieza 

* Limpieza bajo demanda : No limpio, las paginas sevan cuando las reemplazo
* Limpieza adelantada : El SO decide sacar paginas que los procesos no estan usando para liberar la memoria RAM, para que quede libre para futuras peticiones 

## Trashing 

> Sobrepaginacion 

* Situacion donde se producen demasiado fallos de pagina 
* Se da cuando fracasa la memoria virtual, por que todo el tiempo se piden paginas que no estan 
* Causas 
  * Un algoritmo de reemplazo que hace mal su trabajo 
  * Cargo mcuos procesos 
    * Tienen asignado pocos marcos 
* Genera una baja tasa de uso de CPU. Los procesos se bloquean y no pueden ejecutar 

