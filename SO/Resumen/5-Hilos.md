# Hilos 

## Imagen de un proceso 

1. Codigo Programa 
2. Datos Estaticos
3. Heap 
4. Stack 
5. Registros de CPU 
6. Estados del proceso 
7. PCB

Cada hilo tendra su propio TCB y Pila 

**Hilo:** Es una linea de ejcucion de un proceso 

* Comparte 
  * Codigo 
  * Datos 
  * Recursos 
* No comparte 
  * Stack 
  * TCB 

## Multihilos 

* Contiene dos o mas partes que se pueden ejecutar de manera concurrente 

## Estructuras involucradas 

* PCB:
* TCB: Thread control block 
  * Prioridad 
  * ID
  * Estado
  * Contexto de ejecucion 
    * PC 
    * FLAGS 

## Ventajas 

* Capacidad de respuesta 
* Menos overhead 
  * No es necesario hacer cambio de modo 
* Comparticion de recursos entre hilos 
* Comunicacion eficiente 
* Multiprocesamiento
* Procesamiento asincronico

## Deventajas 

* Seguridad
  * A la hora de compartir recursos 

## KLT ( Hilos de kernel ) 

* Administrado por el SO ( Crea, destruye y administra ) 
* Planificacion: Van a estar sujeto a la misma planificacion del sistema operativo. 
* Ventajas 
  * Syscall bloqueantes solo bloquean al hilo en cuestion 
  * Multiprocesamiento del mismo proceso ( Pueden ejecutar en procesadores distintos ) 
  * Menor overhead que si se usaran distintos procesos 
* Desventajas 
  * Mayor overhead que ULT 
  * Son menos estables y seguros que los procesos 

## ULT ( Hilos de usuario ) 

> No son conocidos por el SO 

* Administracion por una biblioteca de usuario. 
* Planificacion: Doble una del SO y otras de la biblioteca
* Ventajas : 
  * Bajo overhead 
  * Planificacion Personalizada 
  * Portabilidad 
  * Creacion mas liviana como los cambio de hilos
* Desventajas:
  * No permite ejecutar en procesadores distintos 
  * Syscall Bloqueante: todo el proceso se bloquea ( Se puede resolver con jacketing ) 


## Jacketing 

Es cuando en lugar de hacer las syscall bloqueante se efecutar en modo no bloqueante. 
La biblioteca, si se le pide hacer una tarea bloqueante la pasa como no bloqueante y permite seguir realizando otras tareas. 



