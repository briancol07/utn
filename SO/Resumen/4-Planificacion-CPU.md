# Planificacion 

* Problemas Multiprogramacion: 
  * Hay muchos procesos queriendo ejecutar al mismo tiempo 
    * Disparidad de uso de CPU
    * Procesos que nose ejecutan nunca ( Starvation ) 
    * Degradacion del tiempo de respuesta del sistema 
    * Ram = FULL y CPU = Empty 

Nivel de multiprogramacion : Cantidad de procesos activos en memoria principal.
* Se toma:  
  * Ready
  * Running 
  * Blocked 

* Tipos de procesos : 
  * CPU Bound 
    * Requieren mas tiempo de cpu 
  * I/O Bound
    * Requieren mas tiempo de I/O

## Objetivos 

* Asignar procesos al procesador 
* Rendimiento / Productividad 
* Optimizar algun aspecto del comportamiento del sistemao

## Tipos de Planificadores 

* Largo 
* Mediano 
* Corto 

### Largo Plazo

* Determinar procesos a ser admitidos en el sistema 
  * Admitir un proceso aumente el grado de multiprogramacion
* Determina el grado de multiprogramacion 
* Exit 
  * Finalizar un proceso disminuye el grado de multiprogramacion
* New 
* Exit 

### Mediano Plazo 

Tambien controla el grado de multiprogramacion, pero ahora con:

* Ready/Suspended
* Blocked/Suspended 

Envian un proceso de memoria principal a disco ( Operaciones de swapping ) (todo menos pcb) 

### Corto Plazo 

Decide cual es el proximo proceso que se debe ejcutar 
* Ready
* Running 
* Blocked? 
* Debe minimizar overhead 
* Dispatch y timeout 

## Algoritmo de planificacion 

### FIFO ( First input first output) 

* Problemas 
  * Los procesos largos hacen que tarde mucho en ejecutarse los mas cortos 

### SJF ( Shortest job first ) sin desalojo 

* Problemas 
  * Inanicion / Starvation 
  * Es imposible cunato va usar, imposible implementar , Las rafagas se deben estimar 

### SJF con estimacin de rafagas 

est(n+1) = alpha * Te(n) + (1 - alpha) * Est

### SJF con desalojo ( Shortest remaining time) 

* Problemas 
  * Inanicion 

### Round-Robin 

Cola de ready se maneja con FIFO pero el SO define un quantum que dispara la interrupcion de clock para desalojar . 
quantum no se acumula 

* Quantum chicho = mucho overhead 
* Quantum grande = Termina siendo fifo 

### VRR ( virtual round robin) 

* Favorece I/O bound 
* Una cola con mayor prioridad 
  * En esta iran los que terminan E/S 
* Quantum se acumula

### HRRN 

Contempla Aging, por lo que resuelve el problema de inanicion SJF

$`R=(W+S) / S `$

Tiempo esperado W y S tiepmo que va usar CPU 

### Algoritmo de colas multinivel 

Se define una prioridad para cada proceso y cada prioridad tiene su propia cola ( Puede haber n colas) 

* EJ
  * Alta
  * Media 
  * Baja 

Cada cola puede tener su algoritmo 

## Simultaneidad de eventos 

* Tres procesos quieren entrar a ready 
  * Nuevo proceso
  * Termino de I/O
  * Running y acabo quantum 
* Cual seria el orden ? 
  * Interrupciones antes de syscall 
    * Entre las otras dos interrupciones queda primero clock 
* Quedaria 
  * Interrupcion de clock 
  * Interrupcion por finalizacion de evento 
  * Lammada al sistema por nuevo proceso 

``` mermaid 
flowchart LR
  new([NEW]) -- 3 --> Ready
  CPU([CPU]) -- 1 --> Ready
  Blocked([Blocked]) -- 2 --> Ready
```


