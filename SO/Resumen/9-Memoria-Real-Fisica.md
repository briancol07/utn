# Memoria Real - Fisica 

Para poder ejecutar un programa debe cargarse a la memoria ( Imagen, datos, stack, heap, etc) .

> Pensamos la memoria como un gran array . 

Cada proceso debe acceder al espacio de memoria que le corresponde 

* Funciones del SO :  
  * Reubicacion 
    * De procesos -> Suspencion y manda proceso a disco 
  * Proteccion 
    * Escribir y leer en los espacios de memoria que le correspondan 
    * Registro base y limite 
  * Comparticion  
    * Permite que los procesos compartan memoria entre ellos 
  * Organizcion fisica y logica 
  
## Asignacion de direcciones ( Address binding ) 

1. Programa.c 
  * Compilacion
2. Programa.o
  * Enlazado 
3. programa.exe
  * Address Binding 

## Memory Management unit ( MMU ) 

**Def**: Modulo que se encarga de hacer la traduccion de una direccion logica a una fisica. Mejora le eficiencia de la memoria y su ventaja principal es que logra que las traducciones e realizen de forma mas rapida 

* Direcciones
  * Logica 
    * Referncia a una ubicacion de memoria utilizada por los procesos .
    * Independiente de la ubicacion de memoria real 
    * Direccion Relativa: Apartir de un punto conocido 
  * Fisica 
    * Referencia a una verdadera ubicacio en memoria
    * Los procesos normales no la conocen
* Traducciones 
  * La traduccion de direccion logica a fisica tiene sentido solamente cuando se hace la asignacion en tiempo de ejecucion. 
  * Cuando la asignacion se hace en tiempo de compilacion se usa directamente la direccion fisica. 
  * Cuando es en tiempo de carga se usa la relativa 

## Carga, Enlace y bibliotecas compartidas 

Con las bibliotecas, se puede enlazar en distinto momentos del proceso de contruccion y carga  del programa 

* Enlace Estatico 
* Enlace dinamico 

## Asignacion de memoria para procesos 

**DEF**: El SO debe administrar a todos los procesos que estan en memoria ( el mismo tambien) Para que no accedan a lugares que no corresponde.

* Metodos 
  * Paticionamiento fijo
  * Particionamiento dinamico 
  * Buddy System 
  * Segmentacion
  * Paginacion 
  * Segmentacion paginada 

## Particionamiento FIJO 

> Se divide la memoria en particiones de tamanios fijos. Pueden ser todas las particiones del mismo tamanio o de distintos tamanios apra que ambos casos son tamanios fijos.

Un proceso no puede estar dividido en dos particiones diferentes.

* Grado de multiprogramacion limitado 
* Fragmentacion interna 
* Fragmentacion externa 

> Fragmentacion interna: Cuando no hace buen uso del espacio y queda sobrante. 

> Fragmentacion externa: Cuando tengo espacio para ubicar el proceso pero son particiones distintas 

## Particionamiento dinamico 

Las particiones no estan definidas y se van creando a demanda. Tampoco hay un numero fijo de particiones sino que hay tantas segun se neceisten en un momento determinado .

Deja espacio de huevos pero no contiguos , entonces puede tener fragmentacion externa pero existe algo llamado compactacion que compacta todos esos espacios vacios de memoria en uno ( Tiene un costo asociado: overhead) 

* Ventajas 
  * No tiene limitacion en multiprogramacion
  * No sufre fragmentacion interna
* Desventajas 
  * Sufre Fragmentacion externa 

## Algoritmos de ubicacion 

* Primer Ajuste
* Siguiente Ajuste
* Mejor Ajuste
* Peor Ajuste 

## Buddy System ( Descomposicion Binaria ) 

Es una combinacion de los dos anteriores y compensa las deventajas de cada uno. Se asigna a los procesos tamanios dememoria que son potencia de 2. 

* Desventaja  
  * Fragmentacion interna y externa 

## Segmentacion 

Dividir al proceso en varios segmentos. Se puede guardar un proceso en diferentes lugares de la memoria. Deben estar todos en ram 

* El proceso no necesita estar contiguo en memoria 
* El proceso se divide en segmentos de tamanio variable 
* Tipciacmente cada segmente representa una parte del proceso 
  * Codigo 
  * Pila 
  * Datos 
  * Biblioteca 
  * Heap 

Vamos a tener una tabla de segmentos 
* Ventajas 
  * No hay fragmentacion interna por que cada segmento va a tener asignado el espacio que realmente necesita 
* Desventaja 
  * Hay fragmentacion externa ( menos pero lo hay ) 

## Direccion logica a fisica 

Logicas = ( Segmento , Desplazamiento ) 

Cantidad de segmentos por proceso ( 2 ^ X -> X siendo bits ) 
Tamanio maximo de segmento = ( 2 ^ y -> y siento bits de esplazamiento 

* Medidas de proteccion 
  * Hay una validacion : Direccion fisca no puede ser menor al limite que esta en la tabla de segmentos ( Segmentation fault) 
  * A cada segmento se asignan permisos 

## Paginacion 

El proceso se divide en pequenias porciones ( paginas ). Se divide en tamanios iguales sin criterio 
La ram se encuentra de igual tamanio que las paginas 

* El proceso no necesita estar contiguo en memoria 
* Los procesos se dividen en paginas y la memoria en frames 
* Esos frames tiene el mismo tamanio que las paginas 
  * Esto permite reubicacion en cualquier lado de la memoria ram 
* Una tabla de paginas por proceso 

* Ventajas  
  * No hay fragmentacion externa por que puede ir en cualquier lado 
* Desventajas 
  * Tiene un poco de fragmentacion interna 
  * Requiere 2 accesos a memoria 

Direccion logica y fisica : igual que en el caso anterior 
9 * el tamanio de la pagina + desplazamiento 

* Medidas de proteccion 
  * Programa dividido en paginas aleatorias , dificil asignar permisos pero igual pueden definirse
  * Cada pagina tiene un bit que indica si es valida ono 
  * Comparticion: Manera facil de compartir recursos 

## Segmentacion paginada 

Combinacion de dos esquemas, venajass de ambos 

El proceso se divide en segmentos y despues cada segmento se divide en paginas, terminando teniendo una tabla de paginas para cada segmento del proceso. 

* Ventajas 
  * No hay fragmentacion externa
  * Se peuden asignar buenos permizos para garantizar la proteccion 
* Desventajas 
  * Fragmentacion interna 
