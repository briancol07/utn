# Quizizz

## Repaso 2 ( SO )  

1. Las Principales ventajas de las syscalls respecto a los wrappers son: 
  * Flexibilidad 
  * Eficiencia / Performance 
2. Las formas de pasar de modo kernel a modo usuario son: 
  * Ejecutar una instruccion privilegiada 
  * Restaurando el contexto de ejecucion 
3. Cual/es de los siguiente segmentos de la imagen del proceso esta/n siendo modificado/s durante la ejecucion del programa
  * Data 
  * Heap 
  * Stack 
4. El PCB, a diferencia del resto de la imagen del proceso, siempre tiene que estar en memoria principal 
  * Verdadero 
5. Cuales son algunos de los atributos del PCB de un proceso? 
  * ID
  * Estado del proceso 
  * PC/IP 
6. Seleccione los estados que corresponden al planificador de mediano plazo 
  * Ready Suspended 
  * Blocked Suspended 
7. Un proceso de impresion se bloquea luego de ejecutar esperando un dispositivo externo y es movido a swap. Despues se desbloquea en swap y vuelve a la memoria para ejecutar y finalizar. Que cantidad de planificadores y estados entran en juego ? 
  * 3 planificadores y 7 estados 
8. A la cantidad de procesos cargados en disco se los denomina grado de multiprogramacion 
  * Falso
9. Cuales de las siguientes afirmaciones es correcta? 
  * Si ocurre un cambio de proceso va a ocurrir mas de un cambio de contexto 
  * Si ocurre un cambio de proceso va a ocurrir mas de un cambio de modo 
  * Si ocurre un cambio de modo va a ocurrir un cambio de contexto 
10. Cual/es de las siguientes afirmaciones sobre hilos son correcta/s?  
  * Es la minima unidad de utilizacion de la cpu 
  * Permiten paralelismo entre hilos pares 
  * Son mas eficientes y livianos que los procesos 
11. Un KLT puede correr en modo usuario o modo kernel segun lo necesita. ya que el SO lo reconoce 
  * Falso 
12. En sistemas multiprocesador es mas performante utilizar ULTs en lugar de KLTs debido a que realizan el cambio de contexto aun mas rapido 
  * Falso 

## Sincronizacion 

1. Cual/es de las siguientes afirmaciones sobre condicion de carrera son correctas? 
  * El resultado final depende del orden de ejecucion de las instruciones en los multiples hilos/procesos
2. Cual/es de las siguientes afirmaciones sobre region critica son correctas? 
  * Debe contener alguna operacion sobre un recurso compartido 
  * Debe ser lo mas chica posible 
  * Debe ejecutarse de forma atomica 
  * Debe estar rodeada de un protocolo de entrada y salida 
3. Que requerimientos debe cumplir una solucion al problema de la seccion critica? 
  * Solo se permite un proceso al tiempo dentro de su seccion critica, de entre todos los procesos que tienen secciones criticas para el mismo recurso u objeto compartido 
  * No debe ser posible que un proceso que solicite acceso a su seccion critica sea postergrado indefinidamente ( ni interbloqueo ni inanicion)
  * La solucion debe funcionar sin importar como los procesos usen sus SCs . 
4. Cual/es de las siguientes son opciones posibles para garantizar mutua exclusion?
  * Solucion de peterson 
  * Semaforos 
  * Testand Set 
  * Deshabilitar interrupciones 
5. Cual/es de las siguientes afirmaciones sobre las soluciones a nivel de software son correctas? 
  * No requiere cambio de modo 
  * Solo es implementable con espera activa 
6. Cual/es de las siguientes afirmaciones sobre soluciones a nivel hardware son correctas? 
  * En general nos permiten soluicionar que las instrucciones de la SC sean interrumpidas con las de ortos procesos 
  * Deshabilidtar las interrupciones implica cambios de modo 
7. En que casos me conviene utilizar semaofros con espera activa en lugar de semaforos con bloqueo? 
  * Cuando hay poca probabilidad de ocurrencia de condiciones de carrera 
  * Cuando la seccion critica es chica 
  * Cuando quier priorizar la eficiencia sobre el uso de CPU
8. Cuales de los siguientes valores de inicializacion de semaforos son correctos 
  * 1
  * 10 
  * 0
9. Ingresar el valor de los dem

