# Repaso Arquitectura

## Procesador 

* Formado por:
  * Registros 
  * ALU 
  * Unidad de control

## Registros 

Es una porcion de mmoria dodne el SO almacena informacion. 

* Registros Uso general:  
  * Pueden ser modificados directa o indirectamente 
* Registros de control y estado
  * Modificados por un programador 
  * Los utiliza HW y SW ( los modifican de acuerdo a la ultima instruccion que ejecuto) 
  * Tipos:
    * PC
    * IR
    * MAR
    * MBR
    * PSW 

## Memoria RAM 

Conjunto de direcciones lineales ( Gran array de informacion) 

## BUS 

Dispositivo capaz de transferir datos entre los distintos componentes de una computdora. 

* Bus de datos 
* Bus direcciones 
* Bus de control 

## Instrucciones 

Cada procesador tiene un set de instrucciones que son simplemente las funciones que realiza el procesador 

* Tipos 
  * Instrucciones no privilegiadas 
    * Cualquier programa puede ejecutarlas 
  * Intrsucciones privilegiadas 
    * Solo SO puede ejecutarlas 

## Ciclo de instruccione 

> Solo los programas que estan en la memoria RAM pueden ejecutarse. 

* Fetch 
* Decode 
* Execute 

## Interrupciones 

 Es una mecanismo mediante el cual se da aviso a la CPU de que ha ocurrido un evneto. Se realiza un cambio a modo kernel  ( Este es el encargador de manejar interrupciones). 

 * Clasificacions :
  * Hardware 
    * Provienen de cualquier lado menos CPU 
  * Software 
    * Son generadas por CPU , internas 
  * Enmascarables 
    * Pueden ser ignoradas temporalmente. Estas interrupciones no son atendidas inmediatamente ( No es algo critico ) 
  No- enmascarable 
    * Tienen que ser atendidas inmediatamente. representan algo critico dentro del sistema 
  * Sincronicas 
    * Son las generadas por la CPU al ejecutar instrucciones. Son internals 
  * Asicncornicas 
    * Son generadas por los dispoitivos externos al procesador.
  * E/S 
    * Son interrupciones que indican que finalizo un evento de I/O
  * Fallas de HW
  * De clock 
  * Excepciones 
    * Abort 
    * Fallos 
    * Traps 

## Pasos para interrupciones 

1. Se genera una interrupcion en cualquier momento 
2. Finaliza la instruccion actual normalmente 
3. Se determina que efectivamente ha ocurrido una interrupcion en la ejecucion y se identifica de que dispositivo provino
4. Se guarda el PC y PSW del programa que staba ejecutando 
5. Se carga en el PC la idreccione del manejador de interrupciones ycomienza a ejecutar el so 
6. Guarda todo el resot de la informacion que estaba en el procesador en los registros 
7. Se inhabilitan las interrupciones. 
8. Se procesa la interrupcion 
9. Una vez que s hayan ejecutado toas las instrucciones del manejador de interrupciones. se restaura 
10. Se restaura PC y PSW 
11. Se habilitan nuevamente las interrupciones 

## Multiples interrupciones 

* Se atienden en orden secuencial 
* Se las ordena por prioridades 
* Deshabilitar las interrupciones mientras una interrupcion esta siendo procesada 

## Pregunta parcial 

1. En que consiste el ciclo de ejecucion de una instruccion? Podria una interrupcion surgir como consecuencia de dicho cilo? 

Si podria surgir una interrupcion durante la ejecucion de una instruccion, por ejemplo, una division por cero. 
