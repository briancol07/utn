# Deadlock 

Es un bloqueo permanente de un conjunto de procesos donde cada uno de esots procesos esta esperando un evento que solo puede generar un proceso del conjunto.

* Consecuencia de : 
  * Mala sincronizacion 
  * Mal uso de recursos compartidos 
* Recursos limitados 
  * Gestionados por SO
  * No gestionados por el SO 
* Tipo de recursos
  * Reutilizables ( aca deadlock ) 
  * Consumibles 

## Grafos de asignacion de recursos 

* Cuadrados Recursos 
  * Puntitos cantidad de intancias de un recurs o en particular 
* Circulos procesos 
* Aristas: Cantidad de recursos solicitados o asignados


## Condiciones para exista deadlock

* Condicion necearias 
  * Mutua Exclusion 
  * Retencion y espera 
  * No haya desalojo de recursos 
* Condiciones necesarias y suficientes 
  * Las tres anteriores
  * Espera circular 

## Como lidiar con deadlock

* Prevenirlo 
  * No lo garantiza 
  * Se encarga de impedir que se produzca alguna de las 4 condiciones necesarias y suficientes para que exista deadlock
* Evasion o prediccion de deadlock 
  * Denegar el inicio de un proceso 
  * Denegar la asignacion de un recurso
    * Algoritmo del banquero
    * Algoritmo para ver si en estado seguro 
    * Algoritmo para simular asignacion de recursos 
* Deteccion y Recuperacion 
  * Ocurre pero nos recuperamos 
  * No hay restricciones para asignar recurso disponibles ( Piden y se lo damos ) 
  * Opciones de recuperacion 
    * Matar procesos involucrados 
    * Volver a un estado anterior 
    * Identificar procesos y matar de auno 
    * Desalojar recursos 
    * Criterios 
      * Menor tiempo de procesador consumido 
      * Menos cantidad de salida producida 
      * Mayor tiempo restante estimado 
      * Menos numero total de recursos asignados 
      * Menor prioridad 
* No Tratarlo 

  
  *
