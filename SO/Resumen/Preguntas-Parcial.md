# Preguntas Parcial 

## Procesos 

1. Quienes pueden crear o finalizar procesos en un sistema? Como lo hacen? que le sucede al proceso hijo si su padre finaliza inesperadamente ? 

Los Procesos puede ser creador por otros procesos o por el SO. 
En cualquier de los casos crear un procesos es necesario hacer una llamada al sistema y el SO se encargara 

* Asignarle un PID
* Reservar espacio para estructuras 
* Inicializar PCB
* Mover el pcb a las listas de planificacion 

En caso que el proceso padre finalice el hijo podra seguir ejecutando normalmente. 

2. Que comparten un proceso padre y un proceso hijo ? y en el caso de dos hilos del mismo proceso ? 

Un proceso padre y su hijo no comparten cosas. La unica relacion entre ellos es que el hijo conoce el ID de su padre (PPID).
Dos hilos del mismo proceso comparten PCB, codigo, datos y heap.

3. Relaciones entre 
  * Running -> Ready 
    * Interrupcion clock 
  * Suspended/Ready -> Ready
    * Cuando nivel de multiprogramacion disminuye
  * Ready -> exit 
    * Cuando se decide finalizar un proceso y el mismo no estaba ejecutando
  * Ready -> blocked 
    * Transicion imposible 

## Hilos 

1. Utilizando semaforos en hilos ult, no requieren realizar un cambio de modo para ejecutar las operaciones de wait/signal. Indicar verdadero o falso 

Falso , wait y signal son syscalls por lo que implican un cambio a modo kernel 

2. Para compartir memoria entre procesos o entre KLTs se necesita interverncion del SO. Entre ULTs no es necesario, debido a que se gestionan en espacio de usuario. V o F 

Falso ambos comparten memoria 

3. V o F. La utilizacion de KLTs en lugar de procesos a pesar de ser mas rapido su Switch puede generar problemas de memory leaks 

Si, por que finalizaria los procesos que comparten recursos y si no se liberan correctamente hay memory leaks 

4. Ventajas y desventajas de ult en lugar de KTLS

* Ventajas 
  * Velocidad de operaciones 
  * Portabilidad 
  * Tener su propio planificador
  * Bajo overhead
* Deventajas 
  * No se permite ejecutar en procesadores distintos 
  * Si no hay jacketing una syscall bloqueante bloquea todo el proceso 
 
5. V o F Los KLT no pueden ocasionar memory leaks dado que el TCB no tiene referencia al heap del proceso 

Falso. Aunque no tengan referencia directa, los KLT comparten heap. 

6. V o F: Los KLT de un mismo proceso pueden competir por el uso del procesador. En cambio no es posible que los ULT de un mimso proceso compitan por el procesador 

Falso, si bien lo KLTs pueden competir por ser elegidos por el planificador de corto plazo del SO, los ULTs compiten por ser elegido por el planificador de hilos de la biblioteca 

7. Explique las operaciones asociadas con el cambio de ejecucion de un KLT a otro del mismo proceso 

* Se guarda el contexto ( PC, Flags ) en el stack del sistema
* El SO comineza su ejecucion y guarda el resto de los registros 
* El SO determina que debe realizar un Thread switch 
* Mueve el contexto de ejecucion completo del stack del sistema al TCB del hilo 
* Elige el proximo hilo a ejecutar
* Copia todo el contexto del TCB del nuevo hilo al procesador y realiza un cambio de modo ( usuario) 

8. En que situaciones convendria usar hilos de usuario en lugar de hilos de kernel. 

* Conviene cuando :
  * Cuando el proceso quiere planificar a uss hilos con su propio planificador 
  * Cuando se busca que el overhead sea bajo 
  * Para que el proceso sea portable 
  * Para que las operaciones con hilos sean mas rapidas 

* 2 Atributos TCB
  * TID
  * Estado de hilo 

## Deadlock 

1. En que se diferencian las tecnicas de prevencion con deteccion de dealock ? en que caso utilizarian cada estrategia? 

En las tecnicas de prevecion puede ocurrir deadlock y las de deteccion deja que ocurra para luego tratarlo. 

2. V o F A) En una situacion de deadlock una buena solucion suele ser matar a todos los procesos involucrados 

Falso, Es una posible solucion pero no la mas optima. Una mejor seria eliminar una a una 

B) Utilizando el algoritmo de evasion podria llegar a ocurrir un deadlock si es que todos los procesos piden lo maximo podria pedir? 

Falso este algoritmo asegura que no existira deadlock 

3. Describa las condiciones necesarias y suficientes para la existencia de deadlock 

* Mutua Exclusion 
* Que no haya desalojo de recursos 
* Retencion y espera 

