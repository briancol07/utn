# Resumenes Imagenes 

* Norma 4508: Se establecen las normas del rotulo y sus caracteristicas 
* Norma 4503: Se establecen las caracteristicas de las letras tamanios trazo 
* Norma 4507: Se establecen las definiciones de secciones y cortes e indicacion de cortes 
  * Seccion : figura que resulta de la interseccion de un plano con el cuerpo 
  * Corte : la porcion de un cuerpo o pieza resultante de un seccionamiento, desde la seccion en la direccion indicada por las flechas 
    * Longitudinal : Se obtiene en cuerpos segun la mayor medida del cuerpo o pieza es de revolucion si el plano de corte pasa por el eje de longitud 
    * Transversal Se obtiene de sus medidas menores, si el cuerpo es de revolucion el plano de corte perpendicular al eje longitudinal 
  * Los planos de corte tendran lineas ... y corta , extremos gruesos, reto grosor medio 
  * La linea de indicacion de corte puede ser recta, quebrada o curva 
  * Debe llevar letras mayusculas y flechas 

* Norma 4513:  Se establece las formas de acotar 
  * Cota : expresion numerica del valor de una medida 
  * Linea de cota : linea que indica la cota 
  * Acotacion en cadena : lineas de cotas consecutivas 
  * Acotacion en paralelo: las lineas de cotas se disponen en paralelo partiendo de una misma linea auxiliar
  * Acotacion Combinada : Acota en cadena y paralelo
  * Acotacion Progresiva : serie de longitudes cuya medicion se hace desde un origen, indicando sobre una linea de cotas, en forma sucesiva las sumas acumuladas de las medidas 
  * Acotacion por coordenadas : determina la pose de puntas mediantes absisas y cordenadas 

## Sistema de representacion cilindricos 

* Ortogonales --> axonometria 
  * Isometria - dimetria - trimetria 
* Oblicuos 
  * caballera 
  * militar 

Axometria isometrica 
  * Las direcciones son paralelas a los ejes dominantes del espacio X,Y,Z 
  * Sobre las direcciones se dibuja la verdadera magnitud del objeto 
  * Todas las caras tienen la misma jerarquia
  * Escuadra de 30 grados para trazar lineas ( diagolanes ) , x y z forma 120 por lado 

## Proyeccion oblicua con el plano vertical en Verdadera magnitud caballera 

Las direcciones son paralelas a los ejes dominantes del espacio xyz 
* Sobre las paralelas a la direccion Y,  se dibuja la mitad de la Verdadera magnitud del objeto 
* Sobre las paralelas a las direcciones ZX se dibuja la verdadera maginitud 
* Una de las caras tiene mas jerarquia 
* Escuadra de 45 grados para trazos sobre eje XY


## Proyecccion oblicua con el plano horizontal en Verdadera magnitud militar 45 45

* Las direcciones son paralelas a los ejes dominantes del espacio x,y,z 
* Sobre las paralelas a la direcicon z se dibuja 3/4 de la verdadera magnitud del objeto 
* La cara horizontal tiene mas jerarquia y las otras dos mismas 

## Proyeccion oblicua con el plano horizontal en Verdadera magnitud militar 30 60 

* Las direcciones son paralelas a los ejes dominantes del espacio XYZ
* sobre las paralelas a la direcicon za se dibuja la 3/4 de la Verdadera magnitud del objeto 
* La cara horizontal itene mas jerarquia y las otras dos distintas 

## Isometrica 

* las tres superficies visibles son de igual importancia 
* No se puede usar plantas y alzadas ortogonales 

## Perspectiva militar 

* Punto de vista 
