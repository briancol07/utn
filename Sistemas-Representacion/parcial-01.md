# Primer Parcial 

## Temas 

* Tipo de lineas utilizadas en el dibujo tecnico y letras 
* Rotulos 
* Escalas 
* Perspectivas ( que es para que sirve y cual es la mas utilizada ) 

## Tipos de lineas  4502

![lineas](./img/Lineas.png) 

### USOS 

* Linea A: Se utiliza para la representacion de contornos y aristas visibles 
* Linea B: 
  * Lineas de cota 
  * Lineas auxiliares 
  * Rayadas en secciones y cortes 
  * Diametro interior de rosca 
  * Borde y emplames 
* Linea E: Representacion de contornos y aristas no visibles
* Linea F:
  * Representacion de ejes 
  * Lineas de centros y circunferencias primitivas de engranajes 
  * Posiciones extremas de piezas moviles 
* Linea G: Se utiliza para la indicacion de secciones de cortes 
* Linea H: Se utilizara para indicar incrementos o demasias en piezas a ser mecanizadas 
* Linea C: Se utilizara como linea de interrupcion ( area a cortar grande ) 
* Linea D: Se utilizara para interrumpoir el dibujo de vistas y cortes parciales 

## Formatos 

![Formatos](./img/formatos.png)

## Rotulos 4508

* 4502 Lineas 
* 4503 Letras y numeros 
* 4504 Formatos laminas 

> Rotulo : Recuadro en el cua lse indical la denominaion y clave o numero de lo representado, las siblas o nombres de la firma propietaria del plano , fecha y demas caracteristicas referentes a la confeccion. Ubicado inferior derecha 

* Lista materiales : hoja aparte 
* List modificaciones 

![Rotulo](./img/rotulo.png)

1. Para Anotaciones complementarias : tolerancias ( generales , posicion) y formas , normas iram 
2. Escala del dibujo 
3. Mestodo ISO
4. Tolerancias y rugosidades de superficies en general 
5. Fechas y nombres correspondientes a la ejecucion, revision y aprobacion 
6. Nombres del cliente el cual se confecciona el plano 
7. Denominacion de lo representado 
8. Siglas o nombre de la empresa propietaria o confeccionado del plano 
9. Clave o numero de lo representado 
10. Espacio cuando fuese necesario para consignar fehca de emision o numero del plano 
11. Clave o numero del plano que reemplaz del plano reemplazante 
12. Para la lista de modificaciones 

> Cuando en un mismo plano se utilizan escalas distintas se indican todas ellas en el rotulo destacando la escal prncipal con numeros de mayor tamanio. 

## Escalas 4505

Tipo | Descripcion 
-----|------------
Escala | Relacion arimtetica entre las dimensiones del dibujo , que se indican en el numerador y las respectivas dimensiones del cuerpo que se indican en el denominador 
Lineal | La que relaciona dimensiones lineales del dibujo y del cuerpo
natural | Lineal las dimensiones del dibujo son iguales a las respectivas dimensiones del cuerpo
Reduccion | Lineal, En las cuales las dimensiones del dibujo son menores que las respectivas dimensiones del cuerpo
Ampliacion | Lineal, En la cual las dimensiones del dibujo son mayores que las respectivas dimensiones del cuerpo.

> En las lineales las unidades del numerador y denominador son las mismas 

![Escalas](./img/escalas.png)

## Letras y numero 4503

inclinacion 75 o 90 

![letras](./img/letras.png)

## Vistas 4501

* Lineas 4502
* Lineas 4507
* Lineas 4509

### Definiciones

* Triedro fundamental: EL fomrado por tres planos ortogonales situados detras, debajo y a la derecha del cuerpo 
* Vista : Proyeccion ortogonal sobre un plano, de un cuerpo o pieza situado entre el plano y el observador 
* Vista fundamental: Proyeccion del cuerpo o pieza sobre uno de los planos del triedro fundamental (A,B,C)
* Vistas principales: Proyeccion del cuerpo o piezas sobre planos paralelos ( D,E,F)
* Vista Auxiliares : Proyectar el cuerpo sobre planos no paralelos a los del triedro fundamental

![vistas](./img/vistas.png)

Vista | Caracteristica
------|----------------
Anterior | La que se obtiene de observar el cuerpo o pieza de frente, considerando esta posicion como inicial del observador 
Superior | La que se obtiene al observar el cuerpo desde arriba 
Lateral izquierda | Desde la posicion inicial visto desde la izquierda 
Lateral Derecha | Desde la posicion inicial visto desde la derecha 
Inferior | Visto desde abajo 
Posterior | Visto desde detras 

![vistas1](./img/vistas1.png)

## Perspectiva 4540

> Son utilizadas para representar objetos tridimensionales en una superficie bidimensional, mostrando profundidad y la posicion espacial de los objetos 

* Proyeccion oblicua caballera : Oblicua y paralela a una direccion dada sobre un plano de proyeccion paralelo a una de las caras.
* Proyeccion exonometrica 
  * Proyeccion ortogonal del cuerpo o pieza sobre un plano 
  * Tipos 
    * Isometrica 120 grados 
    * Trimetrica 
    * Dimetrica 
* Caballera comun 
  * Lineas de fuga a 45 grados 

#### Aplicaciones de la isometrica 

La mas utilizada, Permite representar objetos en sus verdaderas proporciones sin distoricion, lo que facilita la medicion directa de las dimensiones 

### Perpectiva trimetrica 

Es adecuada para obetner mayor superficie de cada vista, destacando la de mayor importancia 

### Dimetrica 

* usual : Es adecuada para representar los cuerpos o piezas que tienen una cara preponderante 
* Vertical : Para representar los cuerpos o piezas que son de configuraicon alargada
